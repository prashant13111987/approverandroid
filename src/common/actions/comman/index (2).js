
import React from 'react';
import config from '../../config'
import axios from 'axios';

var axioInstance = axios.create();

// Override timeout default for the library
// Now all requests will wait 2.5 seconds before timing out
axioInstance.defaults.timeout = 50000;
axioInstance.defaults.headers =  {
    Accept: 'application/json',
    'Content-Type': 'application/x-www-form-urlencoded',
};

export const get = (nodeURL, dispatch) => new Promise(((resolve, reject) => {
    console.log("User Token OBject :::::::::::::;;;", config.USER_TOKEN_DETAILS.auth_token)
    console.log('Get req',config.BASE_URL + nodeURL)
    axioInstance.get(config.BASE_URL + nodeURL ,{ headers: { 'X-USER-TOKEN': config.USER_TOKEN_DETAILS.auth_token } })
        .then((result) => {
            resolve(result.data);
        })
        .catch((error) => {
            console.log("error:: ", error)
            reject( error.response.data.error);
        });
}));

export const deletewithoutFormbody = (nodeURL, dispatch) => new Promise(((resolve, reject) => {
    axioInstance.delete(config.BASE_URL + nodeURL ,{ headers: { 'X-USER-TOKEN': config.USER_TOKEN_DETAILS.auth_token } })
        .then((result) => {
            console.log('result in delete',result);
            resolve(result.data);
        })
        .catch((error) => {
            console.log("error:: ", error)
            reject(error);
        });
}));
export const delet = (nodeURL, formBody, dispatch) => new Promise(((resolve, reject) => {
    console.log('delete req',config.BASE_URL + nodeURL+ config.USER_TOKEN_DETAILS,formBody)
    axioInstance.delete(config.BASE_URL + nodeURL,formBody)
        .then((result) => {
            console.log('result in delete',result);
            resolve(result.data);
        })
        .catch((error) => {
            console.log("error:: ", error)
            reject(error);
        });
}));


export const postWithoutToken = (nodeURL, formBody, dispatch) => new Promise(((resolve, reject) => {
    console.log("Inside Fetch>>>>>>>>>>>",config.BASE_URL)
    axioInstance.post(config.BASE_URL + nodeURL,formBody )
        .then((result) => {
            resolve(result.data);
        })
        .catch((error) => {
            reject(error);
        });
}));


export const put = (nodeURL, formBody, dispatch) => new Promise(((resolve, reject) => {
    axioInstance.put(config.BASE_URL + nodeURL,{ headers: { 'X-USER-TOKEN': config.USER_TOKEN_DETAILS.auth_token } }, formBody)
        .then((result) => {
            console.log('result >>>',result.data)
            resolve(result.data);
        })
        .catch((error) => {
            reject(error);
        });
}));


export const post = (nodeURL, formBody, dispatch) => new Promise(((resolve, reject) => {

    axioInstance.defaults.headers = {"X-USER-TOKEN":config.USER_TOKEN_DETAILS.auth_token};
    // var axioInstance1 = axios.create();
    // axioInstance1.defaults.headers =  {
    //     Accept: 'application/json',
    //     'Content-Type': 'application/x-www-form-urlencoded',
    //     'X-USER-TOKEN': config.USER_TOKEN_DETAILS.auth_token
    // };
    axioInstance.post(config.BASE_URL + nodeURL,formBody)
        .then((result) => {
            resolve(result);
        })
        .catch((error) => {
            reject(error);
        });
}));


export const patch = (nodeURL, formBody, dispatch) => new Promise(((resolve, reject) => {

    axioInstance.defaults.headers = {"X-USER-TOKEN":config.USER_TOKEN_DETAILS.auth_token};
    // var axioInstance1 = axios.create();
    // axioInstance1.defaults.headers =  {
    //     Accept: 'application/json',
    //     'Content-Type': 'application/x-www-form-urlencoded',
    //     'X-USER-TOKEN': config.USER_TOKEN_DETAILS.auth_token
    // };
    axioInstance.patch(config.BASE_URL + nodeURL,formBody)
        .then((result) => {
            resolve(result);
        })
        .catch((error) => {
            reject(error);
        });
}));
