
import { checkStatus, parseJSON, parseError } from '../../util';
import config from '../../config';
import { showAlertWithTitle } from '../../routing';
import Constants from '../../constants';
import { saveTokenObject } from '../../db';


export function handleError(dispatch, error, callback) {
    // error.json().then((errorResult)=> {
    if (error.statusCode === 401) {
        renewAccessToken(dispatch).then(() => {
            callback();
        }).catch((tokenError) => {
            callback(tokenError);
        });
    } else if (error.message === 'Network request failed') {
        dispatch({
            type: Constants.REQUEST_COMPLETED,
        });
    } else if (error.statusCode === 404) {
        showAlertWithTitle('Error', 'Server is not available. Please try later');
        dispatch({
            type: Constants.REQUEST_COMPLETED,
        });
    } else if (error.statusCode === 500) {
        showAlertWithTitle('Error', 'The server was unable to complete your request. Please try later');
        dispatch({
            type: Constants.REQUEST_COMPLETED,
        });
    } else {
        callback(error);
    }
}


export const get = (nodeURL, dispatch) => new Promise(((resolve, reject) => {
    fetch(config.BASE_URL + nodeURL, {
        method: 'get',
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded',
            Accept: 'application/json',
            Authorization: `Bearer ${config.USER_TOKEN_DETAILS.access_token}`,
        },
    })
        .then(checkStatus)
        .then(response => parseJSON(response))
        .then((result) => {
            resolve(result);
        })
        .catch((error) => {
            handleError(dispatch, error, (err) => {
                if (err) {
                    parseError(error.response).then((errorObj) => {
                        reject(errorObj);
                    }).catch((jsonParsingError) => {
                        reject(jsonParsingError);
                    });
                } else {
                    get(nodeURL, dispatch).then((result) => {
                        resolve(result);
                    })
                        .catch((finalError) => {
                            reject(finalError);
                        });
                }
            });
        });
}));


export const post = (nodeURL, formBody, dispatch) => new Promise(((resolve, reject) => {
    fetch(config.BASE_URL + nodeURL, {
        method: 'POST',
        timeout: 5000,
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded',
            Accept: 'application/json',
            Authorization: `Bearer ${config.USER_TOKEN_DETAILS.access_token}`,
        },
        body: formBody,
    })
        .then(checkStatus)
        .then(response => parseJSON(response))
        .then((result) => {
            resolve(result);
        })
        .catch((error) => {
            handleError(dispatch, error, (err) => {
                if (err) {
                    parseError(error.response).then((errorObj) => {
                        reject(errorObj);
                    }).catch((jsonParsingError) => {
                        reject(jsonParsingError);
                    });
                } else {
                    post(nodeURL, formBody, dispatch)
                        .then((result) => {
                            resolve(result);
                        })
                        .catch((finalError) => {
                            reject(finalError);
                        });
                }
            });
        });
}));

export const put = (nodeURL, formBody, dispatch) => new Promise(((resolve, reject) => {
    fetch(config.BASE_URL + nodeURL, {
        method: 'PUT',
        timeout: 5000,
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded',
            Accept: 'application/json',
            Authorization: `Bearer ${config.USER_TOKEN_DETAILS.access_token}`,
        },
        body: formBody,
    })
        .then(checkStatus)
        .then(response => parseJSON(response))
        .then((result) => {
            resolve(result);
        })
        .catch((error) => {
            handleError(dispatch, error, (err) => {
                if (err) {
                    parseError(error.response).then((errorObj) => {
                        reject(errorObj);
                    }).catch((jsonParsingError) => {
                        reject(jsonParsingError);
                    });
                } else {
                    post(nodeURL, formBody, dispatch)
                        .then((result) => {
                            resolve(result);
                        })
                        .catch((finalError) => {
                            reject(finalError);
                        });
                }
            });
        });
}));

export const del = (nodeURL, formBody, dispatch) => new Promise(((resolve, reject) => {
    fetch(config.BASE_URL + nodeURL, {
        method: 'DELETE',
        timeout: 5000,
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded',
            Accept: 'application/json',
            Authorization: `Bearer ${config.USER_TOKEN_DETAILS.access_token}`,
        },
        body: formBody,
    })
        .then(checkStatus)
        .then(response => parseJSON(response))
        .then((result) => {
            resolve(result);
        })
        .catch((error) => {
            handleError(dispatch, error, (err) => {
                if (err) {
                    parseError(error.response).then((errorObj) => {
                        reject(errorObj);
                    }).catch((jsonParsingError) => {
                        reject(jsonParsingError);
                    });
                } else {
                    del(nodeURL, formBody, dispatch)
                        .then((result) => {
                            resolve(result);
                        })
                        .catch((finalError) => {
                            reject(finalError);
                        });
                }
            });
        });
}));



export const postWithJsonCT = (nodeURL, formBody, dispatch) => new Promise(((resolve, reject) => {
    fetch(config.BASE_URL + nodeURL, {
        method: 'POST',
        timeout: 5000,
        headers: {
            'Content-Type': 'application/json',
            Accept: 'application/json',
            Authorization: `Bearer ${config.USER_TOKEN_DETAILS.access_token}`,
        },
        body: JSON.stringify(formBody),
    })
        .then(checkStatus)
        .then(response => parseJSON(response))
        .then((result) => {
            resolve(result);
        })
        .catch((error) => {
            handleError(dispatch, error, (err) => {
                if (err) {
                    parseError(error.response).then((errorObj) => {
                        reject(errorObj);
                    }).catch((jsonParsingError) => {
                        reject(jsonParsingError);
                    });
                } else {
                    postWithJsonCT(nodeURL, formBody, dispatch)
                        .then((result) => {
                            resolve(result);
                        })
                        .catch((finalError) => {
                            reject(finalError);
                        });
                }
            });
        });
}));

export const auth = (nodeURL, formBody, dispatch) => new Promise(((resolve, reject) => {
    fetch(config.AUTH_BASE_URL + nodeURL, {
        method: 'POST',
        timeout: 5000,
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded',
            Accept: 'application/json',
        },
        body: formBody,
    })
        .then(checkStatus)
        .then(response => parseJSON(response))
        .then((result) => {
            resolve(result);
        })
        .catch((error) => {
            handleError(dispatch, error, (err) => {
                if (err) {
                    parseError(error.response).then((errorObj) => {
                        reject(errorObj);
                    }).catch((jsonParsingError) => {
                        reject(jsonParsingError);
                    });
                }
            });
        });
}));
