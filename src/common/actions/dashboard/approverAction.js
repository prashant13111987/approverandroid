import Constants from '../../constants';
import {
	navigateToDashboardScreen,
	navigateToBack,
	showAlertWithTitle,
	navigateToINVoiceList,
	navigateToInVoiceDashboard,
	navigateToVenderList,
	navigateToSendForApproval,
	navigateToAddItem,
	navigateToSuccessApproval,
	navigateToShipList,
	navigateFromShipList,
	navigateToItemList,
	navigateToEditItem,
	navigateToDistributionList
} from '../../routing';
import { get, del, put, post, auth, postWithoutToken, patch } from '../comman/index (2)';
import config from '../../config';
import qs from 'qs';

// prepare Dashboard InVoice Request
export const getPendingInvoices = () => (dispatch, getState) => {
	const { netState } = getState().deviceInfo;
	if (true) {
		dispatch(requestStarted());
		return get('/invoices?filter=approver', dispatch)
			.then((result) => {
				dispatch(requestCompleted());
				dispatch(recivedInvoiceSuccess(result));
				dispatch(getPendingBills());
				dispatch(getCreditMemosInvoices());
			})
			.catch((error) => {
				dispatch(requestFail());
				if (error.message) {
					showAlertWithTitle('Error', error.message);
				}
				if (error.response.data) {
					showAlertWithTitle('Error', error.response.data.error);
				}
			});
	}
	showAlertWithTitle('Error', config.NO_INTERNET_MESSAGE);
	return null; /**/
};

export const getCreditMemosInvoices = () => (dispatch, getState) => {
	const { netState } = getState().deviceInfo;
	if (true) {
		dispatch(requestStarted());
		return get('/credit_memos?filter=approver', dispatch)
			.then((result) => {
				// dispatch(requestCompleted())
				// dispatch(recivedInvoiceSuccess(result))
				// dispatch(getPendingBills())
				dispatch({
					type: Constants.APPROVER_RECEIVED_CREDIT_MEMOS_INVOICE_SUCCESS,
					payload: result
				});
			})
			.catch((error) => {
				dispatch(requestFail());
				if (error.message) {
					showAlertWithTitle('Error', error.message);
				}
				if (error.response.data) {
					showAlertWithTitle('Error', error.response.data.error);
				}
			});
	}
	showAlertWithTitle('Error', config.NO_INTERNET_MESSAGE);
	return null; /**/
};

// prepare Dashboard ALL Invoice Request
export const getPendingBills = () => (dispatch, getState) => {
	const { netState } = getState().deviceInfo;
	if (true) {
		dispatch(requestStarted());
		return get('/bills?filter=approver', dispatch)
			.then((result) => {
				dispatch(requestCompleted());
				dispatch({
					type: Constants.APPROVER_RECEIVED_BILLS_SUCCESS,
					payload: result
				});
				dispatch(getPendingBillsPayment());
				dispatch(getDebitMemosBills());
				// dispatch(merchantBranchesRequest())
				// dispatch(merchantDepartmentRequest())
				// dispatch(vendorsRequest())
			})
			.catch((error) => {
				dispatch(requestFail());
				// showAlertWithTitle('Error', error);
				if (error.message) {
					showAlertWithTitle('Error', error.message);
				}
				if (error.response.data) {
					showAlertWithTitle('Error', error.response.data.error);
				}
			});
	}
	showAlertWithTitle('Error', config.NO_INTERNET_MESSAGE);
	return null; /**/
};

export const getDebitMemosBills = () => (dispatch, getState) => {
	const { netState } = getState().deviceInfo;
	if (true) {
		dispatch(requestStarted());
		return get('/debit_memos?filter=approver', dispatch)
			.then((result) => {
				dispatch(requestCompleted());
				dispatch({
					type: Constants.APPROVER_RECEIVED_DEBIT_MEMOS_BILLS_SUCCESS,
					payload: result
				});
				// dispatch(merchantBranchesRequest())
				// dispatch(merchantDepartmentRequest())
				// dispatch(vendorsRequest())
			})
			.catch((error) => {
				dispatch(requestFail());
				// showAlertWithTitle('Error', error);
				if (error.message) {
					showAlertWithTitle('Error', error.message);
				}
				if (error.response.data) {
					showAlertWithTitle('Error', error.response.data.error);
				}
			});
	}
	showAlertWithTitle('Error', config.NO_INTERNET_MESSAGE);
	return null; /**/
};

export const getPendingBillsPayment = () => (dispatch, getState) => {
	const { netState } = getState().deviceInfo;
	if (true) {
		dispatch(requestStarted());
		return get('/bill_payments?filter=approver', dispatch)
			.then((result) => {
				dispatch(requestCompleted());
				dispatch({
					type: Constants.APPROVER_RECEIVED_BILL_PAYMENTS_SUCCESS,
					payload: result
				});
				dispatch(getPendingMasterData());
				dispatch(getPurchaseOrders());
				// dispatch(merchantBranchesRequest())
				// dispatch(merchantDepartmentRequest())
				// dispatch(vendorsRequest())
			})
			.catch((error) => {
				dispatch(requestFail());
				// showAlertWithTitle('Error', error);
				if (error.message) {
					showAlertWithTitle('Error', error.message);
				}
				if (error.response.data) {
					showAlertWithTitle('Error', error.response.data.error);
				}
			});
	}
	showAlertWithTitle('Error', config.NO_INTERNET_MESSAGE);
	return null; /**/
};
export const getPurchaseOrders = () => (dispatch, getState) => {
	const { netState } = getState().deviceInfo;
	if (true) {
		dispatch(requestStarted());
		return get('/temp_purchase_orders?filter=approver', dispatch)
			.then((result) => {
				dispatch(requestCompleted());
				dispatch({
					type: Constants.APPROVER_RECEIVED_PURCHASE_ORDER_SUCCESS,
					payload: result
				});
				dispatch(getPendingMasterData());
				// dispatch(merchantBranchesRequest())
				// dispatch(merchantDepartmentRequest())
				// dispatch(vendorsRequest())
			})
			.catch((error) => {
				dispatch(requestFail());
				// showAlertWithTitle('Error', error);
				if (error.message) {
					showAlertWithTitle('Error', error.message);
				}
				if (error.response.data) {
					showAlertWithTitle('Error', error.response.data.error);
				}
			});
	}
	showAlertWithTitle('Error', config.NO_INTERNET_MESSAGE);
	return null; /**/
};

export const getPendingMasterData = () => (dispatch, getState) => {
	const { netState } = getState().deviceInfo;
	if (true) {
		dispatch(requestStarted());
		return get('/temp_pending_masterdata', dispatch)
			.then((result) => {
				dispatch(requestCompleted());
				dispatch({
					type: Constants.APPROVER_RECEIVED_TMP_MASTER_DATA_SUCCESS,
					payload: result
				});
				// dispatch(venderListRequest())
				// dispatch(merchantBranchesRequest())
				// dispatch(merchantDepartmentRequest())
				// dispatch(vendorsRequest())
			})
			.catch((error) => {
				dispatch(requestFail());
				// showAlertWithTitle('Error', error);
				if (error.message) {
					showAlertWithTitle('Error', error.message);
				}
				if (error.response.data) {
					showAlertWithTitle('Error', error.response.data.error);
				}
			});
	}
	showAlertWithTitle('Error', config.NO_INTERNET_MESSAGE);
	return null; /**/
};

// prepare merchantdepartments Request
export const merchantDepartmentRequest = () => (dispatch, getState) => {
	const { netState } = getState().deviceInfo;
	if (true) {
		dispatch(requestStarted());
		return get('/merchant_departments', dispatch)
			.then((result) => {
				dispatch(requestCompleted());
				dispatch(merchantDepartmentSuccess(result));
			})
			.catch((error) => {
				dispatch(requestFail());
				showAlertWithTitle('Error', error);
				if (error.message) {
				}
			});
	}
	showAlertWithTitle('Error', config.NO_INTERNET_MESSAGE);
	return null; /**/
};

// prepare vendors Request
export const vendorsRequest = () => (dispatch, getState) => {
	const { netState } = getState().deviceInfo;
	if (true) {
		dispatch(requestStarted());
		return get('/vendors', dispatch)
			.then((result) => {
				dispatch(requestCompleted());
				dispatch(vendorsSuccess(result));
			})
			.catch((error) => {
				dispatch(requestFail());
				showAlertWithTitle('Error', error);
				if (error.message) {
				}
			});
	}
	showAlertWithTitle('Error', config.NO_INTERNET_MESSAGE);
	return null; /**/
};

// prepare merchantBranches Request
export const merchantBranchesRequest = () => (dispatch, getState) => {
	const { netState } = getState().deviceInfo;
	if (true) {
		dispatch(requestStarted());
		return get('/merchant_branches', dispatch)
			.then((result) => {
				dispatch(requestCompleted());
				dispatch(merchantBranchesSuccess(result));
			})
			.catch((error) => {
				dispatch(requestFail());
				showAlertWithTitle('Error', error);
				if (error.message) {
				}
			});
	}
	showAlertWithTitle('Error', config.NO_INTERNET_MESSAGE);
	return null; /**/
};

export const selectedInvoiceStatus = (data) => (dispatch) => {
	dispatch({
		type: Constants.APPROVER_SELECTED_INVOICE,
		payload: { data }
	});
};

export const resetPieFlagData = (data) => (dispatch) => {
	dispatch({
		type: Constants.APPROVER_SET_PIE_DATA,
		payload: { data }
	});
};

export const resetBillDetails = () => (dispatch) => {
	dispatch({
		type: Constants.APPROVER_RESET_BILL_DETAILS
	});
};
const recivedALLInvoiceSuccess = (data) => (dispatch) => {
	dispatch({
		type: Constants.APPROVER_RECEIVED_ALL_INVOICE_SUCCESS,
		payload: data
	});
};

const recivedInvoiceSuccess = (data) => (dispatch) => {
	dispatch({
		type: Constants.APPROVER_RECEIVED_INVOICE_SUCCESS,
		payload: data
	});
};

const requestFail = () => (dispatch) => {
	dispatch({
		type: Constants.REQUEST_FAIL,
		payload: ''
	});
};

const requestCompleted = () => (dispatch) => {
	dispatch({
		type: Constants.REQUEST_COMPLETED
	});
};

const requestStarted = () => (dispatch) => {
	dispatch({
		type: Constants.REQUEST_STARTED,
		payload: ''
	});
};

export const showFilterData = (data) => (dispatch) => {
	dispatch({
		type: Constants.APPROVER_SHOW_FILTER_DATA,
		payload: { data }
	});
};

const getPOStatusCode = type => {
	if (type === 'approve') {
		return 1
	} else if (type === 'hold') {
		return 2
	} else if (type === 'reject') {
		return 0
	} else if (type === 'cancel') {
		return 7
	} else if (type === 'submitToVendor') {
		return 6
	}
}

export const workFlow = (selectedType, data, note, modalType) => async (dispatch) => {
	try {
		dispatch(requestStarted());
		let request = {
			workflow: {
				status: modalType,
				rush: false,
				note: note
			}
		};

		let url = '/invoice_workflows';
		if (selectedType === 'Invoices') {
			if (data.category === 'CRN') {
				url = '/credit_memo_workflows';
				request['credit_memo_id'] = data.id;
			} else {
				url = '/invoice_workflows';
				request['invoice_id'] = data.id;
			}
		} else if (selectedType === 'Bills') {
			if (data.category === 'DBN') {
				url = '/debit_memo_workflows';
				request['debit_memo_id'] = data.id;
			} else {
				url = '/bill_workflows';
				request['bill_id'] = data.id;
			}
		} else if (selectedType === 'Payments') {
			url = '/bill_payment_workflows';
			request['bill_payment_report_id'] = data.id;
		} else if (selectedType === 'PO') {
			url = '/po_workflows';
			request['purchase_order_id'] = data.id;
			request.workflow['status'] = getPOStatusCode(modalType);
		} else if (selectedType === 'Master Data') {
			request = {}
			if (modalType === 'approve') {
				if (data.category === 'item') {
					if (data.merchant_item_id) {
						url = '/merchant_items/update_from_temp'
						request['temp_data_id'] = data.id
						request['id'] = data.merchant_item_id
					} else {
						url = '/merchant_items/create_from_temp'
						request['temp_data_id'] = data.id
					}
				} else if (data.category === 'product') {
					if (data.merchant_product_id) {
						url = '/merchant_products/update_from_temp'
						request['temp_data_id'] = data.id
						request['id'] = data.merchant_product_id
					} else {
						url = '/merchant_products/create_from_temp'
						request['temp_data_id'] = data.id
					}
				} else if (data.category === 'customer') {
					if (data.customer_id) {
						url = '/customers/update_from_temp'
						request['temp_data_id'] = data.id
						request['id'] = data.customer_id
					} else {
						url = '/customers/create_from_temp'
						request['temp_data_id'] = data.id
					}
				} else if (data.category === 'vendor') {
					if (data.vendor_id) {
						url = '/vendors/update_from_temp'
						request['temp_data_id'] = data.id
						request['id'] = data.vendor_id
					} else {
						url = '/vendors/create_from_temp'
						request['temp_data_id'] = data.id
					}
				}
			} else {
				url = '/master_data_workflows'
				request['temp_data_id'] = data.id
				request['md_type'] = data.category
				request['master_data_workflow'] = { status: modalType, note }
			}
		}
		console.log('Request', `${url}?${qs.stringify(request, { arrayFormat: 'brackets' })}`)
		const result = await post(`${url}?${qs.stringify(request, { arrayFormat: 'brackets' })}`, request, dispatch);
		if ((selectedType === 'PO' && modalType === 'approve' && data.purchase_order_id) || (getPOStatusCode(modalType) === 6 && !data.purchase_order_id)) {
			const newRequest = {
				id: data.id
			}
			const newResult = await post(`/purchase_orders/create_from_temp?${qs.stringify(newRequest, { arrayFormat: 'brackets' })}`, request, dispatch);
			console.log('newResult', newResult)
		}
		console.log('result', result)
		dispatch(requestCompleted());
		dispatch(getPendingInvoices());
		dispatch(goBack())
	} catch (error) {
		console.log('Eror', error);
		dispatch(requestFail());
		// showAlertWithTitle('Error', error);
		if (error.message) {
			showAlertWithTitle('Error', error.message);
		}
		if (error.response.data) {
			showAlertWithTitle('Error', error.response.data.error);
		}
	}
};
export const goBack = () => (dispatch) => {
	navigateToBack(dispatch);
};

export const invoiceDetailEditRequest = (item) => (dispatch, getState) => {
	const { netState } = getState().deviceInfo;
	var urlString = '/ocr_bills';
	const ocrImagesObject = {
		// "bill_id":222,
		bill_id: item.id
	};
	let formBody = [];
	formBody = ocrImagesObject;
	const query = Object.keys(formBody)
		.map((k) => {
			if (Array.isArray(formBody[k])) {
				return formBody[k].map((val) => `${encodeURIComponent(k)}[]=${encodeURIComponent(val)}`).join('&');
			}

			return `${encodeURIComponent(k)}=${encodeURIComponent(formBody[k])}`;
		})
		.join('&');

	urlString += `?${query}`;

	if (true) {
		dispatch(requestStarted());
		return get(urlString, dispatch)
			.then((result) => {
				dispatch(requestCompleted());
				result.shipping_branch_name = '';
				result.vendor_branch_name = '';
				dispatch(billDetailSuccess(result));
			})
			.catch((error) => {
				dispatch(requestFail());
				if (error.message) {
					showAlertWithTitle('Error', error.message);
				}
				if (error.response.data) {
					showAlertWithTitle('Error', error.response.data.error);
				}
			});
	}
	showAlertWithTitle('Error', config.NO_INTERNET_MESSAGE);
	return null; /**/
};

// prepare users Request
export const invoiceDetailRequest = (item) => (dispatch, getState) => {
	const { netState } = getState().deviceInfo;
	const invoiceType = getState().approverDashboard.selectedInvoice.data;

	dispatch({
		type: Constants.APPROVER_SELECTED_ITEM_OBJECT,
		payload: { data: item }
	})
	let urlString = '';
	switch (invoiceType) {
		case 'Invoices':
			switch (item.category) {
				case 'CRN':
					urlString = '/credit_memos/' + item.id;
					break;
				case 'invoice':
					urlString = '/invoices/' + item.id;
			}
			break;
		case 'Bills':
			switch (item.category) {
				case 'DBN':
					urlString = '/debit_memos/' + item.id;
					break;
				case 'bill':
					urlString = '/bills/' + item.id;
			}
			break;
		case 'Payments':
			urlString = '/bill_payments/' + item.id;
			break;
		case 'PO':
			urlString = '/temp_purchase_orders/' + item.id;
			break;
		case 'Master Data':
			switch (item.category) {
				case 'item':
					urlString = '/temp_items/' + item.id;
					break;
				case 'product':
					urlString = '/temp_products/' + item.id;
					break;
				case 'customer':
					urlString = '/temp_customers/' + item.id;
					break;
				case 'vendor':
					urlString = '/temp_vendors/' + item.id;
					break;
			}
			break;
		default:
			urlString = '/invoices/' + item.id;
	}
	dispatch(requestStarted());
	return get(urlString, dispatch)
		.then((result) => {
			dispatch(requestCompleted());
			dispatch({
				type: Constants.APPROVER_SELECTED_INVOICE_DETAILS,
				payload: result
			});
		})
		.catch((error) => {
			dispatch(requestFail());
			showAlertWithTitle('Error', error);
			console.log('Error ==>', error);
			if (error.message) {
			}
		});

	showAlertWithTitle('Error', config.NO_INTERNET_MESSAGE);
	return null; /**/
};

export const billDetailRequest = (item) => (dispatch, getState) => {
	const { netState } = getState().deviceInfo;
	const invoiceType = getState().approverDashboard.selectedInvoice.data;

	let urlString = '/bills/' + item.id;
	dispatch(requestStarted());
	return get(urlString, dispatch)
		.then((result) => {
			dispatch(requestCompleted());
			dispatch({
				type: Constants.APPROVER_SELECTED_INVOICE_DETAILS,
				payload: result
			});
		})
		.catch((error) => {
			dispatch(requestFail());
			showAlertWithTitle('Error', error);
			if (error.message) {
			}
		});

	showAlertWithTitle('Error', config.NO_INTERNET_MESSAGE);
	return null; /**/
};