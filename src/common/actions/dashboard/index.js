
import { Platform } from 'react-native';
import Constants from '../../constants';
import { navigateToDashboardScreen,navigateToBack,showAlertWithTitle,navigateToINVoiceList,navigateToInVoiceDashboard,navigateToVenderList,navigateToSendForApproval,
    navigateToAddItem,navigateToSuccessApproval,navigateToShipList,navigateFromShipList,navigateToItemList ,navigateToEditItem,navigateToDistributionList} from '../../routing';
import { get, del, put, post, auth ,postWithoutToken,patch} from '../comman/index (2)';
import config from '../../config'
import dashboard from "../../reducers/dashboard";
import {saveTokenObject} from "../../db";


export const navigateToDashboard = () => (dispatch) => {
   // navigateToDashboardScreen(dispatch);
    navigateToInVoiceDashboard(dispatch)
    //dispatch(billDetailSuccess({}))
};


export const navigateToAddItemContainer = () => (dispatch) => {
    navigateToAddItem(dispatch)
};

export const navigateToEditItemContainer = () => (dispatch) => {
    navigateToEditItem(dispatch)
};

export const goBack = () => (dispatch) => {
    navigateToBack(dispatch);
};

export const navigateToVoiceList = () => (dispatch) => {
    navigateToINVoiceList(dispatch);
};


export const navigateToVender = (value) => (dispatch) => {
    navigateToVenderList(dispatch,value);
};

export const navigateToShipListContainer = () => (dispatch) => {
    navigateToShipList(dispatch);
};

export const navigateShipFromListContainer = () => (dispatch) => {
    navigateFromShipList(dispatch);
};

export const navigateItemListListContainer = () => (dispatch) => {
    navigateToItemList(dispatch);
};


// Create s_invoice  request
export const addItem_APIRequest = (item) => (dispatch, getState) => {
    const { netState } = getState().deviceInfo;
    var urlString = "/bills/" + item.id
    if (true) {
        dispatch(requestStarted());
        const details = item
        // prepare form body
        let formBody = [];
        formBody = item;
        // formBody.is_original_received = false
        // formBody.rush = false

        return patch(urlString, formBody, dispatch)
            .then((result) => {
                dispatch(requestCompleted())
               // navigateToInVoiceDashboard(dispatch)
                dispatch(billDetailSuccess(result.data))
              //  dispatch(receivedInvoicesRequest())


            }).catch((error) => {
                dispatch(requestFail());
                if (error.message) {
                    showAlertWithTitle('Error', error.message);
                }
                if (error.response.data) {
                    showAlertWithTitle('Error', error.response.data.error);
                }
            });
    }
    showAlertWithTitle('Error', config.NO_INTERNET_MESSAGE);
    return null;/**/
};





// Create email_Uploaded_Invoice  request
export const email_Uploaded_Invoice_APIRequest = (item) => (dispatch, getState) => {

    console.log("Item Values ::::::::::::::::::::::",item)
    const { netState } = getState().deviceInfo;
    if (true) {
        dispatch(requestStarted());
        const ocrImagesObject = {
            source_name:item.source_name,
            source_id:item.id,
            vendor_id:item.vendor_id,
        }
        const ocr = {
            ocr_image :ocrImagesObject
        }
        // prepare form body
        let formBody = [];
        formBody = ocr;
        return post('/ocr_images', formBody, dispatch)
            .then((result) => {
                dispatch(requestCompleted())
                navigateToInVoiceDashboard(dispatch)

            }).catch((error) => {
                dispatch(requestFail());
                if (error.response.data) {
                    showAlertWithTitle('Error', error.response.data.errors);
                }
                if (error.message) {
                    showAlertWithTitle('Error', error.message);
                }

            });
    }
    showAlertWithTitle('Error', config.NO_INTERNET_MESSAGE);
    return null;/**/
};


// Create s_invoice  request
export const S_Invoice_APIRequest = (item) => (dispatch, getState) => {
    const { netState } = getState().deviceInfo;
    if (true) {
        dispatch(requestStarted());
        const details = item
        // prepare form body
         let formBody = [];
        formBody = item;
        formBody.is_original_received = false
        formBody.rush = false

        return post('/bills', formBody, dispatch)
            .then((result) => {
                dispatch(requestCompleted())
                navigateToInVoiceDashboard(dispatch)

            }).catch((error) => {
                dispatch(requestFail());
                if (error.message) {
                    showAlertWithTitle('Error', error.message);
                }
                if (error.response.data) {
                    showAlertWithTitle('Error', error.response.data.error);
                }
            });
    }
    showAlertWithTitle('Error', config.NO_INTERNET_MESSAGE);
    return null;/**/
};

// Create send for approval  request
export const sendForApprovalRequest = (item) => (dispatch, getState) => {
    const { netState } = getState().deviceInfo;
    var urlString = "/bills/" + item.id
    if (true) {
        dispatch(requestStarted());
        const details = item
        // prepare form body
        let formBody = [];
        formBody = item;
        return patch(urlString, formBody, dispatch)
            .then((result) => {
                dispatch(requestCompleted())
                navigateToSuccessApproval(dispatch);
               // navigateToSendForApproval(dispatch)
            }).catch((error) => {
                dispatch(requestFail());
                if (error.message) {
                    showAlertWithTitle('Error', error.message);
                }
                if (error.response.data) {
                    showAlertWithTitle('Error', error.response.data.error);
                }
            });
    }
    showAlertWithTitle('Error', config.NO_INTERNET_MESSAGE);
    return null;/**/
};


// Create updateForApprovalRequest
export const updateForApprovalRequest = (item) => (dispatch, getState) => {
    const { netState } = getState().deviceInfo;
    var urlString = "/bills/" + item.id
    if (true) {
        dispatch(requestStarted());
        const details = item
        // prepare form body
        let formBody = [];
         formBody = item;
         formBody.is_original_received = false
         formBody.rush = false
        return patch(urlString, formBody, dispatch)
            .then((result) => {
                dispatch(requestCompleted())
                navigateToSendForApproval(dispatch)
            }).catch((error) => {
                dispatch(requestFail());
                if (error.message) {
                    showAlertWithTitle('Error', error.message);
                }
                if (error.response.data) {
                    showAlertWithTitle('Error', error.response.data.error);
                }
            });
    }
    showAlertWithTitle('Error', config.NO_INTERNET_MESSAGE);
    return null;/**/
};





// Create s_invoice  request
export const inVoice_Update_APIRequest = (item) => (dispatch, getState) => {
    const { netState } = getState().deviceInfo;
    var urlString = "/bills/" + item.id
    if (true) {
        dispatch(requestStarted());
        const details = item
        // prepare form body
        let formBody = [];
        formBody = item;
        formBody.is_original_received = false
        formBody.submit_for_approval = false

        return post(urlString, formBody, dispatch)
            .then((result) => {
                dispatch(requestCompleted())
                navigateToInVoiceDashboard(dispatch)

            }).catch((error) => {
                dispatch(requestFail());
                if (error.message) {
                    showAlertWithTitle('Error', error.message);
                }
                if (error.response.data) {
                    showAlertWithTitle('Error', error.response.data.error);
                }
            });
    }
    showAlertWithTitle('Error', config.NO_INTERNET_MESSAGE);
    return null;/**/
};


export const sendForApproval = () => (dispatch) => {
    navigateToSendForApproval(dispatch);
};

export const successApproval = () => (dispatch) => {
    navigateToSuccessApproval(dispatch);
};

// prepare Dashboard InVoice Request
export const receivedInvoicesRequest = () => (dispatch, getState) => {
    const { netState } = getState().deviceInfo;
    if (true) {
        dispatch(requestStarted());
        return get('/all_received_invoices', dispatch)
            .then((result) => {
                dispatch(requestCompleted())
                dispatch(recivedInvoiceSuccess(result))
                dispatch(receivedALLInvoiceRequest())

            }).catch((error) => {
                dispatch(requestFail());
                if (error.message) {
                    showAlertWithTitle('Error', error.message);
                }
                if (error.response.data) {
                    showAlertWithTitle('Error', error.response.data.error);
                }
            });
    }
    showAlertWithTitle('Error', config.NO_INTERNET_MESSAGE);
    return null;/**/
};


// prepare Dashboard ALL Invoice Request
export const receivedALLInvoiceRequest = () => (dispatch, getState) => {
    const { netState } = getState().deviceInfo;
    if (true) {
        dispatch(requestStarted());
        return get('/bills?filter=all', dispatch)
            .then((result) => {
                dispatch(requestCompleted())
                dispatch(recivedALLInvoiceSuccess(result))
                dispatch(venderListRequest())
                dispatch(merchantBranchesRequest())
                dispatch(merchantDepartmentRequest())
                dispatch(vendorsRequest())

            }).catch((error) => {
                dispatch(requestFail());
               // showAlertWithTitle('Error', error);
                if (error.message) {
                    showAlertWithTitle('Error', error.message);
                }
                if (error.response.data) {
                    showAlertWithTitle('Error', error.response.data.error);
                }

            });
    }
    showAlertWithTitle('Error', config.NO_INTERNET_MESSAGE);
    return null;/**/
};



// prepare merchantItems Request
export const merchantItemsRequest = () => (dispatch, getState) => {
    const { netState } = getState().deviceInfo;
    if (true) {
        dispatch(requestStarted());
        return get('/merchant_items', dispatch)
            .then((result) => {
                dispatch(requestCompleted())
                dispatch(merchantItemsSuccess(result))

            }).catch((error) => {
                dispatch(requestFail());
                showAlertWithTitle('Error', error);
                if (error.message) {

                }

            });
    }
    showAlertWithTitle('Error', config.NO_INTERNET_MESSAGE);
    return null;/**/
};

const merchantItemsSuccess = data => (dispatch) => {
    dispatch({
        type: Constants.MERCHANT_ITEMS_SUCCESS,
        payload: data,
    });
};


// prepare merchantdepartments Request
export const merchantDepartmentRequest = () => (dispatch, getState) => {
    const { netState } = getState().deviceInfo;
    if (true) {
        dispatch(requestStarted());
        return get('/merchant_departments', dispatch)
            .then((result) => {
                dispatch(requestCompleted())
                dispatch(merchantDepartmentSuccess(result))

            }).catch((error) => {
                dispatch(requestFail());
                showAlertWithTitle('Error', error);
                if (error.message) {

                }

            });
    }
    showAlertWithTitle('Error', config.NO_INTERNET_MESSAGE);
    return null;/**/
};

const merchantDepartmentSuccess = data => (dispatch) => {
    dispatch({
        type: Constants.MERCHANT_DEPARTMENT_SUCCESS,
        payload: data,
    });
};


// prepare merchantBranches Request
export const merchantBranchesRequest = () => (dispatch, getState) => {
    const { netState } = getState().deviceInfo;
    if (true) {
        dispatch(requestStarted());
        return get('/merchant_branches', dispatch)
            .then((result) => {
                dispatch(requestCompleted())
                dispatch(merchantBranchesSuccess(result))

            }).catch((error) => {
                dispatch(requestFail());
                showAlertWithTitle('Error', error);
                if (error.message) {

                }

            });
    }
    showAlertWithTitle('Error', config.NO_INTERNET_MESSAGE);
    return null;/**/
};

const merchantBranchesSuccess = data => (dispatch) => {
    dispatch({
        type: Constants.MERCHANT_BRANCHES_SUCCESS,
        payload: data,
    });
};


// prepare vendors Request
export const vendorsRequest = () => (dispatch, getState) => {
    const { netState } = getState().deviceInfo;
    if (true) {
        dispatch(requestStarted());
        return get('/vendors', dispatch)
            .then((result) => {
                dispatch(requestCompleted())
                dispatch(vendorsSuccess(result))

            }).catch((error) => {
                dispatch(requestFail());
                showAlertWithTitle('Error', error);
                if (error.message) {

                }
            });
    }
    showAlertWithTitle('Error', config.NO_INTERNET_MESSAGE);
    return null;/**/
};

const vendorsSuccess = data => (dispatch) => {
    dispatch({
        type: Constants.VENDORS_SUCCESS,
        payload: data,
    });
};

// prepare bills Request
export const billsRequest = () => (dispatch, getState) => {
    const { netState } = getState().deviceInfo;
    if (true) {
        dispatch(requestStarted());
        return get('/bills', dispatch)
            .then((result) => {
                dispatch(requestCompleted())
                dispatch(billsSuccess(result))

            }).catch((error) => {
                dispatch(requestFail());
                showAlertWithTitle('Error', error);
                if (error.message) {

                }

            });
    }
    showAlertWithTitle('Error', config.NO_INTERNET_MESSAGE);
    return null;/**/
};

const billsSuccess = data => (dispatch) => {
    dispatch({
        type: Constants.BILLS_SUCCESS,
        payload: data,
    });
};

export const updateBillDetail = data => (dispatch) => {
    dispatch({
        type: Constants.UPDATE_BILL_DETAILS,
        payload: data,
    });
};




// prepare users Request
export const usersRequest = () => (dispatch, getState) => {
    const { netState } = getState().deviceInfo;
    if (true) {
        dispatch(requestStarted());
        return get('/users', dispatch)
            .then((result) => {
                dispatch(requestCompleted())
                dispatch(userSuccess(result))

            }).catch((error) => {
                dispatch(requestFail());
                showAlertWithTitle('Error', error);
                if (error.message) {

                }

            });
    }
    showAlertWithTitle('Error', config.NO_INTERNET_MESSAGE);
    return null;/**/
};

const userSuccess = data => (dispatch) => {
    dispatch({
        type: Constants.USERS_SUCCESS,
        payload: data,
    });
};


// prepare users Request
export const billDetailRequest = (item) => (dispatch, getState) => {
    const { netState } = getState().deviceInfo;
    var urlString = "/bills/" + item.id
    if (true) {
        dispatch(requestStarted());
        return get(urlString, dispatch)
            .then((result) => {
                dispatch(requestCompleted())
                dispatch(billDetailSuccess(result))

            }).catch((error) => {
                dispatch(requestFail());
                showAlertWithTitle('Error', error);
                if (error.message) {

                }

            });
    }
    showAlertWithTitle('Error', config.NO_INTERNET_MESSAGE);
    return null;/**/
};


export const billDetailEditRequest = (item) => (dispatch, getState) => {
    const { netState } = getState().deviceInfo;
    var urlString = "/ocr_bills"
       const ocrImagesObject = {
           // "bill_id":222,
           "bill_id":item.id,
        }
    let formBody = [];
     formBody = ocrImagesObject
        const query = Object
            .keys(formBody)
            .map(k => {
                if (Array.isArray(formBody[k])) {
                    return formBody[k]
                        .map(val => `${encodeURIComponent(k)}[]=${encodeURIComponent(val)}`)
                        .join('&')
                }

                return `${encodeURIComponent(k)}=${encodeURIComponent(formBody[k])}`
            })
            .join('&')

        urlString += `?${query}`;

     if (true) {
        dispatch(requestStarted());
        return get(urlString, dispatch)
            .then((result) => {
                dispatch(requestCompleted())
                result.shipping_branch_name = ""
                result.vendor_branch_name = ""
                dispatch(billDetailSuccess(result))

            }).catch((error) => {
                dispatch(requestFail());
                if (error.message) {
                    showAlertWithTitle('Error', error.message);
                }
                if (error.response.data) {
                    showAlertWithTitle('Error', error.response.data.error);
                }

            });
    }
    showAlertWithTitle('Error', config.NO_INTERNET_MESSAGE);
    return null;/**/
};



export const billDetailSuccess = data => (dispatch) => {
    dispatch({
        type: Constants.BILL_DETAILS_SUCCESS,
        payload: data,
    });
};


// prepare  Vender List Request
export const venderListRequest = () => (dispatch, getState) => {
    const { netState } = getState().deviceInfo;
    if (true) {
        dispatch(requestStarted());
        return get('/vendors', dispatch)
            .then((result) => {
                dispatch(requestCompleted())
                dispatch(venderListSuccess(result))

            }).catch((error) => {
                dispatch(requestFail());
                showAlertWithTitle('Error', error);
                if (error.message) {

                }

            });
    }
    showAlertWithTitle('Error', config.NO_INTERNET_MESSAGE);
    return null;/**/
};


// prepare login request
export const uploadImageRequest = (imageObject) => (dispatch, getState) => {
    const { netState } = getState().deviceInfo;
    //const  venderID = getState().dashboard.venderObject.id;
    if (true) {
        dispatch(requestStarted());
        const details = {
            "files[][name]":imageObject.filename,
            "files[][file]":imageObject.path,
            "files[][extension]":imageObject.mime,
            "files[][size]":imageObject.size,
            //"files[][vendor_id]":venderID,
        };
        // prepare form body
        let formBody = [];
        Object.keys(details).forEach((key) => {
            if (details && Object.prototype.hasOwnProperty.call(details, key)) {
                const encodedKey = encodeURIComponent(key);
                const encodedValue = encodeURIComponent(details[key]);
                formBody.push(`${encodedKey}=${encodedValue}`);
            }
        });
        formBody = formBody.join('&');
        return post('/upload_invoice', formBody, dispatch)
            .then((result) => {
               // console.log("Result OBject :::::::::::::;;;", result)
                dispatch(requestCompleted())
                console.log("Upload Image Successfully::::::::::::",result.data[0])
                dispatch(navigateToVender(result.data[0]))

            }).catch((error) => {
                dispatch(requestFail());
                console.log("User Token OBject :::::::::::::;;;", error.response)
                if (error.message) {
                    showAlertWithTitle('Error', error.message);
                }
            });
    }
    showAlertWithTitle('Error', config.NO_INTERNET_MESSAGE);
    return null;/**/
};



export const setVenderObject = data => (dispatch) => {
    dispatch({
        type: Constants.SET_VENDER_OBJECT,
        payload: {data},
    });
};

export const setInvoiceReceiveCount = count => (dispatch) => {
    dispatch({
        type: Constants.SET_RECEIVE_COUNT,
        payload: { count },
    });
};

export const setInvoiceEditableOrNot = isEdit => (dispatch) => {
    dispatch({
        type: Constants.EDITABLE_INVOICE,
        payload: { isEdit },
    });
};




export const resetPieFlagData = data => (dispatch) => {
    dispatch({
        type: Constants.SET_PIE_DATA,
        payload: { data },
    });
};

export const resetBillDetails = () => (dispatch) => {
    dispatch({
        type: Constants.RESET_BILL_DETAILS,
    });
};

export const showFilterData = data => (dispatch) => {
    dispatch({
        type: Constants.SHOW_FILTER_DATA,
        payload: { data },
    });
};

export const setLegendObject = data => (dispatch) => {
    dispatch({
        type: Constants.SET_LEGEND_OBJECT,
        payload: {data},
    });
};


export const setSelectedBillObject = data => (dispatch) => {
    dispatch({
        type: Constants.SELECTED_BILL_OBJECT,
        payload: {data},
    });
};

export const setShipFromObject = data => (dispatch) => {
    dispatch({
        type: Constants.SELECTED_BILL_OBJECT,
        payload: {data},
    });
};

export const setShipToObject = data => (dispatch) => {
    dispatch({
        type: Constants.SELECTED_BILL_OBJECT,
        payload: {data},
    });
};

export const selectedItemNameObject = data => (dispatch) => {
    dispatch({
        type: Constants.SELECTED_ITEM_OBJECT,
        payload: {data},
    });
};

export const shipFromListObject = data => (dispatch) => {
    dispatch({
        type: Constants.SHIPFROM_ITEM_OBJECT,
        payload: {data},
    });
};

export const shipToListObject = data => (dispatch) => {
    dispatch({
        type: Constants.SHIPTO_ITEM_OBJECT,
        payload: {data},
    });
};

export const setDepartmentObject = data => (dispatch) => {
    dispatch({
        type: Constants.DEPARTMENT_OBJECT,
        payload: {data},
    });
};

export const selectedItemIndexObject = data => (dispatch) => {
    dispatch({
        type: Constants.SELECTED_ITEM_INDEX,
        payload: {data},
    });
};

export const resetSelectedItemNameObject = () => (dispatch) => {
    dispatch({
        type: Constants.RESET_SELECTED_ITEM_OBJECT,
    });
};



export const selectedInvoiceStatus = data => (dispatch) => {
    dispatch({
        type: Constants.SELECTED_INVOICE,
        payload: { data },
    });
};


const recivedInvoiceSuccess = data => (dispatch) => {
    dispatch({
        type: Constants.RECEIVED_INVOICE_SUCCESS,
        payload: data,
    });
};

const recivedALLInvoiceSuccess = data => (dispatch) => {
    dispatch({
        type: Constants.RECEIVED_ALL_INVOICE_SUCCESS,
        payload: data,
    });
};

const venderListSuccess = data => (dispatch) => {
    dispatch({
        type: Constants.VENDER_LIST_SUCCESS,
        payload: data,
    });
};

const requestFail = () => (dispatch) => {
    dispatch({
        type: Constants.REQUEST_FAIL,
        payload: '',
    });
};

const requestCompleted = () => (dispatch) => {
    dispatch({
        type: Constants.REQUEST_COMPLETED,
    });
};

const requestStarted = () => (dispatch) => {
    dispatch({
        type: Constants.REQUEST_STARTED,
        payload: '',
    });
};

export const navigateToDistributionListContainer = () => (dispatch) => {
    navigateToDistributionList(dispatch);
};

