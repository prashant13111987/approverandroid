
import { Platform } from 'react-native';
import Constants from '../../constants';
import { navigateToINVoiceList, navigateToApproverINVoiceList, navigateToBillPaymentDetails } from '../../routing';
import { 
    navigateToDashboardScreen,
    showAlertWithTitle,
    navigateToBack,
    navigateToValidateInvoiceScreen,
    navigateToInVoiceDashboard ,
    navigateToLogin,
    navigateToValidateApproverInvoiceScreen
} from '../../routing';
import {saveTokenObject,deleteTokenObjects} from '../../db';
import { get, del, put, post, auth ,postWithoutToken} from '../comman/index (2)';
//import { get, del, put, post, auth ,postWithoutToken} from '../comman/index';
import config from '../../config';


export const navigateToVoiceList = () => (dispatch) => {
    navigateToINVoiceList(dispatch);
    // navigateToValidateInvoiceScreen(dispatch)
};
export const navigateApproverToVoiceList = () => (dispatch) => {
    navigateToApproverINVoiceList(dispatch);
    // navigateToValidateInvoiceScreen(dispatch)
};
export const navigateToBillPaymentDetailsComponent = () => (dispatch) => {
    navigateToBillPaymentDetails(dispatch);
    // navigateToValidateInvoiceScreen(dispatch)
};

export const navigateToValidateScreen = () => (dispatch) => {
    navigateToValidateInvoiceScreen(dispatch)
};

export const navigateToValidateApproverScreen = () => (dispatch) => {
    navigateToValidateApproverInvoiceScreen(dispatch)
};

export const goBack = () => (dispatch) => {
    navigateToBack(dispatch);
};


// prepare login request
export const loginAPIRequest = (email, password) => (dispatch, getState) => {
    const { netState } = getState().deviceInfo;
    if (true) {
        dispatch(requestStarted());
        const details = {
            "session[email]": email,
            "session[password]":password,
        };
        // prepare form body
        let formBody = [];
        Object.keys(details).forEach((key) => {
            if (details && Object.prototype.hasOwnProperty.call(details, key)) {
                const encodedKey = encodeURIComponent(key);
                const encodedValue = encodeURIComponent(details[key]);
                formBody.push(`${encodedKey}=${encodedValue}`);
            }
        });
        formBody = formBody.join('&');
        return postWithoutToken('/login', formBody, dispatch)
            .then((result) => {
                dispatch(loginSuccess(result));
                dispatch(requestCompleted())
                config.USER_TOKEN_DETAILS = result;
                // save token in realm database
                const loginObject = {
                    auth_token : result.auth_token,
                    email : result.email,
                    userName: result.name,
                    loginId : result.id
                }
                saveTokenObject(loginObject);
                navigateToInVoiceDashboard(dispatch)

            }).catch((error) => {
                dispatch(requestFail());
                if (error.message) {
                    showAlertWithTitle('Error', error.message);
                }
                if (error.response.data) {
                    showAlertWithTitle('Error',  error.response.data.errors);
                }

            });
    }
    showAlertWithTitle('Error', config.NO_INTERNET_MESSAGE);
    return null;/**/
};


// logout user from application
export const logoutUser = () => (dispatch, getState) => {
    // delete user details from database
    deleteTokenObjects();
    // remove all user details from application
    dispatch(removeUserData());
    // navigate back to login
    navigateToLogin(dispatch);
};

const removeUserData = () => (dispatch) => {
    dispatch({
        type: Constants.LOG_OUT,
    });
};


const loginSuccess = data => (dispatch) => {
    dispatch({
        type: Constants.LOGIN_SUCCESSS,
        payload: data,
    });
};

const requestFail = () => (dispatch) => {
    dispatch({
        type: Constants.REQUEST_FAIL,
        payload: '',
    });
};

const requestCompleted = () => (dispatch) => {
    dispatch({
        type: Constants.REQUEST_COMPLETED,
    });
};

const requestStarted = () => (dispatch) => {
    dispatch({
        type: Constants.REQUEST_STARTED,
        payload: '',
    });
};