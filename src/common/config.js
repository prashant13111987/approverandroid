
const BASE_API_URL_VERSION = '/api/v1';
// For Test Server
// const HOST_URL = 'http://k12pcloudservice.cloudapp.net';

// For Production Server
// const HOST_URL = 'http://35.154.130.48';
// const HOST_URL = 'http://13.234.19.177:3000';
   const HOST_URL = 'https://api.setlmint.com';



export default {
    HOST: HOST_URL,
    BASE_URL: `${HOST_URL}${BASE_API_URL_VERSION}`,
    AUTH_BASE_URL: HOST_URL,
    GRANT_TYPE: 'password',
    USER_TOKEN_DETAILS: '',
    MONTH_LIST: ['January',
        'February',
        'March',
        'April',
        'May',
        'June',
        'July ',
        'August',
        'September',
        'October',
        'November',
        'December'],
    YEAR_LIST: [
        '2018',
        '2019',
        '2020',
        '2021',
        '2022',
        '2023',
        '2024',
        '2025',
        '2026',
        '2027',
        '2028',
        '2029',
        '2030',
        '2031',
        '2032',
        '2033',
        '2034',
    ],
    NO_INTERNET_MESSAGE: 'Internet not available',
};

