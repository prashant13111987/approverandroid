import Realm from 'realm';
import { Token, AppInfo } from '../../schema';
import config from '../config';

export const getUserToken = () => new Promise((resolve, reject) => {
    console.log("Relaom ::::::::::getUserToken")
    Realm.open({ schema: [Token] })
        .then((realm) => {
            if (realm.objects('Token')) {
                console.log("Relaom ::::::::::",realm.objects('Token'))
                if (realm.objects('Token').length > 0) {
                    config.USER_TOKEN_DETAILS = realm.objects('Token')[0];
                    resolve(realm.objects('Token')[0]);
                } else {
                    resolve(null);
                }
            } else {
                resolve(null);
            }
        })
        .catch((error) => {
            reject(error);
        });
});
export const saveTokenObject = tokenObject => new Promise((resolve, reject) => {
    console.log("Relaom ::::::::::getUserToken",tokenObject)
    Realm.open({ schema: [Token] })
        .then((realm) => {
            console.log("User Token B >>efore right >>>>>>>> ::::::::::",tokenObject)
            realm.write(() => {
                console.log("Relaom ::::::::::saveTokenObject",tokenObject)
                const TokenObject = Object.assign({}, tokenObject, { email: tokenObject.email });
                realm.create('Token', TokenObject, true);
                console.log("User Token Saved >>>>>>>>>> ::::::::::",tokenObject)
                config.USER_TOKEN_DETAILS = tokenObject;
                resolve();
            });
        })
        .catch((error) => {
            console.log("Error ??????", error.toString())
            reject(error);
        });
});


export const saveAppInfo = () => new Promise((resolve, reject) => {
    const appinfo = {
        app:'Test',
        id:1,
    }

    Realm.open({schema: [AppInfo]})
        .then(realm => {
            console.log("App Info >>>>>>", realm)
            // ...use the realm instance here
        })
        .catch(error => {
            console.log("App Info error>>>>>>", error)
            // Handle the error here if something went wrong
        });
});

export const deleteTokenObjects = () => new Promise((resolve, reject) => {
    Realm.open({ schema: [Token] })
        .then((realm) => {
            realm.write(() => {
                const tokens = realm.objects('Token');
                realm.delete(tokens);
                config.USER_TOKEN_DETAILS = '';
                resolve();
            });
        })
        .catch((error) => {
            reject(error);
        });
});
