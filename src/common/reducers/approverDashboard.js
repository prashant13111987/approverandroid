import Constant from '../constants';
import { createReducer } from '../util';

const initialState = {
    isFetching: false,
    receivedInvoiceList: [],
    receivedAllBillsList: [],
    receivedFromEmailList: [],
    receivedFromPhoneList: [],
    receivedFromAppList: [],
    receivedBillPayment: [],
    receivedTmpMasterData: [],
    receivedPurchaseOrder: [],
    receiveCount: 0,
    resetPieFlag: false,
    showFilterData: false,
    legendObject: {},
    selectedInvoice: 'Invoices',
    venderList: [],
    venderObject: {},
    merchantItems: [],
    merchantDepartments: [],
    merchantBranches: [],
    vendors: [],
    bills: [],
    users: [],
    billDetails: {},
    selectedBillObject: {},
    selectedItemObject: {},
    isEditable: false,
    selectedIndex: 0,
    shipFromObject: {},
    shipToObject: {},
    departmentObject: {},
    selectedInvoiceDetails:{}
};

export default createReducer(initialState, {
    [Constant.REQUEST_STARTED]: state => Object.assign({}, state, {
        isFetching: true,
    }),
    [Constant.REQUEST_COMPLETED]: state => Object.assign({}, state, {
        isFetching: false,
    }),
    [Constant.APPROVER_RECEIVED_INVOICE_SUCCESS]: (state, payload) => Object.assign({}, state, {
        isFetching: false,
        receivedInvoiceList: payload,
        // receivedFromEmailList:payload.email_invoices,
        // receivedFromPhoneList:payload.uploaded_invoices,
        // receivedFromAppList:payload.setlmint_invoices,
        //receiveCount:payload.email_invoices.length + payload.uploaded_invoices.length + payload.setlmint_invoices.length
        receiveCount: payload.length
    }),
    [Constant.APPROVER_RECEIVED_CREDIT_MEMOS_INVOICE_SUCCESS]: (state, payload) => Object.assign({}, state, {
        isFetching: false,
        receivedInvoiceList: [...state.receivedInvoiceList, ...payload],
        // receivedFromEmailList:payload.email_invoices,
        // receivedFromPhoneList:payload.uploaded_invoices,
        // receivedFromAppList:payload.setlmint_invoices,
        //receiveCount:payload.email_invoices.length + payload.uploaded_invoices.length + payload.setlmint_invoices.length
        receiveCount: payload.length
    }),
    [Constant.APPROVER_RECEIVED_BILLS_SUCCESS]: (state, payload) => Object.assign({}, state, {
        isFetching: false,
        resetPieFlag: true,
        receivedAllBillsList: payload,
    }),
    [Constant.APPROVER_RECEIVED_DEBIT_MEMOS_BILLS_SUCCESS]: (state, payload) => Object.assign({}, state, {
        isFetching: false,
        resetPieFlag: true,
        receivedAllBillsList: [...state.receivedAllBillsList, ...payload],
    }),
    [Constant.APPROVER_RECEIVED_PURCHASE_ORDER_SUCCESS]: (state, payload) => Object.assign({}, state, {
        isFetching: false,
        resetPieFlag: true,
        receivedPurchaseOrder: payload,
    }),
    [Constant.APPROVER_RECEIVED_TMP_MASTER_DATA_SUCCESS]: (state, payload) => Object.assign({}, state, {
        isFetching: false,
        resetPieFlag: true,
        receivedTmpMasterData: payload,
    }),
    [Constant.APPROVER_RECEIVED_BILL_PAYMENTS_SUCCESS]: (state, payload) => Object.assign({}, state, {
        isFetching: false,
        resetPieFlag: true,
        receivedBillPayment: payload,
    }),
    [Constant.REQUEST_FAIL]: state => Object.assign({}, state, {
        isFetching: false,
    }),
    [Constant.APPROVER_SET_RECEIVE_COUNT]: (state, payload) => Object.assign({}, state, {
        receiveCount: payload.count,
    }),
    [Constant.APPROVER_SET_PIE_DATA]: (state, payload) => Object.assign({}, state, {
        resetPieFlag: payload.data,
    }),
    [Constant.APPROVER_SHOW_FILTER_DATA]: (state, payload) => Object.assign({}, state, {
        showFilterData: payload.data,
    }),
    [Constant.APPROVER_SET_LEGEND_OBJECT]: (state, payload) => Object.assign({}, state, {
        legendObject: payload.data,
    }),
    [Constant.APPROVER_SELECTED_INVOICE]: (state, payload) => Object.assign({}, state, {
        selectedInvoice: payload,
    }),
    [Constant.APPROVER_SELECTED_TYPE]: (state, payload) => Object.assign({}, state, {
        selectedType: payload,
    }),
    [Constant.APPROVER_VENDER_LIST_SUCCESS]: (state, payload) => Object.assign({}, state, {
        isFetching: false,
        venderList: payload,
    }),
    [Constant.APPROVER_SET_VENDER_OBJECT]: (state, payload) => Object.assign({}, state, {
        venderObject: payload.data,
    }),
    [Constant.APPROVER_APPROVER_MERCHANT_ITEMS_SUCCESS]: (state, payload) => Object.assign({}, state, {
        isFetching: false,
        merchantItems: payload,
    }),


    [Constant.APPROVER_MERCHANT_DEPARTMENT_SUCCESS]: (state, payload) => Object.assign({}, state, {
        isFetching: false,
        merchantDepartments: payload,
    }),
    [Constant.APPROVER_MERCHANT_BRANCHES_SUCCESS]: (state, payload) => Object.assign({}, state, {
        isFetching: false,
        merchantBranches: payload,
    }),
    [Constant.APPROVER_VENDORS_SUCCESS]: (state, payload) => Object.assign({}, state, {
        isFetching: false,
        vendors: payload,
    }),
    [Constant.APPROVER_BILLS_SUCCESS]: (state, payload) => Object.assign({}, state, {
        isFetching: false,
        bills: payload,
    }),
    [Constant.APPROVER_USERS_SUCCESS]: (state, payload) => Object.assign({}, state, {
        isFetching: false,
        users: payload,
    }),
    [Constant.APPROVER_BILL_DETAILS_SUCCESS]: (state, payload) => Object.assign({}, state, {
        isFetching: false,
        billDetails: payload,
    }),
    [Constant.APPROVER_SELECTED_BILL_OBJECT]: (state, payload) => Object.assign({}, state, {
        selectedBillObject: payload.data,
    }),
    [Constant.APPROVER_EDITABLE_INVOICE]: (state, payload) => Object.assign({}, state, {
        isEditable: payload.isEdit,
    }),
    [Constant.APPROVER_SELECTED_ITEM_OBJECT]: (state, payload) => Object.assign({}, state, {
        selectedItemObject: payload.data,
    }),
    [Constant.APPROVER_RESET_SELECTED_ITEM_OBJECT]: (state) => Object.assign({}, state, {
        selectedItemObject: {},
    }),
    [Constant.APPROVER_SELECTED_ITEM_INDEX]: (state, payload) => Object.assign({}, state, {
        selectedIndex: payload.data,
    }),
    [Constant.APPROVER_SHIPFROM_ITEM_OBJECT]: (state, payload) => Object.assign({}, state, {
        shipFromObject: payload.data,
    }),
    [Constant.APPROVER_SHIPTO_ITEM_OBJECT]: (state, payload) => Object.assign({}, state, {
        shipToObject: payload.data,
    }),
    [Constant.APPROVER_DEPARTMENT_OBJECT]: (state, payload) => Object.assign({}, state, {
        departmentObject: payload.data,
    }),
    [Constant.APPROVER_UPDATE_BILL_DETAILS]: (state, payload) => Object.assign({}, state, {
        billDetails: payload,
    }),
    [Constant.APPROVER_RESET_BILL_DETAILS]: (state, payload) => Object.assign({}, state, {
        billDetails: {},
    }),
    [Constant.APPROVER_SELECTED_INVOICE_DETAILS]: (state, payload) => Object.assign({}, state, {
        selectedInvoiceDetails: payload,
    }),
    [Constant.LOG_OUT]: (state, payload) => Object.assign({}, state, {
        isFetching: false,
        receivedInvoiceList: [],
        receivedAllBillsList: [],
        receivedFromEmailList: [],
        receivedFromPhoneList: [],
        receivedFromAppList: [],
        receiveCount: 0,
        resetPieFlag: false,
        showFilterData: false,
        legendObject: {},
        selectedInvoice: 'Received',
        venderList: [],
        venderObject: {},
        merchantItems: [],
        merchantDepartments: [],
        merchantBranches: [],
        vendors: [],
        bills: [],
        users: [],
        billDetails: {},
        selectedBillObject: {},
        selectedItemObject: {},
        isEditable: false,
        selectedIndex: 0,
        shipFromObject: {},
        shipToObject: {},
        departmentObject: {}
    }),

});
