import Constant from '../constants';
import { createReducer } from '../util';

const initialState = {
    isFetching: false,
    receivedInvoiceList:[],
    receivedAllInvoiceList:[],
    receivedFromEmailList:[],
    receivedFromPhoneList:[],
    receivedFromAppList:[],
    receiveCount:0,
    resetPieFlag:false,
    showFilterData:false,
    legendObject:{},
    selectedInvoice:'Received',
    venderList:[],
    venderObject:{},
    merchantItems:[],
    merchantDepartments:[],
    merchantBranches:[],
    vendors:[],
    bills:[],
    users:[],
    billDetails:{},
    selectedBillObject:{},
    selectedItemObject:{},
    isEditable:false,
    selectedIndex:0,
    shipFromObject:{},
    shipToObject:{},
    departmentObject:{}
};

export default createReducer(initialState, {
    [Constant.REQUEST_STARTED]: state => Object.assign({}, state, {
        isFetching: true,
    }),
    [Constant.REQUEST_COMPLETED]: state => Object.assign({}, state, {
        isFetching: false,
    }),
    [Constant.RECEIVED_INVOICE_SUCCESS]: (state, payload) => Object.assign({}, state, {
        isFetching: false,
        receivedInvoiceList: payload,
        // receivedFromEmailList:payload.email_invoices,
        // receivedFromPhoneList:payload.uploaded_invoices,
        // receivedFromAppList:payload.setlmint_invoices,
        //receiveCount:payload.email_invoices.length + payload.uploaded_invoices.length + payload.setlmint_invoices.length
        receiveCount:payload.length
    }),
    [Constant.RECEIVED_ALL_INVOICE_SUCCESS]: (state, payload) => Object.assign({}, state, {
        isFetching: false,
        resetPieFlag:true,
        receivedAllInvoiceList: payload,
    }),
    [Constant.REQUEST_FAIL]: state => Object.assign({}, state, {
        isFetching: false,
    }),
    [Constant.SET_RECEIVE_COUNT]: (state, payload) => Object.assign({}, state, {
        receiveCount: payload.count,
    }),
    [Constant.SET_PIE_DATA]: (state, payload) => Object.assign({}, state, {
        resetPieFlag: payload.data,
    }),
    [Constant.SHOW_FILTER_DATA]: (state, payload) => Object.assign({}, state, {
        showFilterData: payload.data,
    }),
    [Constant.SET_LEGEND_OBJECT]: (state, payload) => Object.assign({}, state, {
        legendObject: payload.data,
    }),
    [Constant.SELECTED_INVOICE]: (state, payload) => Object.assign({}, state, {
        selectedInvoice: payload.data,
    }),
    [Constant.VENDER_LIST_SUCCESS]: (state, payload) => Object.assign({}, state, {
        isFetching: false,
        venderList: payload,
    }),
    [Constant.SET_VENDER_OBJECT]: (state, payload) => Object.assign({}, state, {
        venderObject: payload.data,
    }),
    [Constant.MERCHANT_ITEMS_SUCCESS]: (state, payload) => Object.assign({}, state, {
        isFetching: false,
        merchantItems: payload,
    }),
    [Constant.MERCHANT_DEPARTMENT_SUCCESS]: (state, payload) => Object.assign({}, state, {
        isFetching: false,
        merchantDepartments: payload,
    }),
    [Constant.MERCHANT_BRANCHES_SUCCESS]: (state, payload) => Object.assign({}, state, {
        isFetching: false,
        merchantBranches: payload,
    }),
    [Constant.VENDORS_SUCCESS]: (state, payload) => Object.assign({}, state, {
        isFetching: false,
        vendors: payload,
    }),
    [Constant.BILLS_SUCCESS]: (state, payload) => Object.assign({}, state, {
        isFetching: false,
        bills: payload,
    }),
    [Constant.USERS_SUCCESS]: (state, payload) => Object.assign({}, state, {
        isFetching: false,
        users: payload,
    }),
    [Constant.BILL_DETAILS_SUCCESS]: (state, payload) => Object.assign({}, state, {
        isFetching: false,
        billDetails: payload,
    }),
    [Constant.SELECTED_BILL_OBJECT]: (state, payload) => Object.assign({}, state, {
        selectedBillObject: payload.data,
    }),
    [Constant.EDITABLE_INVOICE]: (state, payload) => Object.assign({}, state, {
        isEditable: payload.isEdit,
    }),
    [Constant.SELECTED_ITEM_OBJECT]: (state, payload) => Object.assign({}, state, {
        selectedItemObject: payload.data,
    }),
    [Constant.RESET_SELECTED_ITEM_OBJECT]: (state) => Object.assign({}, state, {
        selectedItemObject:{},
    }),
    [Constant.SELECTED_ITEM_INDEX]: (state, payload) => Object.assign({}, state, {
        selectedIndex: payload.data,
    }),
    [Constant.SHIPFROM_ITEM_OBJECT]: (state, payload) => Object.assign({}, state, {
        shipFromObject: payload.data,
    }),
    [Constant.SHIPTO_ITEM_OBJECT]: (state, payload) => Object.assign({}, state, {
        shipToObject: payload.data,
    }),
    [Constant.DEPARTMENT_OBJECT]: (state, payload) => Object.assign({}, state, {
        departmentObject: payload.data,
    }),
    [Constant.UPDATE_BILL_DETAILS]: (state, payload) => Object.assign({}, state, {
        billDetails: payload,
    }),
    [Constant.RESET_BILL_DETAILS]: (state, payload) => Object.assign({}, state, {
        billDetails: {},
    }),
    [Constant.LOG_OUT]: (state, payload) => Object.assign({}, state, {
        isFetching: false,
        receivedInvoiceList:[],
        receivedAllInvoiceList:[],
        receivedFromEmailList:[],
        receivedFromPhoneList:[],
        receivedFromAppList:[],
        receiveCount:0,
        resetPieFlag:false,
        showFilterData:false,
        legendObject:{},
        selectedInvoice:'Received',
        venderList:[],
        venderObject:{},
        merchantItems:[],
        merchantDepartments:[],
        merchantBranches:[],
        vendors:[],
        bills:[],
        users:[],
        billDetails:{},
        selectedBillObject:{},
        selectedItemObject:{},
        isEditable:false,
        selectedIndex:0,
        shipFromObject:{},
        shipToObject:{},
        departmentObject:{}
    }),

});
