import Constant from '../constants';
import { createReducer } from '../util';

const initialState = {
    isFetching: false,
    loginSuccess:null,
};

export default createReducer(initialState, {
    [Constant.REQUEST_STARTED]: state => Object.assign({}, state, {
        isFetching: true,
    }),
    [Constant.REQUEST_COMPLETED]: state => Object.assign({}, state, {
        isFetching: false,
    }),
    [Constant.REQUEST_FAIL]: state => Object.assign({}, state, {
        isFetching: false,
    }),
    [Constant.LOGIN_SUCCESSS]: (state, payload) => Object.assign({}, state, {
        isFetching: false,
        loginSuccess: payload,
    }),
});
