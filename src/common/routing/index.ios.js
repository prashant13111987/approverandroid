import { NavigationActions } from 'react-navigation';
import { Alert } from 'react-native';

let alertPresent = false;

// export const navigateToAddRecurring = (dispatch) => {
//     dispatch(NavigationActions.navigate({
//         routeName: 'AddRecurringPaymentContainer',
//     }));
// };

// export const navigateToRecurringDetail = (dispatch) => {
//     dispatch(NavigationActions.navigate({
//         routeName: 'RecurringPaymentDetailContainer',
//     }));
// };

export const showAlert = (message) => {
    Alert.alert('Message', message);
};

export const showAlertWithTitle = (title, message) => {
    if (!alertPresent) {
        alertPresent = true;
        Alert.alert(title, message, [{ text: 'OK', onPress: () => { alertPresent = false; } }], { cancelable: false });
    }
};

export const showAlertWithCallback = (title, message, callback) => {
    Alert.alert(
        title,
        message,
        [
            { text: 'OK', onPress: () => callback() },
        ],
    );
};


export const navigateToINVoiceList = (dispatch) => {
    dispatch(NavigationActions.navigate({
        routeName: 'InVoiceListContainer',
    }));
};


// export const navigateToVenderList = (dispatch) => {
//     dispatch(NavigationActions.navigate({
//         routeName: 'VenderListContainer',
//     }));
// };

export const navigateToVenderList = (dispatch,value) => {
    dispatch(NavigationActions.navigate({
        routeName: 'VenderListContainer',
        params: { value },
    }));
};

export const navigateToShipList = (dispatch) => {
    dispatch(NavigationActions.navigate({
        routeName: 'ShipToListContainer',
    }));
};



export const navigateToAddItem = (dispatch) => {
    dispatch(NavigationActions.navigate({
        routeName: 'AddItemContainer',
    }));
};

export const navigateFromShipList = (dispatch) => {
    dispatch(NavigationActions.navigate({
        routeName: 'ShipFromListContainer',
    }));
};

export const navigateToItemList = (dispatch) => {
    dispatch(NavigationActions.navigate({
        routeName: 'ItemNameListContainer',
    }));
};

export const navigateToEditItem = (dispatch) => {
    dispatch(NavigationActions.navigate({
        routeName: 'EditItemContainer',
    }));
};


export const navigateToDistributionList = (dispatch) => {
    dispatch(NavigationActions.navigate({
        routeName: 'DistributionListContainer',
    }));
};





// export const navigateToVenderList = (dispatch, value) => {
//     const navActions = NavigationActions.reset({
//         index: 0,
//         actions: [
//             NavigationActions.navigate({ routeName: 'AddStudentScreen', params: { value } }),
//         ],
//     });
//     dispatch(navActions);
// };



export const navigateToSendForApproval = (dispatch) => {
    dispatch(NavigationActions.navigate({
        routeName: 'SendForApprovalContainer',
    }));
};

export const navigateToSuccessApproval = (dispatch) => {
    dispatch(NavigationActions.navigate({
        routeName: 'SuccessScreenContainer',
    }));
};

// export const navigateToDashboardScreen = (dispatch) => {
//     dispatch(NavigationActions.navigate({
//         routeName: 'DashboardContainer',
//     }));
// };

export const navigateToDashboardScreen = (dispatch) => {
    const navActions = NavigationActions.reset({
        index: 0,
        actions: [
            NavigationActions.navigate({ routeName: 'DashboardContainer' }),
        ],
    });
    dispatch(navActions);
};
export const navigateToBack = (dispatch) => {
    dispatch(NavigationActions.back());
};
export const navigateToValidateInvoiceScreen = (dispatch) => {
    dispatch(NavigationActions.navigate({
        routeName: 'ValidateInvoiceContainer',
    }));
};

export const navigateToInVoiceDashboard = (dispatch) => {
    // dispatch(NavigationActions.navigate({
    //     routeName: 'Drawer',
    // }));

    const navActions = NavigationActions.reset({
        index: 0,
        actions: [
            NavigationActions.navigate({ routeName: 'Drawer' }),
        ],
    });
    dispatch(navActions);
};