
import { createSelector } from 'reselect';
import _ from 'lodash';

const receiveBillsObject = state => state.dashboard.receivedAllInvoiceList;
const receiveList = state => state.dashboard.receivedInvoiceList;
const legendObject = state => state.dashboard.legendObject;
const billDetailsObject = state => state.dashboard.billDetails;

// prepare faq list for section list
export const getlegendObject = createSelector(
    [receiveBillsObject,receiveList,legendObject],
    (receiveBillsObjectList,receiveListObjects,legendObject) => {

        const rejectedStatusList = [];
        const approvalStatusList = [];
        const onHoldStatusList = [];
        const createdStatusList = [];
        const pendingStatusList = [];
        const receiveStatusList = [];

        _.each(receiveBillsObjectList, (reciveItem) => {
           // console.log("Recived Item getlegendObject Status::::::::::::",reciveItem.status)
            let reciveBillObject = {};
            if (reciveItem.status === 0) {
                reciveBillObject = {
                    status: reciveItem.status,
                    payment_status:reciveItem.payment_status,
                    partyname:reciveItem.vendor_id,
                    id:reciveItem.id,
                    invoice_date:reciveItem.invoice_date,
                    total: reciveItem.total,
                    invoice_no:reciveItem.invoice_no,
                    created_at:reciveItem.created_at,
                    buttonText:'VIEW',
                    statusText:"Rejected",
                    creator:reciveItem.creator,
                    total:reciveItem.total
                };
                rejectedStatusList.push(reciveBillObject);
            }
            if (reciveItem.status === 1) {
                reciveBillObject = {
                    status: reciveItem.status,
                    id:reciveItem.id,
                    payment_status:reciveItem.payment_status,
                    invoice_date:reciveItem.invoice_date,
                    partyname:reciveItem.vendor_id,
                    total: reciveItem.total,
                    buttonText:'VIEW',
                    invoice_no:reciveItem.invoice_no,
                    created_at:reciveItem.created_at,
                    statusText:"Approved",
                    creator:reciveItem.creator,
                    total:reciveItem.total
                };
                approvalStatusList.push(reciveBillObject);
            }
            if (reciveItem.status === 2) {
                reciveBillObject = {
                    status: reciveItem.status,
                    id:reciveItem.id,
                    payment_status:reciveItem.payment_status,
                    invoice_date:reciveItem.invoice_date,
                    partyname:reciveItem.vendor_id,
                    total: reciveItem.total,
                    buttonText:'VIEW',
                    invoice_no:reciveItem.invoice_no,
                    created_at:reciveItem.created_at,
                    statusText:"On Hold",
                    creator:reciveItem.creator,
                    total:reciveItem.total
                };
                onHoldStatusList.push(reciveBillObject);
            }
            if (reciveItem.status === 3) {
                reciveBillObject = {
                    status: reciveItem.status,
                    id:reciveItem.id,
                    payment_status:reciveItem.payment_status,
                    invoice_date:reciveItem.invoice_date,
                    partyname:reciveItem.vendor_id,
                    total: reciveItem.total,
                    buttonText:'VIEW',
                    invoice_no:reciveItem.invoice_no,
                    created_at:reciveItem.created_at,
                    statusText:"Created",
                    creator:reciveItem.creator,
                    total:reciveItem.total
                };
                createdStatusList.push(reciveBillObject);
            }
            if (reciveItem.status === 4) {
                reciveBillObject = {
                    status: reciveItem.status,
                    id:reciveItem.id,
                    payment_status:reciveItem.payment_status,
                    invoice_date:reciveItem.invoice_date,
                    partyname:reciveItem.vendor_id,
                    total: reciveItem.total,
                    buttonText:'VIEW',
                    invoice_no:reciveItem.invoice_no,
                    created_at:reciveItem.created_at,
                    statusText:"Pending",
                    creator:reciveItem.creator,
                    total:reciveItem.total
                };
                pendingStatusList.push(reciveBillObject);
            }
        });

        _.each(receiveListObjects.email_invoices, (reciveItem) => {
            receiveStatusList.push(reciveItem)
        });
        _.each(receiveListObjects.setlmint_invoices, (reciveItem) => {
            receiveStatusList.push(reciveItem)
        });
        _.each(receiveListObjects.uploaded_invoices, (reciveItem) => {
            receiveStatusList.push(reciveItem)
        });

        let legendDataObjectArray = [];
        if(legendObject.isApprove  === false)
        {
            _.each(approvalStatusList, (reciveItem) => {
                legendDataObjectArray.push(reciveItem)
            });
        }
        if(legendObject.isReceived  === false)
        {
            _.each(receiveStatusList, (reciveItem) => {
                legendDataObjectArray.push(reciveItem)
            });
        }
        if(legendObject.isScanned  === false)
        {
            _.each(createdStatusList, (reciveItem) => {
                legendDataObjectArray.push(reciveItem)
            });
        }
        if(legendObject.isPending  === false)
        {
            _.each(pendingStatusList, (reciveItem) => {
                legendDataObjectArray.push(reciveItem)
            });

        }
        if(legendObject.isRejected  === false)
        {
            _.each(rejectedStatusList, (reciveItem) => {
                legendDataObjectArray.push(reciveItem)
            });

        }
        if(legendObject.isOnhold  === false)
        {
            _.each(onHoldStatusList, (reciveItem) => {
                legendDataObjectArray.push(reciveItem)
            });
        }
        return legendDataObjectArray;
    },
);



// prepare faq list for section list
export const getStatusForLagent = createSelector(
    [receiveBillsObject,receiveList,legendObject],
    (receiveBillsObjectList,receiveListObjects,legendObject) => {

        const rejectedStatusList = [];
        const approvalStatusList = [];
        const onHoldStatusList = [];
        const createdStatusList = [];
        const pendingStatusList = [];
        const receiveStatusList = [];
      //  console.log("receiveBillsObjectList Object ::::::::::::",receiveBillsObjectList)
        _.each(receiveBillsObjectList, (reciveItem) => {

            console.log("Recived Item Status getStatusForLagent::::::::::::",reciveItem.status,reciveItem)

            let reciveBillObject = {};
            if (reciveItem.status === 0) {
                reciveBillObject = {
                    status: reciveItem.status,
                    id:reciveItem.id,
                    payment_status:reciveItem.payment_status,
                    partyname:reciveItem.vendor_id,
                    total: reciveItem.total,
                    invoice_date:reciveItem.invoice_date,
                    buttonText:'VIEW',
                    invoice_no:reciveItem.invoice_no,
                    created_at:reciveItem.created_at,
                    statusText:"Rejected",
                    creator:reciveItem.creator,
                    total:reciveItem.total
                };
                rejectedStatusList.push(reciveBillObject);
            }
            if (reciveItem.status === 1) {
                reciveBillObject = {
                    status: reciveItem.status,
                    id:reciveItem.id,
                    payment_status:reciveItem.payment_status,
                    invoice_date:reciveItem.invoice_date,
                    partyname:reciveItem.vendor_id,
                    total: reciveItem.total,
                    buttonText:'VIEW',
                    invoice_no:reciveItem.invoice_no,
                    created_at:reciveItem.created_at,
                    statusText:"Approved",
                    creator:reciveItem.creator,
                    total:reciveItem.total
                };
                approvalStatusList.push(reciveBillObject);
            }
            if (reciveItem.status === 2) {
                reciveBillObject = {
                    status: reciveItem.status,
                    id:reciveItem.id,
                    payment_status:reciveItem.payment_status,
                    invoice_date:reciveItem.invoice_date,
                    partyname:reciveItem.vendor_id,
                    total: reciveItem.total,
                    buttonText:'VIEW',
                    invoice_no:reciveItem.invoice_no,
                    created_at:reciveItem.created_at,
                    statusText:"On Hold",
                    creator:reciveItem.creator,
                    total:reciveItem.total
                };
                onHoldStatusList.push(reciveBillObject);
            }
            if (reciveItem.status === 3) {

              //  console.log("Billing Number Invoice List ::::::::::::::::;",reciveItem.bill_no)
                reciveBillObject = {
                    status: reciveItem.status,
                    id:reciveItem.id,
                    payment_status:reciveItem.payment_status,
                    invoice_date:reciveItem.invoice_date,
                    partyname:reciveItem.vendor_id,
                    total: reciveItem.total,
                    buttonText:'VIEW',
                    invoice_no:reciveItem.bill_no,
                    created_at:reciveItem.created_at,
                    statusText:"Created",
                    creator:reciveItem.creator,
                    total:reciveItem.total
                };
                createdStatusList.push(reciveBillObject);
            }
            if (reciveItem.status === 4) {
                reciveBillObject = {
                    status: reciveItem.status,
                    id:reciveItem.id,
                    payment_status:reciveItem.payment_status,
                    invoice_date:reciveItem.invoice_date,
                    partyname:reciveItem.vendor_id,
                    total: reciveItem.total,
                    buttonText:'VIEW',
                    invoice_no:reciveItem.invoice_no,
                    created_at:reciveItem.created_at,
                    statusText:"Pending",
                    creator:reciveItem.creator,
                    total:reciveItem.total
                };
                pendingStatusList.push(reciveBillObject);
            }
        });

        _.each(receiveListObjects.email_invoices, (reciveItem) => {
            receiveStatusList.push(reciveItem)
        });
        _.each(receiveListObjects.setlmint_invoices, (reciveItem) => {
            receiveStatusList.push(reciveItem)
        });
        _.each(receiveListObjects.uploaded_invoices, (reciveItem) => {
            receiveStatusList.push(reciveItem)
        });

        const legentStatus ={
            isApprove:true,
            isReceived:true,
            isScanned:true,
            isPending:true,
            isRejected:true,
            isOnhold:true,

        }
        if(legendObject.isApprove  === false)
        {
            legentStatus.isApprove = false
        }
        if(legendObject.isReceived  === false)
        {
            legentStatus.isReceived = false
        }
        if(legendObject.isScanned  === false)
        {
            legentStatus.isScanned = false
        }
        if(legendObject.isPending  === false)
        {
            legentStatus.isPending = false

        }
        if(legendObject.isRejected  === false)
        {
            legentStatus.isRejected = false
        }
        if(legendObject.isOnhold  === false)
        {
            legentStatus.isOnhold = false
        }
       // console.log("Legent Object ::::::::::::::::::", legentStatus)
        return legentStatus;
    },
);


// prepare faq list for section list
export const getBillDetails = createSelector(
    [billDetailsObject],
    (billDetail) => {

       // console.log("Bill Details :::::::::::::::::::::::::::::::::",JSON.stringify(billDetail))
        var cgstTotal = 0.0
        var igstTotal = 0.0
        var sgstTotal = 0.0
        var gstTotal  = 0.0
        var subTotal  = 0.0
        var amountTotal = 0.0
        let gstObject = {};
        let amountObject = {};
        let billObject = {};
        let gstInfo = {};
        _.each(billDetail.bill_items, (billItem) => {
            cgstTotal = parseFloat(cgstTotal) + parseFloat(billItem.cgst)
            igstTotal = parseFloat(igstTotal) + parseFloat(billItem.igst)
            sgstTotal = parseFloat(sgstTotal) + parseFloat(billItem.sgst)
            subTotal =  parseFloat(subTotal) + (parseFloat(billItem.price)*parseFloat(billItem.quantity))
        });

        // Total GST Calculation
        gstTotal =  parseFloat(cgstTotal) + parseFloat(igstTotal) + parseFloat(sgstTotal)
        gstObject  = {
            cgst: cgstTotal,
            igst: igstTotal,
            sgst: sgstTotal,
            gst: gstTotal
        };

        // Total Amount Calculation
        amountTotal = parseFloat(subTotal) + parseFloat(gstTotal) + parseFloat(billDetail.packing_charges)
            + parseFloat(billDetail.delivery_charges) + parseFloat(billDetail.other_charges)
        amountObject = {
            subTotal:subTotal,
            gstTotal:gstTotal,
            packagingCharges:billDetail.packing_charges,
            deliverycharges:billDetail.delivery_charges,
            other_charges:billDetail.other_charges,
            amountTotal:amountTotal
        }

        gstInfo = {
            shipFromName:billDetail.shipping_branch_name,
            ShipToName:billDetail.vendor_branch_name,
            buyer_gstin:billDetail.buyer_gstin,
            vendor_gstin:billDetail.vendor_gstin,
            eway_bill_no:billDetail.eway_bill_no,
        }

        billObject = {
            gst: gstObject,
            gstInfo:gstInfo,
            imageUrl:billDetail.image,
            vendor_id:billDetail.vendor_id,
            is_original_received:billDetail.is_original_received,
            rush:billDetail.rush,
            notes:billDetail.notes,
            billId:billDetail.id,
            items:billDetail.bill_items,
            amount:amountObject,
            bill_no:billDetail.bill_no,
            po_no:billDetail.po_no,
            due_date:billDetail.due_date,
            invoiceNumber:billDetail.ref_invoice_no,
            vendor_name:billDetail.vendor_name,
            shipInfo:billDetail.transp_ref_no,
            invoiceDate:billDetail.invoice_date,
            billDate:billDetail.created_at
        }
       // console.log("Final Bill Object :::::::::::::::::::::::::::::::::",JSON.stringify(billObject))
        return billObject;
    },
);

//  Approver Selector
export const getApproverlegendObject = createSelector(
    [receiveBillsObject,receiveList,legendObject],
    (receiveBillsObjectList,receiveListObjects,legendObject) => {

        const rejectedStatusList = [];
        const approvalStatusList = [];
        const onHoldStatusList = [];
        const createdStatusList = [];
        const pendingStatusList = [];
        const receiveStatusList = [];

        _.each(receiveBillsObjectList, (reciveItem) => {
           // console.log("Recived Item getlegendObject Status::::::::::::",reciveItem.status)
            let reciveBillObject = {};
            if (reciveItem.status === 0) {
                reciveBillObject = {
                    status: reciveItem.status,
                    payment_status:reciveItem.payment_status,
                    partyname:reciveItem.vendor_id,
                    id:reciveItem.id,
                    invoice_date:reciveItem.invoice_date,
                    total: reciveItem.total,
                    invoice_no:reciveItem.invoice_no,
                    created_at:reciveItem.created_at,
                    buttonText:'VIEW',
                    statusText:"Invoices",
                    creator:reciveItem.creator,
                    total:reciveItem.total
                };
                rejectedStatusList.push(reciveBillObject);
            }
            if (reciveItem.status === 1) {
                reciveBillObject = {
                    status: reciveItem.status,
                    id:reciveItem.id,
                    payment_status:reciveItem.payment_status,
                    invoice_date:reciveItem.invoice_date,
                    partyname:reciveItem.vendor_id,
                    total: reciveItem.total,
                    buttonText:'VIEW',
                    invoice_no:reciveItem.invoice_no,
                    created_at:reciveItem.created_at,
                    statusText:"Bills",
                    creator:reciveItem.creator,
                    total:reciveItem.total
                };
                approvalStatusList.push(reciveBillObject);
            }
            if (reciveItem.status === 2) {
                reciveBillObject = {
                    status: reciveItem.status,
                    id:reciveItem.id,
                    payment_status:reciveItem.payment_status,
                    invoice_date:reciveItem.invoice_date,
                    partyname:reciveItem.vendor_id,
                    total: reciveItem.total,
                    buttonText:'VIEW',
                    invoice_no:reciveItem.invoice_no,
                    created_at:reciveItem.created_at,
                    statusText:"Payment",
                    creator:reciveItem.creator,
                    total:reciveItem.total
                };
                onHoldStatusList.push(reciveBillObject);
            }
            if (reciveItem.status === 3) {
                reciveBillObject = {
                    status: reciveItem.status,
                    id:reciveItem.id,
                    payment_status:reciveItem.payment_status,
                    invoice_date:reciveItem.invoice_date,
                    partyname:reciveItem.vendor_id,
                    total: reciveItem.total,
                    buttonText:'VIEW',
                    invoice_no:reciveItem.invoice_no,
                    created_at:reciveItem.created_at,
                    statusText:"PO",
                    creator:reciveItem.creator,
                    total:reciveItem.total
                };
                createdStatusList.push(reciveBillObject);
            }
            if (reciveItem.status === 4) {
                reciveBillObject = {
                    status: reciveItem.status,
                    id:reciveItem.id,
                    payment_status:reciveItem.payment_status,
                    invoice_date:reciveItem.invoice_date,
                    partyname:reciveItem.vendor_id,
                    total: reciveItem.total,
                    buttonText:'VIEW',
                    invoice_no:reciveItem.invoice_no,
                    created_at:reciveItem.created_at,
                    statusText:"Master Data",
                    creator:reciveItem.creator,
                    total:reciveItem.total
                };
                pendingStatusList.push(reciveBillObject);
            }
        });

        _.each(receiveListObjects.email_invoices, (reciveItem) => {
            receiveStatusList.push(reciveItem)
        });
        _.each(receiveListObjects.setlmint_invoices, (reciveItem) => {
            receiveStatusList.push(reciveItem)
        });
        _.each(receiveListObjects.uploaded_invoices, (reciveItem) => {
            receiveStatusList.push(reciveItem)
        });

        let legendDataObjectArray = [];
        if(legendObject.isApprove  === false)
        {
            _.each(approvalStatusList, (reciveItem) => {
                legendDataObjectArray.push(reciveItem)
            });
        }
        if(legendObject.isReceived  === false)
        {
            _.each(receiveStatusList, (reciveItem) => {
                legendDataObjectArray.push(reciveItem)
            });
        }
        if(legendObject.isScanned  === false)
        {
            _.each(createdStatusList, (reciveItem) => {
                legendDataObjectArray.push(reciveItem)
            });
        }
        if(legendObject.isPending  === false)
        {
            _.each(pendingStatusList, (reciveItem) => {
                legendDataObjectArray.push(reciveItem)
            });

        }
        if(legendObject.isRejected  === false)
        {
            _.each(rejectedStatusList, (reciveItem) => {
                legendDataObjectArray.push(reciveItem)
            });

        }
        if(legendObject.isOnhold  === false)
        {
            _.each(onHoldStatusList, (reciveItem) => {
                legendDataObjectArray.push(reciveItem)
            });
        }
        return legendDataObjectArray;
    },
);



// prepare faq list for section list
export const getApproverStatusForLagent = createSelector(
    [receiveBillsObject,receiveList,legendObject],
    (receiveBillsObjectList,receiveListObjects,legendObject) => {

        const rejectedStatusList = [];
        const approvalStatusList = [];
        const onHoldStatusList = [];
        const createdStatusList = [];
        const pendingStatusList = [];
        const receiveStatusList = [];
      //  console.log("receiveBillsObjectList Object ::::::::::::",receiveBillsObjectList)
        _.each(receiveBillsObjectList, (reciveItem) => {

            console.log("Recived Item Status getStatusForLagent::::::::::::",reciveItem.status,reciveItem)

            let reciveBillObject = {};
            if (reciveItem.status === 0) {
                reciveBillObject = {
                    status: reciveItem.status,
                    id:reciveItem.id,
                    payment_status:reciveItem.payment_status,
                    partyname:reciveItem.vendor_id,
                    total: reciveItem.total,
                    invoice_date:reciveItem.invoice_date,
                    buttonText:'VIEW',
                    invoice_no:reciveItem.invoice_no,
                    created_at:reciveItem.created_at,
                    statusText:"Invoices",
                    creator:reciveItem.creator,
                    total:reciveItem.total
                };
                rejectedStatusList.push(reciveBillObject);
            }
            if (reciveItem.status === 1) {
                reciveBillObject = {
                    status: reciveItem.status,
                    id:reciveItem.id,
                    payment_status:reciveItem.payment_status,
                    invoice_date:reciveItem.invoice_date,
                    partyname:reciveItem.vendor_id,
                    total: reciveItem.total,
                    buttonText:'VIEW',
                    invoice_no:reciveItem.invoice_no,
                    created_at:reciveItem.created_at,
                    statusText:"Bills",
                    creator:reciveItem.creator,
                    total:reciveItem.total
                };
                approvalStatusList.push(reciveBillObject);
            }
            if (reciveItem.status === 2) {
                reciveBillObject = {
                    status: reciveItem.status,
                    id:reciveItem.id,
                    payment_status:reciveItem.payment_status,
                    invoice_date:reciveItem.invoice_date,
                    partyname:reciveItem.vendor_id,
                    total: reciveItem.total,
                    buttonText:'VIEW',
                    invoice_no:reciveItem.invoice_no,
                    created_at:reciveItem.created_at,
                    statusText:"Payment",
                    creator:reciveItem.creator,
                    total:reciveItem.total
                };
                onHoldStatusList.push(reciveBillObject);
            }
            if (reciveItem.status === 3) {

              //  console.log("Billing Number Invoice List ::::::::::::::::;",reciveItem.bill_no)
                reciveBillObject = {
                    status: reciveItem.status,
                    id:reciveItem.id,
                    payment_status:reciveItem.payment_status,
                    invoice_date:reciveItem.invoice_date,
                    partyname:reciveItem.vendor_id,
                    total: reciveItem.total,
                    buttonText:'VIEW',
                    invoice_no:reciveItem.bill_no,
                    created_at:reciveItem.created_at,
                    statusText:"PO",
                    creator:reciveItem.creator,
                    total:reciveItem.total
                };
                createdStatusList.push(reciveBillObject);
            }
            if (reciveItem.status === 4) {
                reciveBillObject = {
                    status: reciveItem.status,
                    id:reciveItem.id,
                    payment_status:reciveItem.payment_status,
                    invoice_date:reciveItem.invoice_date,
                    partyname:reciveItem.vendor_id,
                    total: reciveItem.total,
                    buttonText:'VIEW',
                    invoice_no:reciveItem.invoice_no,
                    created_at:reciveItem.created_at,
                    statusText:"Master Data",
                    creator:reciveItem.creator,
                    total:reciveItem.total
                };
                pendingStatusList.push(reciveBillObject);
            }
        });

        _.each(receiveListObjects.email_invoices, (reciveItem) => {
            receiveStatusList.push(reciveItem)
        });
        _.each(receiveListObjects.setlmint_invoices, (reciveItem) => {
            receiveStatusList.push(reciveItem)
        });
        _.each(receiveListObjects.uploaded_invoices, (reciveItem) => {
            receiveStatusList.push(reciveItem)
        });

        const legentStatus ={
            isApprove:true,
            isReceived:true,
            isScanned:true,
            isPending:true,
            isRejected:true,
            isOnhold:true,

        }
        if(legendObject.isApprove  === false)
        {
            legentStatus.isApprove = false
        }
        if(legendObject.isReceived  === false)
        {
            legentStatus.isReceived = false
        }
        if(legendObject.isScanned  === false)
        {
            legentStatus.isScanned = false
        }
        if(legendObject.isPending  === false)
        {
            legentStatus.isPending = false

        }
        if(legendObject.isRejected  === false)
        {
            legentStatus.isRejected = false
        }
        if(legendObject.isOnhold  === false)
        {
            legentStatus.isOnhold = false
        }
       // console.log("Legent Object ::::::::::::::::::", legentStatus)
        return legentStatus;
    },
);


// prepare faq list for section list
export const getApproverBillDetails = createSelector(
    [billDetailsObject],
    (billDetail) => {

       // console.log("Bill Details :::::::::::::::::::::::::::::::::",JSON.stringify(billDetail))
        var cgstTotal = 0.0
        var igstTotal = 0.0
        var sgstTotal = 0.0
        var gstTotal  = 0.0
        var subTotal  = 0.0
        var amountTotal = 0.0
        let gstObject = {};
        let amountObject = {};
        let billObject = {};
        let gstInfo = {};
        _.each(billDetail.bill_items, (billItem) => {
            cgstTotal = parseFloat(cgstTotal) + parseFloat(billItem.cgst)
            igstTotal = parseFloat(igstTotal) + parseFloat(billItem.igst)
            sgstTotal = parseFloat(sgstTotal) + parseFloat(billItem.sgst)
            subTotal =  parseFloat(subTotal) + (parseFloat(billItem.price)*parseFloat(billItem.quantity))
        });

        // Total GST Calculation
        gstTotal =  parseFloat(cgstTotal) + parseFloat(igstTotal) + parseFloat(sgstTotal)
        gstObject  = {
            cgst: cgstTotal,
            igst: igstTotal,
            sgst: sgstTotal,
            gst: gstTotal
        };

        // Total Amount Calculation
        amountTotal = parseFloat(subTotal) + parseFloat(gstTotal) + parseFloat(billDetail.packing_charges)
            + parseFloat(billDetail.delivery_charges) + parseFloat(billDetail.other_charges)
        amountObject = {
            subTotal:subTotal,
            gstTotal:gstTotal,
            packagingCharges:billDetail.packing_charges,
            deliverycharges:billDetail.delivery_charges,
            other_charges:billDetail.other_charges,
            amountTotal:amountTotal
        }

        gstInfo = {
            shipFromName:billDetail.shipping_branch_name,
            ShipToName:billDetail.vendor_branch_name,
            buyer_gstin:billDetail.buyer_gstin,
            vendor_gstin:billDetail.vendor_gstin,
            eway_bill_no:billDetail.eway_bill_no,
        }

        billObject = {
            gst: gstObject,
            gstInfo:gstInfo,
            imageUrl:billDetail.image,
            vendor_id:billDetail.vendor_id,
            is_original_received:billDetail.is_original_received,
            rush:billDetail.rush,
            notes:billDetail.notes,
            billId:billDetail.id,
            items:billDetail.bill_items,
            amount:amountObject,
            bill_no:billDetail.bill_no,
            po_no:billDetail.po_no,
            due_date:billDetail.due_date,
            invoiceNumber:billDetail.ref_invoice_no,
            vendor_name:billDetail.vendor_name,
            shipInfo:billDetail.transp_ref_no,
            invoiceDate:billDetail.invoice_date,
            billDate:billDetail.created_at
        }
       // console.log("Final Bill Object :::::::::::::::::::::::::::::::::",JSON.stringify(billObject))
        return billObject;
    },
);


// // prepare pending transfer list for section list
// export const getStatusForLagent = createSelector(
//     [getlegendObject],
//
//     console.log("Legent Object ::::::::::::::::::", getlegendObject)
//     // (result) => {
//     //     console.log("Legent Object ::::::::::::::::::", result)
//     // }
// );