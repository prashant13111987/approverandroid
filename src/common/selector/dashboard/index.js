import { createSelector } from 'reselect';
import _ from 'lodash';

const receiveBillsObject = state => state.dashboard.receivedAllInvoiceList;
const receiveList = state => state.dashboard.receivedInvoiceList;
const venderList = state => state.dashboard.venderList;

const receiveBillsObjectApprover = state => state.approverDashboard.receivedAllInvoiceList;
const receiveListApprover = state => state.approverDashboard.receivedInvoiceList;
const venderListApprover = state => state.approverDashboard.venderList;

const approverInvoices = state => state.approverDashboard.receivedInvoiceList;
const approverBills = state => state.approverDashboard.receivedAllBillsList;
const approverPayments = state => state.approverDashboard.receivedBillPayment;
const approverMasterData = state => state.approverDashboard.receivedTmpMasterData;
const approverPurchaseOrder = state => state.approverDashboard.receivedPurchaseOrder;



// prepare faq list for section list
export const getReceiveObject = createSelector(
    [receiveBillsObject, receiveList, venderList],
    (receiveBillsObjectList, receiveListObjects) => {

        // console.log("Receive Object :::::::::::::",receiveBillsObjectList)
        // console.log("Receive Object  In  Selector List :::::::::::::",receiveListObjects)
        const rejectedStatusList = [];
        const approvalStatusList = [];
        const onHoldStatusList = [];
        const createdStatusList = [];
        const pendingStatusList = [];
        const receiveStatusList = receiveListObjects;

        _.each(receiveBillsObjectList, (reciveItem) => {
            let reciveBillObject = {};
            if (reciveItem.status === 0) {
                reciveBillObject = {
                    status: reciveItem.status,
                    total: reciveItem.total,
                    payment_status: reciveItem.payment_status,
                    invoice_date: reciveItem.invoice_date,
                    partyname: reciveItem.vendor_id,
                    id: reciveItem.id,
                    invoice_no: reciveItem.invoice_no,
                    created_at: reciveItem.created_at,
                    statusText: "Rejected",
                    creator: reciveItem.creator,
                    total: reciveItem.total
                };
                rejectedStatusList.push(reciveBillObject);
            }
            if (reciveItem.status === 1) {
                reciveBillObject = {
                    status: reciveItem.status,
                    id: reciveItem.id,
                    payment_status: reciveItem.payment_status,
                    invoice_date: reciveItem.invoice_date,
                    partyname: reciveItem.vendor_id,
                    total: reciveItem.total,
                    invoice_no: reciveItem.invoice_no,
                    created_at: reciveItem.created_at,
                    statusText: "Approved",
                    creator: reciveItem.creator,
                    total: reciveItem.total
                };
                approvalStatusList.push(reciveBillObject);
            }
            if (reciveItem.status === 2) {
                reciveBillObject = {
                    status: reciveItem.status,
                    id: reciveItem.id,
                    payment_status: reciveItem.payment_status,
                    invoice_date: reciveItem.invoice_date,
                    partyname: reciveItem.vendor_id,
                    total: reciveItem.total,
                    invoice_no: reciveItem.invoice_no,
                    created_at: reciveItem.created_at,
                    statusText: "On Hold",
                    creator: reciveItem.creator,
                    total: reciveItem.total
                };
                onHoldStatusList.push(reciveBillObject);
            }
            if (reciveItem.status === 3) {
                reciveBillObject = {
                    status: reciveItem.status,
                    id: reciveItem.id,
                    payment_status: reciveItem.payment_status,
                    invoice_date: reciveItem.invoice_date,
                    partyname: reciveItem.vendor_id,
                    total: reciveItem.total,
                    invoice_no: reciveItem.invoice_no,
                    created_at: reciveItem.created_at,
                    statusText: "Created",
                    creator: reciveItem.creator,
                    total: reciveItem.total
                };
                createdStatusList.push(reciveBillObject);
            }
            if (reciveItem.status === 4) {
                reciveBillObject = {
                    status: reciveItem.status,
                    id: reciveItem.id,
                    payment_status: reciveItem.payment_status,
                    invoice_date: reciveItem.invoice_date,
                    partyname: reciveItem.vendor_id,
                    total: reciveItem.total,
                    invoice_no: reciveItem.invoice_no,
                    created_at: reciveItem.created_at,
                    statusText: "Pending",
                    creator: reciveItem.creator,
                    total: reciveItem.total
                };
                pendingStatusList.push(reciveBillObject);
            }
        });

        _.each(receiveStatusList, (reciveItem) => {
            // console.log("Source Name ::::::::::::::::::",reciveItem.source_name)
        });
        // console.log("Receive Object Status l:::::::::::::",receiveStatusList)

        let ststusBillObject = {};
        ststusBillObject = {
            rejected: rejectedStatusList,
            approval: approvalStatusList,
            onHold: onHoldStatusList,
            create: createdStatusList,
            pending: pendingStatusList,
            received: receiveStatusList
        };
        return ststusBillObject;
    },
);


// prepare faq list for section list
export const getTotalFromObject = createSelector(
    [receiveBillsObject, receiveList],
    (receiveBillsObjectList, receiveListObjects) => {

        // console.log("Receive Object :::::::::::::",receiveBillsObjectList)
        // console.log("Receive Object  In  Selector List :::::::::::::",receiveListObjects)
        const rejectedStatusList = [];
        const approvalStatusList = [];
        const onHoldStatusList = [];
        const createdStatusList = [];
        const pendingStatusList = [];
        const receiveStatusList = receiveListObjects;

        _.each(receiveBillsObjectList, (reciveItem) => {
            let reciveBillObject = {};
            if (reciveItem.status === 0) {
                reciveBillObject = {
                    status: reciveItem.status,
                    id: reciveItem.id,
                    payment_status: reciveItem.payment_status,
                    invoice_date: reciveItem.invoice_date,
                    buttonText: 'VIEW',
                    partyname: reciveItem.vendor_id,
                    total: reciveItem.total,
                    invoice_no: reciveItem.invoice_no,
                    created_at: reciveItem.created_at,
                    statusText: "Rejected",
                    creator: reciveItem.creator,
                    total: reciveItem.total
                };
                rejectedStatusList.push(reciveBillObject);
            }
            if (reciveItem.status === 1) {
                reciveBillObject = {
                    status: reciveItem.status,
                    id: reciveItem.id,
                    payment_status: reciveItem.payment_status,
                    invoice_date: reciveItem.invoice_date,
                    partyname: reciveItem.vendor_id,
                    buttonText: 'VIEW',
                    total: reciveItem.total,
                    invoice_no: reciveItem.invoice_no,
                    created_at: reciveItem.created_at,
                    statusText: "Approved",
                    creator: reciveItem.creator,
                    total: reciveItem.total
                };
                approvalStatusList.push(reciveBillObject);
            }
            if (reciveItem.status === 2) {
                reciveBillObject = {
                    status: reciveItem.status,
                    id: reciveItem.id,
                    payment_status: reciveItem.payment_status,
                    invoice_date: reciveItem.invoice_date,
                    total: reciveItem.total,
                    partyname: reciveItem.vendor_id,
                    buttonText: 'VIEW',
                    invoice_no: reciveItem.invoice_no,
                    created_at: reciveItem.created_at,
                    statusText: "On Hold",
                    creator: reciveItem.creator,
                    total: reciveItem.total
                };
                onHoldStatusList.push(reciveBillObject);
            }
            if (reciveItem.status === 3) {
                // console.log("Billing Number Dashboard ::::::::::::::::;",reciveItem.bill_no)
                reciveBillObject = {
                    status: reciveItem.status,
                    id: reciveItem.id,
                    payment_status: reciveItem.payment_status,
                    invoice_date: reciveItem.invoice_date,
                    total: reciveItem.total,
                    partyname: reciveItem.vendor_id,
                    buttonText: 'VIEW',
                    invoice_no: reciveItem.bill_no,
                    created_at: reciveItem.created_at,
                    statusText: "Created",
                    creator: reciveItem.creator,
                    total: reciveItem.total
                };
                createdStatusList.push(reciveBillObject);
            }
            if (reciveItem.status === 4) {
                reciveBillObject = {
                    status: reciveItem.status,
                    id: reciveItem.id,
                    payment_status: reciveItem.payment_status,
                    invoice_date: reciveItem.invoice_date,
                    total: reciveItem.total,
                    partyname: reciveItem.vendor_id,
                    buttonText: 'VIEW',
                    invoice_no: reciveItem.invoice_no,
                    created_at: reciveItem.created_at,
                    statusText: "Pending",
                    creator: reciveItem.creator,
                    total: reciveItem.total
                };
                pendingStatusList.push(reciveBillObject);
            }
        });


        let createdTotal = 0.0;
        _.each(createdStatusList, (reciveItem) => {
            if (reciveItem.total) {
                createdTotal = parseFloat(createdTotal) + parseFloat(reciveItem.total)
            }

        });

        let rejectedTotal = 0.0;
        _.each(rejectedStatusList, (reciveItem) => {
            if (reciveItem.total) {
                rejectedTotal = parseFloat(rejectedTotal) + parseFloat(reciveItem.total)
            }
        });
        let approvalTotal = 0.0;
        _.each(approvalStatusList, (reciveItem) => {
            if (reciveItem.total) {
                approvalTotal = parseFloat(approvalTotal) + parseFloat(reciveItem.total)
            }
        });

        let onHoldTotal = 0.0;
        _.each(onHoldStatusList, (reciveItem) => {
            if (reciveItem.total) {
                onHoldTotal = parseFloat(onHoldTotal) + parseFloat(reciveItem.total)
            }
        });

        let pendingTotal = 0.0;
        _.each(pendingStatusList, (reciveItem) => {
            if (reciveItem.total) {
                pendingTotal = parseFloat(pendingTotal) + parseFloat(reciveItem.total)
            }
        });
        let receivedTotal = 0.0;
        _.each(receiveStatusList, (reciveItem) => {
            if (reciveItem.total) {
                receivedTotal = parseFloat(receivedTotal) + parseFloat(reciveItem.total)
            }
        });

        let totalInvoiceCount = rejectedStatusList.length + approvalStatusList.length + onHoldStatusList.length + createdStatusList.length +
            pendingStatusList.length + receiveStatusList.length;
        let totalAMount = parseFloat(rejectedTotal) + parseFloat(approvalTotal) + parseFloat(onHoldTotal) + parseFloat(createdTotal) + parseFloat(pendingTotal) + parseFloat(receivedTotal);
        let totalAmoutnInvoiceObject = {};
        totalAmoutnInvoiceObject = {
            rejected: rejectedTotal,
            approval: approvalTotal,
            onHold: onHoldTotal,
            create: createdTotal,
            pending: pendingTotal,
            received: receivedTotal,
            totalAmount: totalAMount,
            totalCount: totalInvoiceCount
        };
        //  console.log("TotalAmoutnInvoiceObject Total ::::::::::::::::::::::::",totalAmoutnInvoiceObject);
        return totalAmoutnInvoiceObject;
    },
);
// prepare faq list for section list
export const getApproverReceiveObject = createSelector(
    [approverInvoices, approverBills, approverPayments, approverMasterData, approverPurchaseOrder],
    (invoices, bills, payments, masterData, po) => {

        // console.log("Receive Object :::::::::::::",receiveBillsObjectList)
        // console.log("Receive Object  In  Selector List :::::::::::::",receiveListObjects)
        // const invoiceStatusList = [];
        // const billStatusList = [];
        // const paymentStatusList = [];
        // const POStatusList = [];
        // const masterDataStatusList = [];
        // const receiveStatusList = receiveListObjects;

        // _.each(receiveBillsObjectList, (reciveItem) => {
        //     let reciveBillObject = {};
        //     if (reciveItem.status === 0) {
        //         reciveBillObject = {
        //             status: reciveItem.status,
        //             total: reciveItem.total,
        //             payment_status: reciveItem.payment_status,
        //             invoice_date: reciveItem.invoice_date,
        //             partyname: reciveItem.vendor_id,
        //             id: reciveItem.id,
        //             invoice_no: reciveItem.invoice_no,
        //             created_at: reciveItem.created_at,
        //             statusText: "Invoices",
        //             creator: reciveItem.creator,
        //             total: reciveItem.total
        //         };
        //         invoiceStatusList.push(reciveBillObject);
        //     }
        //     if (reciveItem.status === 1) {
        //         reciveBillObject = {
        //             status: reciveItem.status,
        //             id: reciveItem.id,
        //             payment_status: reciveItem.payment_status,
        //             invoice_date: reciveItem.invoice_date,
        //             partyname: reciveItem.vendor_id,
        //             total: reciveItem.total,
        //             invoice_no: reciveItem.invoice_no,
        //             created_at: reciveItem.created_at,
        //             statusText: "Bills",
        //             creator: reciveItem.creator,
        //             total: reciveItem.total
        //         };
        //         billStatusList.push(reciveBillObject);
        //     }
        //     if (reciveItem.status === 2) {
        //         reciveBillObject = {
        //             status: reciveItem.status,
        //             id: reciveItem.id,
        //             payment_status: reciveItem.payment_status,
        //             invoice_date: reciveItem.invoice_date,
        //             partyname: reciveItem.vendor_id,
        //             total: reciveItem.total,
        //             invoice_no: reciveItem.invoice_no,
        //             created_at: reciveItem.created_at,
        //             statusText: "Payment",
        //             creator: reciveItem.creator,
        //             total: reciveItem.total
        //         };
        //         paymentStatusList.push(reciveBillObject);
        //     }
        //     if (reciveItem.status === 3) {
        //         reciveBillObject = {
        //             status: reciveItem.status,
        //             id: reciveItem.id,
        //             payment_status: reciveItem.payment_status,
        //             invoice_date: reciveItem.invoice_date,
        //             partyname: reciveItem.vendor_id,
        //             total: reciveItem.total,
        //             invoice_no: reciveItem.invoice_no,
        //             created_at: reciveItem.created_at,
        //             statusText: "PO",
        //             creator: reciveItem.creator,
        //             total: reciveItem.total
        //         };
        //         POStatusList.push(reciveBillObject);
        //     }
        //     if (reciveItem.status === 4) {
        //         reciveBillObject = {
        //             status: reciveItem.status,
        //             id: reciveItem.id,
        //             payment_status: reciveItem.payment_status,
        //             invoice_date: reciveItem.invoice_date,
        //             partyname: reciveItem.vendor_id,
        //             total: reciveItem.total,
        //             invoice_no: reciveItem.invoice_no,
        //             created_at: reciveItem.created_at,
        //             statusText: "Master Data",
        //             creator: reciveItem.creator,
        //             total: reciveItem.total
        //         };
        //         masterDataStatusList.push(reciveBillObject);
        //     }
        // });

        // _.each(receiveStatusList, (reciveItem) => {
        //     // console.log("Source Name ::::::::::::::::::",reciveItem.source_name)
        // });
        // // console.log("Receive Object Status l:::::::::::::",receiveStatusList)

        let ststusBillObject = {};
        ststusBillObject = {
            invoices,
            bills,
            payments,
            PO: po,
            masterData
        };
        return ststusBillObject;
    },
);


// prepare faq list for section list
export const getApproverTotalFromObject = createSelector(
    [approverInvoices, approverBills, approverPayments, approverMasterData, approverPurchaseOrder],
    (invoices, bills, payments, masterData, po) => {


        let invoicesTotal = 0.0;
        _.each(invoices, (inovice) => {
            if (inovice.total) {
                invoicesTotal = parseFloat(invoicesTotal) + parseFloat(inovice.total)
            }

        });

        let billsTotal = 0.0;
        _.each(bills, (bill) => {
            if (bill.total) {
                billsTotal = parseFloat(billsTotal) + parseFloat(bill.total)
            }
        });
        let paymentsTotal = 0.0;
        _.each(payments, (pament) => {
            if (pament.total) {
                paymentsTotal = parseFloat(paymentsTotal) + parseFloat(pament.total)
            }
        });

        let poTotal = 0.0;
        _.each(po, (p) => {
            if (p.total) {
                poTotal = parseFloat(poTotal) + parseFloat(p.total)
            }
        });

        let masterDataTotal = 0.0;
        _.each(masterData, (master) => {
            if (master.total) {
                masterDataTotal = parseFloat(masterDataTotal) + parseFloat(master.total)
            }
        });


        let totalCount = invoices.length + bills.length + payments.length + masterData.length + po.length;
        let totalAmount = parseFloat(invoicesTotal) + parseFloat(billsTotal) + parseFloat(paymentsTotal) + parseFloat(masterDataTotal) + parseFloat(poTotal);
        let totalAmoutnInvoiceObject = {};
        totalAmoutnInvoiceObject = {
            invoicesTotal,
            billsTotal,
            paymentsTotal,
            masterDataTotal,
            POTotal: poTotal,
            totalAmount,
            totalCount
        };
        //  console.log("TotalAmoutnInvoiceObject Total ::::::::::::::::::::::::",totalAmoutnInvoiceObject);
        return totalAmoutnInvoiceObject;
    },
);
