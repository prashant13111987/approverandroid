import thunk from 'redux-thunk';
import logger from 'redux-logger';
import { applyMiddleware, createStore } from 'redux';
import { createReactNavigationReduxMiddleware } from 'react-navigation-redux-helpers';
import rootReducer from '../../mobile/reducer';


export default function configureStore() {
    const middleware = createReactNavigationReduxMiddleware(
        'root',
        state => state.nav,
    );
    const store = createStore(rootReducer, applyMiddleware(logger, thunk, middleware));
    return store;
}
