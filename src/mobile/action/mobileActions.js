import Constant from '../../common/constants';
import config from '../../common/config';
import { navigateToBack } from '../../common/routing';


export const onNetworkStateChanged = netState => ({
    type: Constant.NET_STATE_CHANGED,
    payload: netState,
});

export const goBack = () => (dispatch) => {
    navigateToBack(dispatch);
};

