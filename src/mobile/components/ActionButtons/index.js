import React from 'react';
import {
	StyleSheet,
	View,
	Text,
    Image
} from 'react-native';
import Cancel from '../../../common/images/icons-cancel.png';
import Hand from '../../../common/images/icons-hand.png';
import check from '../../../common/images/ic_success_filled.png';

const ActionButtons = (props) =>{
    return(
        <View style={{ flex: 1, alignItems: 'center', paddingTop: 10, backgroundColor: 'white' }}>
        <View style={styles.chartFooterButtonsGroupRight}>
            <View
                style={[ styles.viewButton, { flexDirection: 'row', justifyContent: 'space-between' } ]}
                onPress={props.onAllPress}
            >
                <View >
                    <Image
                        source={Cancel}
                        style={{
                            width: 30,
                            height: 30
                        }}
                    />
                    <Text style={styles.viewText}>Reject</Text>
                </View>
                <View>
                    <Image
                        source={Hand}
                        style={{
                            width: 30,
                            height: 30
                        }}
                    />
                    <Text style={styles.viewText}>On Hold</Text>
                </View>
                <View >
                    <Image
                        source={check}
                        style={{
                            width: 30,
                            height: 30
                        }}
                    />
                    <Text style={styles.viewText}>Approve</Text>
                </View>
            </View>
        </View>
    </View>
    )
}
const styles = StyleSheet.create({
	buttonText: { fontSize: 14, color: '#0059a6' },
	chartFooterButtonsGroupRight: {
		flex:1,
		// height: '15%',
		flexDirection: 'row',
		justifyContent: 'space-evenly'
	},
	viewButton: {
		width: '80%',
		height: '70%',
		borderRadius: 35,
		borderColor: '#4785bd',
		borderWidth: 1,
		backgroundColor: 'white',
		alignItems: 'center',
		justifyContent: 'center',
        marginTop: 5,
        padding:20
	},
	viewText: {
		fontSize: 10,
		fontWeight: 'bold',
		color: '#4785bd'
	}
});

export default ActionButtons;