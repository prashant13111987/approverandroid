/* eslint-disable global-require */
/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React from 'react';
import {
    StyleSheet,
    View,
    Dimensions,
    Text,
    TouchableOpacity,
    TouchableHighlight,
    TextInput,
    Image,
    TouchableWithoutFeedback, Keyboard
} from 'react-native';
const { width ,height} = Dimensions.get('window');
import PropTypes from 'prop-types';
import InputText from '../CustomComponent/TextInput'
import CustomView from '../CustomComponent/CustomBorderView'
import nextImage from '../../../common/images/nextArrow.png';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'

const AddItemComponent = props => {

    return(

        <View style = {styles.container}>
            <KeyboardAwareScrollView
                scrollEnabled
                showsVerticalScrollIndicator={true}

                enableOnAndroid
                keyboardShouldPersistTaps="handled"
                enableResetScrollToCoords={true}
                flex={1}
            >
            <View style ={styles.imageBackgroundView}>
                <Image
                   // source={{uri:props.imageUrl}}
                    //  minimumZoomScale={0.5}
                    //   maximumZoomScale={3}
                    //   androidScaleType="center"
                    //   onLoad={() => console.log("Image loaded!")}
                    style={[styles.imageBackgroundChildView]} />
            </View>
            <View style = {{backgroundColor:'white',marginLeft:8,marginRight:10,height:'100%',marginTop:10}}>
                <View style = {{backgroundColor:'white'}}>
                    <TouchableOpacity
                        onPress={props.onNamePress}
                        style={{ marginTop: 4}}
                    >
                        <CustomView styles={{
                            flexDirection: 'row',
                            justifyContent: 'space-between',
                            // backgroundColor: 'white',
                        }}
                        >
                            <Text
                                allowFontScaling={false}
                                style={styles.listItemText}
                            >{props.itemName}
                            </Text>
                            <Image
                                source={nextImage}
                                style={styles.forwardArrowImageStyle}
                            />
                        </CustomView>
                    </TouchableOpacity>
                    <InputText
                        style={{ marginTop: 8}}
                        onChangeText={props.handleCGSTAmountChange}
                        value={props.hsnName}
                        autoCapitalize="none"
                        autoCorrect={false}
                        maxLength={50}
                        blurOnSubmit={false}
                        editable={props.isEditable}
                        name="hsnCode"
                        onTextInputCreated={props.setInputReference}
                        returnKeyType="next"
                        onSubmitEditing={() => props.onSubmitEditing('discount')}
                    />

                    <View style={{
                        width: '100%',
                        marginTop: 8,
                        flexDirection: 'row',
                        justifyContent: 'space-between'
                    }}>
                        <View style={{width: '45%'}}>
                            <Text style={{color: '#555555'}}>Discount</Text>

                            <InputText
                                style={{ marginTop: 4}}
                                onChangeText={props.handleQuantityChange}
                               // value={item.quantity}
                                autoCapitalize="none"
                                autoCorrect={false}
                                testID="quantity"
                                maxLength={50}
                                editable={props.isEditable}
                                blurOnSubmit={false}
                                name="discount"
                                onTextInputCreated={props.setInputReference}
                                returnKeyType="next"
                                onSubmitEditing={() => props.onSubmitEditing("taxrate")}
                            />
                        </View>
                        <View style={{width: '45%'}}>
                            <Text style={{color: '#555555'}}>Tax Rate</Text>
                            <InputText
                                style={{ marginTop: 4}}
                                onChangeText={props.handlePriceChange}
                               // value={item.price}
                                autoCapitalize="none"
                                autoCorrect={false}
                                testID="taxrate"
                                maxLength={50}
                                editable={props.isEditable}
                                blurOnSubmit={false}
                                name="taxrate"
                                onTextInputCreated={props.setInputReference}
                                returnKeyType="next"
                                onSubmitEditing={() => props.onSubmitEditing("Quantity")}
                            />
                        </View>
                    </View>

                    <View style={{width:'100%',marginTop:8,flexDirection:'row',justifyContent:'space-between'}}>
                        <View style={{width:'30%'}}>
                            <Text style={{color:'#555555'}}>Quantity</Text>
                            <InputText
                                style={{ marginTop: 4}}
                                onChangeText={props.handleQuantityChange}
                                value={props.quantity}
                                autoCapitalize="none"
                                autoCorrect={false}
                                maxLength={50}
                                blurOnSubmit={false}
                                editable={props.isEditable}
                                name="Quantity"
                                onTextInputCreated={props.setInputReference}
                                returnKeyType="next"
                                onSubmitEditing={() => props.onSubmitEditing('rate')}
                            />
                        </View>
                        <View style={{width:'25%'}}>
                            <Text style={{color:'#555555'}}>Rate</Text>
                            <InputText
                                style={{ marginTop: 4}}
                                onChangeText={props.handlePriceChange}
                                value={props.price}
                                autoCapitalize="none"
                                autoCorrect={false}
                                maxLength={50}
                                blurOnSubmit={false}
                                editable={props.isEditable}
                                name="rate"
                                onTextInputCreated={props.setInputReference}
                                returnKeyType="done"
                                onSubmitEditing={() => { Keyboard.dismiss(); }}
                            />
                        </View>
                        <View style={{width:'30%'}}>
                            <Text style={{color:'#555555'}}>Amount</Text>
                            <InputText
                                style={{ marginTop: 4}}
                                onChangeText={props.handleCGSTAmountChange}
                                value={props.totalPrice.toString()}
                                autoCapitalize="none"
                                autoCorrect={false}
                                maxLength={50}
                                blurOnSubmit={false}
                                editable={false}
                               // name="cgstAmount"
                                // onTextInputCreated={props.setInputReference}
                                // returnKeyType="next"
                                // onSubmitEditing={() => props.onSubmitEditing('sgstAmount')}
                            />
                        </View>
                    </View>
                    <TouchableOpacity style={styles.addItemButton}
                                      onPress={props.onSavePress}>
                        <Text style={styles.addItemText}>ADD</Text>
                    </TouchableOpacity>
                </View>
            </View>
            </KeyboardAwareScrollView>
        </View>

    )};

const styles = StyleSheet.create({

    container: {
        flex: 1,
        backgroundColor:'white'
    },
    imageBackgroundView:{
        height:'100%',
        backgroundColor:'white'
    },
    imageBackgroundChildView:{
        height:'100%',
        backgroundColor:'white',
        resizeMode: 'contain',
    },

    textInput:{
        marginTop:5,
        paddingLeft:10,
        borderColor:'#0d60aa',
        borderRadius:0,
        height:30
    },
    validatePlaceHolder:{color:'rgba(98,98,98,1)',fontWeight:'600', fontSize:15},
    addItemButton:{
        width :'100%',
        height:35,
        borderRadius:35,
        borderColor:'#4785bd',
        borderWidth:1,
        backgroundColor:'#0059a6',
        alignItems:'center',
        justifyContent: 'center',
        marginTop:20
    },
    addItemText:{
        fontSize:12,
        fontWeight:'bold',
        color:'white'
    },
    listItemText: {
        color: '#0F2C5A',
        fontSize: 14,
        fontFamily: 'System',
        // fontWeight: Platform.OS === 'ios' ? '500' : '400',
        //marginLeft: 16,
        alignSelf: 'center',
        width: width - 150,

    },
    forwardArrowImageStyle: {
        width: 15,
        height: 15,
        //  resizeMode: Image.resizeMode.contain,
        alignSelf: 'center',
        marginRight: 15,
    },

})

AddItemComponent.propTypes = {
    getActiveSlide: PropTypes.func,
    onScroll: PropTypes.func,
    activeSlide:PropTypes.string
};

export default AddItemComponent;
