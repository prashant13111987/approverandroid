

/* eslint-disable global-require */
/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React from 'react';
import {
    StyleSheet,
    View,
    Dimensions,
    Text,
    TouchableOpacity,
    TouchableHighlight,
    TextInput,
    TouchableWithoutFeedback, Keyboard
} from 'react-native';
import PropTypes from 'prop-types';
import InputText from '../CustomComponent/TextInput'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';

const AmountApproverInvoiceComponent = props => {
    return (
        <KeyboardAwareScrollView
            scrollEnabled
            showsVerticalScrollIndicator={true}
            enableOnAndroid
            style={{ height: '88%' }}
            keyboardShouldPersistTaps="handled"
            enableResetScrollToCoords={true}
        >

            <View style={{ flex: 1, alignItems: 'center', paddingTop: 10, paddingBottom: 10 }}>

                <View style={{ width: '80%', marginTop: 10 }}>
                    <Text style={styles.validatePlaceHolder}>Sub total</Text>
                    <InputText
                        style={{ marginTop: 4 }}
                        onChangeText={props.handleSubtotalChange}
                        value={props.data.subtotal ? props.data.subtotal.toString() : ''}
                        autoCapitalize="none"
                        autoCorrect={false}
                        testID="subtotal"
                        maxLength={50}
                        editable={props.isEditable}
                        blurOnSubmit={false}
                        name="subtotal"
                        onTextInputCreated={props.setInputReference}
                        returnKeyType="next"
                        onSubmitEditing={() => props.onSubmitEditing('GSTTotal')}
                    />
                </View>
                <View style={{ width: '80%', marginTop: 20 }}>
                    <Text style={styles.validatePlaceHolder}>Del. Chg</Text>
                    <InputText
                        style={{ marginTop: 4 }}
                        onChangeText={props.handleGSTtotalChange}
                        value={props.data.delivery_charges ? parseFloat(props.data.delivery_charges).toFixed(2) : ''}
                        autoCapitalize="none"
                        autoCorrect={false}
                        testID="GSTTotal"
                        maxLength={50}
                        blurOnSubmit={false}
                        editable={props.isEditable}
                        name="GSTTotal"
                        onTextInputCreated={props.setInputReference}
                        returnKeyType="next"
                        onSubmitEditing={() => props.onSubmitEditing('packaging')}
                    />
                </View>

                <View style={{ width: '80%', marginTop: 20 }}>
                    <Text style={styles.validatePlaceHolder}>Pack. Chg</Text>
                    <InputText
                        style={{ marginTop: 4 }}
                        onChangeText={props.handlePackagingChange}
                        value={props.data.packing_charges ? props.data.packing_charges.toString() : ''}
                        autoCapitalize="none"
                        autoCorrect={false}
                        testID="packaging"
                        maxLength={50}
                        name="packaging"
                        blurOnSubmit={false}
                        editable={props.isEditable}
                        onTextInputCreated={props.setInputReference}
                        returnKeyType="next"
                        onSubmitEditing={() => props.onSubmitEditing('delivery')}
                    />
                </View>

                <View style={{ width: '80%', marginTop: 20 }}>
                    <Text style={styles.validatePlaceHolder}>Oth. Chg</Text>
                    <InputText
                        style={{ marginTop: 4 }}
                        onChangeText={props.handleOtherFieldChange}
                        value={props.data.other_charges ? props.data.other_charges.toString() : ''}
                        autoCapitalize="none"
                        name="otherField"
                        autoCorrect={false}
                        testID="otherField"
                        maxLength={50}
                        blurOnSubmit={false}
                        editable={props.isEditable}
                        onTextInputCreated={props.setInputReference}
                        returnKeyType="done"
                        onSubmitEditing={() => { Keyboard.dismiss(); }}
                    />
                </View>
                <View style={{ width: '80%', marginTop: 20 }}>
                    <Text style={styles.validatePlaceHolder}>GST Total</Text>
                    <InputText
                        style={{ marginTop: 4 }}
                        onChangeText={props.handleOtherFieldChange}
                        value={(props.data.igst_total + props.data.sgst_total + props.data.cgst_total).toString()}
                        autoCapitalize="none"
                        name="otherField"
                        autoCorrect={false}
                        testID="otherField"
                        maxLength={50}
                        blurOnSubmit={false}
                        editable={props.isEditable}
                        onTextInputCreated={props.setInputReference}
                        returnKeyType="done"
                        onSubmitEditing={() => { Keyboard.dismiss(); }}
                    />
                </View>
                <View style={{ width: '80%', marginTop: 20 }}>
                    <Text style={styles.validatePlaceHolder}>Gross</Text>
                    <InputText
                        style={{ marginTop: 4 }}
                        onChangeText={props.handleOtherFieldChange}
                        value={props.data.total ? props.data.total.toString() : ''}
                        autoCapitalize="none"
                        name="otherField"
                        autoCorrect={false}
                        testID="otherField"
                        maxLength={50}
                        blurOnSubmit={false}
                        editable={props.isEditable}
                        onTextInputCreated={props.setInputReference}
                        returnKeyType="done"
                        onSubmitEditing={() => { Keyboard.dismiss(); }}
                    />
                </View>
            </View>
        </KeyboardAwareScrollView>
    )
};

const styles = StyleSheet.create({
    textInput: {
        marginTop: 5,
        paddingLeft: 10,
        borderColor: '#0d60aa',
        borderRadius: 0,
        height: 30
    },
    validatePlaceHolder: { color: 'rgba(98,98,98,1)', fontWeight: '600', fontSize: 12 },
    bottomButtons: {
        alignItems: 'center', backgroundColor: '#0d60aa', justifyContent: 'center', width: 150, height: 35, borderRadius: 40
    }

})

AmountApproverInvoiceComponent.propTypes = {
    handleTotalAmountChange: PropTypes.func,
    handleSubtotalChange: PropTypes.func,
    handleGSTtotalChange: PropTypes.func,
    handlePackagingChange: PropTypes.func,
    handleOtherFieldChange: PropTypes.func,
    handleDeliveryChange: PropTypes.func,
    onSubmitEditing: PropTypes.func,
    setInputReference: PropTypes.func,
    focusNextField: PropTypes.func,
    totalAmount: PropTypes.string,
    subtotal: PropTypes.string,
    GSTTotal: PropTypes.string,
    delivery: PropTypes.string,
    otherField: PropTypes.string,
    packaging: PropTypes.string,
    onSendForApproval: PropTypes.func,
    onUpdate: PropTypes.func,
};

export default AmountApproverInvoiceComponent;
