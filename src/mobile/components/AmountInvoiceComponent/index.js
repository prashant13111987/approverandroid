/* eslint-disable global-require */
/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React from 'react';
import {
    StyleSheet,
    View,
    Dimensions,
    Text,
    TouchableOpacity,
    TouchableHighlight,
    TextInput,
    TouchableWithoutFeedback, Keyboard
} from 'react-native';
import PropTypes from 'prop-types';
import InputText from '../CustomComponent/TextInput'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';

const AmountInvoiceComponent = props => {
    return(
        <View style={{height:'94%'}}>
        <KeyboardAwareScrollView
            scrollEnabled
            showsVerticalScrollIndicator={true}
            enableOnAndroid
            style ={{height:'80%'}}
            keyboardShouldPersistTaps="handled"
            enableResetScrollToCoords={true}
        >

        <View style = {{alignItems:'center',paddingTop:10,paddingBottom:10}}>
            <View style = {{width:'80%',marginTop:10}}>
                <Text style = {styles.validatePlaceHolder}>Total</Text>
                <InputText
                    style={{ marginTop: 4}}
                    onChangeText={props.handleTotalAmountChange}
                    value={parseFloat(props.totalAmount).toFixed(2)}
                   // value={props.totalAmount.toString().toFixed(2)}
                    autoCapitalize="none"
                    autoCorrect={false}
                    testID="totalAmount"
                    maxLength={50}
                    blurOnSubmit={false}
                    editable={props.isEditable}
                    name="totalAmount"
                    onTextInputCreated={props.setInputReference}
                    returnKeyType="next"
                    onSubmitEditing={() => props.onSubmitEditing('subtotal')}
                />
            </View>
            <View style = {{width:'80%',marginTop:10}}>
                <Text style = {styles.validatePlaceHolder}>Subtotal</Text>
                <InputText
                    style={{ marginTop: 4}}
                    onChangeText={props.handleSubtotalChange}
                    value={props.subtotal.toString()}
                    autoCapitalize="none"
                    autoCorrect={false}
                    testID="subtotal"
                    maxLength={50}
                    editable={props.isEditable}
                    blurOnSubmit={false}
                    name="subtotal"
                    onTextInputCreated={props.setInputReference}
                    returnKeyType="next"
                    onSubmitEditing={() => props.onSubmitEditing('GSTTotal')}
                />
            </View>
            <View style = {{width:'80%',marginTop:20}}>
                <Text style = {styles.validatePlaceHolder}>GST Total</Text>
                <InputText
                    style={{ marginTop: 4}}
                    onChangeText={props.handleGSTtotalChange}
                    value={props.GSTTotal.toFixed(2)}
                    autoCapitalize="none"
                    autoCorrect={false}
                    testID="GSTTotal"
                    maxLength={50}
                    blurOnSubmit={false}
                    editable={props.isEditable}
                    name="GSTTotal"
                    onTextInputCreated={props.setInputReference}
                    returnKeyType="next"
                    onSubmitEditing={() => props.onSubmitEditing('packaging')}
                />
            </View>

            <View style = {{width:'80%',marginTop:20}}>
                <Text style = {styles.validatePlaceHolder}>Packaging</Text>
                <InputText
                    style={{ marginTop: 4}}
                    onChangeText={props.handlePackagingChange}
                    value={props.packaging.toString()}
                    autoCapitalize="none"
                    autoCorrect={false}
                    testID="packaging"
                    maxLength={50}
                    name="packaging"
                    blurOnSubmit={false}
                    editable={props.isEditable}
                    onTextInputCreated={props.setInputReference}
                    returnKeyType="next"
                    onSubmitEditing={() => props.onSubmitEditing('delivery')}
                />
            </View>

            <View style = {{width:'80%',marginTop:20}}>
                <Text style = {styles.validatePlaceHolder}>Delivery</Text>
                <InputText
                    style={{ marginTop: 4}}
                    onChangeText={props.handleDeliveryChange}
                    value={props.delivery.toString()}
                    autoCapitalize="none"
                    name="delivery"
                    autoCorrect={false}
                    testID="delivery"
                    maxLength={50}
                    blurOnSubmit={false}
                    editable={props.isEditable}
                    onTextInputCreated={props.setInputReference}
                    returnKeyType="next"
                    onSubmitEditing={() => props.onSubmitEditing('otherField')}
                />
            </View>

            <View style = {{width:'80%',marginTop:20}}>
                <Text style = {styles.validatePlaceHolder}>Other</Text>
                <InputText
                    style={{ marginTop: 4}}
                    onChangeText={props.handleOtherFieldChange}
                    value={props.otherField.toString()}
                    autoCapitalize="none"
                    name="otherField"
                    autoCorrect={false}
                    testID="otherField"
                    maxLength={50}
                    blurOnSubmit={false}
                    editable={props.isEditable}
                    onTextInputCreated={props.setInputReference}
                    returnKeyType="done"
                    onSubmitEditing={() => { Keyboard.dismiss(); }}
                />
            </View>
        </View>
        </KeyboardAwareScrollView>
            <View style = {{height:'20%',paddingLeft:35,paddingRight:35, flexDirection:'row', justifyContent:'space-between',alignItems:'center'}}>
                <TouchableOpacity style={styles.bottomButtons} onPress={() => (props.onUpdate())}>
                    <Text style={{color:'white',fontSize:12}}>UPDATE</Text>
                </TouchableOpacity>

                <TouchableOpacity style={styles.bottomButtons} onPress={() => (props.onSendForApproval())}>
                   <Text style={{color:'white',fontSize:12}}>SEND FOR APPROVAL</Text>
                </TouchableOpacity>

            </View>
        </View>
    )};

const styles = StyleSheet.create({
    textInput:{
        marginTop:5,
        paddingLeft:10,
        borderColor:'#0d60aa',
        borderRadius:0,
        height:30
    },
    validatePlaceHolder:{color:'rgba(98,98,98,1)',fontWeight:'600', fontSize:12},
    bottomButtons:{
        alignItems:'center', backgroundColor:'#0d60aa',justifyContent:'center',width:150,height:35,borderRadius:40
    }

})

AmountInvoiceComponent.propTypes = {
    handleTotalAmountChange :PropTypes.func,
    handleSubtotalChange :PropTypes.func,
    handleGSTtotalChange :PropTypes.func,
    handlePackagingChange :PropTypes.func,
    handleOtherFieldChange :PropTypes.func,
    handleDeliveryChange :PropTypes.func,
    onSubmitEditing :PropTypes.func,
    setInputReference :PropTypes.func,
    focusNextField :PropTypes.func,
    totalAmount:PropTypes.string,
    subtotal:PropTypes.string,
    GSTTotal:PropTypes.string,
    delivery:PropTypes.string,
    otherField:PropTypes.string,
    packaging:PropTypes.string,
    onSendForApproval:PropTypes.func,
    onUpdate:PropTypes.func,
    };

export default AmountInvoiceComponent;
