/* eslint-disable global-require */
/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React from 'react';
import {
    StyleSheet,
    View,
    Dimensions,
    Text,
    Image,
    TouchableOpacity,
    KeyboardAvoidingView,
    Keyboard,
    TouchableWithoutFeedback,
    Picker, Platform
} from 'react-native';
import PropTypes from 'prop-types';

import InputText from '../CustomComponent/TextInput'
import DatePicker from 'react-native-datepicker'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import { normalizeFont } from "../../../common/util";
import Cancel from '../../../common/images/icons-cancel.png';
import Hand from '../../../common/images/icons-hand.png';
import check from '../../../common/images/ic_success_filled.png';

const { width } = Dimensions.get('window');
const BasicApproverInvoiceComponent = props => (

    <KeyboardAwareScrollView
        scrollEnabled
        showsVerticalScrollIndicator={true}
        enableOnAndroid
        style={{ height: '88%' }}
        keyboardShouldPersistTaps="handled"
        enableResetScrollToCoords={true}
    >
        <View style={{ alignItems: 'center', paddingTop: 10, backgroundColor: 'white', paddingBottom: 90 }}>
            <View style={{ width: '80%', marginTop: 20 }}>
                <Text style={styles.validatePlaceHolder}>{props.data.debit_memo ? 'Debit Memo No' : 'Bill No'}</Text>
                <InputText
                    style={{ marginTop: 4 }}
                    onChangeText={props.handleInvoiceNumberChange}
                    value={props.data.document_no}
                    autoCapitalize="none"
                    autoCorrect={false}
                    testID="billNumber"
                    maxLength={50}
                    blurOnSubmit={false}
                    editable={props.isEditable}
                    name="billNumber"
                    onTextInputCreated={props.setInputReference}
                    returnKeyType="next"
                    onSubmitEditing={() => props.onSubmitEditing('billNumber')}
                />
            </View>
            {props.data.invoice_date ? <View style={{ width: '80%', marginTop: 20 }}>
                <Text style={styles.validatePlaceHolder}>Invoice date</Text>
                <DatePicker
                    style={{ width: '100%', marginTop: 4 }}
                    date={props.data.invoice_date}
                    mode="date"
                    placeholder="Invoice date"
                    format="YYYY-MM-DD"
                    minDate="2016-05-01"
                    maxDate="2019-06-01"
                    confirmBtnText="Confirm"
                    cancelBtnText="Cancel"
                    showIcon={false}
                    disabled={!props.isEditable}
                    onDateChange={props.onBillDateChange}
                    customStyles={{
                        dateText: {
                            fontSize: 15
                        },
                        dateInput: {
                            alignItems: 'flex-start',
                            paddingLeft: 10,
                            height: 40,
                            // borderColor: '#cfcfcf',
                            borderColor: '#0d60aa',
                            borderRadius: 4,
                            borderWidth: 1,
                            color: '#0F2C5A',
                            backgroundColor: 'white',
                        }
                        // ... You can check the source to find the other keys.
                    }}
                />
            </View> : null}
            <View style={{ width: '80%', marginTop: 20 }}>
                <Text style={styles.validatePlaceHolder}>TDS Ledger</Text>
                <InputText
                    style={{ marginTop: 4 }}
                    onChangeText={props.handlePoNumberChange}
                    value={props.data.tds_ledger_name}
                    autoCapitalize="none"
                    name="tdsLedger"
                    autoCorrect={false}
                    editable={props.isEditable}
                    returnKeyType="next"
                    testID="tdsLedger"
                    maxLength={50}
                    blurOnSubmit={false}
                    onTextInputCreated={props.setInputReference}
                    onSubmitEditing={() => props.onSubmitEditing('shipInfo')}
                // onSubmitEditing={() => { Keyboard.dismiss(); }}
                />
            </View>
            <View style={{ width: '80%', marginTop: 20 }}>
                <Text style={styles.validatePlaceHolder}>Bill date</Text>
                <DatePicker
                    style={{ width: '100%', marginTop: 4 }}
                    date={props.data.created_at}
                    mode="date"
                    placeholder="Bill date"
                    format="YYYY-MM-DD"
                    minDate="2016-05-01"
                    maxDate="2019-06-01"
                    confirmBtnText="Confirm"
                    cancelBtnText="Cancel"
                    showIcon={false}
                    disabled={!props.isEditable}
                    onDateChange={props.onBillDateChange}
                    customStyles={{
                        dateText: {
                            fontSize: 15
                        },
                        dateInput: {
                            alignItems: 'flex-start',
                            paddingLeft: 10,
                            height: 40,
                            // borderColor: '#cfcfcf',
                            borderColor: '#0d60aa',
                            borderRadius: 4,
                            borderWidth: 1,
                            color: '#0F2C5A',
                            backgroundColor: 'white',
                        }
                        // ... You can check the source to find the other keys.
                    }}
                />
            </View>
            <View style={{ width: '80%', marginTop: 20 }}>
                <Text style={styles.validatePlaceHolder}>PO Number</Text>
                <InputText
                    style={{ marginTop: 4 }}
                    onChangeText={props.handleShipFromChange}
                    value={props.data.po_no}
                    autoCapitalize="none"
                    name="shipInfo"
                    autoCorrect={false}
                    testID="shipFrom"
                    maxLength={50}
                    blurOnSubmit={false}
                    editable={props.isEditable}
                    onTextInputCreated={props.setInputReference}
                    returnKeyType="done"
                    onSubmitEditing={() => { Keyboard.dismiss(); }}
                />
            </View>
            <View style={{ width: '80%', marginTop: 20 }}>
                <Text style={styles.validatePlaceHolder}>Shipping Info</Text>
                <InputText
                    style={{ marginTop: 4 }}
                    onChangeText={props.handleShipFromChange}
                    value={props.data.transp_ref_no}
                    autoCapitalize="none"
                    name="shipInfo"
                    autoCorrect={false}
                    testID="shipFrom"
                    maxLength={50}
                    blurOnSubmit={false}
                    editable={props.isEditable}
                    onTextInputCreated={props.setInputReference}
                    returnKeyType="done"
                    onSubmitEditing={() => { Keyboard.dismiss(); }}
                />
            </View>
            {props.category === 'DBN' ? <View style={{ width: '80%', marginTop: 20 }}>
                <Text style={styles.validatePlaceHolder}>Ref Bill no.</Text>
                <InputText
                    style={{ marginTop: 4 }}
                    onChangeText={props.handleShipFromChange}
                    value={props.data.bill_no}
                    autoCapitalize="none"
                    name="refInvoiceNo"
                    autoCorrect={false}
                    testID="shipFrom"
                    maxLength={50}
                    blurOnSubmit={false}
                    editable={props.isEditable}
                    onTextInputCreated={props.setInputReference}
                    returnKeyType="done"
                    onSubmitEditing={() => { Keyboard.dismiss(); }}
                />
            </View> : null}
        </View>
        {/* <View style={{ flex: 1, alignItems: 'center', paddingTop: 10, backgroundColor: 'white' }}>
			<View style={styles.chartFooterButtonsGroupRight}>
				<View
					style={[ styles.viewButton, { flexDirection: 'row', justifyContent: 'space-between' } ]}
					onPress={props.onAllPress}
				>
					<View >
						<Image
							source={Cancel}
							style={{
								width: 30,
								height: 30
							}}
						/>
						<Text style={styles.viewText}>Reject</Text>
					</View>
					<View>
						<Image
							source={Hand}
							style={{
								width: 30,
								height: 30
							}}
						/>
						<Text style={styles.viewText}>On Hold</Text>
					</View>
					<View >
						<Image
							source={check}
							style={{
								width: 30,
								height: 30
							}}
						/>
						<Text style={styles.viewText}>Approve</Text>
					</View>
				</View>
			</View>
		</View> */}
    </KeyboardAwareScrollView>
);

BasicApproverInvoiceComponent.propTypes = {
    setInputReference: PropTypes.func,
    onSubmitEditing: PropTypes.func,
    onValidateInvoiceOptionSelect: PropTypes.func,
    selectedBillDate: PropTypes.string,
    selectedInvoiceDate: PropTypes.string,
    onInvoiceDateChange: PropTypes.func,
    onBillDateChange: PropTypes.func,
    handlePoNumberChange: PropTypes.func,
    handleShipFromChange: PropTypes.func,
    handleBillNumberChange: PropTypes.func,
    handleInvoiceNumberChange: PropTypes.func,
    invoiceTitle: PropTypes.string,
    invoiceNumber: PropTypes.string,
    billNumber: PropTypes.string,
    shipFrom: PropTypes.string,
    shipTo: PropTypes.string,
    lorryReceipt: PropTypes.string,
    poNumber: PropTypes.string,
    vendor: PropTypes.string,
};


const styles = StyleSheet.create({

    textInput: {
        marginTop: 5,
        paddingLeft: 10,
        borderColor: '#0d60aa',
        borderRadius: 0,
        height: 30
    },
    validatePlaceHolder: { color: 'rgba(98,98,98,1)', fontWeight: '600', fontSize: 12 },
    chartFooterButtonsGroupRight: {
        flex: 1,
        // height: '15%',
        flexDirection: 'row',
        justifyContent: 'space-evenly'
    },
    viewButton: {
        width: '80%',
        height: '70%',
        borderRadius: 35,
        borderColor: '#4785bd',
        borderWidth: 1,
        backgroundColor: 'white',
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: 5,
        padding: 20
    },
    viewText: {
        fontSize: 10,
        fontWeight: 'bold',
        color: '#4785bd'
    },
});
export default BasicApproverInvoiceComponent;
