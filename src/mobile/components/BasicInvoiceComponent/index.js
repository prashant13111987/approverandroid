/* eslint-disable global-require */
/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React from 'react';
import {
    StyleSheet,
    View,
    Dimensions,
    Text,
    Image,
    TouchableOpacity,
    KeyboardAvoidingView, Keyboard,
    TouchableWithoutFeedback,
    Picker, Platform
} from 'react-native';
import PropTypes from 'prop-types';

import InputText from '../CustomComponent/TextInput'
import DatePicker from 'react-native-datepicker'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import {normalizeFont} from "../../../common/util";

const { width } = Dimensions.get('window');
const BasicInvoiceComponent = props => (

    <KeyboardAwareScrollView
        scrollEnabled
        showsVerticalScrollIndicator={true}
        enableOnAndroid
        style ={{height:'80%'}}
        keyboardShouldPersistTaps="handled"
        enableResetScrollToCoords={true}
    >
    <View style = {{alignItems:'center',paddingTop:10,backgroundColor:'white',paddingBottom:90}}>
        <View style = {{width:'80%',marginTop:20}}>
            <Text style = {styles.validatePlaceHolder}>Invoice Number</Text>
            <InputText
                style={{ marginTop: 4}}
                onChangeText={props.handleInvoiceNumberChange}
                value={props.invoiceNumber}
                autoCapitalize="none"
                autoCorrect={false}
                testID="invoiceNumber"
                maxLength={50}
                blurOnSubmit={false}
                editable={props.isEditable}
                name="invoiceNumber"
                onTextInputCreated={props.setInputReference}
                returnKeyType="next"
                onSubmitEditing={() => props.onSubmitEditing('billNumber')}
            />
        </View>
        <View style = {{width:'80%',marginTop:20}}>
            <Text style = {styles.validatePlaceHolder}>Bill Number</Text>
            <InputText
                style={{ marginTop: 4}}
                onChangeText={props.handleBillNumberChange}
                value={props.billNumber}
                autoCapitalize="none"
                autoCorrect={false}
                testID="billNumber"
                maxLength={50}
                editable={props.isEditable}
                name="billNumber"
                blurOnSubmit={false}
                onTextInputCreated={props.setInputReference}
                returnKeyType="next"
                onSubmitEditing={() => props.onSubmitEditing('poNumber')}
            />
        </View>
        <View style = {{width:'80%',marginTop:20}}>
            <Text style = {styles.validatePlaceHolder}>Po Number</Text>
            <InputText
                style={{ marginTop: 4}}
                onChangeText={props.handlePoNumberChange}
                value={props.poNumber}
                autoCapitalize="none"
                name="poNumber"
                autoCorrect={false}
                editable={props.isEditable}
                returnKeyType="next"
                testID="poNumber"
                maxLength={50}
                blurOnSubmit={false}
                onTextInputCreated={props.setInputReference}
                onSubmitEditing={() => props.onSubmitEditing('shipInfo')}
               // onSubmitEditing={() => { Keyboard.dismiss(); }}
            />
        </View>
        <View style = {{width:'80%',marginTop:20}}>
            <Text style = {styles.validatePlaceHolder}>Shipping Info</Text>
            <InputText
                style={{ marginTop: 4}}
                onChangeText={props.handleShipFromChange}
                value={props.shipInfo}
                autoCapitalize="none"
                name="shipInfo"
                autoCorrect={false}
                testID="shipFrom"
                maxLength={50}
                blurOnSubmit={false}
                editable={props.isEditable}
                onTextInputCreated={props.setInputReference}
                returnKeyType="done"
                onSubmitEditing={() => { Keyboard.dismiss(); }}
            />
        </View>
        <View style = {{width:'80%',marginTop:20}}>
            <Text style = {styles.validatePlaceHolder}>Bill date</Text>
            <DatePicker
                style={{width: '100%',marginTop:4}}
                date={props.selectedBillDate}
                mode="date"
                placeholder="Select bill date"
                format="YYYY-MM-DD"
                minDate="2016-05-01"
                maxDate="2019-06-01"
                confirmBtnText="Confirm"
                cancelBtnText="Cancel"
                showIcon={false}
                disabled={!props.isEditable}
                onDateChange={props.onBillDateChange}
                customStyles={{
                    dateText: {
                        fontSize:15
                    },
                    dateInput: {
                        alignItems:'flex-start',
                        paddingLeft:10,
                        height: 40,
                        // borderColor: '#cfcfcf',
                        borderColor: '#0d60aa',
                        borderRadius: 4,
                        borderWidth: 1,
                        color: '#0F2C5A',
                        backgroundColor: 'white',
                    }
                    // ... You can check the source to find the other keys.
                }}
            />
        </View>
        <View style = {{width:'80%',marginTop:20}}>
            <Text style = {styles.validatePlaceHolder}>Invoice Date</Text>
            <DatePicker
                style={{width: '100%',marginTop:4}}
                date={props.selectedInvoiceDate}
                mode="date"
                placeholder="Select invoice date"
                format="YYYY-MM-DD"
                minDate="2016-05-01"
                maxDate="2019-06-01"
                confirmBtnText="Confirm"
                cancelBtnText="Cancel"
                showIcon={false}
                disabled={!props.isEditable}
                onDateChange={props.onInvoiceDateChange}
                customStyles={{
                    dateText: {
                        fontSize:15
                    },
                    dateInput: {
                        alignItems:'flex-start',
                        paddingLeft:10,
                        height: 40,
                        // borderColor: '#cfcfcf',
                        borderColor: '#0d60aa',
                        borderRadius: 4,
                        borderWidth: 1,
                        color: '#0F2C5A',
                        backgroundColor: 'white',
                    }
                    // ... You can check the source to find the other keys.
                }}
            />
        </View>
    </View>
    </KeyboardAwareScrollView>
);

BasicInvoiceComponent.propTypes = {
    setInputReference: PropTypes.func,
    onSubmitEditing: PropTypes.func,
    onValidateInvoiceOptionSelect: PropTypes.func,
    selectedBillDate:PropTypes.string,
    selectedInvoiceDate:PropTypes.string,
    onInvoiceDateChange:PropTypes.func,
    onBillDateChange:PropTypes.func,
    handlePoNumberChange:PropTypes.func,
    handleShipFromChange:PropTypes.func,
    handleBillNumberChange:PropTypes.func,
    handleInvoiceNumberChange:PropTypes.func,
    invoiceTitle:PropTypes.string,
    invoiceNumber:PropTypes.string,
    billNumber:PropTypes.string,
    shipFrom:PropTypes.string,
    shipTo:PropTypes.string,
    lorryReceipt:PropTypes.string,
    poNumber:PropTypes.string,
    vendor:PropTypes.string,
};


const styles = StyleSheet.create({

    textInput:{
        marginTop:5,
        paddingLeft:10,
        borderColor:'#0d60aa',
        borderRadius:0,
        height:30
    },
    validatePlaceHolder:{color:'rgba(98,98,98,1)',fontWeight:'600', fontSize:12},
 });
export default BasicInvoiceComponent;
