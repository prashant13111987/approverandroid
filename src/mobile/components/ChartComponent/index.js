/* eslint-disable global-require */
/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React from 'react';
import {
    StyleSheet,
    View,
    Dimensions,
    Text,
    Image,
    TouchableOpacity,
    KeyboardAvoidingView, Keyboard,
    TouchableWithoutFeedback,
    processColor
} from 'react-native';
import { PieChart } from 'react-native-charts-wrapper';
import PropTypes from 'prop-types';


const { width } = Dimensions.get('window');

const ChartComponent = props => {
    const centerText = "Total" + "\n" + "#invoices" + "\n" + props.totalInvoiceCount + "\n" + "₹Amount" + "\n" + props.totalAmount.toFixed(2)
    const centerApproverText = "Total" + "\n" + "#count" + "\n" + props.totalInvoiceCount

    const text = props.type === 'Approver' ? centerApproverText : centerText
    return (
        <View style={styles.container}>
            <PieChart
                style={styles.chart}
                logEnabled={true}
                chartBackgroundColor={processColor('white')}
                chartDescription={props.description}
                data={props.data}
                legend={props.legend}
                highlights={props.highlights}

                entryLabelColor={processColor('green')}
                //entryLabelTextSize={5}
                drawEntryLabels={false}

                rotationEnabled={true}
                rotationAngle={45}
                usePercentValues={true}
                // centerTextRadiusPercent={100}
                holeRadius={70}
                holeColor={processColor('#f0f0f0')}
                transparentCircleRadius={0}
                transparentCircleColor={processColor('#f0f0f088')}
                maxAngle={360}
                onChange={(event) => props.onChange(event.nativeEvent)}
                onSelect={(event) => props.handleSelect(event)}
                centerText={text}

            />
        </View>

    );
}


ChartComponent.propTypes = {
    pieChartData: PropTypes.object,
    handleSelect: PropTypes.func,
    onChange: PropTypes.func

};


const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 25,
        width: '100%',
        height: '100%',
    },
    containerChart: {
        flex: 1,
        backgroundColor: '#F5FCFF'
    },
    chart: {
        flex: 1,
        height: '80%',
        width: '90%',
        borderRadius: 25
    }
});
export default ChartComponent;
