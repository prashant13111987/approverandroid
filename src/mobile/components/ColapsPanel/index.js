import React, { Component } from 'react';
import { StyleSheet, Text, View, TouchableHighlight, Animated, Image } from 'react-native';
import up from '../../../common/images/Arrowhead-up2.png';
import down from '../../../common/images/Arrowhead-down1.png';
export default class Accordion extends Component {
	constructor(props) {
		super(props);
		this.icons = {
			open: up,
			close: down
		};

		this.state = { expanded: false };
	}

	toggle() {
		this.setState({
			expanded: !this.state.expanded
		});
	}
	render() {
		let icon = this.icons['open'];
		if (this.state.expanded) {
			icon = this.icons['close'];
		}
		return (
			<View style={styles.container}>
				<View style={styles.titleContainer}>
					<Text style={styles.Header}>{this.props.Header}</Text>
					<Text style={styles.Header}>{this.props.subHeader}</Text>
					<TouchableHighlight style={styles.button} onPress={this.toggle.bind(this)} underlayColor="white">
						<Image style={styles.FAIcon} source={icon} />
					</TouchableHighlight>
				</View>

				{this.state.expanded && <View style={styles.body}>{this.props.children}</View>}
			</View>
		);``
	}
}

var styles = StyleSheet.create({
	container: {
		backgroundColor: '#fff',
		margin: 10,
		overflow: 'hidden',
		borderWidth: 1,
		borderRadius: 10,
		borderColor: 'lightgrey',
		borderBottomWidth: 1
	},
	titleContainer: {
		flexDirection: 'row',
		justifyContent: 'center',
		alignItems: 'center'
	},
	Header: {
		flex: 1,
		padding: 10,
		color: '#2a2f43'
	},
	FAIcon: {
		marginRight: 5,
		width: 25,
		height: 25
	},
	body: {
		padding: 10,
		paddingTop: 0
	}
});
