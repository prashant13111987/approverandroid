/* eslint-disable global-require */
/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React from 'react';
import {
    StyleSheet,
    View,
    Dimensions,
    Text,
    Image,
    TouchableOpacity,
    KeyboardAvoidingView, Keyboard,
    TouchableWithoutFeedback,
    ScrollView
} from 'react-native';
import PropTypes from 'prop-types';

import statusImage from '../../../common/images/statusImage.png'
import Panel from './../Panel';
const SECTIONS = [
    {
        title: 'First',
        content: ['A', 'B', 'D', 'E', 'F', 'G', 'H'
        ]
    },
    {
        title: 'Second',
        content: ['1', '2', '3', '4', '5', '6', '7'
        ]
    }
];


const { width } = Dimensions.get('window');
const listData = [
    {
        title: 'Notifications',
        data: [{ invoiceNumber: 'INVC04343', status: 'Approval pending', date: 'Due on 21-12-2018' },
        { invoiceNumber: 'INVC04343', status: 'Approval pending', date: 'Due on 21-12-2018' },
        { invoiceNumber: 'INVC04343', status: 'Approval pending', date: 'Due on 21-12-2018' },
        { invoiceNumber: 'INVC04343', status: 'Approval pending', date: 'Due on 21-12-2018' },
        { invoiceNumber: 'INVC04343', status: 'Approval pending', date: 'Due on 21-12-2018' }
        ]
    },
    {
        title: 'Chats',
        data: [{ invoiceNumber: 'INVC04343', status: 'Approval pending', date: 'Due on 21-12-2018' },
        { invoiceNumber: 'INVC04343', status: 'Approval pending', date: 'Due on 21-12-2018' },
        { invoiceNumber: 'INVC04343', status: 'Approval pending', date: 'Due on 21-12-2018' },
        { invoiceNumber: 'INVC04343', status: 'Approval pending', date: 'Due on 21-12-2018' },
        { invoiceNumber: 'INVC04343', status: 'Approval pending', date: 'Due on 21-12-2018' }
        ]
    }
]

const CollapsableDashboardListComponent = props => {
    return (
        <ScrollView>
            <View style={styles.container}>
                {listData.map((value, i) =>
                    (
                        <Panel title={value.title} key={i}>
                            {value.data.map((content, i) =>
                                (
                                    <View style={{ flexDirection: 'row', justifyContent: 'space-between', borderBottomWidth: 0.6, borderColor: '#a0a0a0' }} key={i}>
                                        <View style={{ flexDirection: 'row', padding: 5, justifyContent: 'space-between', width: '100%' }}>
                                            <View style={{ flexDirection: 'column', padding: 5, justifyContent: 'space-between' }}>
                                                <Text style={{ fontSize: 13, color: '#4785bd' }}>{content.invoiceNumber}</Text>
                                                <Text style={{ fontSize: 11, color: '#505050' }}>{content.status}</Text>
                                            </View>
                                            <View style={{ padding: 5 }}>
                                                <Text style={{ fontSize: 11, color: '#505050' }}>{content.date}</Text>
                                            </View>
                                        </View>
                                        <Image
                                            style={styles.buttonImage}
                                            source={statusImage}
                                        ></Image>
                                    </View>

                                )
                            )}
                        </Panel>
                    )
                )}
            </View>
        </ScrollView>
    );
}

CollapsableDashboardListComponent.propTypes = {

};


const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        backgroundColor: 'white',
    },
    buttonImage: {
        width: 18,
        height: 18
    },
    containerStyle: {
        width: '100%',
        height: '100%'
    },
    activeTitleHeader: {
        padding: 25,
        justifyContent: 'space-between',
        flexDirection: 'row',
    },
    inActiveTitleHeader: {
        padding: 25,
        justifyContent: 'space-between',
        borderBottomWidth: 1,
        borderColor: 'lightgray',
        flexDirection: 'row',
    },
    activeTitleTextHeader: {
        fontWeight: '700',
        fontSize: 16,
        color: '#0F2C5A',
    },
    inActiveTitleTextHeader: {
        fontWeight: '500',
        fontSize: 16,
        color: '#0F2C5A',
    },
    activeQueHeader: {
        fontWeight: '700',
        fontSize: 16,
        color: '#0F2C5A',
    },
    inActiveQueHeader: {
        fontWeight: '600',
        fontSize: 16,
        color: 'rgba(107,120,137,1)',
    },
    ansView: {
        padding: 5,
        justifyContent: 'center',
        paddingLeft: 40,
    },
    ansTextView: {
        fontWeight: '400',
        fontSize: 16,
        color: 'red',
    },

});
export default CollapsableDashboardListComponent;
