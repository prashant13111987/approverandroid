/**
 * Created by synerzip on 025/06/18.
 */
/* eslint-disable global-require */
/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React from 'react';
import {
    StyleSheet,
    View,
    Dimensions,
    Text,
    TouchableOpacity,
    FlatList, Keyboard,
} from 'react-native';

import PropTypes from 'prop-types';
import { normalizeFont } from '../../../common/util';
import InputText from '../CustomComponent/TextInput';

const { width } = Dimensions.get('window');
const CommonListComponent = (props) => {
    let filterData = [];
    if (props.data && props.keyName === 'state_list') {
        filterData = props.data.filter((item) => {
            const itemData = item.name.toUpperCase();
            const textData = props.searchText.toUpperCase();
            return itemData.indexOf(textData) > -1;
        });
    } else   if (props.data && props.keyName === 'shipTo_list') {
        filterData = props.data.filter((item) => {
            const itemData = item.name.toUpperCase();
            const textData = props.searchText.toUpperCase();
            return itemData.indexOf(textData) > -1;
        });
    }
    else  if (props.data && props.keyName === 'shipFrom_list') {
        filterData = props.data.filter((item) => {
            const itemData = item.name.toUpperCase();
            const textData = props.searchText.toUpperCase();
            return itemData.indexOf(textData) > -1;
        });
    } else
    if (props.data && props.keyName === 'Item_list') {
        filterData = props.data.filter((item) => {
            let itemData = []
            if(item.item_name)
            {
                itemData = item.item_name.toUpperCase();
            }else
            {
                if(item.name)
                {
                    itemData = item.name.toUpperCase();
                }

            }

            const textData = props.searchText.toUpperCase();
            return itemData.indexOf(textData) > -1;
        });
    }else  if (props.data && props.keyName === 'Distribution_list') {
        filterData = props.data.filter((item) => {
            const itemData = item.name.toUpperCase();
            const textData = props.searchText.toUpperCase();
            return itemData.indexOf(textData) > -1;
        });
    } else {
       // address_line1
       // shipFrom_list
        //Item_list
        filterData = props.data;
    }

    const renderItem = (item, index) => {
        const value = index + 1;

        const stateView = (
            <Text
                allowFontScaling={false}
                style={[styles.row]}
            >{item.name}
            </Text>);

        const ShipView = (
            <Text
                allowFontScaling={false}
                style={[styles.row]}
            >{item.name}
            </Text>);

        const ShipFromView = (
            <Text
                allowFontScaling={false}
                style={[styles.row]}
            >{item.name}
            </Text>);

        const itemNameView = (
            <Text
                allowFontScaling={false}
                style={[styles.row]}
            >{item.item_name?item.item_name:item.name}
            </Text>);

        const departmentNameView = (
            <Text
                allowFontScaling={false}
                style={[styles.row]}
            >{item.name}
            </Text>);



        return (

            <View style = {{flex:1,backgroundColor:'white'}}>
                <TouchableOpacity  onPress={() => (props.onRowPress(item))}>
                    <View style={{
                        width:'100%',flexDirection: 'row', backgroundColor: '#F2F3F8', height: 50, alignItems: 'center',
                    }}
                    >
                        {props.keyName === 'state_list' ? stateView : props.keyName === 'shipTo_list'? ShipView:
                            props.keyName === 'shipFrom_list'? ShipFromView : props.keyName === 'Item_list'? itemNameView:props.keyName === 'Distribution_list'? departmentNameView:ShipView}
                    </View>
                </TouchableOpacity>
                <View style={styles.divider} />
            </View>
        );
    };

    let showSearchBox = null;
    if (props.keyName === 'state_list' || props.keyName === 'shipTo_list' || props.keyName === 'shipFrom_list' || props.keyName === 'Item_list' || props.keyName === 'Distribution_list' ) {
        showSearchBox = (
            <View style={{
                width:'100%',
                height: 50,
                alignItems: 'center',
                justifyContent:'center',
                backgroundColor:'lightgray'
               // marginLeft:5
               // marginBottom: 15,
            }}
            >
                <InputText
                    style={{ width: '95%',paddingLeft:5,}}
                    placeholder="Select name"
                    maxLength={15}
                    onChangeText={props.searchFilterFunction}
                    value={props.searchText?props.searchText:props.selectedText}
                    autoCapitalize="words"
                    autoCorrect={false}
                    returnKeyType="done"
                    onSubmitEditing={() => { Keyboard.dismiss(0); }}
                />
            </View>
        );
    } else {
        showSearchBox = (
            <View />
        );
    }


    return (
        <View style={styles.container}>
            {showSearchBox}
            <View style={styles.divider} />
            <View style={{width:'100%',height:'100%'}}>
            <FlatList
                data={filterData}
                renderItem={({ item, index }) => renderItem(item, index)}
                extraData={props}
            />
            </View>
        </View>
    );
};

CommonListComponent.propTypes = {
    data: PropTypes.array,
    keyName: PropTypes.string,
    onRowPress: PropTypes.func,
    searchFilterFunction: PropTypes.func,
    searchText: PropTypes.string,
};

const styles = StyleSheet.create({
    container: {
        //flex: 1,
        alignItems: 'center',
        width:'100%',
        height:'90%',
        borderRadius:10
    },
    divider: {
       // width:'70%',
        height: 1,
        backgroundColor: 'lightgray',
    },
    row: {
        backgroundColor: '#F2F3F8',
        paddingLeft: 30,
        color: '#0F2C5A',
        fontWeight: '600',
        fontSize: normalizeFont(16),
        //marginRight: 30,
    },

});

export default CommonListComponent;
