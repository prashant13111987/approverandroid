/**
 * Created by Rahul Nakate on 11/06/18
 */

import React from 'react';
import {
  StyleSheet,
  Text,
} from 'react-native';
import PropTypes from 'prop-types';
//import { normalizeFont } from '../../../../common/util';


const AmountText = (props) => {
  let value = [];
  let amountText = '0';
  if (props.text) {
    amountText = `${props.text}`;
  }
  if (amountText && amountText.indexOf('.') !== -1) {
    value = amountText.toString().split('.');
  } else if (amountText && parseFloat(amountText) > 0) {
    value[0] = amountText;
    value[1] = '00';
  } else {
    value[0] = '00';
    value[1] = '00';
  }
  return (
    <Text
      allowFontScaling={false}
      style={[styles.textStyle, props.normalTextStyle, props.textStyle]}
    >$<Text style={[styles.mainAmountBoldTextStyle, props.boldTextStyle]}>{value[0]}</Text>
      <Text allowFontScaling={false} style={[styles.textStyle, props.normalTextStyle, props.smallDigitStyle]}>
        {value[1].length === 2 ? `.${value[1]}` : `.${value[1]}0`}
      </Text>
    </Text>
  );
};
AmountText.propTypes = {
  text: PropTypes.string,
  normalTextStyle: PropTypes.object,
  boldTextStyle: PropTypes.object,
  smallDigitStyle: PropTypes.object,
  textStyle: PropTypes.object,
};


const styles = StyleSheet.create({
  buttonStyle: {
    height: 50,
    backgroundColor: '#ff9400',
    borderRadius: 27,
    justifyContent: 'center',
    alignItems: 'center',
  },
  textStyle: {
    color: '#0f2c5a',
   // fontSize: normalizeFont(17),
      fontSize: 12,
    fontWeight: '300',
  },
  mainAmountBoldTextStyle: {
    color: '#0f2c5a',
      fontSize: 12,
    fontWeight: '600',
  },
});

export default AmountText;
