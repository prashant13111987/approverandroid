/**
 * Created by Rahul Nakate on 09/05/18
 */
import React from 'react';
import {
    StyleSheet,
    TouchableOpacity,
    Text,
} from 'react-native';
import PropTypes from 'prop-types';

import {normalizeFont } from '../../../../common/util';
import { withPreventDoubleClick } from '../PreventDoubleClick';

const TouchableOpacityEx = withPreventDoubleClick(TouchableOpacity);

const Button = props => (
    <TouchableOpacityEx
        style={[styles.buttonStyle, props.style]}
        testID={props.testID ? props.testID : props.text}
        onPress={props.onPress}
    >
        <Text allowFontScaling={false} style={[styles.textStyle, props.textStyle]}> {props.text} </Text>
    </TouchableOpacityEx>
);

Button.propTypes = {
    style: PropTypes.object,
    textStyle: PropTypes.object,
    text: PropTypes.string,
    onPress: PropTypes.func,
    testID: PropTypes.string,
};


const styles = StyleSheet.create({
    buttonStyle: {
        height: 35,
        backgroundColor: '#000',
        borderRadius: 4,
        justifyContent: 'center',
        alignItems: 'center',
    },
    textStyle: {
        color: '#00599c',
        fontSize: normalizeFont(16),
        fontWeight: '600',
    },

});

export default Button;
