/**
 * Created by Rahul Nakate on 09/05/18
 */
import React from 'react';
import {
    Platform,
    StyleSheet,
    View,
} from 'react-native';
import PropTypes from 'prop-types';

import {getTextInputHeight, normalizeFont} from '../../../../common/util';

const height = getTextInputHeight();
const CustomView = props => (
  <View
    style={[styles.viewStyle, props.styles]}
  >
    {props.children}
  </View>
);

CustomView.propTypes = {
  children: PropTypes.array,
  styles: PropTypes.object,
};


const styles = StyleSheet.create({
  viewStyle: {
      height: 40,
      borderColor: '#cfcfcf',
      borderRadius: 4,
      borderWidth: 1,
      paddingLeft: 10,
      fontSize: normalizeFont(16),
      color: '#0F2C5A',
      fontWeight: Platform.OS === 'ios' ? '500' : '400',
      backgroundColor: 'white',
  },
});

export default CustomView;
