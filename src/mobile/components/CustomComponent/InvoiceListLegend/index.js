import React from 'react';
import {
    StyleSheet,
    TouchableOpacity,
    Text,
    View

} from 'react-native';
import PropTypes from 'prop-types';

import {normalizeFont } from '../../../../common/util';
import { withPreventDoubleClick } from '../PreventDoubleClick';

const TouchableOpacityEx = withPreventDoubleClick(TouchableOpacity);

const InvoiceListLegend = props => (
    <View style = {[styles.legendView, props.legendView]}>
            <View style = {{flexDirection:'column'}}>
                <View style = {{alignItems:'center',flexDirection:'row'}}>
                <TouchableOpacity
                    style={[styles.buttonStyle, props.buttonStyle]}
                    testID={props.testID ? props.testID : props.text}
                    onPress={props.onPress}
                >
                    <Text style = {[styles.textStyle,props.buttonTextStyle]}>{props.buttonText}</Text>
                </TouchableOpacity>
                <View style = {[styles.legendSeperator,props.legendSeperator]}/>
                </View>
                <View>
                    <View style = {[styles.labelStyle,props.style]}>
                        <Text allowFontScaling={false} style={[styles.legendTextStyle, props.legendTextStyle]}>{props.legendText}</Text>
                    </View>
                </View>
            </View>
        </View>
);

InvoiceListLegend.propTypes = {
    style: PropTypes.object,
    textStyle: PropTypes.object,
    text: PropTypes.string,
    onPress: PropTypes.func,
    testID: PropTypes.string,
    legendText: PropTypes.string,
    enabledStatus:PropTypes.bool,
    buttonTextStyle:PropTypes.object,

};


const styles = StyleSheet.create({
    legendView:{
        flexDirection:'row',
        backgroundColor:'white',
        borderColor:'white',
        borderBottomWidth:2
    },
    buttonStyle: {
        width:35,
        height: 35,
        borderRadius: 20,
        borderWidth:1.5,
        borderColor:'#629ee4',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor:'white'
    },
    labelStyle:{
        marginLeft:0,
        width:45,
    },
    legendTextStyle:{
        color: 'black',
        fontSize: normalizeFont(8),
    },
    textStyle: {
        color: '#00599c',
        fontSize: normalizeFont(16),
    },
    legendSeperator:{
        height:1.5,
        width:28,
        backgroundColor:'#629ee4',
        display:'flex'
    },
    buttonTextStyle:{

    }
});

export default InvoiceListLegend;
