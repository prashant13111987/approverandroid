import React from 'react';
import {
    StyleSheet,
    TouchableOpacity,
    Text,
    View

} from 'react-native';
import PropTypes from 'prop-types';

import { normalizeFont } from '../../../../common/util';
import { withPreventDoubleClick } from '../PreventDoubleClick';

const TouchableOpacityEx = withPreventDoubleClick(TouchableOpacity);

const Legend = props => (
    <View style={[styles.legendView, props.legendView]}>
        <View style={{ width: 30, justifyContent: 'center', alignItems: 'center' }}>
            <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                <TouchableOpacity
                    style={[styles.buttonStyle, props.buttonStyle]}
                    testID={props.testID ? props.testID : props.text}
                    onPress={props.onPress}
                >
                </TouchableOpacity>
                <View style={[styles.legendSeperator, props.legendSeperator]} />
            </View>
        </View>
        <View style={[styles.labelStyle, props.style]}>
            <Text allowFontScaling={false} style={[styles.legendTextStyle, props.legendTextStyle]}>{props.legendText}</Text>
            <Text allowFontScaling={false} style={[styles.legendAmountTextStyle, props.legendTextStyle]}>
                {props.receiveCount}</Text>
            {props.legendText !== 'Master Data' && <Text allowFontScaling={false} style={[styles.legendSubTextStyle, props.legendTextStyle]}>
                ₹{props.amount.toFixed(2)}</Text>}
        </View>
    </View>
);

Legend.propTypes = {
    style: PropTypes.object,
    textStyle: PropTypes.object,
    text: PropTypes.string,
    onPress: PropTypes.func,
    testID: PropTypes.string,
    legendText: PropTypes.string,
    enabledStatus: PropTypes.bool

};


const styles = StyleSheet.create({
    legendView: {
        flexDirection: 'row'
    },
    buttonStyle: {
        width: 18,
        height: 18,
        borderRadius: 10,
        borderWidth: 1.5,
        borderColor: '#629ee4',
        justifyContent: 'center',
        alignItems: 'center',
    },
    labelStyle: {
        marginLeft: 0,
        width: 70,
        top: -5

    },
    legendTextStyle: {
        color: '#1a1a1a',
        fontSize: normalizeFont(12),
        fontWeight: '600'
    },
    legendSubTextStyle: {
        color: '#555555',
        fontSize: normalizeFont(12),
        width: 90

    },
    textStyle: {
        color: '#00599c',
        fontSize: normalizeFont(16),
    },
    legendSeperator: {
        height: 35,
        width: 1.5,
        backgroundColor: '#629ee4',
        display: 'flex'
    },
    legendAmountTextStyle: {
        color: '#555555',
        fontSize: normalizeFont(12),
        fontWeight: '600'
    }
});

export default Legend;
