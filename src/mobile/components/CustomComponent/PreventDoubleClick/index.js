/* eslint-disable react/sort-comp,no-unused-expressions */
/**
 * Created by Rahul Nakate on 11/07/18
 */
import React from 'react';
import debounce from 'lodash.debounce'; // 4.0.8
import PropTypes from 'prop-types';


export const withPreventDoubleClick = (WrappedComponent, time = 1000) => {
    class PreventDoubleClick extends React.PureComponent {
        debouncedOnPress = () => {
            this.props.onPress && this.props.onPress();
        }

        onPress = debounce(this.debouncedOnPress, time, { leading: true, trailing: false });

        render() {
            return <WrappedComponent {...this.props} onPress={this.onPress} />;
        }
    }

    PreventDoubleClick.propTypes = {
        onPress: PropTypes.func,
    };

    PreventDoubleClick.displayName = `withPreventDoubleClick(${WrappedComponent.displayName || WrappedComponent.name})`;
    return PreventDoubleClick;
};

// export default withPreventDoubleClick;
