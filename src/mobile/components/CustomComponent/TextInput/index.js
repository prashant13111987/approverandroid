/**
 * Created by Prashant Pawar
 */
import React from 'react';
import {
    StyleSheet,
    TextInput,
    Platform,
} from 'react-native';
import PropTypes from 'prop-types';
import { normalizeFont } from '../../../../common/util';


const InputText = props => (
    <TextInput
        style={[styles.textInputStyle, props.style]}
        placeholder={props.placeholder}
        value={props.value}
        placeholderTextColor="#6B7889"
        secureTextEntry={props.secureTextEntry ? props.secureTextEntry : false}
        onChangeText={props.onChangeText}
        autoCapitalize={props.autoCapitalize}
        autoCorrect={props.autoCorrect}
        maxLength={props.maxLength}
        keyboardType={props.keyboardType}
        underlineColorAndroid="transparent"
        returnKeyType={props.returnKeyType}
        textAlignVertical='top'
        numberOfLines={props.numberOfLines}
        ref={input =>
            props.onTextInputCreated(props.name, input)
        }
        onSubmitEditing={props.onSubmitEditing}
        testID={props.testID ? props.testID : props.placeholder}
        accessible
        accessibleLabel="testId-index-view"
        editable={props.editable}
        multiline={props.multiline}
        blurOnSubmit={false}
        allowFontScaling={false}
    />
);

InputText.propTypes = {
    placeholder: PropTypes.string,
    value: PropTypes.string,
    secureTextEntry: PropTypes.bool,
    autoCapitalize: PropTypes.string,
    autoCorrect: PropTypes.bool,
    style: PropTypes.object,
    onChangeText: PropTypes.func,
    maxLength: PropTypes.string,
    keyboardType: PropTypes.string,
    returnKeyType: PropTypes.string,
    onTextInputCreated: PropTypes.func,
    name: PropTypes.string,
    onSubmitEditing: PropTypes.func,
    testID: PropTypes.string,
    editable: PropTypes.bool,
    multiline: PropTypes.bool,
};

InputText.defaultProps = {
    onTextInputCreated: () => null,
};


const styles = StyleSheet.create({
    textInputStyle: {
        height: 40,
        borderColor: '#cfcfcf',
        //borderColor: '#0d60aa',
        borderRadius: 4,
        borderWidth: 1,
        paddingLeft: 10,
        // fontSize: 14,
        fontFamily: 'System',
        fontSize: normalizeFont(14),
        color: '#0F2C5A',
        // fontWeight: Platform.OS === 'ios' ? '500' : '400',
        backgroundColor: 'white',
    },

});

export default InputText;
