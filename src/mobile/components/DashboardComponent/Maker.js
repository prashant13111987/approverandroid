/* eslint-disable global-require */
/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React from 'react';
import {
    StyleSheet,
    View,
    Text,
    Dimensions,
    TouchableOpacity
} from 'react-native';
import PropTypes from 'prop-types';
import ChartContainer from '../../containers/ChartContainer';
 import CollapsableDashboardListContainer from '../CollapsableDashboardListComponent';
import Legend from "../CustomComponent/Legend";

const { width } = Dimensions.get('window');
var heightValue = 0;
const DashboardComponentMaker = props => {
    find_dimesions = (layout) => {
        const { x, y, width, height } = layout;
        heightValue = height
    }
    return (
        <View style={styles.container}>
            <View style={styles.tabBackgroundView}>
                <View style={styles.sourceLabel}>
                    <Text style={{ fontSize: 13 }}>Source</Text>
                </View>
                <View style={styles.tabButtonsView}>
                    <TouchableOpacity
                        style={props.selectedSource === 'all' ? styles.selectedAllButton : styles.allButton}
                        onPress={() => props.onSelectSource('all')}
                    >
                        <Text
                            style={props.selectedSource === 'all' ? styles.selectedTabButtonsTextStyle : styles.tabButtonsTextStyle}>All</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                        style={props.selectedSource === 'phone' ? styles.selectedPhoneButton : styles.phoneButton}
                        onPress={() => props.onSelectSource('phone')}>
                        <Text
                            style={props.selectedSource === 'phone' ? styles.selectedTabButtonsTextStyle : styles.tabButtonsTextStyle}>Phone</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                        style={props.selectedSource === 'mail' ? styles.selectedMailButton : styles.mailButton}
                        onPress={() => props.onSelectSource('mail')}>
                        <Text
                            style={props.selectedSource === 'mail' ? styles.selectedTabButtonsTextStyle : styles.tabButtonsTextStyle}>Mail</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                        style={props.selectedSource === 'app' ? styles.selectedAppButton : styles.appButton}
                        onPress={() => props.onSelectSource('app')}>
                        <Text
                            style={props.selectedSource === 'app' ? styles.selectedTabButtonsTextStyle : styles.tabButtonsTextStyle}>App</Text>
                    </TouchableOpacity>
                </View>
            </View>
            <View style={styles.chartBackgroundView}>
                <View style={styles.chartView}>
                    <View style={{ width: '100%', height: '85%', flexDirection: 'row' }}>
                        <View onLayout={(event) => {
                            this.find_dimesions(event.nativeEvent.layout)
                        }} style={{ width: '40%', backgroundColor: 'white', height: '100%', justifyContent: 'center', paddingTop: '5%' }}>
                            <Legend
                                legendView={{ height: heightValue > 0 ? heightValue / 6 : 55 }}
                                buttonStyle={props.isReceived ? { backgroundColor: '#629ee4' } : { backgroundColor: 'white' }}
                                receivedInvoiceList={props.receivedInvoiceList}
                                receiveCount={props.receiveCount}
                                amount={props.totalAmountObject ? props.totalAmountObject.received : 0}
                                onPress={() => props.onSelectStatus('Received')}
                                legendText='Received'
                                legendSeperator={{ height: heightValue > 0 ? (heightValue / 7) : 55 }} />
                            <Legend
                                legendView={{ height: heightValue > 0 ? heightValue / 6 : 55 }}
                                buttonStyle={props.isScanned ? { backgroundColor: '#004C7F' } : { backgroundColor: 'white' }}
                                onPress={() => props.onSelectStatus('Created')}
                                amount={props.totalAmountObject ? props.totalAmountObject.create : 0}
                                receiveCount={props.ststusBillObject.create ? props.ststusBillObject.create.length : 0}
                                legendText='Created'
                                legendSeperator={{ height: heightValue > 0 ? (heightValue / 7) : 55 }} />
                            <Legend
                                legendView={{ height: heightValue > 0 ? heightValue / 6 : 55 }}
                                buttonStyle={props.isPending ? { backgroundColor: '#002640' } : { backgroundColor: 'white' }}
                                onPress={() => props.onSelectStatus("Pending")}
                                amount={props.totalAmountObject ? props.totalAmountObject.pending : 0}
                                receiveCount={props.ststusBillObject.pending ? props.ststusBillObject.pending.length : 0}
                                legendText="Pending"
                                legendSeperator={{ height: heightValue > 0 ? (heightValue / 7) : 55 }} />
                            <Legend
                                legendView={{ height: heightValue > 0 ? heightValue / 6 : 55 }}
                                buttonStyle={props.isRejected ? { backgroundColor: '#0072BF' } : { backgroundColor: 'white' }}
                                onPress={() => props.onSelectStatus('Rejected')}
                                amount={props.totalAmountObject ? props.totalAmountObject.rejected : 0}
                                receiveCount={props.ststusBillObject.rejected ? props.ststusBillObject.rejected.length : 0}
                                legendText='Rejected'
                                legendSeperator={{ height: heightValue > 0 ? (heightValue / 7) : 55 }} />
                            <Legend
                                legendView={{ height: heightValue > 0 ? heightValue / 6 : 55 }}
                                buttonStyle={props.isOnhold ? { backgroundColor: '#44A5E5' } : { backgroundColor: 'white' }}
                                onPress={() => props.onSelectStatus('On hold')}
                                amount={props.totalAmountObject ? props.totalAmountObject.onHold : 0}
                                receiveCount={props.ststusBillObject.onHold ? props.ststusBillObject.onHold.length : 0}
                                legendText='On hold'
                                legendSeperator={{ height: heightValue > 0 ? (heightValue / 7) : 55 }} />
                            <Legend
                                legendView={{ height: heightValue > 0 ? heightValue / 6 : 55 }}
                                buttonStyle={props.isApproved ? { backgroundColor: '#3fd1d1' } : { backgroundColor: 'white' }}
                                onPress={() => props.onSelectStatus('Approved')}
                                amount={props.totalAmountObject ? props.totalAmountObject.approval : 0}
                                receiveCount={props.ststusBillObject.approval ? props.ststusBillObject.approval.length : 0}
                                legendText='Approved'
                                legendSeperator={{ backgroundColor: 'white', height: heightValue > 0 ? (heightValue / 7) : 55 }}
                            />
                        </View>
                        <ChartContainer
                            legend={props.legend}
                            onRemoveSource={this.onRemoveSource}
                            data={props.data}
                            highlights={props.highlights}
                            description={props.description}
                            originalData={props.originalData}
                            totalAmount={props.totalAmount}
                            totalInvoiceCount={props.totalInvoiceCount}
                            pieChartDataValues={props.pieChartDataValues}
                        />
                    </View>
                    <View style={styles.chartFooterView}>
                        <View style={styles.separator}>
                        </View>
                        <View style={styles.chartFooterButtonsGroup}>
                            <View style={styles.chartFooterButtonsGroupLeft}>
                                <TouchableOpacity style={props.activeScreen === 'approver' ? [styles.viewButtonToggleRight, styles.viewToggleButtonActive] : styles.viewButtonToggleRight}
                                    onPress={() => props.onViewChange('approver')}>
                                    <Text style={props.activeScreen === 'approver' ? [styles.viewText, styles.viewToggleButtonActiveText] : styles.viewText}>APPROVER  </Text>
                                </TouchableOpacity>
                                <TouchableOpacity style={props.activeScreen === 'maker' ? [styles.viewButtonToggleLeft, styles.viewToggleButtonActive] : styles.viewButtonToggleLeft}
                                    onPress={() => props.onViewChange('maker')}>
                                    <Text style={props.activeScreen === 'maker' ? [styles.viewText, styles.viewToggleButtonActiveText] : styles.viewText}>MAKER  </Text>
                                </TouchableOpacity>
                            </View>
                            <View style={styles.chartFooterButtonsGroupRight}>
                                <TouchableOpacity style={styles.viewButton}
                                    onPress={props.onAllPress}>
                                    <Text style={styles.viewText}>VIEW</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </View>
                </View>
            </View>
            <View style={styles.collapsableListView}>
                <CollapsableDashboardListContainer />
            </View>
        </View>
    )

};

DashboardComponentMaker.propTypes = {
    onSelectSource: PropTypes.func,
    onSelectStatus: PropTypes.func,
    selectedSource: PropTypes.object,
    legend: PropTypes.object,
    data: PropTypes.object,
    highlights: PropTypes.array,
    description: PropTypes.object,
    originalData: PropTypes.object,
    pieChartDataValues: PropTypes.object,
    isReceived: PropTypes.bool,
    isScanned: PropTypes.bool,
    isPending: PropTypes.bool,
    isRejected: PropTypes.bool,
    isOnhold: PropTypes.bool,
    isApproved: PropTypes.bool,
    isCompleted: PropTypes.bool,
};


const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        backgroundColor: '#eeeeee',
    },
    tabBackgroundView: {
        width: '95%',
        height: '6%',
        alignItems: 'center',
        flexDirection: 'row',
        justifyContent: 'space-between',
        backgroundColor: '#eeeeee'
    },
    sourceLabel: {
        backgroundColor: '#eeeeee',
        width: '20%',
        height: '95%',
        justifyContent: 'center'
    },
    tabButtonsView: {
        width: '80%',
        flexDirection: 'row',
        height: '60%',
        alignItems: 'center',
        borderRadius: 30,
        backgroundColor: '#eeeeee'
    },
    allButton: {
        width: '25%',
        borderTopLeftRadius: 30,
        borderBottomLeftRadius: 30,
        backgroundColor: '#f7f7f7',
        height: '100%',
        alignItems: 'center',
        justifyContent: 'center'
    }
    ,
    selectedAllButton: {
        width: '25%',
        borderTopLeftRadius: 30,
        borderBottomLeftRadius: 30,
        backgroundColor: '#0059a6',
        height: '100%',
        alignItems: 'center',
        justifyContent: 'center'
    },
    phoneButton: {
        width: '25%',
        backgroundColor: '#f7f7f7',
        height: '100%',
        alignItems: 'center',
        justifyContent: 'center'
    },
    selectedPhoneButton: {
        width: '25%',
        backgroundColor: '#0059a6',
        height: '100%',
        alignItems: 'center',
        justifyContent: 'center'
    },
    mailButton: {
        width: '25%',
        backgroundColor: '#f7f7f7',
        height: '100%',
        alignItems: 'center',
        justifyContent: 'center'
    },
    selectedMailButton: {
        width: '25%',
        backgroundColor: '#0059a6',
        height: '100%',
        alignItems: 'center',
        justifyContent: 'center'
    },
    appButton: {
        width: '25%',
        borderTopRightRadius: 30,
        borderBottomRightRadius: 30,
        backgroundColor: '#f7f7f7',
        height: '100%',
        alignItems: 'center',
        justifyContent: 'center'
    },
    selectedAppButton: {
        width: '25%',
        borderTopRightRadius: 30,
        borderBottomRightRadius: 30,
        backgroundColor: '#0059a6',
        height: '100%',
        alignItems: 'center',
        justifyContent: 'center'
    },
    tabButtonsTextStyle: {
        color: '#0059a6',
        fontWeight: '600',
        fontSize: 13
    },
    selectedTabButtonsTextStyle: {
        color: 'white',
        fontWeight: '600',
        fontSize: 13
    },
    chartBackgroundView: {
        backgroundColor: '#eeeeee',
        width: '100%',
        height: '60%',
        alignItems: 'center'
    },
    chartView: {
        backgroundColor: 'white',
        width: '95%',
        height: '100%',
        borderColor: '#dadada',
        borderRadius: 5,
        borderWidth: 1
    },
    chartFooterView: {
        width: '100%',
        height: '15%',
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center'
    },
    chartFooterButtonsGroup: {
        flex: 1,
        // alignItems: 'stretch',
        flexDirection: 'row',
        width: '100%',
        // height: '15%',
        // justifyContent: 'space-evenly'
    },
    chartFooterButtonsGroupLeft: {
        width: '70%',
        flexDirection: 'row',
        // justifyContent: 's'

    },
    chartFooterButtonsGroupRight: {
        width: '30%',
        // height: '15%',
        // justifyContent: 'space-evenly'

    },
    separator: {
        width: '98%',
        height: 0.6,
        backgroundColor: '#a0a0a0'
    },
    viewButtonToggleRight: {
        width: '40%',
        height: '70%',
        borderRadius: 35,
        borderTopRightRadius: 0,
        borderBottomRightRadius: 0,
        borderColor: '#4785bd',
        borderWidth: 1,
        backgroundColor: 'white',
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: 5,
        marginLeft: 20
    },
    viewButtonToggleLeft: {
        width: '40%',
        height: '70%',
        borderRadius: 35,
        borderTopLeftRadius: 0,
        borderBottomLeftRadius: 0,
        borderColor: '#4785bd',
        borderWidth: 1,
        backgroundColor: 'white',
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: 5,
        marginLeft: 0
    },
    viewToggleButtonActive: {
        backgroundColor: '#0059a6',
    },
    viewToggleButtonActiveText: {
        color: 'white',
        fontWeight: '600',
        fontSize: 13
    },
    viewButton: {
        width: '80%',
        height: '70%',
        borderRadius: 35,
        borderColor: '#4785bd',
        borderWidth: 1,
        backgroundColor: 'white',
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: 5
    },
    viewText: {
        fontSize: 10,
        fontWeight: 'bold',
        color: '#4785bd'
    },
    collapsableListView: {
        backgroundColor: '#eeeeee',
        width: '100%',
        height: '40%',
        paddingBottom: 50
    }

});
export default DashboardComponentMaker;
