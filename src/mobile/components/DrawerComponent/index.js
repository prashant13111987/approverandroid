

import React from 'react';
import {
    StyleSheet,
    Text,
    View,
    SectionList,
    TouchableHighlight,
    Image,
} from 'react-native';
import PropTypes from 'prop-types';


// type Props = {};
const DrawerComponent = (props) => {
    const renderListItem = (item) => {
        return (
            <TouchableHighlight onPress={() => props.handleItemClick(item)}>
                <View key={`parentView${item}`} style={styles.listItemParent}>
                    <Text key={`topicCat${item}`} style={styles.listTextStyle}>{item}</Text>
                </View>
            </TouchableHighlight>);
    };

    const renderListHeader = title => (
        <View key={`parentView${title}`} style={styles.listHeaderParent}>
            <Text key={`topicCat${title}`} style={styles.headerTextStyle} writingDirection="rtl">{title}</Text>
        </View>
    );

    return (
        <View style={styles.container}>
            <SectionList
                key="flatlistexample"
                sections={props.sectionList}
                renderItem={({ item, index, section }) => renderListItem(item, index, section)}
                renderSectionHeader={({ section: { title } }) => renderListHeader(title)}
                keyExtractor={(item, index) => item + index}
            />
        </View>
    );
};

DrawerComponent.propTypes = {
    handleItemClick: PropTypes.func,
    sectionList: PropTypes.array,
};


const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        backgroundColor: 'white',
        paddingTop: 50,
    },
    listTextStyle: {
        color: 'rgba(47,168,220,1)',
        marginLeft: 20,
        fontSize: 16,
        fontWeight: '600',
    },
    listImageStyle: {
        marginLeft: 16,
        height: 30,
        width: 30,
        //resizeMode: Image.resizeMode.contain,
    },
    listItemParent: {
        height: 50,
        width: 300,
        backgroundColor: '#FFFFFF',
        flex: 1,
        alignItems: 'center',
        borderBottomWidth: 1,
        borderBottomColor: 'rgba(198,213,227,1)',
        flexDirection: 'row',
    },
    listHeaderParent: {
        height: 40,
        width: 300,
        backgroundColor: '#FFFFFF',
        flex: 1,
        alignItems: 'center',
        borderBottomWidth: 1,
        borderBottomColor: 'rgba(198,213,227,1)',
        flexDirection: 'row',
    },
    instructions: {
        textAlign: 'center',
        color: '#333333',
        marginBottom: 5,
    },
    headerTextStyle: {
        textAlign: 'center',
        color: 'rgba(103,53,144,1)',
        fontSize: 18,
        fontWeight: 'bold',
        marginLeft: 16,
        fontStyle: 'italic',
    },
});
export default DrawerComponent;
