// Import libraries for making a component
import React from 'react';
import { Text, View, Dimensions, Image } from 'react-native';
import { TouchableOpacity } from 'react-native';
import menu from '../../../common/images/menu.png'
var { height, width } = Dimensions.get('window');

// Make a component
const TopHeader = (props) => {
    const { textStyle, viewStyle, imageStyle } = styles;

    return (
        <View style={viewStyle}>
            <TouchableOpacity
                style={{ width: '20%'}}
                onPress={() => {
                    props.onDrawerPress();
                }}
            >
                <View style={{height: '90%', alignItems: 'center', justifyContent: 'center'}}>
                    <Image
                        style={{ width: 15, height: 15, alignSelf: 'center' }}
                        source={menu}
                    />
                </View>
            </TouchableOpacity>

            <View style={{ width: '60%', alignItems: 'center'}}>
                <Text style={textStyle}>{props.headerText}</Text>
            </View>
        </View>
    );
};



const styles = {
    viewStyle: {
        //#F2F2F2
        //#3791BE
        //#393939
        backgroundColor: '#3791BE',
        height: 70,
        paddingTop: 15,
        width: '100%',
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'center'
    },
    textStyle: {
        fontSize: 20,
        textAlign:'center',
        color: 'white',
    },
    imageStyle: {
        color: 'white',
    }
};

// Make the component available to other parts of the app
export default TopHeader;
