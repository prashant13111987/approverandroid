/* eslint-disable global-require */
/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React from 'react';
import {
    StyleSheet,
    View,
    Dimensions,
    Text,
    Image,
    TouchableOpacity,
    TouchableHighlight,
    TextInput,
    TouchableWithoutFeedback, Keyboard
} from 'react-native';
const { width ,height} = Dimensions.get('window');
import PropTypes from 'prop-types';
import InputText from '../CustomComponent/TextInput'
import CustomView from '../CustomComponent/CustomBorderView'
import nextImage from '../../../common/images/nextArrow.png';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'

const GSTInvoiceComponent = props => {
    return(
        <KeyboardAwareScrollView
            scrollEnabled
            showsVerticalScrollIndicator={true}
            enableOnAndroid
            style ={{height:'80%'}}
            keyboardShouldPersistTaps="handled"
            enableResetScrollToCoords={true}
        >
        <View style = {{alignItems:'center',paddingTop:10,backgroundColor:'white',paddingBottom:40 ,}}>
            <View style = {{width:'80%',marginTop:10 }}>
                <Text style = {styles.validatePlaceHolder}>Ship To</Text>
                <TouchableOpacity
                    onPress={props.onShipToPress}
                    style={{ marginTop: 4}}
                >
                    <CustomView styles={{
                        flexDirection: 'row',
                        justifyContent: 'space-between',
                       // backgroundColor: 'white',
                    }}
                    >
                        <Text
                            allowFontScaling={false}
                            style={styles.listItemText}
                        >{props.shipTo}
                        </Text>
                        <Image
                            source={nextImage}
                            style={styles.forwardArrowImageStyle}
                        />
                    </CustomView>
                </TouchableOpacity>
            </View>
            <View style = {{width:'80%',marginTop:10}}>
                <Text style = {styles.validatePlaceHolder}>GSTIN</Text>
                <InputText
                    style={{ marginTop: 4}}
                    onChangeText={props.handleShipToGSTIN}
                    value={props.buyerGstin}
                    autoCapitalize="none"
                    autoCorrect={false}
                    testID="cgstAmount"
                    maxLength={50}
                    blurOnSubmit={false}
                    editable={props.isEditable}
                    name="cgstAmount"
                    onTextInputCreated={props.setInputReference}
                    returnKeyType="next"
                    onSubmitEditing={() => props.onSubmitEditing('igstAmount')}
                />
            </View>
            <View style = {{width:'80%',marginTop:20}}>
                <Text style = {styles.validatePlaceHolder}>Ship From</Text>
                <TouchableOpacity
                    onPress={props.onShipFromPress}
                    style={{ marginTop: 4}}
                >
                    <CustomView styles={{
                        flexDirection: 'row',
                        justifyContent: 'space-between',
                        // backgroundColor: 'white',
                    }}
                    >
                        <Text
                            allowFontScaling={false}
                            style={styles.listItemText}
                        >{props.shipFrom}
                        </Text>
                        <Image
                            source={nextImage}
                            style={styles.forwardArrowImageStyle}
                        />
                    </CustomView>
                </TouchableOpacity>
            </View>

            <View style = {{width:'80%',marginTop:20}}>
                <Text style = {styles.validatePlaceHolder}>GSTIN</Text>
                <InputText
                    style={{ marginTop: 4}}
                    onChangeText={props.handleShipFromGSTFrom}
                    value={props.venderGstin}
                    autoCapitalize="none"
                    autoCorrect={false}
                    testID="igstAmount"
                    maxLength={50}
                    name="igstAmount"
                    blurOnSubmit={false}
                    editable={props.isEditable}
                     onTextInputCreated={props.setInputReference}
                     returnKeyType="next"
                     onSubmitEditing={() => props.onSubmitEditing('ebillNumber')}

                />
            </View>
            <View style = {{width:'80%',marginTop:20}}>
                <Text style = {styles.validatePlaceHolder}>E Way Bill Number</Text>
                <InputText
                    style={{ marginTop: 4}}
                    onChangeText={props.handleIGSTAmountChange}
                    value={props.eWayBill}
                    autoCapitalize="none"
                    autoCorrect={false}
                    maxLength={50}
                    name="ebillNumber"
                    blurOnSubmit={false}
                    editable={props.isEditable}
                    onTextInputCreated={props.setInputReference}
                    returnKeyType="done"
                    onSubmitEditing={() => { Keyboard.dismiss(); }}
                />
            </View>
        </View>
        </KeyboardAwareScrollView>
    )};

const styles = StyleSheet.create({
    textInput:{
        marginTop:5,
        paddingLeft:10,
        borderColor:'#0d60aa',
        borderRadius:0,
        height:30
    },
    validatePlaceHolder:{color:'rgba(98,98,98,1)',fontWeight:'600', fontSize:12},
    forwardArrowImageStyle: {
        width: 15,
        height: 15,
      //  resizeMode: Image.resizeMode.contain,
        alignSelf: 'center',
        marginRight: 15,
    },
    listItemText: {
        color: '#0F2C5A',
        fontSize: 14,
        fontFamily: 'System',
       // fontWeight: Platform.OS === 'ios' ? '500' : '400',
        //marginLeft: 16,
        alignSelf: 'center',
        width: width - 150,

    },

})

GSTInvoiceComponent.propTypes = {
    handleTotalGSTAmountChange :PropTypes.func,
    handleCGSTAmountChange :PropTypes.func,
    handleSGSTAmountChange :PropTypes.func,
    handleIGSTAmountChange :PropTypes.func,
    onSubmitEditing :PropTypes.func,
    setInputReference :PropTypes.func,
    focusNextField :PropTypes.func,
    totalGSTAmount:PropTypes.string,
    cgstAmount:PropTypes.string,
    sgstAmount:PropTypes.string,
    igstAmount:PropTypes.string,

};

export default GSTInvoiceComponent;
