/* eslint-disable global-require */
/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React from 'react';
import {
    StyleSheet,
    View,
    Dimensions,
    Text,
    Image,
    TouchableOpacity,
    KeyboardAvoidingView, Keyboard,
    TouchableWithoutFeedback,
} from 'react-native';
import PropTypes from 'prop-types';

import InputText from '../CustomComponent/TextInput'
import Button from '../CustomComponent/Button';
import logo from '../../../common/images/logo.png'
import { normalizeFont } from '../../../common/util';



const { width } = Dimensions.get('window');
const ImageComponent = props => (
    <TouchableWithoutFeedback onPress={() => { Keyboard.dismiss(0); }}>
        <KeyboardAvoidingView style={styles.container} enable>
            <View style={styles.headerContainer}>
                <Image source={logo} style={{ height: 60, width: 60, }} />
            </View>
        </KeyboardAvoidingView>
    </TouchableWithoutFeedback>

);

ImageComponent.propTypes = {
};


const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        backgroundColor: '#F2F3F8',
    },
    divider: {
        height: 1,
        backgroundColor: 'rgba(231,231,231,1)',
    },
    headerContainer: {
        flex: 1,
        width,
        alignItems: 'center',
        backgroundColor: '#F2F3F8',
        justifyContent: 'center',
    },
    loginBoxContainer: {
        flex: 1,
        paddingLeft: 40,
        paddingRight: 40,
        backgroundColor: '#F2F3F8',
        justifyContent: 'center',
        width,
    },
    forgetPasswordParentStyle: {
        flex: 1,
        backgroundColor: '#F2F3F8',
        width,
        paddingLeft: 40,
        paddingRight: 40,
    },
    forgotPasswordTextStyle: {
        color: '#6B7889',
        justifyContent: 'center',
        fontSize: normalizeFont(16),
    },
    getHelpButtonTextStyle: {
        fontWeight: '700',
        fontSize: normalizeFont(18),
        textAlign: 'center',
        color: '#FF9400',
    },
    headerTitle: {
        color: 'black',
        fontSize: normalizeFont(36),

        fontWeight: '300',
        fontFamily: 'System',
        marginTop: 42,
    },
    facebookButtonStyle: {
        backgroundColor: 'rgba(61,90,150,1)',
        height: 45,
        borderRadius: 4,
        marginTop: 16,
        justifyContent: 'center',
        alignItems: 'center',
    },
    facebookParentStyle: {
        flex: 1,
        backgroundColor: '#F2F3F8',
        justifyContent: 'space-between',

    },
});
export default ImageComponent;
