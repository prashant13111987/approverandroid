import React, { Component } from 'react';
import { Modal, Text, TouchableHighlight, View, Alert, Dimensions } from 'react-native';
import ImageView from 'react-native-image-view';
const { width, height } = Dimensions.get('window');

export default class ImageViewer extends Component {
	state = {
		modalVisible: false
	};

	setModalVisible(visible) {
		this.props.isImageCliked;
		this.setState({ modalVisible: this.props.isImageCliked ? this.props.isImageCliked : false });
	}

	render() {
		const images = [
			{
				source: {
					uri: this.props.imageUrl
				},
				title: '',
				width: width,
				height: height/1.5
			}
		];

		return (<ImageView
					images={images}
					imageIndex={0}
                    isVisible={this.props.isImageCliked}
                    onClose={this.props.onImageClikedClosed}
					renderFooter={(currentImage) => (
						<View>
							<Text>My footer</Text>
						</View>
					)}
				/>
		);
	}
}
