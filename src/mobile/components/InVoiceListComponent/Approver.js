/**
 * Created by synerzip on 025/06/18.
 */
/* eslint-disable global-require */
/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React from 'react';
import {
	StyleSheet,
	View,
	Dimensions,
	Text,
	Button,
	TouchableOpacity,
	FlatList,
	Keyboard,
	Platform,
	Image,
	Animated,
	TouchableHighlight
} from 'react-native';

import PropTypes from 'prop-types';
import { normalizeFont } from '../../../common/util';
import logo from '../../../common/images/logo.png';
import email from '../../../common/images/email.png';
import imageIcon from '../../../common/images/ic_image.png';
import calender from '../../../common/images/ic_listItem_date.png';
import status from '../../../common/images/ic_listItem_status.png';
import emailInvoice from '../../../common/images/ic_mail.png';
import paid from '../../../common/images/paid.png';
import partialPaid from '../../../common/images/paid_partial.png';
import unpaid from '../../../common/images/unpaid.png';
import appInvoice from '../../../common/images/logo_brand.png';
import mobileInvoice from '../../../common/images/ic_handset.png';
import camera from '../../../common/images/bt_capture.png';
import chat from '../../../common/images/ic_chat.png';
import call from '../../../common/images/ic_call.png';
import gallary from '../../../common/images/gallery.png';
import more from '../../../common/images/ellipsis.png';
import Cancel from '../../../common/images/icons-cancel.png';
import Hand from '../../../common/images/icons-hand.png';
import User from '../../../common/images/ic_user.png';
import add from '../../../common/images/btn_add.png';
import cancel from '../../../common/images/btn_cancel.png';
import InvoiceListLegend from './../CustomComponent/InvoiceListLegend';
import config from '../../../common/config';
import { Popover, PopoverController } from 'react-native-modal-popover';
import Moment from 'react-moment';
import moment from 'moment';
import _ from 'lodash';
// invoice_no:reciveItem.invoice_no,
//     created_at:reciveItem.created_at,
//     statusText:"Created"
const { width, height } = Dimensions.get('window');

const ApproverInVoiceList = (props) => {
	// onCreatePress={this.onCreatePress}
	// onEditPress={this.onEditPress}
	// onViewPress={this.onViewPress}

	const createButton = (item, index) => {

		return (
			<View>
				<TouchableOpacity
					style={{ height: '75%', alignItems: 'center', justifyContent: 'center', width: '100%' }}
					onPress={() => props.onShowModalPress(index, 'approve')}
				>
					<Text style={{ fontSize: 12 }}>APPROVE</Text>
				</TouchableOpacity>
			</View>
		);
	};

	const renderItem = (item, index) => {
		//  console.log("Render Item Object ::::::::::::",config.USER_TOKEN_DETAILS.loginId)
		const { onShowModalPress, selectedInvoice } = props
		let invoiceSource = null;
		let name = null;
		let partyname = null;
		let invoicelist = props.venderList;
		let approvalDate = null;

		if (item.source_name === 's_invoice') {
			invoiceSource = logo;
			name = item.invoice_no;
			_.each(invoicelist, (reciveItem) => {
				if (item.vendor_id === reciveItem.id) {
					partyname = reciveItem.name;
				}
			});
			// partyname = item.vendor_id
		} else if (item.source_name === 'email_invoice') {
			invoiceSource = emailInvoice;
			name = item.seller_name;
			partyname = item.vendor_name;
		} else if (item.source_name === 'uploaded_invoice') {
			invoiceSource = mobileInvoice;
			name = item.file_name;
			partyname = item.file_name;
		} else {
			_.each(invoicelist, (reciveItem) => {
				if (item.partyname === reciveItem.id) {
					partyname = reciveItem.name;
				}
			});
		}
		if (item.statusText == 'Approved') {
			if (item.payment_status === 0) {
				invoiceSource = unpaid;
			} else if (item.payment_status === 1) {
				invoiceSource = paid;
			} else if (item.payment_status === 2) {
				invoiceSource = partialPaid;
			}
			approvalDate = item.approval_date ? item.approval_date : 'NA';
		}
		// Button Text
		// if(item.statusText == "Created" || item.statusText == "Rejected")
		// {
		//     console.log("Login Sucess Id",props.loginSuccess)
		//     if(item.creator === config.USER_TOKEN_DETAILS.loginId){
		//         buttonText = "EDIT"
		//     }else{
		//         buttonText = "VIEW"
		//     }
		// }else  if(item.statusText == "Pending" || item.statusText == "On Hold" || item.statusText == "Approved"){
		//     buttonText = "VIEW"
		// }else{
		//     buttonText = "CREATED"
		// }

		// <Text style={styles.textStyleRecived}>{item.statusText?item.statusText:"Received"}</Text>
		//<Text style={{ fontSize:12,}}>{item.statusText ?"VIEW":"CREATE"}</Text>
		let amount = item.total ? item.total : null;
		let amountInRs = null;
		if (amount) {
			amountInRs = amount ? `₹${parseFloat(amount).toFixed(0)}` : null;
		}

		// createButton(item);

		const createdDate = item.statusText
			? moment(item.received_date).format('DD/MM/YYYY')
			: moment(item.submission_date).format('DD/MM/YYYY');
		return (
			<View style={{ backgroundColor: '#eeeeee' }}>
				<TouchableOpacity style={{ alignItems: 'center' }} onPress={() => props.onRowPress(item)}>
					<View
						style={{
							flex: 1,
							flexDirection: 'row',
							width: '98%',
							height: 70,
							alignItems: 'flex-start',
							marginTop: 5,
							borderRadius: 10
						}}
					>
						<View style={{ backgroundColor: 'white', flexDirection: 'column', width: '80%' }}>
							<View
								style={{
									backgroundColor: 'white',
									flexDirection: 'row',
									width: '100%',
									height: '30%',
									justifyContent: 'space-between',
									alignItems: 'center'
								}}
							>
								<Text style={styles.textStyleEmail} numberOfLines={1} ellipsizeMode="tail">
									{(item.party_name || item.bank_name || item.name) ? item.party_name || item.bank_name || item.name : 'No Name found'}
								</Text>
								<View style={{ flexDirection: 'row' }}>
									<Text style={styles.textStyleCategory}>{item.category ? item.category.toUpperCase() : ''}</Text>
									<Image
										source={item.statusText == 'Approved' ? paid : item.statusText ? paid : ''}
										style={{
											width: 20,
											height: 15,
											marginRight: 5,
											resizeMode: 'contain'
										}}
									/>
									<Image
										source={
											item.statusText == 'Approved' ? (
												invoiceSource
											) : item.statusText ? (
												''
											) : (
														invoiceSource
													)
										}
										style={{
											width: 20,
											height: 15,
											marginRight: 10,
											resizeMode: 'contain'
										}}
									/>
								</View>
							</View>
							<View style={{ flexDirection: 'row', width: '90%' }}>
								<View style={{ width: '50%', height: '75%', backgroundColor: 'white' }}>
									<View
										style={{
											flexDirection: 'row',
											backgroundColor: 'white',
											height: '60%',
											justifyContent: 'flex-start',
											alignItems: 'center'
										}}
									>
										{
											item.category === 'item' || item.category === 'product' ? <><Text
												style={{
													marginLeft: 5,
													color: '#699cc8'
												}}
											>

											</Text>
												<Text style={styles.textStyleRecived}>{item.hsn_code ? item.hsn_code : 'NA '}</Text></> :
												item.category === 'customer' || item.category === 'vendor' ? <><Text
													style={{
														marginLeft: 5,
														color: '#699cc8'
													}}
												>

												</Text>
													<Text style={styles.textStyleRecived}>{item.credit_terms ? item.credit_terms : 'NA '}</Text></> :
													<>
														<Text
															style={{
																marginLeft: 5,
																color: '#699cc8'
															}}
														>
															₹
										</Text>
														<Text style={styles.textStyleRecived}>{item.total ? item.total : 'NA '}</Text></>
										}
									</View>
									<View
										style={{
											flexDirection: 'row',
											backgroundColor: 'white',
											height: '50%',
											justifyContent: 'space-between',
											alignItems: 'center'
										}}
									>
										<View style={{ flexDirection: 'row' }}>
											<Image
												source={calender}
												style={{
													width: 10,
													height: 10,
													marginLeft: 5,
													marginTop: 2
												}}
											/>
											<Text style={styles.textStyleDate}>{moment(item.created_at).format('DD/MM/YYYY')}</Text>
										</View>
									</View>
								</View>

								<View style={{ width: '50%', height: '75%', backgroundColor: 'white' }}>
									<View
										style={{
											flexDirection: 'row',
											backgroundColor: 'white',
											height: '60%',
											// justifyContent: 'center',
											alignItems: 'center'
										}}
									>
										<Image
											source={status}
											style={{
												width: 10,
												height: 10,
												marginLeft: 5,
												marginTop: 2
											}}
										/>
										<Text style={styles.textStyleRecived}>{item.status_code ? item.status_code.charAt(0).toUpperCase() + item.status_code.slice(1) : ''}</Text>
									</View>
									<View
										style={{
											flexDirection: 'row',
											backgroundColor: 'white',
											height: '40%',
											justifyContent: 'space-between',
											alignItems: 'center'
										}}
									>
										<View style={{ flexDirection: 'row', alignItems: 'center' }}>
											<Image
												source={User}
												style={{
													width: 18,
													height: 18,
													// marginLeft: 5,
													marginTop: 2
												}}
											/>
											<Text style={styles.textStyleDate}>{item.creator_name ? item.creator_name : 'NA'}</Text>
										</View>
										<Text style={styles.textStyleAmount}>{approvalDate}</Text>
									</View>
								</View>
							</View>
						</View>
						<View
							style={{
								width: '20%',
								alignItems: 'center',
								height: 70,
								flexDirection: 'column',
								justifyContent: 'center',
								backgroundColor: '#d9d9d9',
								borderTopRightRadius: 5,
								borderBottomRightRadius: 5
							}}
						>
							{createButton(item, index)}
							<View
								style={{
									height: '1%',
									alignItems: 'center',
									justifyContent: 'center',
									width: '100%',
									backgroundColor: 'darkgray'
								}}
							/>
							<PopoverController>
								{({
									openPopover,
									closePopover,
									popoverVisible,
									setPopoverAnchor,
									popoverAnchorRect
								}) => (
										<React.Fragment>
											<TouchableOpacity
												ref={setPopoverAnchor}
												onPress={openPopover}
												style={{
													height: '25%',
													alignItems: 'center',
													justifyContent: 'center',
													width: '100%'
												}}
											>
												<Image
													source={more}
													style={{
														width: '40%',
														height: '25%',
														marginLeft: 5
													}}
												/>
											</TouchableOpacity>

											<Popover
												contentStyle={styles.content}
												arrowStyle={styles.arrow}
												backgroundStyle={styles.background}
												visible={popoverVisible}
												onClose={closePopover}
												fromRect={popoverAnchorRect}
												placement={'bottom'}
												supportedOrientations={['portrait', 'landscape']}
											>
												<View>
													<View style={styles.popoverItemsBackground} >
														<Image
															source={Hand}
															style={{
																width: 15,
																height: 15
															}}
														/>
														<TouchableOpacity style={styles.popoverItems} onPress={() => {
															closePopover();
															onShowModalPress(index, 'hold')
														}}>
															<Text style={styles.popoverItemText}>On Hold</Text>
														</TouchableOpacity>
													</View>
													<View style={styles.popoverItemsBackground}>
														<Image
															source={Cancel}
															style={{
																width: 15,
																height: 15
															}}
														/>
														<TouchableOpacity style={styles.popoverItems} onPress={() => {
															closePopover();
															onShowModalPress(index, 'reject')
														}}>
															<Text style={styles.popoverItemText}>Reject</Text>
														</TouchableOpacity>
													</View>
													{
														selectedInvoice.data === 'PO' && <>
															<View style={styles.popoverItemsBackground}>

																<TouchableOpacity style={styles.popoverItems} onPress={() => {
																	closePopover();
																	onShowModalPress(index, 'cancel')
																}}>
																	<Text style={styles.popoverItemText}>Cancel</Text>
																</TouchableOpacity>
															</View>
															<View style={styles.popoverItemsBackground}>

																<TouchableOpacity style={styles.popoverItems} onPress={() => {
																	closePopover();
																	onShowModalPress(index, 'submitToVendor')
																}}>
																	<Text style={[styles.popoverItemText, { fontSize: 10 }]}>SubmitToVendor</Text>
																</TouchableOpacity>
															</View>
														</>
													}
													{/* <View style={styles.popoverItemsBackground}>
														<Image
															source={chat}
															style={{
																width: 15,
																height: 15
															}}
														/>
														<TouchableOpacity style={styles.popoverItems}>
															<Text style={styles.popoverItemText}>Chat</Text>
														</TouchableOpacity>
													</View>
													<View
														style={{
															height: 0.5,
															alignItems: 'center',
															justifyContent: 'center',
															width: '100%',
															backgroundColor: 'darkgray'
														}}
													/>

													<View style={styles.popoverItemsBackground}>
														<Image
															source={call}
															style={{
																width: 15,
																height: 15
															}}
														/>
														<TouchableOpacity style={styles.popoverItems}>
															<Text style={styles.popoverItemText}>Call</Text>
														</TouchableOpacity>
													</View>
													<View
														style={{
															height: 0.5,
															alignItems: 'center',
															justifyContent: 'center',
															width: '100%',
															backgroundColor: 'darkgray'
														}}
													/>

													<View style={styles.popoverItemsBackground}>
														<Image
															source={emailInvoice}
															style={{
																width: 15,
																height: 15
															}}
														/>
														<TouchableOpacity style={styles.popoverItems}>
															<Text style={styles.popoverItemText}>Mail</Text>
														</TouchableOpacity>
													</View> */}
												</View>
											</Popover>
										</React.Fragment>
									)}
							</PopoverController>
						</View>
					</View>
				</TouchableOpacity>
			</View>
		);
	};

	const drawLegent = () => {
		if (props.showFilterData === true) {
			return (
				<View
					onLayout={(event) => {
						props.findInvoiceLEgendViewdimesions(event.nativeEvent.layout);
					}}
					style={{
						height: '10%',
						width: '100%',
						flexDirection: 'row',
						alignItems: 'center',
						justifyContent: 'center',
						backgroundColor: 'white'
					}}
				>
					<InvoiceListLegend
						buttonText={props.statusBillObject.invoices ? props.statusBillObject.invoices.length : 0}
						legendText="Invoice"
						buttonTextStyle={props.legentStatus.isReceived ? { color: '#0059a6' } : { color: 'white' }}
						buttonStyle={
							!props.legentStatus.isReceived ? (
								{ backgroundColor: '#0059a6' }
							) : (
									{ backgroundColor: 'white' }
								)
						}
						onPress={() => props.onInvoiceListLegendSelect('Received')}
						legendSeperator={{ width: props.widthValue > 0 ? props.widthValue / 17 : 28 }}
					/>
					<InvoiceListLegend
						buttonText={props.statusBillObject.bills ? props.statusBillObject.bills.length : 0}
						legendText="Bills"
						buttonTextStyle={props.legentStatus.isScanned ? { color: '#0059a6' } : { color: 'white' }}
						buttonStyle={
							!props.legentStatus.isScanned ? (
								{ backgroundColor: '#0059a6' }
							) : (
									{ backgroundColor: 'white' }
								)
						}
						onPress={() => props.onInvoiceListLegendSelect('Created')}
						legendSeperator={{ width: props.widthValue > 0 ? props.widthValue / 17 : 28 }}
					/>
					<InvoiceListLegend
						buttonText={props.statusBillObject.payments ? props.statusBillObject.payments.length : 0}
						legendText="Payment"
						buttonTextStyle={props.legentStatus.isPending ? { color: '#0059a6' } : { color: 'white' }}
						buttonStyle={
							!props.legentStatus.isPending ? (
								{ backgroundColor: '#0059a6' }
							) : (
									{ backgroundColor: 'white' }
								)
						}
						onPress={() => props.onInvoiceListLegendSelect('Pending')}
						legendSeperator={{ width: props.widthValue > 0 ? props.widthValue / 17 : 28 }}
					/>
					<InvoiceListLegend
						buttonText={props.statusBillObject.PO ? props.statusBillObject.PO.length : 0}
						legendText="PO"
						legendSeperator={{ display: 'none', width: props.widthValue > 0 ? props.widthValue / 17 : 28 }}
						buttonTextStyle={props.legentStatus.isApprove ? { color: '#0059a6' } : { color: 'white' }}
						buttonStyle={
							!props.legentStatus.isApprove ? (
								{ backgroundColor: '#00599c' }
							) : (
									{ backgroundColor: 'white' }
								)
						}
						onPress={() => props.onInvoiceListLegendSelect('Approved')}
					/>
				</View>
			);
		} else {
			return (
				<View
					onLayout={(event) => {
						props.findInvoiceLEgendViewdimesions(event.nativeEvent.layout);
					}}
					style={{
						height: '10%',
						width: '100%',
						flexDirection: 'row',
						alignItems: 'center',
						justifyContent: 'center',
						backgroundColor: 'white'
					}}
				>
					<InvoiceListLegend
						buttonText={props.statusBillObject.invoices ? props.statusBillObject.invoices.length : 0}
						legendText="Invoices"
						buttonTextStyle={
							props.defaultInvoiceStatus.data === 'Invoices' ? { color: 'white' } : { color: '#0059a6' }
						}
						buttonStyle={
							props.defaultInvoiceStatus.data === 'Invoices' ? (
								{ backgroundColor: '#0059a6' }
							) : (
									{ backgroundColor: 'white' }
								)
						}
						onPress={() => props.onInvoiceListLegendSelect('Invoices')}
						legendSeperator={{ width: props.widthValue > 0 ? props.widthValue / 17 : 28 }}
					/>
					<InvoiceListLegend
						buttonText={props.statusBillObject.bills ? props.statusBillObject.bills.length : 0}
						legendText="Bills"
						buttonTextStyle={
							props.selectedInvoice.data === 'Bills' ? { color: 'white' } : { color: '#0059a6' }
						}
						buttonStyle={
							props.selectedInvoice.data === 'Bills' ? (
								{ backgroundColor: '#0059a6' }
							) : (
									{ backgroundColor: 'white' }
								)
						}
						onPress={() => props.onInvoiceListLegendSelect('Bills')}
						legendSeperator={{ width: props.widthValue > 0 ? props.widthValue / 17 : 28 }}
					/>
					<InvoiceListLegend
						buttonText={props.statusBillObject.payments ? props.statusBillObject.payments.length : 0}
						legendText="Payments"
						buttonTextStyle={
							props.selectedInvoice.data === 'Payments' ? { color: 'white' } : { color: '#0059a6' }
						}
						buttonStyle={
							props.selectedInvoice.data === 'Payments' ? (
								{ backgroundColor: '#0059a6' }
							) : (
									{ backgroundColor: 'white' }
								)
						}
						onPress={() => props.onInvoiceListLegendSelect('Payments')}
						legendSeperator={{ width: props.widthValue > 0 ? props.widthValue / 17 : 28 }}
					/>
					<InvoiceListLegend
						buttonText={props.statusBillObject.PO ? props.statusBillObject.PO.length : 0}
						legendText="PO"
						buttonTextStyle={
							props.selectedInvoice.data === 'PO' ? { color: 'white' } : { color: '#0059a6' }
						}
						buttonStyle={
							props.selectedInvoice.data === 'PO' ? (
								{ backgroundColor: '#0059a6' }
							) : (
									{ backgroundColor: 'white' }
								)
						}
						onPress={() => props.onInvoiceListLegendSelect('PO')}
						legendSeperator={{ width: props.widthValue > 0 ? props.widthValue / 17 : 28 }}
					/>
					<InvoiceListLegend
						buttonText={props.statusBillObject.masterData ? props.statusBillObject.masterData.length : 0}
						legendText="Master Data"
						legendSeperator={{ display: 'none', width: props.widthValue > 0 ? props.widthValue / 17 : 28 }}
						buttonTextStyle={
							props.selectedInvoice.data === 'Master Data' ? { color: 'white' } : { color: '#0059a6' }
						}
						buttonStyle={
							props.selectedInvoice.data === 'Master Data' ? (
								{ backgroundColor: '#0059a6' }
							) : (
									{ backgroundColor: 'white' }
								)
						}
						onPress={() => props.onInvoiceListLegendSelect('Master Data')}
					/>
				</View>
			);
		}
	};

	let myStyle = {
		width: props.animation,
		left: props.animationSlide.x
	};

	return (
		<View style={styles.container}>
			{drawLegent()}
			<View style={{ height: '100%' }}>
				<FlatList
					data={props.data}
					renderItem={({ item, index }) => renderItem(item, index)}
					extraData={props.data.length}
					keyExtractor={(item, index) => index.toString()}
				/>
			</View>
			{/* <View style={{ height: '10%', width: '100%', flexDirection: 'row' }}>
				<TouchableOpacity
					onPress={() => props.onOpenCamera()}
					style={{ width: '50%', alignItems: 'center', justifyContent: 'center' }}
				>
					<Image source={camera} style={{ height: 25, width: 25 }} />

					<Text style={{ fontWeight: 'bold', fontSize: 13 }}>Camera</Text>
				</TouchableOpacity>

				<TouchableOpacity
					onPress={() => props.onOpenGallery()}
					style={{ width: '50%', height: '100%', alignItems: 'center', justifyContent: 'center' }}
				>
					<Image source={camera} style={{ height: 25, width: 25 }} />

					<Text style={{ fontWeight: 'bold', fontSize: 13, marginTop: 5 }}>Gallery</Text>
				</TouchableOpacity>
			</View> */}
		</View>
	);
};

ApproverInVoiceList.propTypes = {
	data: PropTypes.array,
	keyName: PropTypes.string,
	onRowPress: PropTypes.func,
	searchFilterFunction: PropTypes.func,
	onInvoiceListLegendSelect: PropTypes.func,
	findInvoiceLEgendViewdimesions: PropTypes.func,
	searchText: PropTypes.string,
	selectedInvoice: PropTypes.string,
	setMaxHeightNew: PropTypes.func,
	setMinHeightNew: PropTypes.func,
	onOpenCamera: PropTypes.func,
	onOpenGallery: PropTypes.func,
	toggleNew: PropTypes.func
};

const styles = StyleSheet.create({
	container: {
		flex: 1,
		alignItems: 'center',
		backgroundColor: '#eeeeee'
	},
	divider: {
		width,
		height: 1
	},
	row: {
		backgroundColor: '#F2F3F8',
		paddingLeft: 30,
		color: '#0F2C5A',
		fontWeight: '600',
		fontSize: normalizeFont(16),
		marginRight: 30
	},
	textStyleEmail: {
		padding: 0,
		fontSize: 16,
		color: 'rgba(67,67,67,1)',
		marginLeft: 5,
		width: '70%'
	},
	textStyleCategory: {
		padding: 0,
		fontSize: 12,
		fontWeight: '600',
		color: 'rgba(67,67,67,1)',
		marginLeft: 5,
		width: '70%'
	},
	textStyle: {
		padding: 0,
		color: 'rgba(67,67,67,1)'
	},
	textStyleRecived: {
		padding: 0,
		marginLeft: 10,
		fontSize: 13,
		color: 'rgba(67,67,67,1)'
	},
	textStyleAmount: {
		padding: 0,
		// alignSelf:'flex-end',
		// justifyContent:'flex-end',
		fontSize: 13,
		color: 'rgba(67,67,67,1)'
		// backgroundColor:'red'
	},
	textStyleDate: {
		padding: 0,
		marginLeft: 10,
		fontSize: 13,
		color: 'rgba(67,67,67,1)'
		// backgroundColor:'blue'
	},
	insideContainer: {
		borderRadius: 40,
		borderColor: '#dadada',
		width: 70,
		shadowColor: '#000',
		shadowOffset: { width: 1, height: 3 },
		shadowOpacity: 0.6,
		backgroundColor: 'white'
	},
	titleContainer: {
		flexDirection: 'row',
		justifyContent: 'center',
		alignContent: 'center',
		alignItems: 'center',
		height: '100%'
	},
	title: {
		//paddingBottom : 5,
		color: '#2a2f43',
		fontSize: 13
	},
	buttonImage: {
		marginLeft: 10,
		width: 25,
		height: 25
	},
	body: {
		marginTop: 5,
		borderColor: '#505050',
		borderTopWidth: 0.6
	},
	insideNewContainer: { width: 60 },
	outsideContainer: { width: '100%', height: '20%', backgroundColor: 'green', flexDirection: 'row' },
	arrow: {
		borderTopColor: 'white'
	},
	content: {
		width: 100,
		padding: 4,
		backgroundColor: 'white',
		borderRadius: 4
	},
	popoverItems: {
		height: 30,
		alignItems: 'center',
		justifyContent: 'center',
		width: '100%'
	},
	popoverItemText: {
		fontWeight: '500',
		fontSize: 13
	},
	popoverItemsBackground: {
		flexDirection: 'row',
		alignItems: 'center',
		paddingLeft: 5,
		paddingRight: 5
	}
});

export default ApproverInVoiceList;
