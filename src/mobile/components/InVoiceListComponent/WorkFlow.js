import React from 'react';
import {
  StyleSheet,
  View,
  Dimensions,
  Text,
  Button,
  TouchableOpacity,
  FlatList,
  Keyboard,
  Platform,
  Image,
  Animated,
  TouchableHighlight
} from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import InputText from '../CustomComponent/TextInput'
import calender from '../../../common/images/ic_listItem_date.png';
import moment from 'moment';

const WorkFlow = props => {
  const { modalHeaderText, name, start, end, confirm, cancel, total, showNote, onNoteChange, note } = props
  return (
    <View style={styles.container}>
      <KeyboardAwareScrollView >
        <Text style={styles.header}>{modalHeaderText}</Text>
        <Text style={styles.name}>{name}</Text>
        <View style={styles.row}>
          <Text style={{ fontSize: 20, marginLeft: 5, color: '#699cc8', marginRight: 5, }}>₹</Text>
          <Text style={styles.textStyle}>{total}</Text>
        </View>
        <View style={styles.calRow}>
          <Image
            source={calender}
            style={styles.image}
          />
          <Text style={styles.textStyleDate}>{moment(start).format('DD/MM/YYYY')}</Text>
        </View>
        <View style={styles.calRow}>
          <Image
            source={calender}
            style={styles.image}
          />
          <Text style={styles.textStyleDate}>{moment(end).format('DD/MM/YYYY')}</Text>
        </View>
        {
          showNote && <View style={styles.noteContainer}>
            <Text style={styles.textStyleDate}>Remark</Text>
            <InputText
              style={{ marginTop: 4, width: '100%', height: 100 }}
              onChangeText={onNoteChange}
              placeholder='Enter your remark...'
              value={note}
              autoCapitalize="none"
              autoCorrect={false}
              blurOnSubmit={false}
              editable={props.isEditable}
              name="note"
              onTextInputCreated={props.setInputReference}
              returnKeyType="next"
              multiline
              numberOfLines={3}
              onSubmitEditing={() => props.onSubmitEditing('discount')}
            />
          </View>
        }
        <View style={styles.btn}>
          <TouchableOpacity style={[styles.viewButtonToggleRight, styles.viewToggleButtonActive]}
            onPress={() => { confirm() }}>
            <Text style={[styles.viewText, styles.viewToggleButtonActiveText]}>CONFIRM  </Text>
          </TouchableOpacity>
          <TouchableOpacity style={[styles.viewButton]}
            onPress={() => cancel()}>
            <Text style={[styles.viewText]}>CANCEL  </Text>
          </TouchableOpacity>

        </View>
      </KeyboardAwareScrollView>
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 0,
    alignItems: 'center',
    backgroundColor: '#eeeeee',
    padding: 40,
  },
  header: {
    fontSize: 34,
    fontWeight: '100',
    color: '#0059a6'
  },
  name: {
    marginTop: 10,
    fontSize: 26,
    fontWeight: '100',
    color: 'black'
  },
  row: {
    flexDirection: 'row',
    marginTop: 10,
  },
  image: {
    width: 20,
    height: 20,
    marginLeft: 5,
    marginTop: 2
  },
  textStyle: {
    fontSize: 20
  },
  textStyleDate: {
    fontSize: 20,
    margin: 5,
    paddingTop: 5
  },
  calRow: {
    flexDirection: 'row',
    alignItems: 'center',
    // justifyContent: 'center'
  },
  btn: {
    marginTop: 20,
    flex: 1,
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center'
  },
  viewButtonToggleRight: {
    minWidth: 100,
    width: '60%',
    height: 50,
    borderRadius: 35,
    borderColor: '#4785bd',
    borderWidth: 1,
    backgroundColor: 'white',
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 5,
    // marginLeft: 20
  },
  viewButtonToggleLeft: {
    width: '60%',
    borderRadius: 35,
    borderTopLeftRadius: 0,
    borderBottomLeftRadius: 0,
    borderColor: '#4785bd',
    borderWidth: 1,
    backgroundColor: 'white',
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 5,
    marginLeft: 0
  },
  viewToggleButtonActive: {
    backgroundColor: '#0059a6',
  },
  viewToggleButtonInActive: {
    backgroundColor: '#939ea7',
  },
  viewToggleButtonActiveText: {
    color: 'white',
    fontWeight: '600',
    fontSize: 20
  },
  viewButton: {
    width: '80%',
    borderRadius: 35,
    // borderColor: '#4785bd',
    // borderWidth: 1,
    // backgroundColor: 'white',
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 20
  },
  viewText: {
    fontSize: 20,
    fontWeight: 'bold',
    color: '#4785bd'
  },
  noteContainer: {
    flex: 1,
    width: '100%'
  }
});

export default WorkFlow;