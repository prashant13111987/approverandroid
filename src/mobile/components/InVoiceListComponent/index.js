/**
 * Created by synerzip on 025/06/18.
 */
/* eslint-disable global-require */
/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React from 'react';
import {
    StyleSheet,
    View,
    Dimensions,
    Text,
    Button,
    TouchableOpacity,
    FlatList, Keyboard, Platform,Image,Animated,TouchableHighlight
} from 'react-native';

import PropTypes from 'prop-types';
import { normalizeFont } from '../../../common/util';
import logo from '../../../common/images/logo.png'
import email from '../../../common/images/email.png'
import imageIcon from '../../../common/images/ic_image.png'
import calender from '../../../common/images/ic_listItem_date.png'
import status from '../../../common/images/ic_listItem_status.png'
import emailInvoice from '../../../common/images/ic_mail.png'
import paid from '../../../common/images/paid.png'
import partialPaid from '../../../common/images/paid_partial.png'
import unpaid from '../../../common/images/unpaid.png'
import appInvoice from '../../../common/images/logo_brand.png'
import mobileInvoice from '../../../common/images/ic_handset.png'
import camera from '../../../common/images/bt_capture.png'
import chat from '../../../common/images/ic_chat.png'
import call from '../../../common/images/ic_call.png'
import gallary from '../../../common/images/gallery.png'
import more from '../../../common/images/ellipsis.png'
import add from '../../../common/images/btn_add.png'
import cancel from '../../../common/images/btn_cancel.png'
import InvoiceListLegend from './../CustomComponent/InvoiceListLegend'
import config from '../../../common/config'
import { Popover, PopoverController } from 'react-native-modal-popover';
import Moment from 'react-moment';
import moment from 'moment';
import _ from "lodash";
// invoice_no:reciveItem.invoice_no,
//     created_at:reciveItem.created_at,
//     statusText:"Created"
const { width,height } = Dimensions.get('window');


const InVoiceList = (props) => {

    // onCreatePress={this.onCreatePress}
    // onEditPress={this.onEditPress}
    // onViewPress={this.onViewPress}

    const createButton = (item) => {

        if(item.statusText == "Created" || item.statusText == "Rejected")
        {
            if(item.creator === config.USER_TOKEN_DETAILS.loginId){
                return (
                    <View>
                        <TouchableOpacity style={{height:'75%',alignItems: 'center', justifyContent:'center',width:'100%'}} onPress={() => (props.onEditPress(item))}>
                            <Text style={{ fontSize:12,}}>EDIT</Text>
                        </TouchableOpacity>
                    </View>
                )
            }else{
                return (
                    <View>
                        <TouchableOpacity style={{height:'75%',alignItems: 'center', justifyContent:'center',width:'100%'}} onPress={() => (props.onViewPress(item))}>
                            <Text style={{ fontSize:12,}}>VIEW</Text>
                        </TouchableOpacity>
                    </View>
                )
            }
        }else  if(item.statusText == "Pending" || item.statusText == "On Hold" || item.statusText == "Approved"){
            return (
                <View>
                    <TouchableOpacity style={{height:'75%',alignItems: 'center', justifyContent:'center',width:'100%'}} onPress={() => (props.onViewPress(item))}>
                        <Text style={{ fontSize:12,}}>VIEW</Text>
                    </TouchableOpacity>
                </View>
            )
        }else{

            return (
                <View>
                    <TouchableOpacity style={{height:'75%',alignItems: 'center', justifyContent:'center',width:'100%'}} onPress={() => (props.onCreatePress(item))}>
                        <Text style={{ fontSize:12,}}>CREATED</Text>
                    </TouchableOpacity>
                </View>
            )
        }
    }



    const renderItem = (item, index) => {

      //  console.log("Render Item Object ::::::::::::",config.USER_TOKEN_DETAILS.loginId)
        let invoiceSource = null;
        let name = null
        let partyname = null
        let invoicelist = props.venderList
        let approvalDate = null

        if (item.source_name === 's_invoice') {
            invoiceSource = logo;
            name = item.invoice_no
            _.each(invoicelist, (reciveItem) => {
                if(item.vendor_id === reciveItem.id)
                {
                    partyname = reciveItem.name
                }
            });
           // partyname = item.vendor_id

        } else  if (item.source_name === 'email_invoice') {
            invoiceSource = emailInvoice;
            name = item.seller_name
            partyname = item.vendor_name

        } else if (item.source_name === 'uploaded_invoice') {
            invoiceSource = mobileInvoice;
            name = item.file_name
            partyname = item.file_name
        }else
        {
            _.each(invoicelist, (reciveItem) => {
                if(item.partyname === reciveItem.id)
                {
                    partyname = reciveItem.name
                }
            });
        }
        if (item.statusText == "Approved")
        {
            if( item.payment_status === 0)
            {
                invoiceSource = unpaid;
            }else if( item.payment_status === 1){
                invoiceSource = paid;
            }else  if( item.payment_status === 2)
            {
                invoiceSource = partialPaid
            }
            approvalDate =  item.approval_date?item.approval_date:"NA"
        }
        // Button Text
        // if(item.statusText == "Created" || item.statusText == "Rejected")
        // {
        //     console.log("Login Sucess Id",props.loginSuccess)
        //     if(item.creator === config.USER_TOKEN_DETAILS.loginId){
        //         buttonText = "EDIT"
        //     }else{
        //         buttonText = "VIEW"
        //     }
        // }else  if(item.statusText == "Pending" || item.statusText == "On Hold" || item.statusText == "Approved"){
        //     buttonText = "VIEW"
        // }else{
        //     buttonText = "CREATED"
        // }

       // <Text style={styles.textStyleRecived}>{item.statusText?item.statusText:"Received"}</Text>
        //<Text style={{ fontSize:12,}}>{item.statusText ?"VIEW":"CREATE"}</Text>
        let amount = item.total?item.total:null
        let amountInRs = null
        if(amount)
        {
            amountInRs = amount ? `₹${parseFloat(amount).toFixed(0)}` : null

        }

        createButton(item)

        const createdDate = item.statusText?moment(item.received_date).format('DD/MM/YYYY'):moment(item.submission_date).format('DD/MM/YYYY')
        return (
            <View style = {{backgroundColor:'#eeeeee'}}>
                <TouchableOpacity style = {{alignItems:'center'}} onPress={() => (props.onRowPress(item))}>
                    <View style={{
                        flex: 1, flexDirection: 'row', width:'98%',height: 70, alignItems: 'flex-start',marginTop:5,borderRadius:10,
                    }}
                    >

                        <View style = {{backgroundColor:'white',flexDirection: 'column',width:'80%'}} >
                            <View style={{backgroundColor:'white',flexDirection: 'row',width:'100%',height:'30%',justifyContent:'space-between',alignItems:'center'
                            }}
                            >
                                <Text style={styles.textStyleEmail} numberOfLines = { 1 } ellipsizeMode = 'tail' >{partyname?partyname:"No Name found"}</Text>
                             <View style={{flexDirection:'row'}}>
                                <Image
                                    source={item.statusText == "Approved"?paid:item.statusText?paid:""}
                                    style={{
                                        width: 20,
                                        height: 15,
                                        marginRight:5,
                                        resizeMode: 'contain',

                                    }}
                                />
                                <Image
                                    source={item.statusText == "Approved"?invoiceSource:item.statusText?"":invoiceSource}
                                    style={{
                                        width: 20,
                                        height: 15,
                                        marginRight:10,
                                        resizeMode: 'contain',

                                    }}
                                />
                             </View>
                            </View>
                            <View style = {{flexDirection:'row',width:'90%'}}>
                            <View style = {{width:'50%',height:'68%',backgroundColor:'white'}}>
                                <View style={{flexDirection: 'row', backgroundColor: 'white',height:'60%', justifyContent:'flex-start',alignItems:'center'
                                }}
                                >
                                    <Text style={{
                                        marginLeft:5,
                                        color:'#699cc8'
                                    }}>₹</Text>
                                    <Text style={styles.textStyleRecived}>{amountInRs?amountInRs:"NA "}</Text>
                                </View>
                                <View style={{flexDirection: 'row', backgroundColor: 'white',height:'40%', justifyContent:'space-between',alignItems:'center'
                                }}
                                >
                                    <View  style={{flexDirection: 'row'}}>
                                    <Image
                                        source={calender}
                                        style={{
                                            width: 10,
                                            height: 10,
                                            marginLeft:5,
                                            marginTop:2
                                        }}
                                    />
                                    <Text style={styles.textStyleDate}>
                                        {createdDate}
                                    </Text>
                                    </View>
                                </View>
                            </View>

                                <View style = {{width:'50%',height:'68%',backgroundColor:'white'}}>
                                    <View style={{flexDirection: 'row', backgroundColor: 'white',height:'60%', justifyContent:'flex-start',alignItems:'center'
                                    }}
                                    >
                                        <Text style={styles.textStyleRecived}>{item.statusText?"Approver":""}</Text>
                                    </View>
                                    <View style={{flexDirection: 'row', backgroundColor: 'white',height:'40%', justifyContent:'space-between',alignItems:'center'
                                    }}
                                    >
                                        <View  style={{flexDirection: 'row'}}>
                                            <Text style={styles.textStyleDate}>
                                                {item.statusText?"Creator":""}
                                            </Text>
                                        </View>
                                        <Text style={styles.textStyleAmount}>{approvalDate}
                                        </Text>
                                    </View>
                                </View>
                            </View>
                        </View>
                        <View style = {{width:'20%',alignItems: 'center',height: 70 , flexDirection: 'column', justifyContent:'center',backgroundColor:'#d9d9d9', borderTopRightRadius:5, borderBottomRightRadius:5}} >
                            {createButton(item)}
                            <View style={{height:'1%',alignItems: 'center', justifyContent:'center',width:'100%',backgroundColor:'darkgray'}}>
                            </View>
                            <PopoverController>
                                {({ openPopover, closePopover, popoverVisible, setPopoverAnchor, popoverAnchorRect }) => (
                                  <React.Fragment>
                                      <TouchableOpacity ref={setPopoverAnchor} onPress={openPopover} style={{height:'25%',alignItems: 'center', justifyContent:'center',width:'100%'}}>
                                          <Image
                                            source={more}
                                            style={{
                                                width:'40%',
                                                height:'25%',
                                                marginLeft:5
                                            }}
                                          />
                                      </TouchableOpacity>

                                      <Popover
                                        contentStyle={styles.content}
                                        arrowStyle={styles.arrow}
                                        backgroundStyle={styles.background}
                                        visible={popoverVisible}
                                        onClose={closePopover}
                                        fromRect={popoverAnchorRect}
                                        placement={"bottom"}
                                        supportedOrientations={['portrait', 'landscape']}
                                      >
                                          <View>
                                              <View style={styles.popoverItemsBackground}>
                                                  <Image
                                                    source={chat}
                                                    style={{
                                                        width: 15,
                                                        height: 15,
                                                    }}
                                                  />
                                                  <TouchableOpacity  style={styles.popoverItems}>
                                                      <Text style={styles.popoverItemText}>Chat</Text>
                                                  </TouchableOpacity>

                                              </View>
                                              <View style={{height:0.5,alignItems: 'center', justifyContent:'center',width:'100%',backgroundColor:'darkgray'}}>
                                              </View>

                                              <View  style={styles.popoverItemsBackground}>
                                                  <Image
                                                    source={call}
                                                    style={{
                                                        width: 15,
                                                        height: 15,
                                                      }}
                                                  />
                                              <TouchableOpacity  style={styles.popoverItems}>
                                                  <Text style={styles.popoverItemText}>Call</Text>
                                              </TouchableOpacity>

                                              </View>
                                              <View style={{height:0.5,alignItems: 'center', justifyContent:'center',width:'100%',backgroundColor:'darkgray'}}>
                                              </View>

                                              <View  style={styles.popoverItemsBackground}>
                                                  <Image
                                                    source={emailInvoice}
                                                    style={{
                                                        width: 15,
                                                        height: 15,
                                                    }}
                                                  />
                                              <TouchableOpacity style={styles.popoverItems}>
                                                  <Text style={styles.popoverItemText}>Mail</Text>
                                              </TouchableOpacity>

                                              </View>
                                          </View>
                                      </Popover>
                                  </React.Fragment>
                                )}
                            </PopoverController>
                        </View>
                    </View>
                </TouchableOpacity>
            </View>
        );
    };

    const drawLegent = () => {

        if (props.showFilterData === true) {
            return (
                <View onLayout={(event) => {
                    props.findInvoiceLEgendViewdimesions(event.nativeEvent.layout)
                }}style = {{height:'10%',width:'100%',flexDirection:'row',alignItems:'center',justifyContent:'center',backgroundColor:'white'}}>
                    <InvoiceListLegend
                        buttonText = {props.statusBillObject.received?props.statusBillObject.received.length:0}
                        legendText= 'Received'
                        buttonTextStyle={props.legentStatus.isReceived?{color:'#0059a6'}:{color:'white'}}
                        buttonStyle = {!props.legentStatus.isReceived?{backgroundColor:'#0059a6'}:{backgroundColor:'white'}}
                        onPress={()=>props.onInvoiceListLegendSelect('Received')}
                        legendSeperator = {{width:props.widthValue>0?(props.widthValue / 17):28}}
                    />
                    <InvoiceListLegend
                        buttonText =  {props.statusBillObject.create?props.statusBillObject.create.length:0}
                        legendText= 'Created'
                        buttonTextStyle={props.legentStatus.isScanned?{color:'#0059a6'}:{color:'white'}}
                        buttonStyle = {!props.legentStatus.isScanned?{backgroundColor:'#0059a6'}:{backgroundColor:'white'}}
                        onPress={()=>props.onInvoiceListLegendSelect('Created')}
                        legendSeperator = {{width:props.widthValue>0?(props.widthValue / 17):28}}/>
                    <InvoiceListLegend
                        buttonText =  {props.statusBillObject.pending?props.statusBillObject.pending.length:0}
                        legendText= 'Pending'
                        buttonTextStyle={props.legentStatus.isPending?{color:'#0059a6'}:{color:'white'}}
                        buttonStyle = {!props.legentStatus.isPending?{backgroundColor:'#0059a6'}:{backgroundColor:'white'}}
                        onPress={()=>props.onInvoiceListLegendSelect('Pending')}
                        legendSeperator = {{width:props.widthValue>0?(props.widthValue / 17):28}}/>
                    <InvoiceListLegend
                        buttonText = {props.statusBillObject.rejected?props.statusBillObject.rejected.length:0}
                        legendText= 'Rejected'
                        buttonTextStyle={props.legentStatus.isRejected?{color:'#0059a6'}:{color:'white'}}
                        buttonStyle = {!props.legentStatus.isRejected?{backgroundColor:'#0059a6'}:{backgroundColor:'white'}}
                        onPress={()=>props.onInvoiceListLegendSelect('Rejected')}
                        legendSeperator = {{width:props.widthValue>0?(props.widthValue / 17):28}}/>
                    <InvoiceListLegend
                        buttonText = {props.statusBillObject.onHold?props.statusBillObject.onHold.length:0}
                        legendText= 'On Hold'
                        buttonTextStyle={props.legentStatus.isOnhold?{color:'#0059a6'}:{color:'white'}}
                        buttonStyle = {!props.legentStatus.isOnhold?{backgroundColor:'#00599c'}:{backgroundColor:'white'}}
                        onPress={()=>props.onInvoiceListLegendSelect('On Hold')}
                        legendSeperator = {{width:props.widthValue>0?(props.widthValue / 17):28}}/>

                    <InvoiceListLegend
                        buttonText = {props.statusBillObject.approval?props.statusBillObject.approval.length:0}
                        legendText= 'Approved'
                        legendSeperator = {{display:'none',width:props.widthValue>0?(props.widthValue / 17):28}}
                        buttonTextStyle={props.legentStatus.isApprove?{color:'#0059a6'}:{color:'white'}}
                        buttonStyle = {!props.legentStatus.isApprove?{backgroundColor:'#00599c'}:{backgroundColor:'white'}}
                        onPress={()=>props.onInvoiceListLegendSelect('Approved')}/>

                </View>
            );

        }
        else {
              return (
                  <View onLayout={(event) => {
                      props.findInvoiceLEgendViewdimesions(event.nativeEvent.layout)
                  }} style = {{height:'10%',width:'100%',flexDirection:'row',alignItems:'center',justifyContent:'center',backgroundColor:'white'}}>
                      <InvoiceListLegend
                          buttonText = {props.statusBillObject.received?props.statusBillObject.received.length:0}
                          legendText= 'Received'
                          buttonTextStyle={props.defaultInvoiceStatus ==='Received'?{color:'white'}:{color:'#0059a6'}}
                          buttonStyle = {props.defaultInvoiceStatus ==='Received'?{backgroundColor:'#0059a6'}:{backgroundColor:'white'}}
                          onPress={()=>props.onInvoiceListLegendSelect('Received')}
                          legendSeperator = {{width:props.widthValue>0?(props.widthValue / 17):28}}/>
                      <InvoiceListLegend
                          buttonText =  {props.statusBillObject.create?props.statusBillObject.create.length:0}
                          legendText= 'Created'
                          buttonTextStyle={props.selectedInvoice ==='Created'?{color:'white'}:{color:'#0059a6'}}
                          buttonStyle = {props.selectedInvoice ==='Created'?{backgroundColor:'#0059a6'}:{backgroundColor:'white'}}
                          onPress={()=>props.onInvoiceListLegendSelect('Created')}
                          legendSeperator = {{width:props.widthValue>0?(props.widthValue / 17):28}}/>
                      <InvoiceListLegend
                          buttonText =  {props.statusBillObject.pending?props.statusBillObject.pending.length:0}
                          legendText= 'Pending'
                          buttonTextStyle={props.selectedInvoice ==='Pending'?{color:'white'}:{color:'#0059a6'}}
                          buttonStyle = {props.selectedInvoice ==='Pending'?{backgroundColor:'#0059a6'}:{backgroundColor:'white'}}
                          onPress={()=>props.onInvoiceListLegendSelect('Pending')}
                          legendSeperator = {{width:props.widthValue>0?(props.widthValue / 17):28}}/>
                      <InvoiceListLegend
                          buttonText = {props.statusBillObject.rejected?props.statusBillObject.rejected.length:0}
                          legendText= 'Rejected'
                          buttonTextStyle={props.selectedInvoice ==='Rejected'?{color:'white'}:{color:'#0059a6'}}
                          buttonStyle = {props.selectedInvoice ==='Rejected'?{backgroundColor:'#0059a6'}:{backgroundColor:'white'}}
                          onPress={()=>props.onInvoiceListLegendSelect('Rejected')}
                          legendSeperator = {{width:props.widthValue>0?(props.widthValue / 17):28}}/>
                      <InvoiceListLegend
                          buttonText = {props.statusBillObject.onHold?props.statusBillObject.onHold.length:0}
                          legendText= 'On Hold'
                          buttonTextStyle={props.selectedInvoice ==='On Hold'?{color:'white'}:{color:'#00599c'}}
                          buttonStyle = {props.selectedInvoice ==='On Hold'?{backgroundColor:'#00599c'}:{backgroundColor:'white'}}
                          onPress={()=>props.onInvoiceListLegendSelect('On Hold')}
                          legendSeperator = {{width:props.widthValue>0?(props.widthValue / 17):28}}/>
                      <InvoiceListLegend
                          buttonText = {props.statusBillObject.approval?props.statusBillObject.approval.length:0}
                          legendText= 'Approved'
                          legendSeperator = {{display:'none',width:props.widthValue>0?(props.widthValue / 16):28}}
                          buttonTextStyle={props.selectedInvoice ==='Approved'?{color:'white'}:{color:'#00599c'}}
                          buttonStyle = {props.selectedInvoice ==='Approved'?{backgroundColor:'#00599c'}:{backgroundColor:'white'}}
                          onPress={()=>props.onInvoiceListLegendSelect('Approved')}/>
                  </View>
              );
        }

    }



    let myStyle = {
        width: props.animation,
        left:props.animationSlide.x
    };

    return (

        <View style={styles.container}>
            {drawLegent()}
            <View style = {{height:'80%'}}>
            <FlatList
                data={props.data}
                renderItem={({ item, index }) => renderItem(item, index)}
                extraData={props}
            />
            </View>
            <View style = {{height:'10%',width:'100%',flexDirection:'row'}}>
                    <TouchableOpacity onPress={() => (props.onOpenCamera())} style={{width:'50%',alignItems:'center',justifyContent:'center'}}>
                        <Image source={camera} style={{ height: 25, width: 25}} />


                    <Text style={{fontWeight:'bold',fontSize:13}}>Camera</Text>
                    </TouchableOpacity>


                <TouchableOpacity onPress={() => (props.onOpenGallery())} style={{width:'50%',height:'100%',alignItems:'center',justifyContent:'center'}}>
                        <Image source={camera} style={{ height: 25, width: 25}} />


                    <Text style={{fontWeight:'bold',fontSize:13,marginTop:5}}>Gallery</Text>
                </TouchableOpacity>
            </View>

        </View>
    );
};

InVoiceList.propTypes = {
    data: PropTypes.array,
    keyName: PropTypes.string,
    onRowPress: PropTypes.func,
    searchFilterFunction: PropTypes.func,
    onInvoiceListLegendSelect: PropTypes.func,
    findInvoiceLEgendViewdimesions: PropTypes.func,
    searchText: PropTypes.string,
    selectedInvoice: PropTypes.string,
    setMaxHeightNew : PropTypes.func,
    setMinHeightNew : PropTypes.func,
    onOpenCamera : PropTypes.func,
    onOpenGallery : PropTypes.func,
    toggleNew : PropTypes.func,
    expanded:PropTypes.boolean

};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        backgroundColor: '#eeeeee',
        },
    divider: {
        width,
        height: 1,
    },
    row: {
        backgroundColor: '#F2F3F8',
        paddingLeft: 30,
        color: '#0F2C5A',
        fontWeight: '600',
        fontSize: normalizeFont(16),
        marginRight: 30,
    },
    textStyleEmail: {
        padding:0,
        fontSize:16,
        color: 'rgba(67,67,67,1)',
        marginLeft:5,
        width:'70%',
    },
    textStyle: {
        padding:0,
        color: 'rgba(67,67,67,1)',
    },
    textStyleRecived:{
        padding:0,
        marginLeft:10,
        fontSize:13,
        color: 'rgba(67,67,67,1)',
    },
    textStyleAmount:{
        padding:0,
        // alignSelf:'flex-end',
        // justifyContent:'flex-end',
        fontSize:13,
        color: 'rgba(67,67,67,1)',
       // backgroundColor:'red'
    },
    textStyleDate:{
        padding:0,
        marginLeft:10,
        fontSize:13,
        color: 'rgba(67,67,67,1)',
       // backgroundColor:'blue'
    },
    insideContainer   : {
        borderRadius:40,
        borderColor:'#dadada',
        width:70,
        shadowColor: '#000',
        shadowOffset: { width: 1, height: 3},
        shadowOpacity: 0.6,
        backgroundColor:'white',

    },
    titleContainer : {
        flexDirection: 'row',
        justifyContent:'center',
        alignContent:'center',
        alignItems:'center',
        height:'100%',

    },
    title       : {
        //paddingBottom : 5,
        color   :'#2a2f43',
        fontSize:13
    },
    buttonImage : {
        marginLeft:10,
        width   : 25,
        height  : 25,
    },
    body        : {
        marginTop:5,
        borderColor:'#505050',
        borderTopWidth:.6
    },
    insideNewContainer:{width:60},
    outsideContainer:{width:'100%',height:'20%',backgroundColor:'green',flexDirection:'row'},
    arrow: {
        borderTopColor: 'white',
    },
    content: {
        width:100,
        padding: 4,
        backgroundColor: 'white',
        borderRadius: 4,
    },
    popoverItems:{
        height:30,alignItems: 'center',justifyContent:'center',width:'100%'
    },
    popoverItemText:{
        fontWeight:'500',
        fontSize:13
    },
    popoverItemsBackground:{
        flexDirection:'row',alignItems:'center',paddingLeft: 5,paddingRight: 5
    }

});

export default InVoiceList;
