/* eslint-disable global-require */
/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React from 'react';
import {
    StyleSheet,
    View,
    Dimensions,
    Text,
    TouchableOpacity,
    TouchableHighlight,
    TextInput,
    TouchableWithoutFeedback, Keyboard
} from 'react-native';
import PropTypes from 'prop-types';
import Swiper from 'react-native-swiper';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import InputText from '../CustomComponent/TextInput'


const ItemsInvoiceComponent = props => {

    console.log("Bill Items ::::::::::::::::",props.billDetails)
    let totalAmount = 0
    let selectedIndex = 0
    if(props.billDetails.items.length > 0){
        return(
            <View style = {{ backgroundColor:'#ededed',alignItems:'center'}}>
                <KeyboardAwareScrollView
                    scrollEnabled
                    showsVerticalScrollIndicator={true}
                    enableOnAndroid
                    keyboardShouldPersistTaps="handled"
                    enableResetScrollToCoords={true}
                >
                    <View>
                        <Swiper
                                style = {{backgroundColor:'#ededed',height:260,width:320}}
                                onIndexChanged={(index) => props.onIndexPress(index)}
                                showsButtons={false}>
                            {props.billDetails.items.map((item,index)=> {

                                const calculatedAmount = (parseFloat(item.price)*parseFloat(item.quantity)).toFixed(2)
                                let finalCalculatedAmount = 0
                                if(item.discount > 0){
                                    const discountValue = item.discount?calculatedAmount * ((parseFloat(item.discount))/100.0):'0.0'
                                    finalCalculatedAmount = calculatedAmount - discountValue
                                }else
                                {
                                    finalCalculatedAmount = calculatedAmount;
                                }
                                if(finalCalculatedAmount){
                                    totalAmount = parseFloat(totalAmount) + parseFloat(finalCalculatedAmount)
                                }else{
                                    totalAmount = parseFloat(totalAmount) + parseFloat(calculatedAmount)
                                }
                                selectedIndex = index

                                return   (
                                    <View style = {{backgroundColor:'#ededed',height:240,width:320,padding:5}}>
                                        <InputText
                                            style={styles.textInput}
                                            onChangeText={props.handleItemNameChange}
                                            value={item.item_name}
                                            autoCapitalize="none"
                                            autoCorrect={false}
                                            testID= {"itemName"}
                                            maxLength={50}
                                            blurOnSubmit={false}
                                            editable={false}
                                            name={"itemName" + index}
                                            onTextInputCreated={props.setInputReference}
                                            returnKeyType="next"
                                            onSubmitEditing={() => props.onSubmitEditing("hsn_code" + index)}
                                        />

                                        <InputText
                                            style={styles.textInput}
                                            onChangeText={props.handleHsnCodeChange}
                                            value={item.hsn_code}
                                            autoCapitalize="none"
                                            autoCorrect={false}
                                            testID={item.hsn_code}
                                            editable={false}
                                            maxLength={50}
                                            blurOnSubmit={false}
                                            name={"hsn_code" + index}
                                            onTextInputCreated={props.setInputReference}
                                            returnKeyType="next"
                                            onSubmitEditing={() => props.onSubmitEditing("quantity" + index)}
                                        />

                                        <View style={{
                                            width: '100%',
                                            marginTop: 8,
                                            flexDirection: 'row',
                                            justifyContent: 'space-between'
                                        }}>
                                            <View
                                                style={{width: '45%'}}>
                                                <Text style={{color: '#555555'}}>Discount</Text>

                                                <InputText
                                                    style={styles.textInput}
                                                    onChangeText={props.handleQuantityChange}
                                                    value={item.discount}
                                                    autoCapitalize="none"
                                                    autoCorrect={false}
                                                    testID="quantity"
                                                    maxLength={50}
                                                    editable={false}
                                                    blurOnSubmit={false}
                                                    name="quantity"
                                                    onTextInputCreated={props.setInputReference}
                                                    returnKeyType="next"
                                                    onSubmitEditing={() => props.onSubmitEditing("price" + index)}
                                                />
                                            </View>
                                            <View style={{width: '45%'}}>
                                                <Text style={{color: '#555555'}}>Tax Rate</Text>
                                                <InputText
                                                    style={styles.textInput}
                                                    onChangeText={props.handlePriceChange}
                                                    value={parseFloat(item.tax).toString()}
                                                    autoCapitalize="none"
                                                    autoCorrect={false}
                                                    testID="price"
                                                    maxLength={50}
                                                    editable={false}
                                                    blurOnSubmit={false}
                                                    name={"price" + index}
                                                    onTextInputCreated={props.setInputReference}
                                                    returnKeyType="next"
                                                    onSubmitEditing={() => props.onSubmitEditing("amount"+ index)}
                                                />
                                            </View>
                                        </View>

                                        <View style={{
                                            width: '100%',
                                            marginTop: 8,
                                            flexDirection: 'row',
                                            justifyContent: 'space-between'
                                        }}>
                                            <View style={{width: '30%'}}>
                                                <Text style={{color: '#555555'}}>Quantity</Text>

                                                <InputText
                                                    style={styles.textInput}
                                                    onChangeText={props.handleQuantityChange}
                                                    value={item.quantity}
                                                    autoCapitalize="none"
                                                    autoCorrect={false}
                                                    testID="quantity"
                                                    maxLength={50}
                                                    editable={false}
                                                    blurOnSubmit={false}
                                                    name={"quantity" + index}
                                                    onTextInputCreated={props.setInputReference}
                                                    returnKeyType="next"
                                                    onSubmitEditing={() => props.onSubmitEditing("price" + index)}
                                                />
                                            </View>
                                            <View style={{width: '25%'}}>
                                                <Text style={{color: '#555555'}}>Rate</Text>
                                                <InputText
                                                    style={styles.textInput}
                                                    onChangeText={props.handlePriceChange}
                                                    value={item.price}
                                                    autoCapitalize="none"
                                                    autoCorrect={false}
                                                    testID="price"
                                                    maxLength={50}
                                                    editable={false}
                                                    blurOnSubmit={false}
                                                    name={"price" + index}
                                                    onTextInputCreated={props.setInputReference}
                                                    returnKeyType="next"
                                                    onSubmitEditing={() => props.onSubmitEditing("amount"+ index)}
                                                />
                                            </View>
                                            <View style={{width: '30%'}}>
                                                <Text style={{color: '#555555'}}>Amount</Text>
                                                <InputText
                                                    style={styles.textInput}
                                                    onChangeText={props.handleAmountChange}
                                                    value={parseFloat(finalCalculatedAmount).toFixed(2)}
                                                    autoCapitalize="none"
                                                    autoCorrect={false}
                                                    editable={false}
                                                    testID="amount"
                                                    maxLength={50}
                                                    blurOnSubmit={false}
                                                    name={"amount"+ index}
                                                    onTextInputCreated={props.setInputReference}
                                                    returnKeyType="next"
                                                    onSubmitEditing={() => { Keyboard.dismiss();}}
                                                />
                                            </View>
                                        </View>
                                    </View>
                                )}
                            )}
                        </Swiper>
                    </View>
                </KeyboardAwareScrollView>
                <View style = {{ backgroundColor:'white',width:'100%',alignItems:'center',flexDirection:'column'}}>
                    <View style ={{width:'100%',flexDirection:'row',backgroundColor:'white',justifyContent:'space-between',padding:5}}>
                        <TouchableOpacity style={styles.editItemButton}
                                          onPress={() => (props.editItemPress(selectedIndex))}>
                            <Text style={styles.addItemText}>EDIT ITEM</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.addItemButton}
                                          onPress={props.addItemPress}>
                            <Text style={styles.addItemText}>ADD ITEM</Text>
                        </TouchableOpacity>
                    </View>
                    <View style ={{width:'100%',backgroundColor:'white',flexDirection:'row'}}>
                        <View style = {{marginLeft:20,flexDirection:'row'}}>
                            <View style = {{height:20,marginTop:15,backgroundColor:'white'}}>
                                <Text style = {styles.validatePlaceHolder}>Amount</Text>
                            </View>
                            <InputText
                                style={{width:'80%',marginLeft:10,marginTop:5}}
                                onChangeText={props.handleOtherFieldChange}
                                value={parseFloat(totalAmount).toFixed(2)}
                                autoCapitalize="none"
                                name="otherField"
                                autoCorrect={false}
                                testID="otherField"
                                maxLength={50}
                                blurOnSubmit={false}
                                editable={false}
                                onTextInputCreated={props.setInputReference}
                                returnKeyType="done"
                                onSubmitEditing={() => { Keyboard.dismiss(); }}
                            />
                        </View>
                    </View>
                </View>
            </View>

        )

    }
     else{
        return(
            <View style={{justifyContent:'center',alignItems:'center'}}>
                <Text> NO ITEMS</Text>
            </View>
        )
    }


    };

const styles = StyleSheet.create({
    textInput:{
        marginTop:5,
        paddingLeft:10,
       // borderColor:'#0d60aa',
        borderColor:'#e4e4e4',
        borderRadius:0,
        height:35
    },
    validatePlaceHolder:{color:'rgba(98,98,98,1)',fontWeight:'600', fontSize:15},
    addItemButton:{
        width :'40%',
        height:35,
        borderRadius:35,
        borderColor:'#4785bd',
        borderWidth:1,
        backgroundColor:'#0059a6',
        alignItems:'center',
        justifyContent: 'center',
        marginTop:5
    },
    editItemButton:{
        width :'40%',
        height:35,
        borderRadius:35,
        borderColor:'#4785bd',
        borderWidth:1,
        backgroundColor:'#0059a6',
        alignItems:'center',
        justifyContent: 'center',
        marginTop:5
    },
    addItemText:{
        fontSize:12,
        fontWeight:'bold',
        color:'white'
    }

})

ItemsInvoiceComponent.propTypes = {
    getActiveSlide: PropTypes.func,
    addItemPress : PropTypes.func,
    onScroll: PropTypes.func,
    activeSlide:PropTypes.string,
    onSubmitEditing :PropTypes.func,
    setInputReference :PropTypes.func,
    focusNextField :PropTypes.func,
};

export default ItemsInvoiceComponent;
