/* eslint-disable global-require */
/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React from 'react';
import {
    StyleSheet,
    View,
    Dimensions,
    Text,
    Image,
    TouchableOpacity,
    KeyboardAvoidingView, Keyboard,
    TouchableWithoutFeedback,
} from 'react-native';
import PropTypes from 'prop-types';

import InputText from '../CustomComponent/TextInput'
import Button from '../CustomComponent/Button';
import logo from '../../../common/images/logo_brand.png'
import { normalizeFont } from '../../../common/util';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';



const { width,height } = Dimensions.get('window');
const LoginComponent = props => (
    <TouchableWithoutFeedback onPress={() => { Keyboard.dismiss(0); }}>
        <KeyboardAwareScrollView
            scrollEnabled
            showsVerticalScrollIndicator={true}
            enableOnAndroid
            style ={{height:'100%'}}
            keyboardShouldPersistTaps="handled"
            enableResetScrollToCoords={true}
        >
            <View style = {styles.backgroundView}>
                <View style={styles.headerContainer}>
                    <Image source={logo} style={{ height: 60, width: 60, }} />
                </View>
                <View style={styles.loginBoxContainer}>

                    <View>
                        <Text style = {{color:'white',fontWeight:'600', fontSize:15}}>User Name</Text>
                        <InputText
                            style ={styles.textInput}
                            onChangeText={props.handleUsernameChange}
                            value={props.username}
                            autoCapitalize="none"
                            autoCorrect={false}
                            returnKeyType="next"
                            testID="Username"
                           // maxLength={50}
                            blurOnSubmit={false}
                            onTextInputCreated={props.setInputReference}
                            returnKeyType="next"
                            onSubmitEditing={() => props.onSubmitEditing('password')}
                        />
                    </View>
                    <View style = {{ marginTop:10}} >
                        <Text style = {{color:'white',fontWeight:'600', fontSize:15}}>Password</Text>
                        <InputText
                            style ={styles.textInput}
                            secureTextEntry
                            autoCapitalize="none"
                            onChangeText={props.handlePasswordChange}
                            value={props.password}
                            name="password"
                            returnKeyType="done"
                           // maxLength={10}
                            blurOnSubmit={false}
                            onTextInputCreated={props.setInputReference}
                            onSubmitEditing={() => { Keyboard.dismiss(); }}
                        />
                    </View>
                </View>
                <View style={styles.forgetPasswordParentStyle}>
                    <Button style={{ marginTop: 10, borderRadius:30, backgroundColor:'#d9e6f7', width:'40%' }} text="Log In" testID="loginButton" onPress={props.onPressLogin} />
                    <Text style={{color:'white', marginTop:20}}>Forget Password?</Text>
                </View>
            </View>
        </KeyboardAwareScrollView>
    </TouchableWithoutFeedback>

);

LoginComponent.propTypes = {
};


const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
    },
    backgroundView:{
        backgroundColor: '#4888d9',
        flex:1,
        height:height,
        width:'100%',
        paddingTop:'30%',
        justifyContent:'center',
        alignItems:'center'
    },
    headerContainer: {
        flex: 0.2,
        width,
        alignItems: 'center',
        justifyContent: 'center',
    },
    loginBoxContainer: {
        flex: 1,
        paddingLeft: 60,
        paddingRight: 60,
        justifyContent: 'center',
        width,
        //backgroundColor:'red'
    },
    textInput:{
        backgroundColor:'#5994dc',
        borderWidth:0,
        borderRadius:0,
        marginTop:5,
        width:'100%',
        paddingLeft:10,
        color:'white'

    },
    forgetPasswordParentStyle: {
        flex: 1,
        width,
        justifyContent:'center',
        alignItems:'center',
        //backgroundColor:'pink'
    },
    forgotPasswordTextStyle: {
        color: '#6B7889',
        justifyContent: 'center',
        fontSize: normalizeFont(16),
    },
    getHelpButtonTextStyle: {
        fontWeight: '700',
        fontSize: normalizeFont(18),
        textAlign: 'center',
        color: '#FF9400',
    },
    headerTitle: {
        color: 'black',
        fontSize: normalizeFont(36),

        fontWeight: '300',
        fontFamily: 'System',
        marginTop: 42,
    },
    facebookButtonStyle: {
        backgroundColor: 'rgba(61,90,150,1)',
        height: 45,
        borderRadius: 4,
        marginTop: 16,
        justifyContent: 'center',
        alignItems: 'center',
    },
    facebookParentStyle: {
        flex: 1,
        backgroundColor: '#F2F3F8',
        justifyContent: 'space-between',

    },
});
export default LoginComponent;
