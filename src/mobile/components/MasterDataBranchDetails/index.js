/* eslint-disable global-require */
/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React from 'react';
import {
	StyleSheet,
	View,
	Dimensions,
	Text,
	TouchableOpacity,
	TouchableHighlight,
	TextInput,
	TouchableWithoutFeedback,
	Keyboardx,
	ScrollView
} from 'react-native';
import Panel from '../Panel';
import moment from 'moment';
import PropTypes from 'prop-types';
import DatePicker from 'react-native-datepicker';
import Swiper from 'react-native-swiper';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import InputText from '../CustomComponent/TextInput';
import Accordion from '../ColapsPanel';

const MasterDataBranchDetails = (props) => {
	let totalAmount = 0;
	let selectedIndex = 0;
	if (props.data && props.data.length > 0) {
		return (
			// <View >
			<KeyboardAwareScrollView
				scrollEnabled
				showsVerticalScrollIndicator={true}
				enableOnAndroid
				style={{ height: '88%' }}
				keyboardShouldPersistTaps="handled"
				enableResetScrollToCoords={true}
			>
				<View style={{ flex: 1 }}>
					{props.data.map((item) => {
						return (
							<View>
								<ScrollView>
									<Accordion Header={`Bank: ${item.name}`} subHeader={`GSTIN: ${item.gstin}`}>
										<View>
											<View style={{ flex: 1, marginBottom: 5 }}>
												<View style={{ flexDirection: 'row' }}>
													<Text style={{ color: '#4785bd' }}>Branch Name:</Text>
													<Text>{`${item.name ? item.name : 'NA'}`}</Text>
												</View>
											</View>
											<View style={{ flex: 1, marginBottom: 5, justifyContent: 'space-between' }}>
												<View style={{ flexDirection: 'row', marginBottom: 5 }}>
													<Text style={{ color: '#4785bd' }}>Branch GSTIN:</Text>
													<Text>{`${item.gstin ? item.gstin : 'NA'}`}</Text>
												</View>
												<View style={{ flexDirection: 'row', marginBottom: 5 }}>
													<Text style={{ color: '#4785bd' }}>Branch GST State code:</Text>
													<Text>{`${item.gst_state_code ? item.gst_state_code : 'NA'}`}</Text>
												</View>
											</View>
											<View style={{ flex: 1, marginBottom: 5, justifyContent: 'space-between' }}>
												<View style={{ flexDirection: 'row', marginBottom: 5 }}>
													<Text style={{ color: '#4785bd' }}>Branch GST Type:</Text>
													<Text>{`${item.registration_type ? item.registration_type : 'NA'}`}</Text>
												</View>
											</View>
											<View style={{ flex: 1, marginBottom: 5, justifyContent: 'space-between' }}>
												<View style={{ flexDirection: 'row', marginBottom: 5 }}>
													<Text style={{ color: '#4785bd' }}>Branch Addr:</Text>
													<Text>{`${item.address_line1} ${item.address_line2}`}</Text>
												</View>
											</View>
											<View style={{ flex: 1, marginBottom: 5, justifyContent: 'space-between' }}>
												<View style={{ flexDirection: 'row', marginBottom: 5 }}>
													<Text style={{ color: '#4785bd' }}>city:</Text>
													<Text>{`${item.city ? item.city : 'NA'}`}</Text>
												</View>
											</View>
											<View style={{ flex: 1, marginBottom: 5, justifyContent: 'space-between' }}>
												<View style={{ flexDirection: 'row', marginBottom: 5 }}>
													<Text style={{ color: '#4785bd' }}>State:</Text>
													<Text>{`${item.state ? item.state : 'NA'}`}</Text>
												</View>
											</View>
											<View style={{ flex: 1, marginBottom: 5, justifyContent: 'space-between' }}>
												<View style={{ flexDirection: 'row', marginBottom: 5 }}>
													<Text style={{ color: '#4785bd' }}>Country:</Text>
													<Text>{`${item.country ? item.country : 'NA'}`}</Text>
												</View>
											</View>
											<View style={{ flex: 1, marginBottom: 5, justifyContent: 'space-between' }}>
												<View style={{ flexDirection: 'row', marginBottom: 5 }}>
													<Text style={{ color: '#4785bd' }}>Postal Code:</Text>
													<Text>{`${item.postal_code ? item.postal_code : 'NA'}`}</Text>
												</View>
											</View>
											<View style={{ flex: 1, marginBottom: 5, justifyContent: 'space-between' }}>
												<View style={{ flexDirection: 'row', marginBottom: 5 }}>
													<Text style={{ color: '#4785bd' }}>Contact Name:</Text>
													<Text>{`${item.contact_name ? item.contact_name : 'NA'}`}</Text>
												</View>
											</View>
											<View style={{ flex: 1, marginBottom: 5, justifyContent: 'space-between' }}>
												<View style={{ flexDirection: 'row', marginBottom: 5 }}>
													<Text style={{ color: '#4785bd' }}>Contact Designation:</Text>
													<Text>{`${item.contact_desg ? item.contact_desg : 'NA'}`}</Text>
												</View>
											</View>
											<View style={{ flex: 1, marginBottom: 5, justifyContent: 'space-between' }}>
												<View style={{ flexDirection: 'row', marginBottom: 5 }}>
													<Text style={{ color: '#4785bd' }}>Contact Email:</Text>
													<Text>{`${item.contact_email ? item.contact_email : 'NA'}`}</Text>
												</View>
											</View>
											<View style={{ flex: 1, marginBottom: 5, justifyContent: 'space-between' }}>
												<View style={{ flexDirection: 'row', marginBottom: 5 }}>
													<Text style={{ color: '#4785bd' }}>Contact Phone:</Text>
													<Text>{`${item.contact_phone ? item.contact_phone : 'NA'}`}</Text>
												</View>
											</View>
											<View style={{ flex: 1, marginBottom: 5, justifyContent: 'space-between' }}>
												<View style={{ flexDirection: 'row', marginBottom: 5 }}>
													<Text style={{ color: '#4785bd' }}>Contact Mobile:</Text>
													<Text>{`${item.contact_mobile ? item.contact_mobile : 'NA'}`}</Text>
												</View>
											</View>
										</View>
									</Accordion>
								</ScrollView>
							</View>
						);
					})}
				</View>
			</KeyboardAwareScrollView>
			// </View>
		);
	} else {
		return (
			<View style={{ justifyContent: 'center', alignItems: 'center' }}>
				<Text> NO ITEMS</Text>
			</View>
		);
	}
};

const styles = StyleSheet.create({
	card: {
		flex: 1,
		flexDirection: 'column',
		marginTop: 5,
		marginBottom: 5,
		marginLeft: 5,
		marginRight: 5,
		paddingLeft: 10,
		paddingRight: 10,
		paddingTop: 10,
		paddingBottom: 10,
		borderWidth: 1,
		borderRadius: 10,
		borderColor: '#000',
		borderBottomWidth: 0,
		shadowColor: '#000',
		shadowOffset: { width: 0, height: 2 },
		shadowOpacity: 0.8,
		shadowRadius: 2,
		elevation: 1,
		backgroundColor: '#fff'
	},
	textInput: {
		marginTop: 5,
		paddingLeft: 10,
		// borderColor:'#0d60aa',
		borderColor: '#e4e4e4',
		borderRadius: 0,
		height: 35
	},
	validatePlaceHolder: { color: 'rgba(98,98,98,1)', fontWeight: '600', fontSize: 15 },
	addItemButton: {
		width: '40%',
		height: 35,
		borderRadius: 35,
		borderColor: '#4785bd',
		borderWidth: 1,
		backgroundColor: '#0059a6',
		alignItems: 'center',
		justifyContent: 'center',
		marginTop: 5
	},
	editItemButton: {
		width: '40%',
		height: 35,
		borderRadius: 35,
		borderColor: '#4785bd',
		borderWidth: 1,
		backgroundColor: '#0059a6',
		alignItems: 'center',
		justifyContent: 'center',
		marginTop: 5
	},
	addItemText: {
		fontSize: 12,
		fontWeight: 'bold',
		color: 'white'
	}
});

MasterDataBranchDetails.propTypes = {
	getActiveSlide: PropTypes.func,
	addItemPress: PropTypes.func,
	onScroll: PropTypes.func,
	activeSlide: PropTypes.string,
	onSubmitEditing: PropTypes.func,
	setInputReference: PropTypes.func,
	focusNextField: PropTypes.func
};

export default MasterDataBranchDetails;
