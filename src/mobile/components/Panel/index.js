
/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View,Dimensions,TouchableHighlight,Image,Animated,ScrollView} from 'react-native';

type Props = {};

const { width } = Dimensions.get('window');

export default class Panel extends Component<Props> {
    constructor(props){
        super(props);

        this.icons = {     //Step 2
            'up'    : require('../../../common/images/Arrowhead-up2.png'),
            'down'  : require('../../../common/images/Arrowhead-down1.png')
        };

        this.state = {       //Step 3
            title       : props.title,
            expanded    : true,
            animation   : new Animated.Value()
        };
    }

    toggle(){
        //Step 1
        let initialValue    = this.state.expanded? this.state.maxHeight + this.state.minHeight : this.state.minHeight,
            finalValue      = this.state.expanded? this.state.minHeight +10: this.state.maxHeight + this.state.minHeight ;

        this.setState({
            expanded : !this.state.expanded  //Step 2
        });

        this.state.animation.setValue(initialValue);  //Step 3
        Animated.spring(     //Step 4
            this.state.animation,
            {
                toValue: finalValue
            }
        ).start();  //Step 5
    }

    _setMaxHeight(event){
        this.setState({
            maxHeight   : event.nativeEvent.layout.height
        });
    }

    _setMinHeight(event){
        this.setState({
            minHeight   : event.nativeEvent.layout.height
        });
    }

    render(){
        let icon = this.icons['down'];

        if(this.state.expanded){
            icon = this.icons['up'];   //Step 4
        }


        //Step 5
        return (
            <View style = {{width:'100%', justifyContent:'center', alignItems:'center',backgroundColor:'#eeeeee'}}>
                <Animated.View
                    // style={[styles.insideContainer,{height: this.state.animation}]}>
                    style={[styles.insideContainer]}>

                    <View style={styles.container} >
                        <View style = {{flexDirection:'row', justifyContent:'space-between', alignItems:'center'}}>
                            <View style={styles.titleContainer} onLayout={this._setMinHeight.bind(this)}>
                                <Text style={styles.title}>{this.state.title}</Text>
                                <View style = {{height:'100%'}}>
                                    <TouchableHighlight
                                        style={styles.buttonArrow}
                                        onPress={this.toggle.bind(this)}
                                        underlayColor="#f1f1f1">
                                        <Image
                                            style={styles.buttonImage}
                                            source={icon}
                                        ></Image>
                                    </TouchableHighlight>
                                </View>
                            </View>
                            <View>
                                <TouchableHighlight
                                    style={styles.button}
                                    onPress={this.toggle.bind(this)}
                                    underlayColor="#f1f1f1">
                                    <Text style = {{fontSize:12, fontWeight:'bold',color:'#4785bd'}}>View all</Text>
                                </TouchableHighlight>
                            </View>
                        </View>

                        <ScrollView>
                            <View style={styles.body} onLayout={this._setMaxHeight.bind(this)}>
                                {this.props.children}
                            </View>
                        </ScrollView>

                    </View>
                </Animated.View>
            </View>
        );
    }
}

var styles = StyleSheet.create({
    container   : {
        marginLeft:10,
        marginRight:10,
        marginTop:5,
        overflow:'hidden',
       // backgroundColor:'green'
    },
    insideContainer   : {
        marginTop:5,
        overflow:'hidden',
        borderRadius:5,
        borderColor:'#dadada',
        borderWidth:1,
        width:'95%',
        backgroundColor:'white',
        flex:1
    },
    titleContainer : {
        flexDirection: 'row',
        justifyContent:'center',
        alignContent:'center',
        alignItems:'center',
        height:'100%',

    },
    title       : {
        //paddingBottom : 5,
        color   :'#2a2f43',
        fontSize:13
    },
    buttonArrow      : {
    },
    buttonImage : {
        marginLeft:10,
        width   : 25,
        height  : 25,
    },
    body        : {
        marginTop:5,
        borderColor:'#505050',
        borderTopWidth:.6
    }
});