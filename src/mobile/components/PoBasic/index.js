/* eslint-disable global-require */
/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React from 'react';
import {
	StyleSheet,
	View,
	Dimensions,
	Text,
	TouchableOpacity,
	TouchableHighlight,
	TextInput,
	TouchableWithoutFeedback,
	Keyboardx
} from 'react-native';
import moment from 'moment';
import PropTypes from 'prop-types';
import DatePicker from 'react-native-datepicker';
import Swiper from 'react-native-swiper';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import InputText from '../CustomComponent/TextInput';

const PoBasic = (props) => {
	let totalAmount = 0;
	let selectedIndex = 0;
	if (props.data) {
		const item = props.data;
		return (
			// <View >
			<KeyboardAwareScrollView
				scrollEnabled
				showsVerticalScrollIndicator={true}
				enableOnAndroid
				style={{ height: '88%' }}
				keyboardShouldPersistTaps="handled"
				enableResetScrollToCoords={true}
			>
				<View style={{ flex: 1 }}>
					<View style={styles.card} key={item.bill_id}>
						<View style={{ flex: 1, marginBottom: 5 }}>
							<View style={{ flexDirection: 'row' }}>
								<Text style={{ color: '#4785bd' }}>PO Number:</Text>
								<Text>{` ${item.po_no ? item.po_no : 'NA'}`}</Text>
							</View>
						</View>
						<View style={{ flex: 1, marginBottom: 5, justifyContent: 'space-between' }}>
							<View style={{ flexDirection: 'row', marginBottom: 5 }}>
								<Text style={{ color: '#4785bd' }}>Shipping Mode:</Text>
								<Text>{` ${item.shipping_mode ? item.shipping_mode : 'NA'}`}</Text>
							</View>
						</View>
						<View style={{ flex: 1, marginBottom: 5, justifyContent: 'space-between' }}>
							<View style={{ flexDirection: 'row', marginBottom: 5 }}>
								<Text style={{ color: '#4785bd' }}>GST %:</Text>
								<Text>{` ${item.gst_to_match ? item.gst_to_match : 'NA'}`}</Text>
							</View>
						</View>
						<View style={{ flex: 1, marginBottom: 5, justifyContent: 'space-between' }}>
							<View style={{ flexDirection: 'row', marginBottom: 5 }}>
								<Text style={{ color: '#4785bd' }}>Delivery Charges Amount:</Text>
								<Text>{` ${item.dc_to_match ? item.dc_to_match : 'NA'}`}</Text>
							</View>
						</View>
						<View style={{ flex: 1, marginBottom: 5, justifyContent: 'space-between' }}>
							<View style={{ flexDirection: 'row', marginBottom: 5 }}>
								<Text style={{ color: '#4785bd' }}>Packing Charges Amount:</Text>
								<Text>{` ${item.pc_to_match ? item.pc_to_match : 'NA'}`}</Text>
							</View>
						</View>
						<View style={{ flex: 1, marginBottom: 5, justifyContent: 'space-between' }}>
							<View style={{ flexDirection: 'row', marginBottom: 5 }}>
								<Text style={{ color: '#4785bd' }}>Other Charges Amount:</Text>
								<Text>{` ${item.oc_to_match ? item.oc_to_match : 'NA'}`}</Text>
							</View>
						</View>
						
						<View style={{ flex: 1, marginBottom: 5, padding: 5, justifyContent: 'space-between', borderWidth: 1, borderRadius: 10, borderColor: '#000'}}>
							<View style={{ flexDirection: 'row', marginBottom: 5 }}>
								<Text style={{ color: '#4785bd' }}>Expense Item Names:</Text>
							</View>
							{
								item.expense_item_names.length ? item.expense_item_names.map(value =>(<View>
										<View style={{ flexDirection: 'row', marginBottom: 5 }}>
											<Text style={{ color: '#4785bd' }}>* {value.name}:</Text>
											<Text>{value.approver_required ? 'True' : 'False'}</Text>
										</View>
									</View>)
								) : <View><Text>NA</Text></View>
							}
						</View>
						<View style={{ flex: 1, marginBottom: 5, padding: 5, justifyContent: 'space-between', borderWidth: 1, borderRadius: 10, borderColor: '#000' }}>
							<View style={{ flexDirection: 'row', marginBottom: 5 }}>
								<Text style={{ color: '#4785bd' }}>Inventary Item Names:</Text>
							</View>
							{
								item.inventory_item_names.length ? item.inventory_item_names.map(value =>(<View style={{marginLeft: 10}}>
										<View style={{ flexDirection: 'row', marginBottom: 5 }}>
											<Text style={{ color: '#4785bd' }}>* {value.name}:</Text>
											<Text>{value.approver_required ? 'True' : 'False'}</Text>
										</View>
									</View> ) 
								)
								: <View><Text>NA</Text></View>
							}
						</View>

					</View>
				</View>
			</KeyboardAwareScrollView>
			// </View>
		);
	} else {
		return (
			<View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
				<Text> NO ITEMS</Text>
			</View>
		);
	}
};

const styles = StyleSheet.create({
	card: {
		flex: 1,
		flexDirection: 'column',
		marginTop: 5,
		marginBottom: 5,
		marginLeft: 5,
		marginRight: 5,
		paddingLeft: 10,
		paddingRight: 10,
		paddingTop: 10,
		paddingBottom: 10,
		borderWidth: 1,
		borderRadius: 10,
		borderColor: '#000',
		borderBottomWidth: 0,
		shadowColor: '#000',
		shadowOffset: { width: 0, height: 2 },
		shadowOpacity: 0.8,
		shadowRadius: 2,
		elevation: 1,
		backgroundColor: '#fff'
	},
	textInput: {
		marginTop: 5,
		paddingLeft: 10,
		// borderColor:'#0d60aa',
		borderColor: '#e4e4e4',
		borderRadius: 0,
		height: 35
	},
	validatePlaceHolder: { color: 'rgba(98,98,98,1)', fontWeight: '600', fontSize: 15 },
	addItemButton: {
		width: '40%',
		height: 35,
		borderRadius: 35,
		borderColor: '#4785bd',
		borderWidth: 1,
		backgroundColor: '#0059a6',
		alignItems: 'center',
		justifyContent: 'center',
		marginTop: 5
	},
	editItemButton: {
		width: '40%',
		height: 35,
		borderRadius: 35,
		borderColor: '#4785bd',
		borderWidth: 1,
		backgroundColor: '#0059a6',
		alignItems: 'center',
		justifyContent: 'center',
		marginTop: 5
	},
	addItemText: {
		fontSize: 12,
		fontWeight: 'bold',
		color: 'white'
	}
});

PoBasic.propTypes = {
	getActiveSlide: PropTypes.func,
	addItemPress: PropTypes.func,
	onScroll: PropTypes.func,
	activeSlide: PropTypes.string,
	onSubmitEditing: PropTypes.func,
	setInputReference: PropTypes.func,
	focusNextField: PropTypes.func
};

export default PoBasic;
