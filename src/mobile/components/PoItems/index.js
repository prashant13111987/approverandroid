/* eslint-disable global-require */
/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React from 'react';
import {
	StyleSheet,
	View,
	Dimensions,
	Text,
	TouchableOpacity,
	TouchableHighlight,
	TextInput,
	TouchableWithoutFeedback,
	Keyboard
} from 'react-native';
import PropTypes from 'prop-types';
import Swiper from 'react-native-swiper';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import InputText from '../CustomComponent/TextInput';

const PoItems = (props) => {
	let totalAmount = 0;
	let selectedIndex = 0;
	if (props.data.stock_items.length > 0) {
		return (
			// <View >
			<KeyboardAwareScrollView
				scrollEnabled
				showsVerticalScrollIndicator={true}
				enableOnAndroid
				style={{ height: '88%' }}
				keyboardShouldPersistTaps="handled"
				enableResetScrollToCoords={true}
			>
				<View style={{ flex: 1 }}>
					{props.data.stock_items.length ? props.data.stock_items.map((item) => {
						return (
							<View
								style={{
									flex: 1,
									flexDirection: 'column',
									// height: 70,
									// alignItems: 'flex-start',
									marginTop: 5,
									marginBottom: 5,
									marginLeft: 5,
									marginRight: 5,
									paddingLeft: 10,
									paddingRight: 10,
									paddingTop: 10,
									paddingBottom: 10,
									borderWidth: 1,
									borderRadius: 10,
									borderColor: '#000',
									borderBottomWidth: 0,
									shadowColor: '#000',
									shadowOffset: { width: 0, height: 2 },
									shadowOpacity: 0.8,
									shadowRadius: 2,
									elevation: 1,
									backgroundColor: '#fff'
								}}
								key={item.bill_id}
							>
								<View style={{ flex: 1, marginBottom: 5 }}>
									<View style={{ flexDirection: 'row' }}>
										<Text style={{ color: '#4785bd' }}>Product Code:</Text>
										<Text>{` ${item.item_code ? item.item_code : 'NA'}`}</Text>
									</View>
								</View>
								<View style={{ flex: 1, marginBottom: 5 }}>
									<View style={{ flexDirection: 'row' }}>
										<Text style={{ color: '#4785bd' }}>Name:</Text>
										<Text>{` ${item.name ? item.name : 'NA'}`}</Text>
									</View>
								</View>
								<View style={{ flex: 1, marginBottom: 5 }}>
									<View style={{ flexDirection: 'row' }}>
										<Text style={{ color: '#4785bd' }}>Description:</Text>
										<Text>{` ${item.description ? item.description : 'NA'}`}</Text>
									</View>
								</View>
								<View style={{ flex: 1, marginBottom: 5, justifyContent: 'space-between' }}>
									<View style={{ flexDirection: 'row', marginBottom: 5 }}>
										<Text style={{ color: '#4785bd' }}>HSN:</Text>
										<Text>{` ${item.hsn_code ? item.hsn_code : 'NA'}`}</Text>
									</View>
								</View>
								<View style={{ flex: 1, marginBottom: 5, justifyContent: 'space-between' }}>
									<View style={{ flexDirection: 'row', marginBottom: 5 }}>
										<Text style={{ color: '#4785bd' }}>Quantity:</Text>
										<Text>{` ${item.quantity ? item.quantity : 'NA'}`}</Text>
									</View>
								</View>
								<View style={{ flex: 1, marginBottom: 5, justifyContent: 'space-between' }}>
									<View style={{ flexDirection: 'row', marginBottom: 5 }}>
										<Text style={{ color: '#4785bd' }}>Unit:</Text>
										<Text>{` ${item.unit ? item.unit : 'NA'}`}</Text>
									</View>
								</View>
								<View style={{ flex: 1, marginBottom: 5, justifyContent: 'space-between' }}>
									<View style={{ flexDirection: 'row', marginBottom: 5 }}>
										<Text style={{ color: '#4785bd' }}>Discount:</Text>
										<Text>{` ${item.discount ? item.discount : 'NA'}`}</Text>
									</View>
								</View>
								<View style={{ flex: 1, marginBottom: 5 }}>
									<View style={{ flexDirection: 'row', marginBottom: 5 }}>
										<Text style={{ color: '#4785bd' }}>Ledger:</Text>
										<Text>{` ${item.ledger_name ? item.ledger_name : 'NA'}`}</Text>
									</View>
									<View style={{ flexDirection: 'row', marginBottom: 5 }}>
										<Text style={{ color: '#4785bd' }}>Amount:</Text>
										<Text>{` ${item.total ? parseFloat(item.total) : 'NA'}`}</Text>
									</View>
								</View>
							</View>
						);
					}) : <View style={{ justifyContent: 'center', alignItems: 'center' }}>
							<Text> NO ITEMS</Text>
						</View>}
				</View>
			</KeyboardAwareScrollView>
			// </View>
		);
	} else {
		return (
			<View style={{ justifyContent: 'center', alignItems: 'center' }}>
				<Text> NO ITEMS</Text>
			</View>
		);
	}
};

const styles = StyleSheet.create({
	textInput: {
		marginTop: 5,
		paddingLeft: 10,
		// borderColor:'#0d60aa',
		borderColor: '#e4e4e4',
		borderRadius: 0,
		height: 35
	},
	validatePlaceHolder: { color: 'rgba(98,98,98,1)', fontWeight: '600', fontSize: 15 },
	addItemButton: {
		width: '40%',
		height: 35,
		borderRadius: 35,
		borderColor: '#4785bd',
		borderWidth: 1,
		backgroundColor: '#0059a6',
		alignItems: 'center',
		justifyContent: 'center',
		marginTop: 5
	},
	editItemButton: {
		width: '40%',
		height: 35,
		borderRadius: 35,
		borderColor: '#4785bd',
		borderWidth: 1,
		backgroundColor: '#0059a6',
		alignItems: 'center',
		justifyContent: 'center',
		marginTop: 5
	},
	addItemText: {
		fontSize: 12,
		fontWeight: 'bold',
		color: 'white'
	}
});

PoItems.propTypes = {
	getActiveSlide: PropTypes.func,
	addItemPress: PropTypes.func,
	onScroll: PropTypes.func,
	activeSlide: PropTypes.string,
	onSubmitEditing: PropTypes.func,
	setInputReference: PropTypes.func,
	focusNextField: PropTypes.func
};

export default PoItems;
