/* eslint-disable global-require */
/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React from 'react';
import {
	StyleSheet,
	View,
	Dimensions,
	Text,
	TouchableOpacity,
	TouchableHighlight,
	TextInput,
	TouchableWithoutFeedback,
	Keyboardx
} from 'react-native';
import moment from 'moment';
import PropTypes from 'prop-types';
import DatePicker from 'react-native-datepicker';
import Swiper from 'react-native-swiper';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import InputText from '../CustomComponent/TextInput';

const PoSummery = (props) => {
	let totalAmount = 0;
	let selectedIndex = 0;
	if (props.data) {
		const item = props.data;
		return (
			// <View >
			<KeyboardAwareScrollView
				scrollEnabled
				showsVerticalScrollIndicator={true}
				enableOnAndroid
				style={{ height: '88%' }}
				keyboardShouldPersistTaps="handled"
				enableResetScrollToCoords={true}
			>
				<View style={{ flex: 1 }}>
					<View style={styles.card} key={item.bill_id}>
						<View style={{ flex: 1, marginBottom: 5 }}>
							<View style={{ flexDirection: 'row' }}>
								<Text style={{ color: '#4785bd' }}>Party Name:</Text>
								<Text>{` ${item.party_name ? item.party_name : 'NA'}`}</Text>
							</View>
						</View>
						<View style={{ flex: 1, marginBottom: 5, justifyContent: 'space-between' }}>
							<View style={{ flexDirection: 'row', marginBottom: 5 }}>
								<Text style={{ color: '#4785bd' }}>Amount:</Text>
								<Text>{` ${item.total ? item.total : 'NA'}`}</Text>
							</View>
						</View>
						<View style={{ flex: 1, marginBottom: 5, justifyContent: 'space-between' }}>
							<View style={{ flexDirection: 'row', marginBottom: 5 }}>
								<Text style={{ color: '#4785bd' }}>Delivery date:</Text>
								<Text>{` ${item.delivery_date
									? moment(item.delivery_date).format('YYYY-MM-DD')
									: 'NA'}`}</Text>
							</View>
						</View>
						<View style={{ flex: 1, marginBottom: 5, justifyContent: 'space-between' }}>
							<View style={{ flexDirection: 'row', marginBottom: 5 }}>
								<Text style={{ color: '#4785bd' }}>Department:</Text>
								<Text>{` ${item.department_name ? item.department_name : 'NA'}`}</Text>
							</View>
						</View>
						<View style={{ flex: 1, marginBottom: 5, justifyContent: 'space-between' }}>
							<View style={{ flexDirection: 'row', marginBottom: 5 }}>
								<Text style={{ color: '#4785bd' }}>Rush:</Text>
								<Text>{` ${item.Rush ? 'Yes' : 'No'}`}</Text>
							</View>
						</View>
						<View style={{ flex: 1, marginBottom: 5, justifyContent: 'space-between' }}>
							<View style={{ flexDirection: 'row', marginBottom: 5 }}>
								<Text style={{ color: '#4785bd' }}>Creator Name:</Text>
								<Text>{` ${item.creator_name ? item.creator_name : item.creator_name}`}</Text>
							</View>
						</View>
					</View>
				</View>
			</KeyboardAwareScrollView>
			// </View>
		);
	} else {
		return (
			<View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
				<Text> NO ITEMS</Text>
			</View>
		);
	}
};

const styles = StyleSheet.create({
	card: {
		flex: 1,
		flexDirection: 'column',
		marginTop: 5,
		marginBottom: 5,
		marginLeft: 5,
		marginRight: 5,
		paddingLeft: 10,
		paddingRight: 10,
		paddingTop: 10,
		paddingBottom: 10,
		borderWidth: 1,
		borderRadius: 10,
		borderColor: '#000',
		borderBottomWidth: 0,
		shadowColor: '#000',
		shadowOffset: { width: 0, height: 2 },
		shadowOpacity: 0.8,
		shadowRadius: 2,
		elevation: 1,
		backgroundColor: '#fff'
	},
	textInput: {
		marginTop: 5,
		paddingLeft: 10,
		// borderColor:'#0d60aa',
		borderColor: '#e4e4e4',
		borderRadius: 0,
		height: 35
	},
	validatePlaceHolder: { color: 'rgba(98,98,98,1)', fontWeight: '600', fontSize: 15 },
	addItemButton: {
		width: '40%',
		height: 35,
		borderRadius: 35,
		borderColor: '#4785bd',
		borderWidth: 1,
		backgroundColor: '#0059a6',
		alignItems: 'center',
		justifyContent: 'center',
		marginTop: 5
	},
	editItemButton: {
		width: '40%',
		height: 35,
		borderRadius: 35,
		borderColor: '#4785bd',
		borderWidth: 1,
		backgroundColor: '#0059a6',
		alignItems: 'center',
		justifyContent: 'center',
		marginTop: 5
	},
	addItemText: {
		fontSize: 12,
		fontWeight: 'bold',
		color: 'white'
	}
});

PoSummery.propTypes = {
	getActiveSlide: PropTypes.func,
	addItemPress: PropTypes.func,
	onScroll: PropTypes.func,
	activeSlide: PropTypes.string,
	onSubmitEditing: PropTypes.func,
	setInputReference: PropTypes.func,
	focusNextField: PropTypes.func
};

export default PoSummery;
