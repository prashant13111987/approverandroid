/* eslint-disable global-require */
/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React from 'react';
import {
    StyleSheet,
    View,
    Dimensions,
    Text,
    TouchableOpacity,
    TouchableHighlight,
    TextInput,
    TouchableWithoutFeedback, Keyboard, Image
} from 'react-native';
import PropTypes from 'prop-types';
import InputText from '../CustomComponent/TextInput'
import DatePicker from 'react-native-datepicker'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import CustomView from '../CustomComponent/CustomBorderView'
import nextImage from "../../../common/images/nextArrow.png";

const { width ,height} = Dimensions.get('window');

const SendForApprovalComponent = props => {
    return(
        <View style={{height:'100%', backgroundColor:'white'}}>
            <KeyboardAwareScrollView
                scrollEnabled
                showsVerticalScrollIndicator={true}
                enableOnAndroid
                keyboardShouldPersistTaps="handled"
                enableResetScrollToCoords={true}
            >

                <View style = {{alignItems:'center',paddingTop:10,paddingBottom:10}}>
                    <View style = {{marginTop:'5%',width:'80%'}}>
                        <Text style = {styles.validatePlaceHolder}>Vender Name</Text>
                        <InputText
                            style={{ marginTop: 4}}
                            onChangeText={props.handleInvoiceTitleChange}
                            value={props.invoiceTitle}
                            autoCapitalize="none"
                            autoCorrect={false}
                            testID="invoiceTitle"
                            maxLength={50}
                            blurOnSubmit={false}
                            editable={false}
                            name="totalAmount"
                            onTextInputCreated={props.setInputReference}
                            returnKeyType="next"
                            onSubmitEditing={() => props.onSubmitEditing('invoiceAmount')}
                        />
                    </View>
                    <View style = {{width:'80%',marginTop:10}}>
                        <Text style = {styles.validatePlaceHolder}>Amount</Text>
                        <InputText
                            style={{ marginTop: 4}}
                            onChangeText={props.handleInvoiceAmountChange}
                            value={parseFloat(props.invoiceAmount).toFixed(2)}
                            //value={props.invoiceAmount.toString()}
                            autoCapitalize="none"
                            autoCorrect={false}
                            testID="invoiceAmount"
                            maxLength={50}
                            blurOnSubmit={false}
                            editable={false}
                            name="invoiceAmount"
                            onTextInputCreated={props.setInputReference}
                            returnKeyType="next"
                            onSubmitEditing={() => props.onSubmitEditing('notes')}
                        />
                    </View>
                    <View style = {{width:'80%',marginTop:10 }}>
                        <Text style = {styles.validatePlaceHolder}>Department</Text>
                        <TouchableOpacity
                            onPress={props.onDistributionPress}
                            style={{ marginTop: 4}}
                        >
                            <CustomView styles={{
                                flexDirection: 'row',
                                justifyContent: 'space-between',
                            }}
                            >
                                <Text
                                    allowFontScaling={false}
                                    style={styles.listItemText}
                                >{props.department}
                                </Text>
                                <Image
                                    source={nextImage}
                                    style={styles.forwardArrowImageStyle}
                                />
                            </CustomView>
                        </TouchableOpacity>
                    </View>

                    <View style = {{width:'80%',marginTop:10}}>
                    <Text style = {styles.validatePlaceHolder}>Notes</Text>
                        <InputText
                            style={{ marginTop: 4}}
                          onChangeText={props.handleNotesChange}
                          value={props.notes}
                          autoCapitalize="none"
                          autoCorrect={false}
                          testID="notes"
                          maxLength={50}
                          blurOnSubmit={false}
                          name="notes"
                          onTextInputCreated={props.setInputReference}
                          returnKeyType="done"
                          onSubmitEditing={() =>{ Keyboard.dismiss(); }}
                        />
                    </View>
                    <View style = {{width:'90%',paddingLeft:20,paddingRight:20,flexDirection:'row',justifyContent:'space-between'}}>
                        <View style = {{height:40}}>
                            <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 20 }}>
                                <TouchableOpacity
                                    style={styles.radioButtonStyle}
                                    onPress={()=>props.onRadioButtonSelect('rush',props.isRushSelected)}
                                >
                                    {props.isRushSelected ? <View style={styles.radioButtonTextStyle} /> : <View />}

                                </TouchableOpacity>
                                <Text style={{marginLeft:8}}>Rush</Text>

                            </View>
                        </View>

                        <View style = {{height:40}}>
                            <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 20 }}>
                                <TouchableOpacity
                                    style={styles.radioButtonStyle}
                                    onPress={()=>props.onRadioButtonSelect('receivedSubmit',props.isReceivedSubmitSelected)}
                                >
                                    {props.isReceivedSubmitSelected ? <View style={styles.radioButtonTextStyle} /> : <View />}

                                </TouchableOpacity>
                                <Text style={{marginLeft:8}}>Orignal received</Text>

                            </View>
                        </View>

                    </View>
                    <View style = {{width:'80%',marginTop:20}}>
                        <Text style = {styles.validatePlaceHolder}>Due date</Text>
                        <DatePicker
                            style={{width: '100%',marginTop:4}}
                            date={props.selectedDueDate}
                            mode="date"
                            placeholder="Select bill date"
                            format="YYYY-MM-DD"
                            minDate="2016-05-01"
                            maxDate="2019-06-01"
                            confirmBtnText="Confirm"
                            cancelBtnText="Cancel"
                            showIcon={false}
                            onDateChange={props.onDueDateChange}
                            customStyles={{
                                dateText: {
                                    fontSize:15
                                },
                                dateInput: {
                                    alignItems:'flex-start',
                                    paddingLeft:10,
                                    height: 40,
                                    // borderColor: '#cfcfcf',
                                    borderColor: '#0d60aa',
                                    borderRadius: 4,
                                    borderWidth: 1,
                                    color: '#0F2C5A',
                                    backgroundColor: 'white',
                                }
                                // ... You can check the source to find the other keys.
                            }}
                        />
                    </View>

        </View>
            </KeyboardAwareScrollView>
            <View style = {{height:'20%',paddingLeft:35,paddingRight:35, flexDirection:'row', justifyContent:'center',}}>

                <TouchableOpacity style={styles.bottomButtons} onPress={() => (props.onSendForApproval())}>
                    <Text style={{color:'white',fontSize:12}}>SEND FOR APPROVAL</Text>
                </TouchableOpacity>

            </View>
        </View>
    )};

const styles = StyleSheet.create({
    textInput:{
        marginTop:5,
        paddingLeft:10,
        borderColor:'#0d60aa',
        borderRadius:0,
        height:30
    },
    listItemText: {
        color: '#0F2C5A',
        fontSize: 14,
        fontFamily: 'System',
        // fontWeight: Platform.OS === 'ios' ? '500' : '400',
        //marginLeft: 16,
        alignSelf: 'center',
        width: width - 150,

    },
    // validatePlaceHolder:{color:'rgba(98,98,98,1)',fontWeight:'600', fontSize:12},
    bottomButtons:{
        alignItems:'center', backgroundColor:'#0d60aa',justifyContent:'center',width:150,height:35,borderRadius:40
    },
    radioButtonStyle: {
        marginLeft: 0,
        width: 15,
        height: 15,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'white',
        borderColor: '#555555',
        borderWidth: 1,
        borderRadius: 50,

    },
    radioButtonTextStyle: {
        width: 8,
        height: 8,
        backgroundColor: 'black',
        borderRadius: 50,
    },
    validatePlaceHolder:{color:'rgba(98,98,98,1)',fontWeight:'600', fontSize:12},
    forwardArrowImageStyle: {
        width: 15,
        height: 15,
        //  resizeMode: Image.resizeMode.contain,
        alignSelf: 'center',
        marginRight: 15,
    }
})

SendForApprovalComponent.propTypes = {
    onSubmitEditing :PropTypes.func,
    setInputReference :PropTypes.func,
    focusNextField :PropTypes.func,
    onDueDateChange :PropTypes.func,
    onRadioButtonSelect:PropTypes.func,
    handleInvoiceTitleChange:PropTypes.func,
    handleInvoiceAmountChange:PropTypes.func,
    selectedDueDate:PropTypes.string,
    invoiceAmount:PropTypes.string,
    invoiceTitle:PropTypes.string,
    onSendForApproval:PropTypes.func,
    isReceivedSubmitSelected:PropTypes.boolean,
    isRushSelected:PropTypes.boolean,
    onDistributionPress:PropTypes.func,
    notes:PropTypes.string,
};

export default SendForApprovalComponent;
