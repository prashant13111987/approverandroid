/* eslint-disable global-require */
/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React from 'react';
import {
    StyleSheet,
    View,
    Dimensions,
    Image,
    Text,
    TouchableOpacity,
    TouchableHighlight,
    TextInput,
    TouchableWithoutFeedback, Keyboard
} from 'react-native';
import PropTypes from 'prop-types';
import successImage from '../../../common/images/ic_success.png'
const SuccessScreenComponent = props => {
    return(
        <View style={{height:'100%',justifyContent:'center', alignItems:'center',backgroundColor:'white'}}>

            <View style = {{width:140, height:140}}>
                <Image source={successImage} style={{ height: 140, width: 140 }} />
            </View>
            <Text style = {{fontSize:20,marginTop:20, color:'black'}}>Successfully</Text>
            <Text style = {{fontSize:20, color:'black'}}>Send For Approval</Text>

            <TouchableOpacity style={styles.bottomButtons} onPress={() => (props.onCreateNew())}>
                <Text style={{color:'white',fontSize:12}}>Create new</Text>
            </TouchableOpacity>

            <TouchableOpacity style ={{marginTop:20}}onPress={() => (props.onGoToHome())}>
                <Text style={{color:'#629ee4',fontSize:14,fontWeight:'bold'}}>Go to home</Text>
            </TouchableOpacity>
        </View>
    )};

const styles = StyleSheet.create({
    bottomButtons:{
        alignItems:'center',marginTop:80, backgroundColor:'#0d60aa',justifyContent:'center',width:150,height:35,borderRadius:40
    }
})

SuccessScreenComponent.propTypes = {
    onGoToHome:PropTypes.func,
    onCreateNew:PropTypes.func,
};

export default SuccessScreenComponent;
