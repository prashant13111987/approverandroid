/* eslint-disable global-require */
/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React from 'react';
import {
	StyleSheet,
	View,
	Dimensions,
	Text,
	Image,
	Icon,
	TouchableOpacity,
	KeyboardAvoidingView,
	Keyboard,
	TouchableWithoutFeedback,
	Picker,
	Platform
} from 'react-native';
import PropTypes from 'prop-types';

import InputText from '../CustomComponent/TextInput';
import DatePicker from 'react-native-datepicker';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import RadioForm, { RadioButton, RadioButtonInput, RadioButtonLabel } from 'react-native-simple-radio-button';
import { normalizeFont } from '../../../common/util';
import Cancel from '../../../common/images/icons-cancel.png';
import Hand from '../../../common/images/icons-hand.png';
import check from '../../../common/images/ic_success_filled.png';

const { width } = Dimensions.get('window');
const radio_props = [
	{ label: 'Low', value: 0 },
	{ label: 'Medium', value: 1 },
	{ label: 'High', value: 1 },
	{ label: 'Urgent', value: 1 }
];
const SummeryInvoiceComponent = (props) => (
	<KeyboardAwareScrollView
		scrollEnabled
		showsVerticalScrollIndicator={true}
		enableOnAndroid
		style={{ height: '88%' }}
		keyboardShouldPersistTaps="handled"
		enableResetScrollToCoords={true}
	>
		<View style={{ flex: 1, alignItems: 'center', paddingTop: 10, backgroundColor: 'white' }}>
			<View style={{ width: '80%', marginTop: 20 }}>
				<Text style={styles.validatePlaceHolder}>Party Name</Text>
				<InputText
					style={{ marginTop: 4 }}
					onChangeText={props.handleInvoiceNumberChange}
					value={props.data.party_name}
					autoCapitalize="none"
					autoCorrect={false}
					testID="partyName"
					maxLength={50}
					blurOnSubmit={false}
					editable={props.isEditable}
					name="partyName"
					onTextInputCreated={props.setInputReference}
					returnKeyType="next"
					onSubmitEditing={() => props.onSubmitEditing('billNumber')}
				/>
			</View>
			<View style={{ width: '80%', marginTop: 20 }}>
				<Text style={styles.validatePlaceHolder}>Amount</Text>
				<InputText
					style={{ marginTop: 4 }}
					value={props.data.total}
					maxLength={50}
					name="amount"
					editable={props.isEditable}
				/>
			</View>
			<View style={{ width: '80%', marginTop: 20 }}>
				<Text style={styles.validatePlaceHolder}>TDS</Text>
				<InputText
					style={{ marginTop: 4 }}
					onChangeText={props.handleBillNumberChange}
					value={props.data.tdstopay}
					autoCapitalize="none"
					autoCorrect={false}
					testID="tds"
					maxLength={50}
					editable={props.isEditable}
					name="tds"
					blurOnSubmit={false}
					onTextInputCreated={props.setInputReference}
					returnKeyType="next"
					onSubmitEditing={() => props.onSubmitEditing('poNumber')}
				/>
			</View>
			{props.data.due_date ? <View style={{ width: '80%', marginTop: 20 }}>
				<Text style={styles.validatePlaceHolder}>Due date</Text>
				<DatePicker
					style={{ width: '100%', marginTop: 4 }}
					date={props.data.due_date}
					mode="date"
					placeholder="Select bill date"
					format="YYYY-MM-DD"
					minDate="2016-05-01"
					maxDate="2019-06-01"
					confirmBtnText="Confirm"
					cancelBtnText="Cancel"
					showIcon={false}
					disabled={!props.isEditable}
					onDateChange={props.onBillDateChange}
					customStyles={{
						dateText: {
							fontSize: 15
						},
						dateInput: {
							alignItems: 'flex-start',
							paddingLeft: 10,
							height: 40,
							// borderColor: '#cfcfcf',
							borderColor: '#0d60aa',
							borderRadius: 4,
							borderWidth: 1,
							color: '#0F2C5A',
							backgroundColor: 'white'
						}
						// ... You can check the source to find the other keys.
					}}
				/>
			</View> : null}
			<View style={{ width: '80%', marginTop: 20 }}>
				<Text style={styles.validatePlaceHolder}>Department</Text>
				<InputText
					style={{ marginTop: 4 }}
					onChangeText={props.handleBillNumberChange}
					value={props.data.department_name}
					autoCapitalize="none"
					autoCorrect={false}
					testID="department"
					maxLength={50}
					editable={props.isEditable}
					name="department"
					blurOnSubmit={false}
					onTextInputCreated={props.setInputReference}
					returnKeyType="next"
					onSubmitEditing={() => props.onSubmitEditing('poNumber')}
				/>
			</View>
		</View>
		{props.category === 'bill' ? <React.Fragment> 
			<View style={{ marginLeft: 40, marginTop: 20, flex: 1, flexDirection: 'row' }}>
				<Text style={styles.buttonText}>Rush: {`    `}</Text>
				<Text>{props.data.rush ? 'Yes' : 'No'}</Text>
			</View>
			<View style={{ marginLeft: 40, marginTop: 20, flex: 1, flexDirection: 'row' }}>
				<Text style={styles.buttonText}>Is Original Received: {`    `}</Text>
				<Text>{props.data.is_original_received ? 'Yes' : 'No'}</Text>
			</View>
		</React.Fragment> : null }
		<View style={{ flex: 1, alignItems: 'center', paddingTop: 10, backgroundColor: 'white' }}>
			<View style={{ width: '80%', marginTop: 20 }}>
				<Text style={styles.validatePlaceHolder}>Notes</Text>
				<InputText
					style={{ marginTop: 4 }}
					onChangeText={props.invoice_no}
					value={props.data.notes}
					autoCapitalize="none"
					autoCorrect={false}
					testID="notes"
					maxLength={50}
					editable={props.isEditable}
					name="notes"
					blurOnSubmit={false}
					onTextInputCreated={props.setInputReference}
					returnKeyType="next"
					onSubmitEditing={() => props.onSubmitEditing('poNumber')}
				/>
			</View>
		</View>
		<View style={{ flex: 1, alignItems: 'center', paddingTop: 10, backgroundColor: 'white' }}>
			{props.debit_memo ? (
				<View style={{ width: '80%', marginTop: 20 }}>
					<Text style={styles.validatePlaceHolder}>Ref Bill no. For Debit memos</Text>
					<InputText
						style={{ marginTop: 4 }}
						onChangeText={props.invoice_no}
						value={props.data.bill_no}
						autoCapitalize="none"
						autoCorrect={false}
						testID="billNo"
						maxLength={50}
						editable={props.isEditable}
						name="billNo"
						blurOnSubmit={false}
						onTextInputCreated={props.setInputReference}
						returnKeyType="next"
						onSubmitEditing={() => props.onSubmitEditing('poNumber')}
					/>
				</View>
			) : null}
			<View style={{ width: '80%', marginTop: 20, marginBottom: 20 }}>
				<Text style={styles.validatePlaceHolder}>Creator</Text>
				<InputText
					style={{ marginTop: 4 }}
					onChangeText={props.handleBillNumberChange}
					value={props.data.creator_name}
					autoCapitalize="none"
					autoCorrect={false}
					testID="Creator"
					maxLength={50}
					editable={props.isEditable}
					name="Creator"
					blurOnSubmit={false}
					onTextInputCreated={props.setInputReference}
					returnKeyType="next"
					onSubmitEditing={() => props.onSubmitEditing('poNumber')}
				/>
			</View>
		</View>
		<View />
	</KeyboardAwareScrollView>
);

SummeryInvoiceComponent.propTypes = {
	setInputReference: PropTypes.func,
	onSubmitEditing: PropTypes.func,
	onValidateInvoiceOptionSelect: PropTypes.func,
	selectedBillDate: PropTypes.string,
	selectedInvoiceDate: PropTypes.string,
	onInvoiceDateChange: PropTypes.func,
	onBillDateChange: PropTypes.func,
	handlePoNumberChange: PropTypes.func,
	handleShipFromChange: PropTypes.func,
	handleBillNumberChange: PropTypes.func,
	handleInvoiceNumberChange: PropTypes.func,
	invoiceTitle: PropTypes.string,
	invoiceNumber: PropTypes.string,
	billNumber: PropTypes.string,
	shipFrom: PropTypes.string,
	shipTo: PropTypes.string,
	lorryReceipt: PropTypes.string,
	poNumber: PropTypes.string,
	vendor: PropTypes.string
};

const styles = StyleSheet.create({
	textInput: {
		marginTop: 5,
		paddingLeft: 10,
		borderColor: '#0d60aa',
		borderRadius: 0,
		height: 30
	},
	buttonText: { fontSize: 14, color: '#0059a6' },
	validatePlaceHolder: { color: 'rgba(98,98,98,1)', fontWeight: '600', fontSize: 12 },
	chartFooterButtonsGroupRight: {
		flex: 1,
		// height: '15%',
		flexDirection: 'row',
		justifyContent: 'space-evenly'
	},
	viewButton: {
		width: '80%',
		height: '70%',
		borderRadius: 35,
		borderColor: '#4785bd',
		borderWidth: 1,
		backgroundColor: 'white',
		alignItems: 'center',
		justifyContent: 'center',
		marginTop: 5,
		padding: 20
	},
	viewText: {
		fontSize: 10,
		fontWeight: 'bold',
		color: '#4785bd'
	},
	popoverItemsBackground: {
		flexDirection: 'row',
		alignItems: 'center',
		paddingLeft: 5,
		paddingRight: 5
	},
	popoverItems: {
		height: 30,
		alignItems: 'center',
		justifyContent: 'center',
		width: '100%'
	},
	popoverItemText: {
		fontWeight: '500',
		fontSize: 13
	}
});
export default SummeryInvoiceComponent;
