/* eslint-disable global-require */
/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React from 'react';
import {
	StyleSheet,
	View,
	Dimensions,
	Text,
	Image,
	TouchableOpacity,
	KeyboardAvoidingView,
	Keyboard,
	TouchableWithoutFeedback,
	Picker,
	ScrollView
} from 'react-native';
import PropTypes from 'prop-types';

import InputText from '../CustomComponent/TextInput';
// import PhotoView from 'react-native-photo-view';
// import ImageViewer from 'react-native-image-zoom-viewer';
//import ImageViewer which will help to get zoom Image
import ImageZoom from 'react-native-image-pan-zoom';
import SummeryInvoiceContainer from '../../containers/SummeryInvoiceContainer';
import SummeryInvoiceComponent from '../../components/SummeryInvoiceComponent';
import BasicInvoiceContainer from '../../containers/BasicInvoiceContainer';
import BasicApproverInvoiceComponent from '../../components/BasicApproverInvoiceComponent';
import ItemsInvoiceContainer from '../../containers/ItemsInvoiceContainer';
import ItemApproverInvoiceCompoent from '../../components/ItemsInvoiceComponent/ItemApproverInvoiceCompoent';
import AmountInvoiceContainer from '../../containers/AmountInvoiceContainer';
import AmountApproverInvoiceComponent from '../../components/AmountInvoiceComponent/AmountApproverInvoiceComponent';
import GSTInvoiceContainer from '../../containers/GSTInvoiceContainer';
import GSTApproverInvoiceComponent from '../../components/GSTInvoiceComponent/GSTApproverInvoiceComponent';
import backGroundImage from '../../../common/images/no-image.jpg';
import SummeryBillComponen from '../../components/SummeryBillComponent';
import BasicBillComponent from '../../components/BasicBillComponent';
import GSTBillComponent from '../../components/GSTBillComponent';
import AmountBillComponent from '../../components/AmountBillComponent';
import ItemsBillComponent from '../../components/ItemsBillComponent';
import SummeryPaymentComponent from '../../components/SummeryPaymentComponent';
import BasicPaymentComponent from '../../components/BasicPaymentComponent';
import InvoiceItemsComponent from '../../components/ItemsInvoiceComponent/ItemApproverInvoiceCompoent';
import SummeryMasterDataItemsAndProductComponent from '../SummeryMasterDataItemsAndProduct';
import MasterDataDetailsComponent from '../MasterDataDetailsComponent';
import MasterDataFinacialDetailsComponent from '../MasterDataFinacialDetailsComponent';
import SummeryMasterDataCustomerAndVender from '../SummeryMasterDataCustomerAndVender';
import MasterDataBranchDetails from '../MasterDataBranchDetails';
import MasterDataBakingInformation from '../MasterDataBakingInformation';
import PaymentAdvances from '../PaymentAdvances';
import PoSummery from '../PoSummery';
import PoBasic from '../PoBasic';
import PoGst from '../PoGst';
import PoItems from '../PoItems';
import PoAmount from '../PoAmount';

const images = [{ url: 'http://aboutreact.com/wp-content/uploads/2018/07/sample_img.png' }];
//http://storage.googleapis.com/mintbktmobi/1/received_invoices/1_1554478677_IMG-4973.jpg?GoogleAccessId=GOOGDCJL7DXEPFIIYUAWLAMW&Signature=7fmiaV%2B52fEGbSIe8OrdcYxKh9I%3D&Expires=1555845542
const url =
	'http://storage.googleapis.com/mintbktmobi/1/received_invoices/1_1554478675_invoice-temp__400x500.jpg?GoogleAccessId=GOOGDCJL7DXEPFIIYUAWLAMW&Signature=txxopR%2Bmp%2BtF9b780iZKgC2ADUM%3D&Expires=1555845186';

const { width } = Dimensions.get('window');
const ValidateApproverInvoiceComponent = (props) => {
	const showImage = () => {
		if (props.imageUrl) {
			return (
				<ImageZoom
					cropWidth={310}
					cropHeight={300}
					cropWidth={Dimensions.get('window').width}
					// cropHeight={Dimensions.get('window').height}
					imageWidth={310}
					imageHeight={300}
				>
					<Image
						style={styles.imageStyle}
						source={{ uri: props.imageUrl ? props.imageUrl : '../../../common/images/no-image.jpg' }}
					/>
				</ImageZoom>
			);
		} else {
			return <Image source={require('../../../common/images/no-image.jpg')} style={styles.imageStyle} />;
		}
	};

	const getSummeryComponentByType = () => {
		// const { selectedInvoice:{data}, selectedInvoice } = props;
		switch (props.selectedInvoice.data) {
			case 'Invoices':
				return <SummeryInvoiceComponent data={props.selectedInvoiceDetails} isEditable={props.isEditable} category={props.selectedItemObject.category} />;
			case 'Bills':
				return <SummeryBillComponen data={props.selectedInvoiceDetails} isEditable={props.isEditable} category={props.selectedItemObject.category} />;
			case 'Payments':
				return <SummeryPaymentComponent data={props.selectedInvoiceDetails} isEditable={props.isEditable} />;
			case 'PO':
				return <PoSummery data={props.selectedInvoiceDetails} isEditable={props.isEditable} />;
			case 'Master Data':
				switch (props.selectedItemObject.category) {
					case 'item':
						return (
							<SummeryMasterDataItemsAndProductComponent
								data={props.selectedInvoiceDetails}
								isEditable={props.isEditable}
							/>
						);
					case 'product':
						return (
							<SummeryMasterDataItemsAndProductComponent
								data={props.selectedInvoiceDetails}
								isEditable={props.isEditable}
							/>
						);
					case 'customer':
						return (
							<SummeryMasterDataCustomerAndVender
								data={props.selectedInvoiceDetails}
								isEditable={props.isEditable}
								category={props.selectedItemObject.category}
							/>
						);
					case 'vendor':
						return (
							<SummeryMasterDataCustomerAndVender
								data={props.selectedInvoiceDetails}
								isEditable={props.isEditable}
								category={props.selectedItemObject.category}
							/>
						);
				}
				break;
			default:
				return <SummeryInvoiceComponent data={props.selectedInvoiceDetails} />;
		}
	};
	const getBasicComponentByType = () => {
		// const { selectedInvoice:{data}, selectedInvoice } = props;
		switch (props.selectedInvoice.data) {
			case 'Invoices':
				return (
					<BasicApproverInvoiceComponent isEditable={props.isEditable} data={props.selectedInvoiceDetails} category={props.selectedItemObject.category} />
				);
			case 'Bills':
				return <BasicBillComponent isEditable={props.isEditable} data={props.selectedInvoiceDetails} category={props.selectedItemObject.category} />;
			case 'Payments':
				return (
					<BasicPaymentComponent
						isEditable={props.isEditable}
						data={props.selectedInvoiceDetails}
						onRowPress={props.onRowPress}
					/>
				);
			case 'PO':
				return <PoBasic isEditable={props.isEditable} data={props.selectedInvoiceDetails} />;
			case 'Master Data':
				switch (props.selectedItemObject.category) {
					case 'item':
						return (
							<MasterDataDetailsComponent
								data={props.selectedInvoiceDetails}
								isEditable={props.isEditable}
								category={props.selectedItemObject.category}
							/>
						);
					case 'product':
						return (
							<MasterDataDetailsComponent
								data={props.selectedInvoiceDetails}
								isEditable={props.isEditable}
								category={props.selectedItemObject.category}
							/>
						);
					case 'customer':
						return (
							<MasterDataBranchDetails
								data={props.selectedInvoiceDetails.branches}
								isEditable={props.isEditable}
								category={props.selectedItemObject.category}
							/>
						);
					case 'vendor':
						return (
							<MasterDataBranchDetails
								data={props.selectedInvoiceDetails.branches}
								isEditable={props.isEditable}
								category={props.selectedItemObject.category}
							/>
						);
				}
				break;
			default:
				return <SummeryInvoiceComponent data={props.selectedInvoiceDetails} />;
		}
	};
	const getGSTComponentByType = () => {
		// const { selectedInvoice:{data}, selectedInvoice } = props;
		switch (props.selectedInvoice.data) {
			case 'Invoices':
				return (
					<GSTApproverInvoiceComponent isEditable={props.isEditable} data={props.selectedInvoiceDetails} />
				);
			case 'Bills':
				return <GSTBillComponent isEditable={props.isEditable} data={props.selectedInvoiceDetails} />;
			case 'Payments':
				return <PaymentAdvances isEditable={props.isEditable} data={props.selectedInvoiceDetails} />;
			case 'PO':
				return <PoGst isEditable={props.isEditable} data={props.selectedInvoiceDetails} />;
			case 'Master Data':
				switch (props.selectedItemObject.category) {
					case 'item':
						return (
							<MasterDataFinacialDetailsComponent
								data={props.selectedInvoiceDetails}
								isEditable={props.isEditable}
								category={props.selectedItemObject.category}
							/>
						);
					case 'product':
						return (
							<MasterDataFinacialDetailsComponent
								data={props.selectedInvoiceDetails}
								isEditable={props.isEditable}
								category={props.selectedItemObject.category}
							/>
						);
					case 'customer':
						return (
							<MasterDataBakingInformation
								data={props.selectedInvoiceDetails}
								isEditable={props.isEditable}
								category={props.selectedItemObject.category}
							/>
						);
					case 'vendor':
						return (
							<MasterDataBakingInformation
								data={props.selectedInvoiceDetails}
								isEditable={props.isEditable}
								category={props.selectedItemObject.category}
							/>
						);
				}
				break;
			default:
				return <SummeryInvoiceComponent data={props.selectedInvoiceDetails} />;
		}
	};
	const getItemsComponentByType = () => {
		// const { selectedInvoice:{data}, selectedInvoice } = props;
		switch (props.selectedInvoice.data) {
			case 'Invoices':
				return <InvoiceItemsComponent isEditable={props.isEditable} data={props.selectedInvoiceDetails} />;
			case 'Bills':
				return <ItemsBillComponent isEditable={props.isEditable} data={props.selectedInvoiceDetails} />;
			case 'Payments':
				return <Text>Payments</Text>;
			case 'PO':
				return <PoItems isEditable={props.isEditable} data={props.selectedInvoiceDetails} />;
			case 'Master Data':
				return <Text>Master Data</Text>;
			default:
				return <SummeryInvoiceComponent data={props.selectedInvoiceDetails} />;
		}
	};
	const getAmountComponentByType = () => {
		// const { selectedInvoice:{data}, selectedInvoice } = props;
		switch (props.selectedInvoice.data) {
			case 'Invoices':
				return (
					<AmountApproverInvoiceComponent isEditable={props.isEditable} data={props.selectedInvoiceDetails} />
				);
			case 'Bills':
				return <AmountBillComponent isEditable={props.isEditable} data={props.selectedInvoiceDetails} />;
			case 'Payments':
				return <Text>Payments</Text>;
			case 'PO':
				return <PoAmount isEditable={props.isEditable} data={props.selectedInvoiceDetails} />;
			case 'Master Data':
				return <Text>Master Data</Text>;
			default:
				return <SummeryInvoiceComponent data={props.selectedInvoiceDetails} />;
		}
	};

	const getSummeryStyle = () => {
		if (props.selectedValidateOption === 'Summery') {
			switch (props.selectedInvoice.data) {
				case 'Invoices':
					return styles.invoiceBasicDetailButtonsSelected;
				case 'Bills':
					return styles.invoiceBasicDetailButtonsSelected;
				case 'PO':
					return styles.invoiceBasicDetailButtonsSelected;
				case 'Payments':
					return styles.invoiceBasicDetailButtonsSelectedPayment;
				case 'Master Data':
					return styles.invoiceBasicDetailButtonsSelectedPayment;
				default:
					return styles.invoiceDetailButtonsForPayment;
			}
		} else {
			if (props.selectedInvoice.data === 'Payments' || props.selectedInvoice.data === 'Master Data') {
				return styles.invoiceDetailButtonsForPayment;
			} else {
				return styles.invoiceDetailButtons;
			}
		}
	};

	const getBasicStyle = () => {
		if (props.selectedValidateOption === 'Basic') {
			switch (props.selectedInvoice.data) {
				case 'Invoices':
					return styles.invoiceBasicDetailButtonsSelected;
				case 'Bills':
					return styles.invoiceBasicDetailButtonsSelected;
				case 'PO':
					return styles.invoiceBasicDetailButtonsSelected;
				case 'Payments':
					return styles.invoiceBasicDetailButtonsSelectedPayment;
				case 'Master Data':
					return styles.invoiceBasicDetailButtonsSelectedPayment;
				default:
					return styles.invoiceDetailButtonsForPayment;
			}
		} else {
			if (props.selectedInvoice.data === 'Payments' || props.selectedInvoice.data === 'Master Data') {
				return styles.invoiceDetailButtonsForPayment;
			} else {
				return styles.invoiceDetailButtons;
			}
		}
	};
	const getGSTStyle = () => {
		if (props.selectedValidateOption === 'GST') {
			switch (props.selectedInvoice.data) {
				case 'Invoices':
					return styles.invoiceBasicDetailButtonsSelected;
				case 'Bills':
					return styles.invoiceBasicDetailButtonsSelected;
				case 'PO':
					return styles.invoiceBasicDetailButtonsSelected;
				case 'Payments':
					return styles.invoiceBasicDetailButtonsSelectedPayment;
				case 'Master Data':
					return styles.invoiceBasicDetailButtonsSelectedPayment;
				default:
					return styles.invoiceDetailButtonsForPayment;
			}
		} else {
			if (props.selectedInvoice.data === 'Payments' || props.selectedInvoice.data === 'Master Data') {
				return styles.invoiceDetailButtonsForPayment;
			} else {
				return styles.invoiceDetailButtons;
			}
		}
	};

	const getBasicMasterDataDetailsTabTiles = () => {
		switch (props.selectedInvoice.data) {
			case 'Payments':
				return 'Bill Payments';
			case 'Master Data':
				switch (props.selectedItemObject.category) {
					case 'item':
						return 'Details';
					case 'product':
						return 'Details';
					case 'customer':
						return 'Branches';
					case 'vendor':
						return 'Branches';
				}
			default:
				return 'Basic';
		}
	};

	const getGSTMasterDataDetailsTabTiles = () => {
		switch (props.selectedInvoice.data) {
			case 'Payments':
				return 'Advances';
			case 'Master Data':
				switch (props.selectedItemObject.category) {
					case 'item':
						return 'Financial Details';
					case 'product':
						return 'Financial Details';
					case 'customer':
						return 'Banking Information';
					case 'vendor':
						return 'Banking Information';
				}
			default:
				return 'GST';
		}
	};

	return (
		<View style={styles.container}>
			{/* <View style ={styles.imageBackgroundView}>
                {showImage()}
            </View> */}
			<ScrollView horizontal={true} style={styles.invoiceDetailBackgroundView}>
				<View style={styles.invoiceDetailHeader}>
					<View style={getSummeryStyle()}>
						<TouchableOpacity
							style={styles.buttonStyle}
							onPress={() => props.onValidateInvoiceOptionSelect('Summery')}
						>
							<Text
								style={
									props.selectedValidateOption === 'Summery' ? (
										styles.selectedButtonText
									) : (
											styles.buttonText
										)
								}
							>
								{' '}
								Summery{' '}
							</Text>
						</TouchableOpacity>
					</View>
					<View style={getBasicStyle()}>
						<TouchableOpacity
							style={styles.buttonStyle}
							onPress={() => props.onValidateInvoiceOptionSelect('Basic')}
						>
							<Text
								style={
									props.selectedValidateOption === 'Basic' ? (
										styles.selectedButtonText
									) : (
											styles.buttonText
										)
								}
							>
								{' '}
								{getBasicMasterDataDetailsTabTiles()}
							</Text>
						</TouchableOpacity>
					</View>
					<View style={getGSTStyle()}>
						<TouchableOpacity
							style={styles.buttonStyle}
							onPress={() => props.onValidateInvoiceOptionSelect('GST')}
						>
							<Text
								style={
									props.selectedValidateOption === 'GST' ? (
										styles.selectedButtonText
									) : (
											styles.buttonText
										)
								}
							>
								{getGSTMasterDataDetailsTabTiles()}
							</Text>
						</TouchableOpacity>
					</View>

					{props.selectedInvoice.data !== 'Payments' && props.selectedInvoice.data !== 'Master Data' ? (
						<View
							style={
								props.selectedValidateOption === 'Items' ? (
									styles.invoiceBasicDetailButtonsSelected
								) : (
										styles.invoiceDetailButtonsForPayment
									)
							}
						>
							<TouchableOpacity
								style={styles.buttonStyle}
								onPress={() => props.onValidateInvoiceOptionSelect('Items')}
							>
								<Text
									style={
										props.selectedValidateOption === 'Items' ? (
											styles.selectedButtonText
										) : (
												styles.buttonText
											)
									}
								>
									Items
								</Text>
							</TouchableOpacity>
						</View>
					) : null}
					{props.selectedInvoice.data !== 'Payments' && props.selectedInvoice.data !== 'Master Data' ? (
						<View
							style={
								props.selectedValidateOption === 'Amount' ? props.selectedInvoice.data !== 'Payments' &&
									props.selectedInvoice.data !== 'Master Data' ? (
										styles.invoiceBasicDetailButtonsSelectedPayment
									) : (
										styles.invoiceBasicDetailButtonsSelected
									) : props.selectedInvoice.data !== 'Payments' ? (
										styles.invoiceDetailButtons
									) : (
											styles.invoiceDetailButtonsForPayment
										)
							}
						>
							<TouchableOpacity
								style={styles.buttonStyle}
								onPress={() => props.onValidateInvoiceOptionSelect('Amount')}
							>
								<Text
									style={
										props.selectedValidateOption === 'Amount' ? (
											styles.selectedButtonText
										) : (
												styles.buttonText
											)
									}
								>
									Amount
								</Text>
							</TouchableOpacity>
						</View>
					) : null}
				</View>
			</ScrollView>
			<View style={{ backgroundColor: 'white' }}>
				{props.selectedValidateOption === 'Summery' ? (
					getSummeryComponentByType()
				) : props.selectedValidateOption === 'Basic' ? (
					getBasicComponentByType()
				) : props.selectedValidateOption === 'Items' ? (
					getItemsComponentByType()
				) : props.selectedValidateOption === 'Amount' ? (
					getAmountComponentByType()
				) : props.selectedValidateOption === 'GST' ? (
					getGSTComponentByType()
				) : (
										<View />
									)}
			</View>
		</View>
	);
};

ValidateApproverInvoiceComponent.propTypes = {
	setInputReference: PropTypes.func,
	onSubmitEditing: PropTypes.func,
	onValidateInvoiceOptionSelect: PropTypes.func,
	selectedValidateOption: PropTypes.string
};

const styles = StyleSheet.create({
	container: {
		flex: 1,
		justifyContent: 'flex-start'
	},
	imageStyle: {
		resizeMode: 'center',
		width: '100%',
		height: '100%'
	},
	imageBackgroundView: {
		// height:'35%',
		backgroundColor: 'white'
	},
	invoiceDetailBackgroundView: {
		// height:'65%',
		backgroundColor: 'white'
	},
	invoiceDetailHeader: {
		alignItems: 'flex-start',
		flexDirection: 'row'
	},
	invoiceDetailButtons: {
		width: width / 5,
		// height:'100%',
		flexDirection: 'column',
		backgroundColor: '#ededed',
		alignItems: 'center',
		justifyContent: 'space-between',
		borderBottomColor: '#9cbddc',
		borderBottomWidth: 2
	},
	invoiceDetailButtonsForPayment: {
		width: width / 3,
		// height:'100%',
		flexDirection: 'column',
		backgroundColor: '#ededed',
		alignItems: 'center',
		justifyContent: 'space-between',
		borderBottomColor: '#9cbddc',
		borderBottomWidth: 2
	},
	textInput: {
		marginTop: 5,
		paddingLeft: 10,
		borderColor: '#0d60aa',
		borderRadius: 0,
		height: 30
	},
	buttonStyle: {
		justifyContent: 'center',
		alignItems: 'center',
		width: width / 4,
		height: 55
		// paddingBottom:6,
		// paddingTop:6
	},
	invoiceBasicDetailButtonsSelected: {
		width: width / 4,
		// height:'100%',
		flexDirection: 'column',
		backgroundColor: 'white',
		alignItems: 'center',
		justifyContent: 'space-between',
		// paddingLeft:5,
		paddingRight: 2,
		borderBottomColor: '#0072BF',
		borderBottomWidth: 2
	},
	invoiceBasicDetailButtonsSelectedPayment: {
		width: width / 3,
		// height:'100%',
		flexDirection: 'column',
		backgroundColor: 'white',
		alignItems: 'center',
		justifyContent: 'space-between',
		// paddingLeft:5,
		paddingRight: 2,
		borderBottomColor: '#0072BF',
		borderBottomWidth: 2
	},
	buttonText: { fontSize: 14, color: '#0059a6' },
	selectedButtonText: { fontWeight: '500', fontSize: 14, color: '#0059a6' }
});
export default ValidateApproverInvoiceComponent;
