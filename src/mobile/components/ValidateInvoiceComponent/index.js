/* eslint-disable global-require */
/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React from 'react';
import {
    StyleSheet,
    View,
    Dimensions,
    Text,
    Image,
    TouchableOpacity,
    KeyboardAvoidingView,
    Keyboard,
    TouchableWithoutFeedback,
    Picker
} from 'react-native';
import PropTypes from 'prop-types';

import InputText from '../CustomComponent/TextInput'
// import PhotoView from 'react-native-photo-view';
// import ImageViewer from 'react-native-image-zoom-viewer';
//import ImageViewer which will help to get zoom Image
import ImageZoom from 'react-native-image-pan-zoom';
import BasicInvoiceContainer from '../../containers/BasicInvoiceContainer'
import ItemsInvoiceContainer from '../../containers/ItemsInvoiceContainer'
import AmountInvoiceContainer from '../../containers/AmountInvoiceContainer'
import GSTInvoiceContainer from '../../containers/GSTInvoiceContainer'
import backGroundImage from '../../../common/images/no-image.jpg'
const images = [{url: 'http://aboutreact.com/wp-content/uploads/2018/07/sample_img.png',},];
//http://storage.googleapis.com/mintbktmobi/1/received_invoices/1_1554478677_IMG-4973.jpg?GoogleAccessId=GOOGDCJL7DXEPFIIYUAWLAMW&Signature=7fmiaV%2B52fEGbSIe8OrdcYxKh9I%3D&Expires=1555845542
const url = "http://storage.googleapis.com/mintbktmobi/1/received_invoices/1_1554478675_invoice-temp__400x500.jpg?GoogleAccessId=GOOGDCJL7DXEPFIIYUAWLAMW&Signature=txxopR%2Bmp%2BtF9b780iZKgC2ADUM%3D&Expires=1555845186"

const { width } = Dimensions.get('window');
const ValidateInvoiceComponent = props => {

    const showImage = () => {
        console.log(Dimensions.get('window').width)
        if(props.imageUrl)
        {
            return (
                <ImageZoom
                    cropWidth={310}
                    cropHeight={300}
                    cropWidth={Dimensions.get('window').width}
                    // cropHeight={Dimensions.get('window').height}
                    imageWidth={310}
                    imageHeight={300}
                >
                    <Image
                        style = { styles.imageStyle }
                        source={{uri:props.imageUrl? props.imageUrl:'../../../common/images/no-image.jpg'}}/>
                </ImageZoom>
            )
        }else
        {
            return (
                <Image
                    source = {require('../../../common/images/no-image.jpg')}
                    style = { styles.imageStyle }
                />
            )
        }
    }
    return (

        <View style = {styles.container}>
            <View style ={styles.imageBackgroundView}>
                {showImage()}
            </View>
            <View style ={styles.invoiceDetailBackgroundView}>

                <View style = {styles.invoiceDetailHeader}>
                    <View style = {props.selectedValidateOption ==='Basic'?styles.invoiceBasicDetailButtonsSelected:styles.invoiceDetailButtons}>
                        <TouchableOpacity style={styles.buttonStyle} onPress= {()=>props.onValidateInvoiceOptionSelect('Basic')}>
                            <Text style = {props.selectedValidateOption ==='Basic'?styles.selectedButtonText:styles.buttonText}> Basic </Text>
                        </TouchableOpacity>

                    </View>

                    <View style =  {props.selectedValidateOption ==='GST'?styles.invoiceBasicDetailButtonsSelected:styles.invoiceDetailButtons}>
                        <TouchableOpacity style={styles.buttonStyle} onPress= {()=>props.onValidateInvoiceOptionSelect('GST')}>
                            <Text style = {props.selectedValidateOption ==='GST'?styles.selectedButtonText:styles.buttonText}>GST</Text>
                        </TouchableOpacity>

                    </View>

                    <View style ={props.selectedValidateOption ==='Items'?styles.invoiceBasicDetailButtonsSelected:styles.invoiceDetailButtons}>
                        <TouchableOpacity style={styles.buttonStyle} onPress= {()=>props.onValidateInvoiceOptionSelect('Items')}>
                            <Text style = {props.selectedValidateOption ==='Items'?styles.selectedButtonText:styles.buttonText}>Items</Text>
                        </TouchableOpacity>

                    </View>
                    <View style ={props.selectedValidateOption ==='Amount'?styles.invoiceBasicDetailButtonsSelected:styles.invoiceDetailButtons}>
                        <TouchableOpacity style={styles.buttonStyle} onPress= {()=>props.onValidateInvoiceOptionSelect('Amount')}>
                            <Text style = {props.selectedValidateOption ==='Amount'?styles.selectedButtonText:styles.buttonText}>Amount</Text>
                        </TouchableOpacity>

                    </View>

                </View>
                <View>
                    {props.selectedValidateOption==='Basic'?
                        <BasicInvoiceContainer
                            setInputReference={props.setInputReference}
                            onSubmitEditing={props.onSubmitEditing}
                            onValidateInvoiceOptionSelect={props.onValidateInvoiceOptionSelect}
                            selectedValidateOption={props.selectedValidateOption}
                        />:props.selectedValidateOption==='Items'?
                            <ItemsInvoiceContainer
                            />:props.selectedValidateOption ==='Amount'?
                                <AmountInvoiceContainer/>
                                :props.selectedValidateOption ==='GST'?<GSTInvoiceContainer/>:<View/>
                    }
                </View>

            </View>
        </View>

        )};

ValidateInvoiceComponent.propTypes = {
    setInputReference: PropTypes.func,
    onSubmitEditing: PropTypes.func,
    onValidateInvoiceOptionSelect: PropTypes.func,
    selectedValidateOption:PropTypes.string
};


const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    imageStyle:
        {
            resizeMode: 'center',
            width: '100%',
            height: '100%',
        },
    imageBackgroundView:{
        height:'35%',
        backgroundColor:'white'
    },
    imageBackgroundChildView:{
        height:'100%',
        backgroundColor:'white',
        resizeMode: 'contain',
    },
    invoiceDetailBackgroundView:{
        height:'65%',
        backgroundColor:'white'
    },
    invoiceDetailHeader:
        {
            height:60,
            alignItems:'center',
            flexDirection: 'row',
         },
    invoiceDetailView:
        {
            backgroundColor:'blue',
            height:'80%'
        },
    invoiceDetailHeaderText:{
        paddingTop:10,
        fontSize:18,
        color:'black'
    },
    invoiceDetailheaderContent: {
        flexDirection: 'row',
        },
    invoiceDetailButtons:{
        width:width/4,
        height:'100%',
        flexDirection: 'column',
        backgroundColor:'#ededed',
        alignItems:'center',
        justifyContent:'space-between',
        borderBottomColor:'#9cbddc',
        borderBottomWidth:2,


    },
    buttonCountView:{
        width:18,
        height:18,
        backgroundColor:'#8e8e8e',
        borderRadius:10,
        alignItems:'center',
        justifyContent:'center'
    },
    selectedButtonCountView:{
        width:18,
        height:18,
        backgroundColor:'#9cbddc',
        borderRadius:10,
        alignItems:'center',
        justifyContent:'center'
    },
    textInput:{
        marginTop:5,
        paddingLeft:10,
        borderColor:'#0d60aa',
        borderRadius:0,
        height:30
    },
    validatePlaceHolder:{color:'rgba(98,98,98,1)',fontWeight:'600', fontSize:12},
    validateInputsScrollView:{
        height:'88%',
        paddingTop:10,
        backgroundColor:'white',
    },
    buttonStyle:{
        justifyContent:'center',
        alignItems:'center',
        width:width/4,
        height:55,
        paddingBottom:6,
        paddingTop:6
    },
    invoiceBasicDetailButtonsSelected:{
        width:width/4,
        height:'100%',
        flexDirection: 'column',
        backgroundColor:'white',
        alignItems:'center',
        justifyContent:'space-between',
       // paddingLeft:5,
        paddingRight:2,
        borderBottomColor:'#0072BF',
        borderBottomWidth:2,

    },
    buttonText:{fontSize:14,color:'#0059a6'},
    selectedButtonText:{fontWeight:'500',fontSize:14,color:'#0059a6'}

});
export default ValidateInvoiceComponent;
