
import React, { Component } from 'react';
import {Alert, Keyboard, Image, Dimensions, Platform, Linking, View, Text, StyleSheet,TouchableOpacity} from 'react-native';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import logo from '../../../common/images/logo.png'
import Modal from "react-native-modal";
import {normalizeFont} from "../../../common/util";
import CommanListComponent from '../../components/CommanListComponent'
import AddItemComponent from '../../components/AddItemComponent'
import {getBillDetails} from "../../../common/selector/InVoiceList";
import {getReceiveObject} from "../../../common/selector/dashboard";
import * as dashboardActionCreator from '../../../common/actions/dashboard';
import backButton from '../../../common/images/back.png'

const { width } = Dimensions.get('window');
class AddItemContainer extends Component {

    static navigationOptions = ({ navigation }) => ({
        headerTitle: (
            <View style={{ alignItems:'flex-start',flex:1}}>
                <Text
                    style={{
                        alignSelf: 'center',
                        color:'white',
                        fontSize:16,
                        fontWeight:'700'
                    }}>
                    Setlmint
                </Text>
            </View>
        ),
        headerStyle: {
            backgroundColor: '#629ee4',
            borderBottomWidth: 0,
            shadowOpacity: 0,
            shadowOffset: {
                height: 0,
            },
            shadowRadius: 0,
            elevation: 0,
        },
        headerLeft: (
            <TouchableOpacity onPress={() => navigation.state.params.handleBack()}>
                <Image source={backButton} style={{ height: 24, width: 24, marginLeft: 12 }} />
            </TouchableOpacity>
        ),
    });


    constructor(props) {
        super(props);
        this.state = {
            itemName: '',
            hsnName:'',
            quantity:'',
            price:'',
            totalPrice:'',
        };
        this.onSavePress = this.onSavePress.bind(this);
        this.handleHSNChange = this.handleHSNChange(this);
        this.handleItemNameChange = this.handleItemNameChange(this);
        this.handleQuantityChange = this.handleQuantityChange.bind(this);
        this.handlePriceChange = this.handlePriceChange.bind(this);
        this.onNamePress = this.onNamePress.bind(this);
        this.onBackPress = this.onBackPress.bind(this);
        this.inputs = {};
        this.focusNextField = this.focusNextField.bind(this);
        this.onSubmitEditing = this.onSubmitEditing.bind(this);
        this.setInputReference = this.setInputReference.bind(this);
    }

    componentDidMount() {
        this.props.navigation.setParams({
            handleBack: this.onBackPress,
        });
    }

    onBackPress() {
        this.props.dashboardAction.goBack();
        this.props.dashboardAction.resetSelectedItemNameObject()
    }

    onNamePress()
    {
        this.props.dashboardAction.navigateItemListListContainer()
    }

    onSavePress(item) {

        var itemObject = {
            id: "",
            item_name: this.props.selectedItemObject.item_name?this.props.selectedItemObject.item_name:"",
            hsn_code: this.props.selectedItemObject.hsn_code?this.props.selectedItemObject.hsn_code:"",
            item_code: null,
            sku_code: null,
            item_desc: null,
            quantity: this.state.quantity,
            unit: 'nos',
            price: this.state.price,
            cgst: '1.0',
            igst: '1.0',
            sgst: '1.0',
            purchase_ledger: '',
            bill_id: null,
            created_at: '2018-11-12T08:56:07.000Z',
            updated_at: '2018-11-12T08:56:07.000Z',
            is_service: false,
            is_coa: null }
           var newItemList =  this.props.billDetailsOrignal.bill_items
             newItemList.push(itemObject)
           this.props.billDetailsOrignal.items = newItemList
           console.log("Origna lItwm :::::::::::::::::",this.props.billDetailsOrignal)
         // this.props.dashboardAction.billDetailSuccess(this.props.billDetails)
         this.props.dashboardAction.resetSelectedItemNameObject()
         this.props.dashboardAction.addItem_APIRequest(this.props.billDetailsOrignal)
       // console.log("Bill Details Added After :::::::::::::::::::",this.props.billDetails.items)
         this.props.dashboardAction.goBack();

    }

    onSubmitEditing(nextFiled) {
        this.focusNextField(nextFiled);
    }
    setInputReference(name, refs) {
        this.inputs[name] = refs;
    }
    focusNextField(id) {
        this.inputs[id].focus();
    }


    handleItemNameChange(text) {
        this.setState({ itemName: text });
    }

    handleHSNChange(text) {
        this.setState({ hsnName: text });
    }

    handleQuantityChange(text) {
        var totalPrice =  parseFloat(this.state.price)*parseFloat(text)
        this.setState({
            quantity: text ,
            totalPrice:totalPrice?totalPrice:'0'
        });
    }

    handlePriceChange(text) {

        var totalPrice =  parseFloat(this.state.quantity)*parseFloat(text)
        this.setState({
            price: text ,
            totalPrice:totalPrice?totalPrice:'0'
        });

    }
    render() {
        let name = ""
        if(this.props.selectedItemObject.item_name){
             name = this.props.selectedItemObject.item_name
        }else if(this.props.selectedItemObject.name){
            name = this.props.selectedItemObject.name
        }else{
            name = "No Item"
        }

        return (
            <View style={{width:'100%',height:'100%',borderBottomLeftRadius:10,borderBottomRightRadius:10}} >
                <AddItemComponent
                    itemName={name?name:"NO Item"}
                    hsnName={this.props.selectedItemObject.hsn_code?this.props.selectedItemObject.hsn_code:""}
                    quantity={this.state.quantity}
                    price={this.state.price}
                    totalPrice={this.state.totalPrice}
                    imageUrl={this.props.billDetails.imageUrl}
                    onSavePress={this.onSavePress}
                    handleItemNameChange={this.handleItemNameChange}
                    handleHSNChange={this.handleHSNChange}
                    handleQuantityChange={this.handleQuantityChange}
                    handlePriceChange={this.handlePriceChange}
                    onNamePress={this.onNamePress}
                    focusNextField={this.focusNextField}
                    onSubmitEditing={this.onSubmitEditing}
                    setInputReference={this.setInputReference}
                />
            </View>
        );
    }
}


const styles = StyleSheet.create({
    modalContent: {
        padding: 22,
        justifyContent: "center",
        alignItems: "center",
        borderRadius: 4,
        borderColor: "rgba(0, 0, 0, 0.1)",
        height:'40%',
        width:'60%',
    },
});

AddItemContainer.propTypes = {
    navigation: PropTypes.object,
    dashboardAction: PropTypes.shape({
        goBack: PropTypes.func,
        billDetailSuccess:PropTypes.func,
        navigateItemListListContainer:PropTypes.func,
        addItem_APIRequest:PropTypes.func,
        resetSelectedItemNameObject:PropTypes.func
    }),
};


const mapStateToProps = state => ({
    loading: state.app.loading,
    billDetails:getBillDetails(state),
    billDetailsOrignal:state.dashboard.billDetails,
    selectedItemObject:state.dashboard.selectedItemObject
});

const mapDispatchToProps = dispatch => ({
    dashboardAction: bindActionCreators(dashboardActionCreator, dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps)(AddItemContainer);
