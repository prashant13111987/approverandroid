
import React, { Component } from 'react';
import { Alert, Keyboard, Image, Dimensions, Platform, Linking, View ,Text,TouchableOpacity} from 'react-native';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import AmountInvoiceComponent from '../../components/AmountInvoiceComponent';
import {getBillDetails} from "../../../common/selector/InVoiceList";
import { bindActionCreators } from 'redux';
import * as dashboardActionCreator from "../../../common/actions/dashboard";
import _ from "lodash";


const { width } = Dimensions.get('window');
class AmountInvoiceContainer extends Component {

    constructor(props) {
        super(props);
        this.state = {
            totalAmount:'',
            subtotal:'',
            GSTTotal:'',
            delivery:'',
            otherField:'',
            packaging:''
        };
        this.inputs = {};
        this.focusNextField = this.focusNextField.bind(this);
        this.onSubmitEditing = this.onSubmitEditing.bind(this);
        this.setInputReference = this.setInputReference.bind(this);
        this.onSendForApproval = this.onSendForApproval.bind(this);
        this.onUpdate = this.onUpdate.bind(this);
    }

    handleTotalAmountChange(){

    }

    handleSubtotalChange(){

    }

    handleGSTtotalChange(){

    }

    handlePackagingChange(){

    }

    handleOtherFieldChange(){

    }

    handleDeliveryChange(){

    }

    onSubmitEditing(nextFiled) {
        this.focusNextField(nextFiled);
    }
    setInputReference(name, refs) {
        this.inputs[name] = refs;
    }
    focusNextField(id) {
        this.inputs[id].focus();
    }

    onUpdate(){
        //console.log("onUpdate")
        // this.props.dashboardAction.sendForApprovalRequest(tempObj)
        let tempObj =  _.cloneDeep(this.props.billDetailsOrignal)
        tempObj.shipping_branch_name = this.props.shipFromObject.name?this.props.shipFromObject.name:this.props.billDetails.gstInfo.shipFromName
        tempObj.vendor_branch_name = this.props.shipToObject.name?this.props.shipToObject.name:this.props.billDetails.gstInfo.ShipToName
        this.props.dashboardAction.updateBillDetail(tempObj)
        console.log("Bill Details Object ::::::::::::::",this.props.billDetails);
        this.props.dashboardAction.updateForApprovalRequest(tempObj)
    }

    onSendForApproval(){
        //console.log("onSendForApproval")
       this.props.dashboardAction.sendForApproval()
    }

    render() {

        let totalGSTValue = 0;
        let subTotalAmount = 0;
        let finalCalculatedAmount = 0
        _.each(this.props.billDetails.items, (item) => {
            const calculatedAmount = (parseFloat(item.price)*parseFloat(item.quantity)).toFixed(2)
            if(item.discount > 0){
                const discountValue = item.discount?calculatedAmount * ((parseFloat(item.discount))/100.0):'0.0'
                finalCalculatedAmount = calculatedAmount - discountValue
                const gstCal = parseFloat(finalCalculatedAmount) *  ((parseFloat(item.tax))/100.0)
                totalGSTValue = parseFloat(totalGSTValue) + parseFloat(gstCal)
            }else
            {
                const gstCal = parseFloat(calculatedAmount) *  ((parseFloat(item.tax))/100.0)
                totalGSTValue = parseFloat(totalGSTValue) + parseFloat(gstCal)
            }

            if(finalCalculatedAmount){
                subTotalAmount = parseFloat(subTotalAmount) + parseFloat(finalCalculatedAmount)
            }else{
                subTotalAmount = parseFloat(subTotalAmount) + parseFloat(calculatedAmount)
            }
        });

         let grossGSTTotal =  parseFloat(subTotalAmount) + parseFloat(totalGSTValue) + parseFloat(this.props.billDetails.amount.deliverycharges)
        + parseFloat(this.props.billDetails.amount.packagingCharges) + parseFloat(this.props.billDetails.amount.other_charges)


        return (
            <AmountInvoiceComponent
                handleTotalAmountChange={this.handleTotalAmountChange}
                handleSubtotalChange={this.handleSubtotalChange}
                handleGSTtotalChange={this.handleGSTtotalChange}
                handlePackagingChange={this.handlePackagingChange}
                handleOtherFieldChange={this.handleOtherFieldChange}
                handleDeliveryChange={this.handleDeliveryChange}
                onSubmitEditing={this.onSubmitEditing}
                setInputReference={this.setInputReference}
                focusNextField={this.focusNextField}
               // totalAmount={this.props.billDetails.amount.amountTotal}
                totalAmount={grossGSTTotal}
                subtotal={subTotalAmount}
                //subtotal={this.props.billDetails.amount.subTotal}
               // GSTTotal={this.props.billDetails.amount.gstTotal}
                GSTTotal={totalGSTValue}
                delivery={this.props.billDetails.amount.deliverycharges}
                packaging={this.props.billDetails.amount.packagingCharges}
                otherField={this.props.billDetails.amount.other_charges}
                onSendForApproval={this.onSendForApproval}
                onUpdate={this.onUpdate}
                isEditable={this.props.isEditable}
            />
        );
    }
}

AmountInvoiceContainer.propTypes = {
    navigation: PropTypes.object,
    dashboardAction: PropTypes.shape({
        sendForApproval: PropTypes.func,
        updateForApprovalRequest:PropTypes.func,
        updateBillDetail:PropTypes.func
    }),
};

const mapStateToProps = state => ({
    billDetails:getBillDetails(state),
    selectedBillObject:state.dashboard.selectedBillObject,
    billDetailsOrignal:state.dashboard.billDetails,
    shipFromObject:state.dashboard.shipFromObject,
    shipToObject:state.dashboard.shipToObject,
    isEditable:state.dashboard.isEditable,
});

const mapDispatchToProps = dispatch => ({
    dashboardAction: bindActionCreators(dashboardActionCreator, dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps)(AmountInvoiceContainer);