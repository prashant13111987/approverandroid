/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import { NetInfo, View, ActivityIndicator, StyleSheet, Dimensions, Linking, BackHandler, Text, Platform, TouchableWithoutFeedback, AppState, Alert } from 'react-native';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as mobileActionCreator from '../../action/mobileActions';
import { addNavigationHelpers } from 'react-navigation';
import RootNavigation from '../RootNavigation';
import {createReduxBoundAddListener} from "react-navigation-redux-helpers";


// get current route name(screen name) from navigatin state
const getCurrentRouteName = (navigationState) => {
    if (!navigationState) {
        return null;
    }
    const route = navigationState.routes[navigationState.index];
    // dive into nested navigators
    if (route.routes) {
        return getCurrentRouteName(route);
    }
    return route.routeName;
};


class AppContainer extends Component {
    constructor(props) {
        super(props);
        this.state = {
            appState: AppState.currentState,
        };
        this.handleFirstConnectivityChange = this.handleFirstConnectivityChange.bind(this);
        this.addListener = createReduxBoundAddListener('root');
    }

    async componentDidMount() {

        NetInfo.isConnected.addEventListener(
            'connectionChange',
            this.handleFirstConnectivityChange,
        );
    }

    componentWillUnmount() {
        NetInfo.isConnected.removeEventListener(
            'connectionChange',
            this.handleFirstConnectivityChange,
        );
    }

    // get current screen (route) name
    getCurrentRoute() {
        const { nav } = this.props;
        const currentRoute = getCurrentRouteName(nav);
        return currentRoute;
    }

    // handle network connectivity state change
    handleFirstConnectivityChange(isConnected) {
        this.props.mobileActions.onNetworkStateChanged(isConnected);
    }

    render() {
        return (
            <View style={{ flex: 1, opacity: 1 }}>
                <RootNavigation navigation={addNavigationHelpers({
                    dispatch: this.props.dispatch,
                    state: this.props.nav,
                    addListener: this.addListener,
                })}
                />
                {
                    this.props.loading ?
                        <View style={style.maskBox}>
                            <ActivityIndicator size="large" />
                        </View>
                        :
                        <View />
                }

            </View>
        );
    }

}

const style = StyleSheet.create({
    maskBox: {
        position: 'absolute',
        width: Dimensions.get('window').width,
        height: Dimensions.get('window').height,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'transparent',
    },
    deleteTitleTextStyle: {
        color: '#0F2C5A',
        alignSelf: 'center',
        fontWeight: '600',
        fontSize: 16,
        marginTop: 5,
        marginLeft: 5,
        // marginRight: 5,
    },
    secondTextStyle: {
        color: '#0F2C5A',
        fontWeight: '600',
        fontSize: 16,
        marginTop: 10,
        marginLeft: 16,
    },
    starStyle: {
        fontWeight: '600',
        fontSize: 30,
        flexDirection: 'row',
        color: '#0F2C5A',
    },
});



AppContainer.propTypes = {
    nav: PropTypes.object,
    dispatch: PropTypes.func,
    loading: PropTypes.bool,
    navigation: PropTypes.object,
    mobileActions: PropTypes.shape({
        onNetworkStateChanged: PropTypes.func,
    }),
    netState: PropTypes.bool,
    isNetworkStateDetected: PropTypes.bool,
};

const mapStateToProps = state => ({
    nav: state.nav,
    loading: state.app.loading,
    netState: state.deviceInfo.netState,
    isNetworkStateDetected: state.deviceInfo.isNetworkStateDetected,
});

const mapDispatchToProps = dispatch => ({
    dispatch,
    mobileActions: bindActionCreators(mobileActionCreator, dispatch),

});

//export default AppContainer;
export default connect(mapStateToProps, mapDispatchToProps)(AppContainer);
