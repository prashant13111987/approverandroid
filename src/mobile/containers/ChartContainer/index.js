
import React, { Component } from 'react';
import { Alert, Keyboard, Image, Dimensions, Platform, Linking, View, Text, processColor } from 'react-native';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import ChartComponent from '../../components/ChartComponent';
import search from '../../../common/images/Arrowhead-down1.png'

const { width } = Dimensions.get('window');
class ChartContainer extends Component {

    constructor(props) {
        super(props);

        this.handleSelect = this.handleSelect.bind(this)
        this.onRemoveSource = this.onRemoveSource.bind(this)
    }

    // handleSelect(){
    //     console.log('handleSelect called::::::::::');
    // }
    handleSelect(event) {
        let entry = event.nativeEvent.label
        if (entry == null) {
            // this.setState({...this.state, selectedEntry: null})
        } else {
            console.log(entry)
            // this.setState({...this.state, selectedEntry: JSON.stringify(entry)})
        }

        //console.log(event.nativeEvent)
    }

    onChange(event) {

        console.log(event.nativeEvent)
    }
    onRemoveSource(sourceValue) {
        // var dataArray = [];
        // this.state.data.dataSets.map((item,index)=>{
        //         item.values.map((items)=>{
        //             if(items.label === sourceValue) {
        //
        //                 console.log('removed item', items)
        //                 //this.state.data.dataSets.splice(index, 1);
        //             }
        //             else{
        //                 dataArray.push(items);
        //             }
        //         })
        // })
        //
        // this.setState(
        //     {
        //         data:{dataSets:[{values:dataArray,label:this.state.pieChartDataValues.label,config:this.state.pieChartDataValues.config}]}
        //     }
        // );

    }

    render() {
        return (
            <ChartComponent
                handleSelect={this.handleSelect}
                onChange={this.onChange}
                legend={this.props.legend}
                onRemoveSource={this.onRemoveSource}
                data={this.props.data}
                highlights={this.props.highlights}
                description={this.props.description}
                totalAmount={this.props.totalAmount}
                totalInvoiceCount={this.props.totalInvoiceCount}
                originalData={this.props.originalData}
                pieChartDataValues={this.props.pieChartDataValues}
                type={this.props.type || 'Maker'}
            />
        );
    }
}

ChartContainer.propTypes = {
    navigation: PropTypes.object,
    data: PropTypes.object,
    legend: PropTypes.object,
    highlights: PropTypes.array,
    description: PropTypes.object,
    originalData: PropTypes.object,
    pieChartDataValues: PropTypes.object,
};

const mapStateToProps = () => ({

});

const mapDispatchToProps = dispatch => ({
});

export default connect(mapStateToProps, mapDispatchToProps)(ChartContainer);