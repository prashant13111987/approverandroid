
import React, { Component } from 'react';
import { Alert, Keyboard, Image, Dimensions, Platform, Linking, View ,Text,ScrollView} from 'react-native';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as authActionCreator from '../../../common/actions/login';
import * as dashboardActionCreator from '../../../common/actions/dashboard';
import CollapsableDashboardListComponent from '../../components/CollapsableDashboardListComponent';

//invoice2go-icon.png

const { width } = Dimensions.get('window');
class CollapsableDashboardListContainer extends Component {

    constructor(props) {
        super(props);
        this.state = {
        };
    }

    render() {
        return (
            <CollapsableDashboardListComponent
              />
            );
    }
}

CollapsableDashboardListContainer.propTypes = {
    navigation: PropTypes.object,
   };

const mapStateToProps = () => ({
});

const mapDispatchToProps = dispatch => ({
 });

export default connect(mapStateToProps, mapDispatchToProps)(CollapsableDashboardListContainer);