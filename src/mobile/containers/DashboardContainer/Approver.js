
import React, { Component } from 'react';
import {
    Alert, Keyboard, Image, Dimensions, Platform, Linking, View, Text, TouchableOpacity,
    processColor
} from 'react-native';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import _ from 'lodash';
import DashboardComponentApprover from '../../components/DashboardComponent/Approver';
import logo from '../../../common/images/logo.png'
import RoundedButton from '../../components/CustomComponent/RoundedComponent';
import notification from '../../../common/images/notification.png'
import search from '../../../common/images/search.png'
import menu from '../../../common/images/menu.png'
import { bindActionCreators } from "redux";
import * as dashboardActionCreator from "../../../common/actions/dashboard/approverAction";
import * as authActionCreator from "../../../common/actions/login";
import { getApproverReceiveObject, getApproverTotalFromObject } from '../../../common/selector/dashboard';

import Button from "../../components/CustomComponent/Button";

const { width } = Dimensions.get('window');

class DashboardContainer extends Component {
    static navigationOptions = ({ navigation }) => ({
        headerTitle: (
            <View style={{ flex: 1 }}>
                <Text
                    style={{
                        alignSelf: 'center',
                        color: 'white',
                        fontSize: 16,
                        fontWeight: '700'
                    }}>
                    Setlmint
                </Text>
            </View>
        ),
        headerRight: (
            <View style={{ flexDirection: 'row', paddingRight: 10, justifyContent: 'space-between', flex: 1 }}>
                <TouchableOpacity onPress={() => { navigation.navigate('Settings'); }} source={notification} >
                    <Image
                        style={{ width: 20, height: 20 }}
                        source={notification}
                    />
                </TouchableOpacity>
            </View>
        ),
        headerLeft: (
            <TouchableOpacity onPress={() => { navigation.navigate('DrawerToggle') }} >
                <Image source={menu} style={{ height: 15, width: 15, marginLeft: 12 }} />
            </TouchableOpacity>
        ),
        headerStyle: {
            backgroundColor: '#629ee4',
            borderBottomWidth: 0,
            shadowOpacity: 0,
            shadowOffset: {
                height: 0,
            },
            shadowRadius: 0,
            elevation: 0,
        },
        headerTitleStyle: { alignSelf: 'center' },
    });



    componentWillMount() {
        this.props.dashboardAction.getPendingInvoices();
    }

    componentDidMount() {
        console.log("Component Did Mount ");
        // this.props.loginAction.logoutUser()
        // this.props.dashboardAction.receivedInvoicesRequest();
        //  this.props.navigation.setParams({
        //      handleDrawerClick: this.onMenuPress,
        //  });
    }

    // onMenuPress() {
    //    // this.props.dashboardAction.goBack();
    //    // this.props.navigation.navigate('DrawerOpen');
    //     this.props.navigation.navigate('DrawerToggle')
    // }


    constructor(props) {
        super(props);
        // this.onMenuPress = this.onMenuPress.bind(this);
        const dataHolder = [{ value: this.props.ststusBillObject.invoices.length, label: 'Invoices' },
        { value: this.props.ststusBillObject.bills.length, label: 'Bills', },
        { value: this.props.ststusBillObject.payments.length, label: 'Payment', },
        { value: this.props.ststusBillObject.PO.length, label: 'PO', },
        { value: this.props.ststusBillObject.masterData.length, label: 'Master Data', },
            // { value: this.props.ststusBillObject.approval.length, label: 'Approved', },
        ];
        this.state = {
            isReceived: true,
            isScanned: true,
            isPending: true,
            isRejected: true,
            isApproved: true,
            selectedSource: 'all',
            legend: {
                height: 40,
                enabled: false,
                textSize: 10,
                form: 'CIRCLE',
                yOffset: 10,
                xEntrySpace: 15,
                yEntrySpace: 20,
                horizontalAlignment: "LEFT",
                verticalAlignment: "CENTER",
                orientation: "VERTICAL",
                wordWrapEnabled: true,
                formSize: 10
            },
            data: {
                dataSets: [{
                    values: dataHolder,
                    label: '',
                    config: {
                        colors: [processColor('#629ee4'), processColor('#004C7F'), processColor('#002640'), processColor('#0072BF'), processColor('#3fd1d1')],
                        valueTextSize: 0,
                        width: 20,
                        valueTextColor: processColor('green'),
                        drawValues: false,
                        sliceSpace: 1,
                        value: '',
                        selectionShift: 8,
                        // xValuePosition: "OUTSIDE_SLICE",
                        // yValuePosition: "OUTSIDE_SLICE",
                        //valueFormatter: "#.#'%'",
                        // valueLineColor: processColor('green'),
                        // valueLinePart1Length: 0.5
                    }
                }],
            },

            highlights: [{ x: 2 }],
            description: {
                text: '',
                textSize: 10,
                textColor: processColor('darkgray'),

            },
            pieChartDataValues: {
                label: '',
                config: {
                    colors: [processColor('#629ee4'), processColor('#004C7F'), processColor('#002640'), processColor('#0072BF'), processColor('#3fd1d1')],
                    valueTextSize: 0,
                    width: 20,
                    //    valueTextColor: processColor('green'),
                    drawValues: false,
                    sliceSpace: 1,
                    value: '',
                    selectionShift: 8,
                }
            }
        };
        this.onSelectSource = this.onSelectSource.bind(this);
        this.onSelectStatus = this.onSelectStatus.bind(this);
        this.onAllPress = this.onAllPress.bind(this);
    }

    componentWillReceiveProps(nextProps) {
        console.log(nextProps.resetPieFlag)
        if (nextProps.resetPieFlag === true) {

            this.setState({
                data: {
                    dataSets: [{
                        values: [{ value: nextProps.ststusBillObject.invoices.length, label: 'Invoices' },
                        { value: nextProps.ststusBillObject.bills.length, label: 'Bills', },
                        { value: nextProps.ststusBillObject.payments.length, label: 'Payment', },
                        { value: nextProps.ststusBillObject.PO.length, label: 'PO', },
                        { value: nextProps.ststusBillObject.masterData.length, label: 'Master Data', },
                            // { value: this.props.ststusBillObject.approval.length, label: 'Approved', },
                        ],
                        label: '',
                        config: {
                            colors: [processColor('#629ee4'), processColor('#004C7F'), processColor('#002640'), processColor('#0072BF'), processColor('#3fd1d1')],
                            valueTextSize: 0,
                            width: 20,
                            valueTextColor: processColor('green'),
                            drawValues: false,
                            sliceSpace: 1,
                            value: '',
                            selectionShift: 8,
                        }
                    }],
                }
            })
            this.props.dashboardAction.resetPieFlagData(false);

        }
    }

    onSelectSource(source) {
        let count = 0;
        if (source === "phone") {
            _.each(this.props.ststusBillObject.invoices, (reciveItem) => {
                console.log("Source Name ::::::::::::::::::", reciveItem.source_name)
                if (reciveItem.source_name === 's_invoice') {
                    count = count + 1
                }
            });
            this.props.dashboardAction.setInvoiceReceiveCount(count)

        } else if (source === 'mail') {
            _.each(this.props.ststusBillObject.invoices, (reciveItem) => {
                console.log("Source Name ::::::::::::::::::", reciveItem.source_name)
                if (reciveItem.source_name === 'email_invoice') {

                    count = count + 1
                }
            });
            this.props.dashboardAction.setInvoiceReceiveCount(count)
        } else if (source === 'app') {
            _.each(this.props.ststusBillObject.invoices, (reciveItem) => {
                console.log("Source Name ::::::::::::::::::", reciveItem.source_name)
                if (reciveItem.source_name === 'uploaded_invoice') {

                    count = count + 1
                }
            });
            this.props.dashboardAction.setInvoiceReceiveCount(count)
        } else if (source === "all") {
            count = this.props.ststusBillObject.invoices.length
            this.props.dashboardAction.setInvoiceReceiveCount(count)
        }
        this.setState({
            selectedSource: source,
        });
    }

    onAllPress() {
        // this.props.loginAction.navigateToVoiceList();
        //this.props.navigateToValidateInvoiceScreen();
        if (this.state.isApproved === false || this.state.isReceived === false || this.state.isScanned === false || this.state.isPending === false
            || this.state.isRejected === false || this.state.isOnhold === false) {
            this.props.dashboardAction.showFilterData(true);
            this.props.dashboardAction.selectedInvoiceStatus("")
            // isScann == Created
            const legendObject = {
                isApprove: this.state.isApproved,
                isReceived: this.state.isReceived,
                isScanned: this.state.isScanned,
                isPending: this.state.isPending,
                isRejected: this.state.isRejected,
                isOnhold: this.state.isOnhold,
            }
            this.props.dashboardAction.setLegendObject(legendObject);
            this.props.loginAction.navigateApproverToVoiceList();
        }
        else {
            this.props.dashboardAction.selectedInvoiceStatus("Invoices")
            this.props.loginAction.navigateApproverToVoiceList();
        }
    }

    onPieChartStatusOff(sourceValue) {
        var clonePieData = JSON.parse(JSON.stringify(this.state.data.dataSets[0].values));
        var clonePieDataColor = JSON.parse(JSON.stringify(this.state.data.dataSets[0].config.colors));

        clonePieData.forEach((obj, index) => {
            if (obj.label === sourceValue) {
                obj.value = 0
            }
        })

        console.log(clonePieData)
        this.setState(
            {
                data: {
                    dataSets: [{
                        values: clonePieData, label: this.state.pieChartDataValues.label, config: {
                            colors: clonePieDataColor, valueTextSize: 0,
                            width: 20,
                            valueTextColor: processColor('green'),
                            drawValues: false,
                            sliceSpace: 1,
                            value: '',
                            selectionShift: 8,
                            valueFormatter: "#.#'%'"
                        },
                    }]
                }
            }
        );

    }


    onPieChartStatusON(sourceValue) {

        var clonePieData = JSON.parse(JSON.stringify(this.state.data.dataSets[0].values));
        var clonePieDataColor = JSON.parse(JSON.stringify(this.state.data.dataSets[0].config.colors));

        clonePieData.forEach((obj, index) => {
            if (obj.label === sourceValue) {
                if (sourceValue === 'Invoices') {
                    obj.value = this.props.ststusBillObject.invoices.length
                }
                if (sourceValue === 'Bills') {
                    obj.value = this.props.ststusBillObject.bills.length
                }
                if (sourceValue === 'Payment') {
                    obj.value = this.props.ststusBillObject.payments.length
                }
                if (sourceValue === 'PO') {
                    obj.value = this.props.ststusBillObject.PO.length
                }
                if (sourceValue === 'Master Data') {
                    obj.value = this.props.ststusBillObject.masterData.length
                }
                if (sourceValue === 'Approved') {
                    // obj.value = this.props.ststusBillObject.approval.length
                }
            }
        })

        console.log(clonePieData)
        this.setState(
            {
                data: {
                    dataSets: [{
                        values: clonePieData, label: this.state.pieChartDataValues.label, config: {
                            colors: clonePieDataColor, valueTextSize: 0,
                            width: 20,
                            valueTextColor: processColor('green'),
                            drawValues: false,
                            sliceSpace: 1,
                            value: '',
                            selectionShift: 8,
                            valueFormatter: "#.#'%'"
                        },
                    }]
                }
            }
        );
    }

    onSelectStatus(sourceValue) {
        if (sourceValue === 'Invoices') {
            if (this.state.isReceived) {
                this.setState({
                    isReceived: false
                })

                this.onPieChartStatusOff(sourceValue)
            }
            else {
                this.setState({
                    isReceived: true
                })
                this.onPieChartStatusON(sourceValue)
            }

        }

        if (sourceValue === 'Bills') {
            if (this.state.isScanned) {
                this.setState({
                    isScanned: false
                })
                this.onPieChartStatusOff(sourceValue)
            }
            else {
                this.setState({
                    isScanned: true
                })
                this.onPieChartStatusON(sourceValue)
            }
        }

        if (sourceValue === 'Payment') {
            if (this.state.isPending) {
                this.setState({
                    isPending: false
                })
                this.onPieChartStatusOff(sourceValue)
            }
            else {
                this.setState({
                    isPending: true
                })
                this.onPieChartStatusON(sourceValue)
            }
        }

        if (sourceValue === 'PO') {
            if (this.state.isRejected) {
                this.setState({
                    isRejected: false
                })
                this.onPieChartStatusOff(sourceValue)
            }
            else {
                this.setState({
                    isRejected: true
                })
                this.onPieChartStatusON(sourceValue)
            }
        }

        if (sourceValue === 'Master Data') {
            if (this.state.isApproved) {
                this.setState({
                    isApproved: false
                })
                this.onPieChartStatusOff(sourceValue)
            }
            else {
                this.setState({
                    isApproved: true
                })
                this.onPieChartStatusON(sourceValue)
            }
        }
    }

    render() {
        return (
            <DashboardComponentApprover
                onSelectSource={this.onSelectSource}
                onSelectStatus={this.onSelectStatus}
                onAllPress={this.onAllPress}
                selectedSource={this.state.selectedSource}
                legend={this.state.legend}
                data={this.state.data}
                highlights={this.state.highlights}
                description={this.state.description}
                pieChartDataValues={this.state.pieChartDataValues}
                isReceived={this.state.isReceived}
                isScanned={this.state.isScanned}
                isPending={this.state.isPending}
                isRejected={this.state.isRejected}
                isOnhold={this.state.isOnhold}
                isApproved={this.state.isApproved}
                isCompleted={this.state.isCompleted}
                receivedInvoiceList={this.props.receivedInvoiceList}
                receiveCount={this.props.receiveCount}
                ststusBillObject={this.props.ststusBillObject}
                totalAmount={this.props.totalAmountObject.totalAmount}
                totalInvoiceCount={this.props.totalAmountObject.totalCount}
                totalAmountObject={this.props.totalAmountObject}
                showFilterData={this.props.showFilterData}
                onViewChange={this.props.onViewChange}
                activeScreen={this.props.activeScreen}
            />
        );
    }
}

DashboardContainer.propTypes = {
    // navigation: PropTypes.object,
    dashboardAction: PropTypes.shape({
        navigateToVoiceList: PropTypes.func,
        receivedInvoicesRequest: PropTypes.func,
        receivedALLInvoiceRequest: PropTypes.func,
        setInvoiceReceiveCount: PropTypes.func,
        resetPieFlagData: PropTypes.func,
        showFilterData: PropTypes.func,
        setLegendObject: PropTypes.func,
        selectedInvoiceStatus: PropTypes.func,
    }),
};

const mapStateToProps = state => ({
    // navigation: state.nav,
    loading: state.app.loading,
    netState: state.deviceInfo.netState,
    isNetworkStateDetected: state.deviceInfo.isNetworkStateDetected,
    receivedInvoiceList: state.approverDashboard.receivedInvoiceList,
    receivedFromEmailList: state.approverDashboard.receivedFromEmailList,
    receivedFromPhoneList: state.approverDashboard.receivedFromPhoneList,
    receivedFromAppList: state.approverDashboard.receivedFromAppList,
    receivedAllInvoiceList: state.approverDashboard.receivedAllInvoiceList,
    receiveCount: state.approverDashboard.receiveCount,
    resetPieFlag: state.approverDashboard.resetPieFlag,
    ststusBillObject: getApproverReceiveObject(state),
    totalAmountObject: getApproverTotalFromObject(state),
    showFilterData: state.approverDashboard.showFilterData
});



const mapDispatchToProps = dispatch => ({
    loginAction: bindActionCreators(authActionCreator, dispatch),
    dashboardAction: bindActionCreators(dashboardActionCreator, dispatch),
});


export default connect(mapStateToProps, mapDispatchToProps)(DashboardContainer);