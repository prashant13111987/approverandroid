
import React, { Component } from 'react';
import {
    Alert, Keyboard, Image, Dimensions, Platform, Linking, View, Text, TouchableOpacity,
    processColor
} from 'react-native';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import _ from 'lodash';
import DashboardComponentMaker from '../../components/DashboardComponent/Maker';
import logo from '../../../common/images/logo.png'
import RoundedButton from '../../components/CustomComponent/RoundedComponent';
import notification from '../../../common/images/notification.png'
import search from '../../../common/images/search.png'
import menu from '../../../common/images/menu.png'
import { bindActionCreators } from "redux";
import * as dashboardActionCreator from "../../../common/actions/dashboard";
import * as authActionCreator from "../../../common/actions/login";
import { getReceiveObject, getTotalFromObject } from '../../../common/selector/dashboard'

import Button from "../../components/CustomComponent/Button";

const { width } = Dimensions.get('window');

class DashboardContainer extends Component {
    static navigationOptions = ({ navigation }) => ({
        headerTitle: (
            <View style={{ flex: 1 }}>
                <Text
                    style={{
                        alignSelf: 'center',
                        color: 'white',
                        fontSize: 16,
                        fontWeight: '700'
                    }}>
                    Setlmint
                </Text>
            </View>
        ),
        headerRight: (
            <View style={{ flexDirection: 'row', paddingRight: 10, justifyContent: 'space-between', flex: 1 }}>
                <TouchableOpacity onPress={() => { navigation.navigate('Settings'); }} source={notification} >
                    <Image
                        style={{ width: 20, height: 20 }}
                        source={notification}
                    />
                </TouchableOpacity>
            </View>
        ),
        headerLeft: (
            <TouchableOpacity onPress={() => { navigation.navigate('DrawerToggle') }} >
                <Image source={menu} style={{ height: 15, width: 15, marginLeft: 12 }} />
            </TouchableOpacity>
        ),
        headerStyle: {
            backgroundColor: '#629ee4',
            borderBottomWidth: 0,
            shadowOpacity: 0,
            shadowOffset: {
                height: 0,
            },
            shadowRadius: 0,
            elevation: 0,
        },
        headerTitleStyle: { alignSelf: 'center' },
    });



    componentWillMount() {
        this.props.dashboardAction.receivedInvoicesRequest();
    }

    componentDidMount() {

        console.log("Component Did Mount ");
        // this.props.dashboardAction.receivedInvoicesRequest();
        //  this.props.navigation.setParams({
        //      handleDrawerClick: this.onMenuPress,
        //  });
    }

    // onMenuPress() {
    //    // this.props.dashboardAction.goBack();
    //    // this.props.navigation.navigate('DrawerOpen');
    //     this.props.navigation.navigate('DrawerToggle')
    // }


    constructor(props) {
        super(props);
        // this.onMenuPress = this.onMenuPress.bind(this);
        const dataHolder = [{ value: this.props.ststusBillObject.received.length, label: 'Received' },
        { value: this.props.ststusBillObject.create.length, label: 'Created', },
        { value: this.props.ststusBillObject.pending.length, label: 'Pending', },
        { value: this.props.ststusBillObject.rejected.length, label: 'Rejected', },
        { value: this.props.ststusBillObject.onHold.length, label: 'On hold', },
        { value: this.props.ststusBillObject.approval.length, label: 'Approved', },
        ];
        this.state = {
            isReceived: 'true',
            isScanned: 'true',
            isPending: 'true',
            isRejected: 'true',
            isOnhold: 'true',
            isApproved: 'true',
            isCompleted: 'true',
            selectedSource: 'all',
            legend: {
                height: 40,
                enabled: false,
                textSize: 10,
                form: 'CIRCLE',
                yOffset: 10,
                xEntrySpace: 15,
                yEntrySpace: 20,
                horizontalAlignment: "LEFT",
                verticalAlignment: "CENTER",
                orientation: "VERTICAL",
                wordWrapEnabled: true,
                formSize: 10
            },
            data: {
                dataSets: [{
                    values: dataHolder,
                    label: '',
                    config: {
                        colors: [processColor('#629ee4'), processColor('#004C7F'), processColor('#002640'), processColor('#0072BF'), processColor('#44A5E5'), processColor('#3fd1d1')],
                        valueTextSize: 0,
                        width: 20,
                        valueTextColor: processColor('green'),
                        drawValues: false,
                        sliceSpace: 1,
                        value: '',
                        selectionShift: 8,
                        // xValuePosition: "OUTSIDE_SLICE",
                        // yValuePosition: "OUTSIDE_SLICE",
                        //valueFormatter: "#.#'%'",
                        // valueLineColor: processColor('green'),
                        // valueLinePart1Length: 0.5
                    }
                }],
            },

            highlights: [{ x: 2 }],
            description: {
                text: '',
                textSize: 10,
                textColor: processColor('darkgray'),

            },
            pieChartDataValues: {
                label: '',
                config: {
                    colors: [processColor('#629ee4'), processColor('#004C7F'), processColor('#002640'), processColor('#0072BF'), processColor('#44A5E5'), processColor('#3fd1d1')],
                    valueTextSize: 0,
                    width: 20,
                    //    valueTextColor: processColor('green'),
                    drawValues: false,
                    sliceSpace: 1,
                    value: '',
                    selectionShift: 8,
                }
            }
        };
        this.onSelectSource = this.onSelectSource.bind(this);
        this.onSelectStatus = this.onSelectStatus.bind(this);
        this.onAllPress = this.onAllPress.bind(this);
    }

    componentWillReceiveProps(nextProps) {
        console.log(nextProps.resetPieFlag)
        if (nextProps.resetPieFlag === true) {

            this.setState({
                data: {
                    dataSets: [{
                        values: [{ value: nextProps.ststusBillObject.received.length, label: 'Received', },
                        { value: nextProps.ststusBillObject.create.length, label: 'Created' },
                        { value: nextProps.ststusBillObject.pending.length, label: 'Pending' },
                        { value: nextProps.ststusBillObject.rejected.length, label: 'Rejected' },
                        { value: nextProps.ststusBillObject.onHold.length, label: 'On hold' },
                        { value: nextProps.ststusBillObject.approval.length, label: 'Approved' },

                        ],
                        label: '',
                        config: {
                            colors: [processColor('#629ee4'), processColor('#004C7F'), processColor('#002640'), processColor('#0072BF'), processColor('#44A5E5'), processColor('#3fd1d1')],
                            valueTextSize: 0,
                            width: 20,
                            valueTextColor: processColor('green'),
                            drawValues: false,
                            sliceSpace: 1,
                            value: '',
                            selectionShift: 8,
                        }
                    }],
                }
            })
            this.props.dashboardAction.resetPieFlagData(false);

        }
    }

    onSelectSource(source) {
        let count = 0;
        if (source === "phone") {
            _.each(this.props.ststusBillObject.received, (reciveItem) => {
                console.log("Source Name ::::::::::::::::::", reciveItem.source_name)
                if (reciveItem.source_name === 's_invoice') {
                    count = count + 1
                }
            });
            this.props.dashboardAction.setInvoiceReceiveCount(count)

        } else if (source === 'mail') {
            _.each(this.props.ststusBillObject.received, (reciveItem) => {
                console.log("Source Name ::::::::::::::::::", reciveItem.source_name)
                if (reciveItem.source_name === 'email_invoice') {

                    count = count + 1
                }
            });
            this.props.dashboardAction.setInvoiceReceiveCount(count)
        } else if (source === 'app') {
            _.each(this.props.ststusBillObject.received, (reciveItem) => {
                console.log("Source Name ::::::::::::::::::", reciveItem.source_name)
                if (reciveItem.source_name === 'uploaded_invoice') {

                    count = count + 1
                }
            });
            this.props.dashboardAction.setInvoiceReceiveCount(count)
        } else if (source === "all") {
            count = this.props.ststusBillObject.received.length
            this.props.dashboardAction.setInvoiceReceiveCount(count)
        }
        this.setState({
            selectedSource: source,
        });
    }

    onAllPress() {
        // this.props.loginAction.navigateToVoiceList();
        //this.props.navigateToValidateInvoiceScreen();
        if (this.state.isApproved === false || this.state.isReceived === false || this.state.isScanned === false || this.state.isPending === false
            || this.state.isRejected === false || this.state.isOnhold === false) {
            this.props.dashboardAction.showFilterData(true);
            this.props.dashboardAction.selectedInvoiceStatus("")
            console.log("Call Filter Data ");
            // isScann == Created
            const legendObject = {
                isApprove: this.state.isApproved,
                isReceived: this.state.isReceived,
                isScanned: this.state.isScanned,
                isPending: this.state.isPending,
                isRejected: this.state.isRejected,
                isOnhold: this.state.isOnhold,
            }
            this.props.dashboardAction.setLegendObject(legendObject);
            this.props.loginAction.navigateToVoiceList();
        }
        else {
            this.props.dashboardAction.selectedInvoiceStatus("Received")
            this.props.loginAction.navigateToVoiceList();
        }
    }

    onPieChartStatusOff(sourceValue) {
        var clonePieData = JSON.parse(JSON.stringify(this.state.data.dataSets[0].values));
        var clonePieDataColor = JSON.parse(JSON.stringify(this.state.data.dataSets[0].config.colors));

        clonePieData.forEach((obj, index) => {
            if (obj.label === sourceValue) {
                obj.value = 0
            }
        })

        console.log(clonePieData)
        this.setState(
            {
                data: {
                    dataSets: [{
                        values: clonePieData, label: this.state.pieChartDataValues.label, config: {
                            colors: clonePieDataColor, valueTextSize: 0,
                            width: 20,
                            valueTextColor: processColor('green'),
                            drawValues: false,
                            sliceSpace: 1,
                            value: '',
                            selectionShift: 8,
                            valueFormatter: "#.#'%'"
                        },
                    }]
                }
            }
        );

    }


    onPieChartStatusON(sourceValue) {

        var clonePieData = JSON.parse(JSON.stringify(this.state.data.dataSets[0].values));
        var clonePieDataColor = JSON.parse(JSON.stringify(this.state.data.dataSets[0].config.colors));

        clonePieData.forEach((obj, index) => {
            if (obj.label === sourceValue) {
                if (sourceValue === 'Received') {
                    obj.value = this.props.ststusBillObject.received.length
                }
                if (sourceValue === 'Created') {
                    obj.value = this.props.ststusBillObject.create.length
                }
                if (sourceValue === 'Pending') {
                    obj.value = this.props.ststusBillObject.pending.length
                }
                if (sourceValue === 'Rejected') {
                    obj.value = this.props.ststusBillObject.rejected.length
                }
                if (sourceValue === 'On hold') {
                    obj.value = this.props.ststusBillObject.onHold.length
                }
                if (sourceValue === 'Approved') {
                    obj.value = this.props.ststusBillObject.approval.length
                }
            }
        })

        console.log(clonePieData)
        this.setState(
            {
                data: {
                    dataSets: [{
                        values: clonePieData, label: this.state.pieChartDataValues.label, config: {
                            colors: clonePieDataColor, valueTextSize: 0,
                            width: 20,
                            valueTextColor: processColor('green'),
                            drawValues: false,
                            sliceSpace: 1,
                            value: '',
                            selectionShift: 8,
                            valueFormatter: "#.#'%'"
                        },
                    }]
                }
            }
        );
    }

    onSelectStatus(sourceValue) {
        if (sourceValue === 'Received') {
            if (this.state.isReceived) {
                this.setState({
                    isReceived: false
                })

                this.onPieChartStatusOff(sourceValue)
            }
            else {
                this.setState({
                    isReceived: true
                })
                this.onPieChartStatusON(sourceValue)
            }

        }

        if (sourceValue === 'Created') {
            if (this.state.isScanned) {
                this.setState({
                    isScanned: false
                })
                this.onPieChartStatusOff(sourceValue)
            }
            else {
                this.setState({
                    isScanned: true
                })
                this.onPieChartStatusON(sourceValue)
            }
        }

        if (sourceValue === 'Pending') {
            if (this.state.isPending) {
                this.setState({
                    isPending: false
                })
                this.onPieChartStatusOff(sourceValue)
            }
            else {
                this.setState({
                    isPending: true
                })
                this.onPieChartStatusON(sourceValue)
            }
        }

        if (sourceValue === 'Rejected') {
            if (this.state.isRejected) {
                this.setState({
                    isRejected: false
                })
                this.onPieChartStatusOff(sourceValue)
            }
            else {
                this.setState({
                    isRejected: true
                })
                this.onPieChartStatusON(sourceValue)
            }
        }

        if (sourceValue === 'On hold') {
            if (this.state.isOnhold) {
                this.setState({
                    isOnhold: false
                })
                this.onPieChartStatusOff(sourceValue)
            }
            else {
                this.setState({
                    isOnhold: true
                })
                this.onPieChartStatusON(sourceValue)
            }
        }

        if (sourceValue === 'Approved') {
            if (this.state.isApproved) {
                this.setState({
                    isApproved: false
                })
                this.onPieChartStatusOff(sourceValue)
            }
            else {
                this.setState({
                    isApproved: true
                })
                this.onPieChartStatusON(sourceValue)
            }
        }
    }

    render() {
        return (
            <DashboardComponentMaker
                onSelectSource={this.onSelectSource}
                onSelectStatus={this.onSelectStatus}
                onAllPress={this.onAllPress}
                selectedSource={this.state.selectedSource}
                legend={this.state.legend}
                data={this.state.data}
                highlights={this.state.highlights}
                description={this.state.description}
                pieChartDataValues={this.state.pieChartDataValues}
                isReceived={this.state.isReceived}
                isScanned={this.state.isScanned}
                isPending={this.state.isPending}
                isRejected={this.state.isRejected}
                isOnhold={this.state.isOnhold}
                isApproved={this.state.isApproved}
                isCompleted={this.state.isCompleted}
                receivedInvoiceList={this.props.receivedInvoiceList}
                receiveCount={this.props.receiveCount}
                ststusBillObject={this.props.ststusBillObject}
                totalAmount={this.props.totalAmountObject.totalAmount}
                totalInvoiceCount={this.props.totalAmountObject.totalCount}
                totalAmountObject={this.props.totalAmountObject}
                showFilterData={this.props.showFilterData}
                onViewChange={this.props.onViewChange}
                activeScreen={this.props.activeScreen}

            />
        );
    }
}

DashboardContainer.propTypes = {
    // navigation: PropTypes.object,
    dashboardAction: PropTypes.shape({
        navigateToVoiceList: PropTypes.func,
        receivedInvoicesRequest: PropTypes.func,
        receivedALLInvoiceRequest: PropTypes.func,
        setInvoiceReceiveCount: PropTypes.func,
        resetPieFlagData: PropTypes.func,
        showFilterData: PropTypes.func,
        setLegendObject: PropTypes.func,
        selectedInvoiceStatus: PropTypes.func,
    }),
};

const mapStateToProps = state => ({
    // navigation: state.nav,
    loading: state.app.loading,
    netState: state.deviceInfo.netState,
    isNetworkStateDetected: state.deviceInfo.isNetworkStateDetected,
    receivedInvoiceList: state.dashboard.receivedInvoiceList,
    receivedFromEmailList: state.dashboard.receivedFromEmailList,
    receivedFromPhoneList: state.dashboard.receivedFromPhoneList,
    receivedFromAppList: state.dashboard.receivedFromAppList,
    receivedAllInvoiceList: state.dashboard.receivedAllInvoiceList,
    receiveCount: state.dashboard.receiveCount,
    resetPieFlag: state.dashboard.resetPieFlag,
    ststusBillObject: getReceiveObject(state),
    totalAmountObject: getTotalFromObject(state),
    showFilterData: state.dashboard.showFilterData
});



const mapDispatchToProps = dispatch => ({
    loginAction: bindActionCreators(authActionCreator, dispatch),
    dashboardAction: bindActionCreators(dashboardActionCreator, dispatch),
});


export default connect(mapStateToProps, mapDispatchToProps)(DashboardContainer);