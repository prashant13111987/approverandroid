import React, { Component } from 'react';
import Maker from './Maker'
import Approver from './Approver'

import {
    Alert, Keyboard, Image, Dimensions, Platform, Linking, View, Text, TouchableOpacity,
    processColor
} from 'react-native';

import logo from '../../../common/images/logo.png'
import RoundedButton from '../../components/CustomComponent/RoundedComponent';
import notification from '../../../common/images/notification.png'
import search from '../../../common/images/search.png'
import menu from '../../../common/images/menu.png'
import Button from "../../components/CustomComponent/Button";


class Dashboard extends Component {
    constructor(props) {
        super(props);
        this.state = {
            activeScreen: 'approver'
        }
    }

    static navigationOptions = ({ navigation }) => ({
        headerTitle: (
            <View style={{ flex: 1 }}>
                <Text
                    style={{
                        alignSelf: 'center',
                        color: 'white',
                        fontSize: 16,
                        fontWeight: '700'
                    }}>
                    Setlmint
            </Text>
            </View>
        ),
        headerRight: (
            <View style={{ flexDirection: 'row', paddingRight: 10, justifyContent: 'space-between', flex: 1 }}>
                <TouchableOpacity onPress={() => { navigation.navigate('Settings'); }} source={notification} >
                    <Image
                        style={{ width: 20, height: 20 }}
                        source={notification}
                    />
                </TouchableOpacity>
            </View>
        ),
        headerLeft: (
            <TouchableOpacity onPress={() => { navigation.navigate('DrawerToggle') }} >
                <Image source={menu} style={{ height: 15, width: 15, marginLeft: 12 }} />
            </TouchableOpacity>
        ),
        headerStyle: {
            backgroundColor: '#629ee4',
            borderBottomWidth: 0,
            shadowOpacity: 0,
            shadowOffset: {
                height: 0,
            },
            shadowRadius: 0,
            elevation: 0,
        },
        headerTitleStyle: { alignSelf: 'center' },
    });


    onViewChange = newView => {
        this.setState({
            activeScreen: newView
        })
    }

    render() {
        const { state: { activeScreen }, onViewChange } = this
        return (<>
            {
                activeScreen === 'maker' ? <Maker onViewChange={onViewChange} activeScreen={activeScreen} /> :
                    <Approver onViewChange={onViewChange} activeScreen={activeScreen} />
            }
        </>)
    }
}

export default Dashboard