
import React, { Component } from 'react';
import {Alert, Keyboard, Image, Dimensions, Platform, Linking, View, Text, StyleSheet,TouchableOpacity} from 'react-native';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import CommanListComponent from '../../components/CommanListComponent'
import backButton from '../../../common/images/back.png'
import * as dashboardActionCreator from "../../../common/actions/dashboard";

const { width } = Dimensions.get('window');

class DistributionListContainer extends Component {

    static navigationOptions = ({ navigation }) => ({
        headerTitle: (
            <View style={{ alignItems:'flex-start',flex:1}}>
                <Text
                    style={{
                        alignSelf: 'center',
                        color:'white',
                        fontSize:16,
                        fontWeight:'700'
                    }}>
                    Departments
                </Text>
            </View>
        ),
        headerStyle: {
            backgroundColor: '#629ee4',
            borderBottomWidth: 0,
            shadowOpacity: 0,
            shadowOffset: {
                height: 0,
            },
            shadowRadius: 0,
            elevation: 0,
        },
        headerLeft: (
            <TouchableOpacity onPress={() => navigation.state.params.handleBack()}>
                <Image source={backButton} style={{ height: 24, width: 24, marginLeft: 12 }} />
            </TouchableOpacity>
        ),
    });

  constructor(props) {
    super(props);

    this.state = {
      searchText: '',
      selectedText:'',
    };
    this.searchFilterFunction = this.searchFilterFunction.bind(this);
    this.onRowPress = this.onRowPress.bind(this);
    this.onBackPress = this.onBackPress.bind(this);
  }

    componentDidMount() {
        this.props.navigation.setParams({
            handleBack: this.onBackPress,
        });
    }

    onBackPress() {
        this.props.dashboardAction.goBack();
    }

    // On search filter for state.
  searchFilterFunction(text) {
    this.setState({
      searchText: text,
    });
  }

  onRowPress(item) {
    this.props.dashboardAction.setDepartmentObject(item)
      this.props.dashboardAction.goBack();
    // const finalRegisterObject = Object.assign(districtObject, districtCustomObje);
  }

  render() {
    return (
      <View style={{width:'100%',height:'100%',borderBottomLeftRadius:10,borderBottomRightRadius:10}} >
        <CommanListComponent
          data={this.props.merchantDepartments}
          onRowPress={this.onRowPress}
          keyName="Distribution_list"
          searchFilterFunction={this.searchFilterFunction}
          searchText={this.state.searchText}
          selectedText={this.state.selectedText}
        />
      </View>
    );
  }
}


const styles = StyleSheet.create({
  modalContent: {
    padding: 22,
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 4,
    borderColor: "rgba(0, 0, 0, 0.1)",
    height:'40%',
    width:'60%',
  },
});

DistributionListContainer.propTypes = {
  navigation: PropTypes.object,
  dashboardAction: PropTypes.shape({
    //venderListRequest:PropTypes.func,
    goBack: PropTypes.func,
      setDepartmentObject:PropTypes.func,
  }),
};


const mapStateToProps = state => ({
  //navigation: state.nav,
  // loading: state.app.loading,
  // vendors:state.dashboard.vendors,
  // billDetails:getBillDetails(state),
    merchantDepartments:state.dashboard.merchantDepartments,
});

const mapDispatchToProps = dispatch => ({
  dashboardAction: bindActionCreators(dashboardActionCreator, dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps)(DistributionListContainer);
