/**
 * Created by Rahul Nakate on 30/04/18
 */

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
    View,
} from 'react-native';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as authActionCreator from '../../../common/actions/login';
import DrawerComponent from '../../components/DrawerComponent';


const general = ['Logout', 'About Us', 'Profile'];
const drawerLists = [{ title: 'General', data: general }];

class DrawerContainer extends Component {
    static navigationOptions = {
        header: null,
    }

    constructor(props) {
        super(props);
        this.handleItemClick = this.handleItemClick.bind(this);
        this.onDrawerPress = this.onDrawerPress.bind(this)
    }
    onDrawerPress() {
        this.props.navigation.navigate('DrawerToggle')
    }

    handleItemClick(item) {
        if (item === 'Logout') {
            this.props.loginAction.logoutUser()
        }
    }


    render() {
        return (
            <View style={{ flex: 1 }}>
                <DrawerComponent
                    handleItemClick={this.handleItemClick}
                    sectionList={drawerLists}
                />
            </View>
        );
    }
}

DrawerContainer.propTypes = {
    // navigation: PropTypes.shape(PropTypes.object),
    loginAction: PropTypes.shape({
        logoutUser: PropTypes.func,
    }),
};


const mapStateToProps = () => ({
});

const mapDispatchToProps = dispatch => ({
    loginAction: bindActionCreators(authActionCreator, dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps)(DrawerContainer);
