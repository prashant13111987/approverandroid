
import React, { Component } from 'react';
import {Alert, Keyboard, Image, Dimensions, Platform, Linking, View, Text, StyleSheet,TouchableOpacity} from 'react-native';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import logo from '../../../common/images/logo.png'
import Modal from "react-native-modal";
import {normalizeFont} from "../../../common/util";
import CommanListComponent from '../../components/CommanListComponent'
import EditItemComponent from '../../components/EditItemComponent'
import {getBillDetails} from "../../../common/selector/InVoiceList";
import {getReceiveObject} from "../../../common/selector/dashboard";
import * as dashboardActionCreator from '../../../common/actions/dashboard';
import backButton from '../../../common/images/back.png'
import _ from "lodash";

const { width } = Dimensions.get('window');
class EditItemContainer extends Component {

    static navigationOptions = ({ navigation }) => ({
        headerTitle: (
            <View style={{ alignItems:'flex-start',flex:1}}>
                <Text
                    style={{
                        alignSelf: 'center',
                        color:'white',
                        fontSize:20,
                        fontWeight:'700'
                    }}>
                    Setlmint
                </Text>
            </View>
        ),
        headerStyle: {
            backgroundColor: '#629ee4',
            borderBottomWidth: 0,
            shadowOpacity: 0,
            shadowOffset: {
                height: 0,
            },
            shadowRadius: 0,
            elevation: 0,
        },
        headerLeft: (
            <TouchableOpacity onPress={() => navigation.state.params.handleBack()}>
                <Image source={backButton} style={{ height: 24, width: 24, marginLeft: 12 }} />
            </TouchableOpacity>
        ),
    });


    constructor(props) {
        super(props);
        this.state = {
            itemName: this.props.billDetails.items[this.props.selectedIndex].item_name,
            hsnName: this.props.billDetails.items[this.props.selectedIndex].hsn_code,
            quantity:this.props.billDetails.items[this.props.selectedIndex].quantity,
            price:this.props.billDetails.items[this.props.selectedIndex].price,
            discount:this.props.billDetails.items[this.props.selectedIndex].discount,
            taxRate:parseFloat(this.props.billDetails.items[this.props.selectedIndex].tax).toString(),
            totalPrice:parseFloat(this.props.billDetails.items[this.props.selectedIndex].price)*parseFloat(this.props.billDetails.items[this.props.selectedIndex].quantity)
        };
        this.onSavePress = this.onSavePress.bind(this);;
        this.handleItemNameChange = this.handleItemNameChange(this);
        this.handleQuantityChange = this.handleQuantityChange.bind(this);
        this.handleItemDiscountChange = this.handleItemDiscountChange.bind(this);
        this.handleHSNChange = this.handleHSNChange.bind(this);
        this.handleItemTaxChange = this.handleItemTaxChange.bind(this);

        this.onNamePress = this.onNamePress.bind(this);
        this.onBackPress = this.onBackPress.bind(this);
        this.inputs = {};
        this.focusNextField = this.focusNextField.bind(this);
        this.onSubmitEditing = this.onSubmitEditing.bind(this);
        this.setInputReference = this.setInputReference.bind(this);
    }

    componentDidMount() {
        this.props.navigation.setParams({
            handleBack: this.onBackPress,
        });
    }

    onSubmitEditing(nextFiled) {
        this.focusNextField(nextFiled);
    }
    setInputReference(name, refs) {
        this.inputs[name] = refs;
    }
    focusNextField(id) {
        this.inputs[id].focus();
    }

    onBackPress() {
        this.props.dashboardAction.goBack();
        this.props.dashboardAction.resetSelectedItemNameObject()
    }

    onNamePress()
    {
        this.props.dashboardAction.navigateItemListListContainer()
    }

    onSavePress(item) {

        let tempObj =  _.cloneDeep(this.props.billDetailsOrignal)
        if(tempObj.bill_items.length > 0){
            let selecteditem = this.props.billDetails.items[this.props.selectedIndex]
            _.each(tempObj.bill_items, (item) => {
                if(item.bill_id === selecteditem.bill_id)
                {
                    item.quantity = this.state.quantity;
                    item.price = this.state.price;
                    item.item_name =this.props.selectedItemObject.item_name?this.props.selectedItemObject.item_name:this.state.itemName;
                    item.hsn_code = this.props.selectedItemObject.hsn_code?this.props.selectedItemObject.hsn_code:this.state.hsnName;
                }
            });
        }
        if(tempObj.rush){
            tempObj.rush = tempObj.rush
        }else{
            tempObj.rush = " "
        }
         this.props.dashboardAction.resetSelectedItemNameObject()
         this.props.dashboardAction.addItem_APIRequest(tempObj)
         this.props.dashboardAction.goBack();

    }

    handleItemNameChange(text) {
        this.setState({ itemName: text });
    }

    handleItemDiscountChange(text) {
        this.setState({ discount: text });
    }
    handleItemTaxChange(text) {

        this.setState({ taxRate: text });
    }

    handleHSNChange(text) {
        this.setState({ hsnName: text });
    }

    handleQuantityChange(text) {
        var totalPrice =  parseFloat(this.state.price)*parseFloat(text)
        this.setState({
            quantity: text ,
            totalPrice: totalPrice
        });
    }

    handlePriceChange(text) {

        var totalPrice =  parseFloat(this.state.quantity)*parseFloat(text)
        this.setState({
            price: text ,
            totalPrice: totalPrice
        });

    }
    render() {
        return (
            <View style={{width:'100%',height:'100%',borderBottomLeftRadius:10,borderBottomRightRadius:10}} >
                <EditItemComponent
                    itemName={this.props.selectedItemObject.item_name?this.props.selectedItemObject.item_name:this.state.itemName}
                    hsnName={this.props.selectedItemObject.hsn_code?this.props.selectedItemObject.hsn_code:this.state.hsnName}
                    quantity={this.state.quantity}
                    price={this.state.price}
                    totalPrice={this.state.totalPrice}
                    imageUrl={this.props.billDetails.imageUrl}
                    discount={this.state.discount}
                    taxRate={this.state.taxRate}
                    onSavePress={this.onSavePress}
                    handleItemNameChange={this.handleItemNameChange}
                    handleItemDiscountChange={this.handleItemDiscountChange}
                    handleHSNChange={this.handleHSNChange}
                    handleQuantityChange={this.handleQuantityChange}
                    handleItemTaxChange={this.handleItemTaxChange}
                    onNamePress={this.onNamePress}
                    focusNextField={this.focusNextField}
                    onSubmitEditing={this.onSubmitEditing}
                    setInputReference={this.setInputReference}
                />
            </View>
        );
    }
}


const styles = StyleSheet.create({
    modalContent: {
        padding: 22,
        justifyContent: "center",
        alignItems: "center",
        borderRadius: 4,
        borderColor: "rgba(0, 0, 0, 0.1)",
        height:'40%',
        width:'60%',
    },
});

EditItemContainer.propTypes = {
    navigation: PropTypes.object,
    dashboardAction: PropTypes.shape({
        goBack: PropTypes.func,
        billDetailSuccess:PropTypes.func,
        navigateItemListListContainer:PropTypes.func,
        addItem_APIRequest:PropTypes.func,
        resetSelectedItemNameObject:PropTypes.func
    }),
};


const mapStateToProps = state => ({
    loading: state.app.loading,
    billDetails:getBillDetails(state),
    billDetailsOrignal:state.dashboard.billDetails,
    selectedItemObject:state.dashboard.selectedItemObject,
    selectedIndex:state.dashboard.selectedIndex
});

const mapDispatchToProps = dispatch => ({
    dashboardAction: bindActionCreators(dashboardActionCreator, dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps)(EditItemContainer);
