
import React, { Component } from 'react';
import { Alert, Keyboard, Image, Dimensions, Platform, Linking, View ,Text,TouchableOpacity} from 'react-native';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import GSTInvoiceComponent from '../../components/GSTInvoiceComponent';
import {getBillDetails} from "../../../common/selector/InVoiceList";
import { bindActionCreators } from 'redux';
import * as dashboardActionCreator from "../../../common/actions/dashboard";
import _ from "lodash";


const { width } = Dimensions.get('window');
class GSTInvoiceContainer extends Component {

    constructor(props) {
        super(props);
        this.state = {
            totalGSTAmount:'',
            cgstAmount:'',
            sgstAmount:'',
            igstAmount:'',
            shipFrom:"",
            shipTo:"",
            buyerGstin:this.props.shipToObject.gstin?this.props.shipToObject.gstin:this.props.billDetails.gstInfo.buyer_gstin,
            venderGstin:this.props.shipToObject.gstin?this.props.shipToObject.gstin:this.props.billDetails.gstInfo.vendor_gstin,
            eWayBill:this.props.billDetails.gstInfo.eway_bill_no?this.props.billDetails.gstInfo.eway_bill_no:"E way bill Number"
        };
        this.inputs = {};
        this.focusNextField = this.focusNextField.bind(this);
        this.onSubmitEditing = this.onSubmitEditing.bind(this);
        this.setInputReference = this.setInputReference.bind(this);
        this.onShipFromPress = this.onShipFromPress.bind(this);
        this.onShipToPress = this.onShipToPress.bind(this);
        this.handleEayWayBillNumber = this.handleEayWayBillNumber.bind(this);
        this.handleShipToGSTIN = this.handleShipToGSTIN.bind(this);
        this.handleShipFromGSTFrom = this.handleShipFromGSTFrom.bind(this);
    }
    // shipFromName:billDetail.shipping_branch_name,
    // ShipToName:billDetail.vendor_branch_name,
    // buyer_gstin:billDetail.buyer_gstin,
    // vendor_gstin:billDetail.vendor_gstin,
    // eway_bill_no:billDetail.eway_bill_no,

    handleEayWayBillNumber(number){
        this.setState({
            eWayBill:number
        })

        let tempObj =  _.cloneDeep(this.props.billDetailsOrignal)
         tempObj.eway_bill_no = number
         this.props.dashboardAction.updateBillDetail(tempObj)
    }

    handleShipToGSTIN(text) {

        this.setState({ buyerGstin: text });
        let tempObj =  _.cloneDeep(this.props.billDetailsOrignal)
        tempObj.buyer_gstin = text
        this.props.dashboardAction.updateBillDetail(tempObj)
    }

    handleShipFromGSTFrom(text) {

        this.setState({ venderGstin: text });
        let tempObj =  _.cloneDeep(this.props.billDetailsOrignal)
        tempObj.vendor_gstin = text
        this.props.dashboardAction.updateBillDetail(tempObj)
    }

    onSubmitEditing(nextFiled) {
        this.focusNextField(nextFiled);
    }
    setInputReference(name, refs) {
        this.inputs[name] = refs;
    }
    focusNextField(id) {
        this.inputs[id].focus();
    }
    onShipFromPress()
    {
         if(this.props.isEditable === true)
         {
             this.props.dashboardAction.navigateShipFromListContainer()
         }

    }
    onShipToPress()
    {
        if(this.props.isEditable === true)
        {
            this.props.dashboardAction.navigateToShipListContainer()
        }

    }

    render() {
        //gstInfo
        return (
            <GSTInvoiceComponent
                handleIGSTAmountChange={this.handleEayWayBillNumber}
                onSubmitEditing={this.onSubmitEditing}
                setInputReference={this.setInputReference}
                onShipFromPress={this.onShipFromPress}
                onShipToPress={this.onShipToPress}
                focusNextField={this.focusNextField}
                handleShipToGSTIN={this.handleShipToGSTIN}
                handleShipFromGSTFrom={this.handleShipFromGSTFrom}
                totalGSTAmount={this.props.billDetails.gst.gst}
                cgstAmount={this.props.billDetails.gst.cgst}
                sgstAmount={this.props.billDetails.gst.sgst}
                igstAmount={this.props.billDetails.gst.igst}
                isEditable={this.props.isEditable}
                billId={this.props.billDetails.billId}
                shipFrom={this.props.shipFromObject.name?this.props.shipFromObject.name:this.props.billDetails.gstInfo.shipFromName}
                shipTo={this.props.shipToObject.name?this.props.shipToObject.name:this.props.billDetails.gstInfo.ShipToName}
                buyerGstin={this.state.buyerGstin}
                venderGstin={this.state.venderGstin}
                eWayBill={this.state.eWayBill}
                />
        );
    }
}

GSTInvoiceContainer.propTypes = {
    navigation: PropTypes.object,
    dashboardAction: PropTypes.shape({
        goBack: PropTypes.func,
        navigateToShipListContainer:PropTypes.func,
        navigateShipFromListContainer:PropTypes.func,
        updateBillDetail:PropTypes.func
    }),

};

const mapStateToProps = state => ({
    billDetails:getBillDetails(state),
    isEditable:state.dashboard.isEditable,
    shipFromObject:state.dashboard.shipFromObject,
    shipToObject:state.dashboard.shipToObject,
    billDetailsOrignal:state.dashboard.billDetails,
});

const mapDispatchToProps = dispatch => ({
    dashboardAction: bindActionCreators(dashboardActionCreator, dispatch),
});


export default connect(mapStateToProps, mapDispatchToProps)(GSTInvoiceContainer);