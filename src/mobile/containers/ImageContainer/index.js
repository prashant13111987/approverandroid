
import React, { Component } from 'react';
import {Alert, Keyboard, Image, Dimensions, Platform, Linking, View, Text, StyleSheet} from 'react-native';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import logo from '../../../common/images/logo.png'
import Modal from "react-native-modal";
import {normalizeFont} from "../../../common/util";
import VenderListContainer from '../../containers/VenderListContainer'
import Button from '../../components/CustomComponent/Button'


const { width } = Dimensions.get('window');
class INVoiceContainer extends Component {

    static navigationOptions = ({ navigation }) => ({
        headerTitle: (
            <Image
                source={logo}
                style={{
                    width: 30,
                    height: 30,
                    alignSelf: 'center',
                    marginLeft: Platform.OS === 'ios' ? 0 : (width / 2) - 75,
                }}
            />
        ),
        headerStyle: {
            backgroundColor: '#0c0302',
            borderBottomWidth: 0,
            shadowOpacity: 0,
            shadowOffset: {
                height: 0,
            },
            shadowRadius: 0,
            elevation: 0,
        },
        headerTitleStyle: { alignSelf: 'center' },
    });

    constructor(props) {
        super(props);
    }


    render() {
        return (
            <View>
                {this.props.isModalVisible? <View  style={{ width:'50%' }}>
                    <Modal isVisible={this.props.isModalVisible}>
                        <View style={styles.modalContent}>
                            <VenderListContainer/>
                            <View style={{height:58,width:'100%', backgroundColor:'lightgray',borderBottomLeftRadius:10,borderBottomRightRadius:10,flexDirection: 'row', padding: 10, justifyContent: 'space-between'}}>
                                    <Button text="PROCESS" style={{width:'45%',backgroundColor: "white",}} onPress={() => this.props.onProccedPress()}/>
                                    <Button text="CANCEL" style={{width:'45%',backgroundColor: "white"}} onPress={()=>this.props.onCancelPress()} />
                            </View>
                        </View>

                    </Modal>
                </View>:<View/>}
            <View style={{width:'100%',height:'100%',justifyContent:'center', alignItems:'center'}}>
                <Image style={{width: '100%', height: '100%'}}   resizeMode={'contain'}  source={this.props.imagePath} />
            </View>
            </View>
        );
    }
}

INVoiceContainer.propTypes = {
    navigation: PropTypes.object,
};

const mapStateToProps = () => ({
});

const mapDispatchToProps = dispatch => ({
});

const styles = StyleSheet.create({
    modalContent: {
        backgroundColor:'lightgray',
        padding: 0,
        justifyContent: "center",
        alignItems: "center",
        borderRadius: 4,
        alignSelf:'center',
        borderColor: "rgba(0, 0, 0, 0.1)",
        height:'50%',
        width:'90%',
        //position:'absolute',
        flexDirection:'column'
        //marginLeft:20
    },
});




export default connect(mapStateToProps, mapDispatchToProps)(INVoiceContainer);
