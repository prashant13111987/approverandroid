
import React, { Component } from 'react';
import { Alert, Keyboard, Image, Dimensions, Platform, Linking, View, Text, TouchableOpacity, Animated, Button } from 'react-native';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import InVoiceListComponent from '../../components/InVoiceListComponent/Approver';
import WorkFlowComponent from '../../components/InVoiceListComponent/WorkFlow';
import ImageContainer from '../../containers/ImageContainer';
import logo from '../../../common/images/logo.png'
import email from '../../../common/images/email.png'
import setting from '../../../common/images/speech-bubble.png'
import backButton from '../../../common/images/back.png'
import RoundedButton from '../../components/CustomComponent/RoundedComponent';
//import ImagePicker from 'react-native-image-crop-picker';
// import ImageCropper from "react-native-android-image-cropper";
import * as dashboardActionCreator from "../../../common/actions/dashboard/approverAction";
import * as authActionCreator from "../../../common/actions/login";
import { getApproverReceiveObject } from "../../../common/selector/dashboard";
import VenderListContainer from '../../containers/VenderListContainer'
import { getApproverStatusForLagent, getApproverBillDetails, getApproverlegendObject } from '../../../common/selector/InVoiceList';

// import ImageCropper from "react-native-android-image-cropper";
var RNFS = require('react-native-fs');
import Modal from "react-native-modal";


const { width, height } = Dimensions.get('window');
var options = {
    guideLines: "on-touch",
    cropShape: "rectangle",
    title: 'MY EXAMPLE',
    cropMenuCropButtonTitle: 'Done'
}

var imageObject = {}
var loginId = null

class InApproverVoiceListViewContainer extends Component {

    static navigationOptions = ({ navigation }) => ({

        headerTitle: (
            <View style={{ flex: 1 }}>
                <Text
                    style={{
                        alignSelf: 'center',
                        color: 'white',
                        fontSize: 16,
                        fontWeight: '700'
                    }}>
                    Setlmint
                </Text>
            </View>
        ),
        headerLeft: (
            <TouchableOpacity onPress={() => navigation.state.params.handleBack()}>
                <Image source={backButton} style={{ height: 20, width: 20, marginLeft: 16 }} />
            </TouchableOpacity>
        ),
        headerRight: (
            <View style={{ flexDirection: 'row', paddingRight: 10, justifyContent: 'space-between', flex: 1 }}>
                <TouchableOpacity onPress={() => navigation.state.params.chatPress()} source={setting} >
                    <Image
                        style={{ width: 20, height: 20 }}
                        source={setting}
                    />
                </TouchableOpacity>
            </View>
        ),
        headerStyle: {
            backgroundColor: '#629ee4',
            borderBottomWidth: 0,
            shadowOpacity: 0,
            shadowOffset: {
                height: 0,
            },
            shadowRadius: 0,
            elevation: 0,
        },
        headerTitleStyle: { alignSelf: 'center' },
    });

    constructor(props) {
        super(props);
        this.onBackPress = this.onBackPress.bind(this);
        this.onChatPress = this.onChatPress.bind(this);

        this.state = {
            expanded: true,
            animation: new Animated.Value(),
            animationSlide: new Animated.ValueXY(0),
            maxHeight: 0,
            minHeight: 0,

            selectedInvoice: this.props.defaultInvoiceStatus,
            image: {
                uri: '',
                width: '',
                height: '',
                mime: ''
            },
            username: '',
            password: '',
            isImage: false,
            path: '',
            data: this.props.statusBillObject.invoices,
            widthValue: 0,
            // isModalVisible: true,
            imagePath: '',
            imageObject: {},
            isModalVisible: false,
            activeIndex: null,
            modalType: 'approve',
            note: ''
        };
        this.onInvoiceListLegendSelect = this.onInvoiceListLegendSelect.bind(this)
        this.findInvoiceLEgendViewdimesions = this.findInvoiceLEgendViewdimesions.bind(this)
        this.toggleNew = this.toggleNew.bind(this);
        this.setMaxHeightNew = this.setMaxHeightNew.bind(this);
        this.setMinHeightNew = this.setMinHeightNew.bind(this);
        this.onCancelPress = this.onCancelPress.bind(this);
        this.onProccedPress = this.onProccedPress.bind(this);
        this.onCreatePress = this.onCreatePress.bind(this);
        this.onEditPress = this.onEditPress.bind(this);
        this.onViewPress = this.onViewPress.bind(this);

    }

    componentWillReceiveProps(nextProps) {
        const value = nextProps.defaultInvoiceStatus.data
        let filterData = []
        if (value === "Invoices") {
            filterData = this.props.statusBillObject.invoices
        } else if (value === "Bills") {
            filterData = this.props.statusBillObject.bills
        }
        else if (value === "Payments") {
            filterData = this.props.statusBillObject.payments
        }
        else if (value === "PO") {
            filterData = this.props.statusBillObject.PO
        }
        else if (value === "Master Data") {
            filterData = this.props.statusBillObject.masterData
        }
        else if (value === "Approved") {
            filterData = this.props.statusBillObject.approval
        }
        this.setState({
            selectedInvoice: value,
            data: filterData
        })
    }

    componentDidMount() {
        this.props.navigation.setParams({
            handleBack: this.onBackPress,
            chatPress: this.onChatPress,
        });
        // this.props.dashboardAction.venderListRequest()
        // getUserToken().then((tokenObject) => {
        //     console.log("Anuth Otokrm :::::::::::::::::",tokenObject.loginId);
        // });
    }
    onChatPress() {
        // this.setState({ isModalVisible: !this.state.isModalVisible });
    }

    onCancelPress = () => {
        this.setState({ isModalVisible: !this.state.isModalVisible });
    }

    onShowModalPress = (id, type) => {
        this.setState({ isModalVisible: true, activeIndex: id, modalType: type });
    }

    onProccedPress = () => {
        this.setState({ isModalVisible: !this.state.isModalVisible });
        // imageObject={
        //     path:this.state.imagePath
        // }
        this.captureImageObject = {
            filename: this.state.imageObject.filename,
            size: this.state.imageObject.size,
            mime: this.state.imageObject.mime,
            path: this.state.imagePath
        }

        console.log("Capture Object Name ::::::::::", this.captureImageObject)

        this.props.dashboardAction.uploadImageRequest(this.captureImageObject)
    }

    onBackPress() {
        const legendObject = {
            isApprove: true,
            isReceived: true,
            isScanned: true,
            isPending: true,
            isRejected: true,
            isOnhold: true,
        }
        this.props.dashboardAction.showFilterData(false);
        // this.props.dashboardAction.setLegendObject(legendObject);
        this.props.dashboardAction.goBack();
    }

    onInvoiceListLegendSelect(value) {
        console.log('value', value)
        this.props.dashboardAction.showFilterData(false);
        let filterData = []
        if (value === "Invoices") {
            filterData = this.props.statusBillObject.invoices
        } else if (value === "Bills") {
            filterData = this.props.statusBillObject.bills
        }
        else if (value === "Payments") {
            filterData = this.props.statusBillObject.payments
        }
        else if (value === "PO") {
            filterData = this.props.statusBillObject.PO
        }
        else if (value === "Master Data") {
            filterData = this.props.statusBillObject.masterData
        }
        else if (value === "Approved") {
            filterData = this.props.statusBillObject.approval
        }
        this.props.dashboardAction.selectedInvoiceStatus(value)
        this.setState({
            selectedInvoice: value,
            data: filterData
        })
    }

    onRowPress = (item) => {

        // this.props.dashboardAction.setSelectedBillObject(item)
        this.props.dashboardAction.invoiceDetailRequest(item)
        this.props.loginAction.navigateToValidateApproverScreen(item)
    }

    onCreatePress = (item) => {
        console.log("On Create Press ", item)
        if (item.source_name == "s_invoice") {
            if (item.vendor_id == "") {
                this.props.dashboardAction.navigateToVender(item)
            } else {
                this.props.dashboardAction.S_Invoice_APIRequest(item)
            }

        } else {
            console.log(" Create OCR API", item)
            if (item.vendor_id == "") {
                this.props.dashboardAction.navigateToVender(item)
            }
            else {
                const ocrImagesObject = {
                    source_name: item.source_name,
                    source_id: item.id,
                    vendor_id: item.vendor_id,
                }
                const ocr = {
                    ocr_image: ocrImagesObject
                }
                this.props.dashboardAction.email_Uploaded_Invoice_APIRequest(ocr)
            }
        }
        this.props.dashboardAction.setSelectedBillObject(item)
        // this.props.loginAction.navigateToValidateScreen()
    }
    onEditPress = (item) => {

        this.props.loginAction.navigateToValidateScreen()
        console.log("On Edit Press ")
        item.operationname = "EDIT"
        this.props.dashboardAction.setInvoiceEditableOrNot(true)
        this.props.dashboardAction.setSelectedBillObject(item)

    }
    onViewPress = (index) => {
        const { state: { activeIndex, data }, props: { dashboardAction } } = this;
        const request = {
            invoice_id: data[activeIndex].id,
            workflow: {
                status: 'hold',
                rush: false,
                note: ''
            }
        }
        dashboardAction.workFlow('/invoice_workflows', request)
        this.setState({
            isModalVisible: false
        })
    }

    findInvoiceLEgendViewdimesions = (layout) => {
        const { x, y, width, height } = layout;
        this.setState({
            widthValue: width
        })
    }

    toggleNew() {
        //Step 1
        let initialValue = this.state.expanded ? this.state.maxHeight + this.state.minHeight : this.state.minHeight,
            finalValue = this.state.expanded ? this.state.minHeight + 280 : this.state.maxHeight + this.state.minHeight;

        this.setState({
            expanded: !this.state.expanded  //Step 2
        });

        this.state.animation.setValue(initialValue);  //Step 3
        Animated.spring(     //Step 4
            this.state.animation,
            {
                toValue: finalValue
            }
        ).start();  //Step 5
    }

    setMaxHeightNew(event) {
        this.setState({
            maxHeight: event.nativeEvent.layout.height
        });
    }

    setMinHeightNew(event) {
        this.setState({
            minHeight: event.nativeEvent.layout.height
        });
    }

    onOpenGallery = () => {
        // ImageCropper.selectImage(options, (response) => {
        //     //error throwns with response.error
        //     if (response && response.uri) {
        //         console.log("Response Value :::::::::::::::", response)
        //         RNFS.stat(response.uri).then(result => {
        //             // console.log("Result LLLLL", result)
        //             RNFS.readFile(response.uri, 'base64').then(b64data => {
        //                 // console.log("b64data ", b64data);
        //                 this.image = {
        //                     filename: result.ctime,
        //                     size: result.size,
        //                     mime: 'jpg',
        //                 }
        //                 this.setState({
        //                     image: { uri: response.uri, width: 500, height: 500, mime: 'jpg' },
        //                     imagePath: b64data,
        //                     imageObject: this.image
        //                 })
        //
        //                 this.captureImageObject = {
        //                     filename: this.state.imageObject.filename,
        //                     size: this.state.imageObject.size,
        //                     mime: this.state.imageObject.mime,
        //                     path: this.state.imagePath
        //                 }
        //                 this.props.dashboardAction.uploadImageRequest(this.captureImageObject)
        //
        //             });
        //         })
        //     }
        // })
    }


    onOpenCamera = () => {
        // ImageCropper.selectImage(options, (response) => {
        //     //error throwns with response.error
        //     if (response && response.uri) {
        //         console.log("Response Value :::::::::::::::", response)
        //         RNFS.stat(response.uri).then(result => {
        //             // console.log("Result LLLLL", result)
        //             RNFS.readFile(response.uri, 'base64').then(b64data => {
        //                 // console.log("b64data ", b64data);
        //                 this.image = {
        //                     filename: result.ctime,
        //                     size: result.size,
        //                     mime: 'jpg',
        //                 }
        //                 this.setState({
        //                     image: { uri: response.uri, width: 500, height: 500, mime: 'jpg' },
        //                     imagePath: b64data,
        //                     imageObject: this.image
        //                 })
        //
        //                 this.captureImageObject = {
        //                     filename: this.state.imageObject.filename,
        //                     size: this.state.imageObject.size,
        //                     mime: this.state.imageObject.mime,
        //                     path: this.state.imagePath
        //                 }
        //                 this.props.dashboardAction.uploadImageRequest(this.captureImageObject)
        //
        //             });
        //         })
        //     }
        // })
    }

    onConfirm = () => {
        const { state: { activeIndex, data, modalType, note }, props: { dashboardAction, defaultInvoiceStatus } } = this;

        dashboardAction.workFlow(defaultInvoiceStatus.data, data[activeIndex], note, modalType)
        this.setState({
            isModalVisible: false,
            activeIndex: null,
            modalType: null,
            note: ''
        })
    }

    hideModal = () => {
        this.setState({
            activeIndex: null,
            modalType: null,
            note: '',
            isModalVisible: false
        })
    }
    onNoteChange = note => {
        this.setState({
            note
        })
    }


    render() {
        const { onHoldPress, onConfirm, hideModal, onNoteChange, state: { isModalVisible, activeIndex, modalType, note } } = this
        let modalHeaderText = 'Approve'
        let showNote = false;
        if (modalType === 'hold') {
            showNote = true
            modalHeaderText = 'On Hold'
        } else if (modalType === 'reject') {
            showNote = true
            modalHeaderText = 'Reject'
        } else if (modalType === 'cancel') {
            showNote = true
            modalHeaderText = 'Cancel'
        } else if (modalType === 'submitToVendor') {
            showNote = true
            modalHeaderText = 'SubmitToVendor'
        }
        const currentElement = this.state.data[activeIndex || 0] || {}
        const name = currentElement.party_name || currentElement.bank_name || currentElement.name || 'NA'
        const start = currentElement.created_at
        const end = currentElement.created_at
        const total = currentElement.total
        return (
            <View style={{ flex: 1 }}>
                <Modal isVisible={isModalVisible}>
                    <View style={{ flex: 1, justifyContent: 'center' }}>
                        <WorkFlowComponent
                            modalHeaderText={modalHeaderText}
                            name={name}
                            start={start}
                            end={end}
                            total={total}
                            confirm={onConfirm}
                            cancel={hideModal}
                            showNote={showNote}
                            note={note}
                            onNoteChange={onNoteChange}
                        />

                    </View>
                </Modal>
                <InVoiceListComponent
                    data={this.props.showFilterData ? this.props.chartLegendObjectsData : this.state.data}
                    onRowPress={this.onRowPress}
                    onCreatePress={this.onCreatePress}
                    onEditPress={this.onEditPress}
                    onShowModalPress={this.onShowModalPress}
                    venderList={this.props.venderList}
                    loginSuccess={this.props.loginSuccess}
                    keyName="state_list"
                    selectedInvoice={this.props.defaultInvoiceStatus}
                    defaultInvoiceStatus={this.props.defaultInvoiceStatus}
                    onInvoiceListLegendSelect={this.onInvoiceListLegendSelect}
                    statusBillObject={this.props.statusBillObject}
                    receiveCount={this.props.receiveCount}
                    legentStatus={this.props.legentStatus}
                    showFilterData={this.props.showFilterData}
                    findInvoiceLEgendViewdimesions={this.findInvoiceLEgendViewdimesions}
                    widthValue={this.state.widthValue}
                    setMinHeightNew={this.setMinHeightNew}
                    setMaxHeightNew={this.setMaxHeightNew}
                    toggleNew={this.toggleNew}
                    animation={this.state.animation}
                    animationSlide={this.state.animationSlide}
                    expanded={this.state.expanded}
                    onOpenGallery={this.onOpenGallery}
                    onOpenCamera={this.onOpenCamera}
                    onHoldPress={onHoldPress}
                />

            </View>
        );
    }
}

InApproverVoiceListViewContainer.propTypes = {
    navigation: PropTypes.object,
    dashboardAction: PropTypes.shape({
        navigateToVoiceList: PropTypes.func,
        receivedInvoicesRequest: PropTypes.func,
        receivedALLInvoiceRequest: PropTypes.func,
        goBack: PropTypes.func,
        showFilterData: PropTypes.func,
        setLegendObject: PropTypes.func,
        uploadImageRequest: PropTypes.func,
        billDetailRequest: PropTypes.func,
        setSelectedBillObject: PropTypes.func,
        navigateToVender: PropTypes.func,
        email_Uploaded_Invoice_APIRequest: PropTypes.func,
        setInvoiceEditableOrNot: PropTypes.func
    }),
    loginAction: PropTypes.shape({
        navigateToValidateScreen: PropTypes.func,
    }),

};

const mapStateToProps = state => ({
    //navigation: state.nav,
    loading: state.app.loading,
    netState: state.deviceInfo.netState,
    isNetworkStateDetected: state.deviceInfo.isNetworkStateDetected,
    receivedInvoiceList: state.approverDashboard.receivedInvoiceList,
    receivedFromEmailList: state.approverDashboard.receivedFromEmailList,
    receivedFromPhoneList: state.approverDashboard.receivedFromPhoneList,
    receivedFromAppList: state.approverDashboard.receivedFromAppList,
    receivedAllInvoiceList: state.approverDashboard.receivedAllInvoiceList,
    legendObject: state.approverDashboard.legendObject,
    receiveCount: state.approverDashboard.receiveCount,
    statusBillObject: getApproverReceiveObject(state),
    chartLegendObjectsData: getApproverlegendObject(state),
    legentStatus: getApproverStatusForLagent(state),
    billDetails: getApproverBillDetails(state),
    showFilterData: state.approverDashboard.showFilterData,
    defaultInvoiceStatus: state.approverDashboard.selectedInvoice,
    venderList: state.approverDashboard.venderList,
    loginSuccess: state.login.loginSuccess,

});

const mapDispatchToProps = dispatch => ({
    loginAction: bindActionCreators(authActionCreator, dispatch),
    dashboardAction: bindActionCreators(dashboardActionCreator, dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps)(InApproverVoiceListViewContainer);
