
import React, { Component } from 'react';
import { Alert, Keyboard, Image, Dimensions, Platform, Linking, View ,Text,TouchableOpacity} from 'react-native';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import ItemsInvoiceComponent from '../../components/ItemsInvoiceComponent';
import {getBillDetails} from "../../../common/selector/InVoiceList";
import * as dashboardActionCreator from "../../../common/actions/dashboard";


const { width } = Dimensions.get('window');
let selectedIndex = 0
class ItemsInvoiceContainer extends Component {

    constructor(props) {
        super(props);
        this.state = {
            activeSlide:0
        };
        this.addItemPress = this.addItemPress.bind(this);
        this.inputs = {};
        this.focusNextField = this.focusNextField.bind(this);
        this.onSubmitEditing = this.onSubmitEditing.bind(this);
        this.setInputReference = this.setInputReference.bind(this);
        this.editItemPress = this.editItemPress.bind(this);
        this.onIndexPress = this.onIndexPress.bind(this);

       }


    onSubmitEditing(nextFiled) {
        this.focusNextField(nextFiled);
    }
    setInputReference(name, refs) {
        // var y = this.inputs.hasOwnProperty(name);
        //     if(!y) {
        //
        //         this.inputs[name] = refs
        //     }
        //console.log("input ref value is :::::",this.inputs)
        this.inputs[name] = refs
    }

    focusNextField(id) {

        this.inputs[id].focus();
    }

    addItemPress=()=>{
        if(this.props.isEditable === true){
            this.props.dashboardAction.navigateToAddItemContainer()
        }

    }
    editItemPress=(index)=>{
        if(this.props.isEditable === true){
            this.props.dashboardAction.navigateToEditItemContainer()
        }
        // console.log("Selected Index ::::::::::::::::::::::::::::::::::",index)

    }

    onIndexPress=(index)=>{

        selectedIndex = index
       // selectedItemIndexObject
        this.props.dashboardAction.selectedItemIndexObject(index)
       // console.log("Selected Index onIndexPress ::::::::::::::::::::::::::::::::::",this.props.billDetails.items[index])
    }


    getActiveSlide=(index)=>{
            console.log(index)
           this.setState({
               activeSlide:index
           })
       }
       onScroll=(index)=>{
            console.log(index)
       }

    render() {
        return (
            <ItemsInvoiceComponent
                activeSlide = {this.state.activeSlide}
                getActiveSlide={this.getActiveSlide}
                onScroll={this.onScroll}
                addItemPress={this.addItemPress}
                editItemPress={this.editItemPress}
                onIndexPress={this.onIndexPress}
                billDetails={this.props.billDetails}
                isEditable={this.props.isEditable}
                onSubmitEditing={this.onSubmitEditing}
                setInputReference={this.setInputReference}
                focusNextField={this.focusNextField}
            />
        );
    }
}

ItemsInvoiceContainer.propTypes = {
    navigation: PropTypes.object,
    dashboardAction: PropTypes.shape({
        navigateToAddItemContainer:PropTypes.func,
        navigateToEditItemContainer:PropTypes.func,
        goBack: PropTypes.func,
        selectedItemIndexObject:PropTypes.func
    }),
};

const mapStateToProps = state => ({
    billDetails:getBillDetails(state),
    isEditable:state.dashboard.isEditable,
});

const mapDispatchToProps = dispatch => ({
    dashboardAction: bindActionCreators(dashboardActionCreator, dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps)(ItemsInvoiceContainer);