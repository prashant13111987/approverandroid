
import React, { Component } from 'react';
import { Alert, Keyboard, Image, Dimensions, Platform, Linking, View, Text } from 'react-native';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as authActionCreator from '../../../common/actions/login';
import * as dashboardActionCreator from '../../../common/actions/dashboard';
import LoginComponent from '../../components/LoginComponent';

//invoice2go-icon.png

const { width } = Dimensions.get('window');
class LoginContainer extends Component {
    static navigationOptions = {
        header: null,
    }
    constructor(props) {
        super(props);
        this.state = {
            username: '',
            password: '',
        };
        this.onPressLogin = this.onPressLogin.bind(this);
        this.handlePasswordChange = this.handlePasswordChange.bind(this);
        this.handleUsernameChange = this.handleUsernameChange.bind(this);
        this.inputs = {};
        this.focusNextField = this.focusNextField.bind(this);
        this.onSubmitEditing = this.onSubmitEditing.bind(this);
        this.setInputReference = this.setInputReference.bind(this);
    }

    onSubmitEditing(nextFiled) {
        this.focusNextField(nextFiled);
    }
    setInputReference(name, refs) {
        this.inputs[name] = refs;
    }
    focusNextField(id) {
        this.inputs[id].focus();
    }
    onPressLogin() {
        // Keyboard.dismiss();
        // if (!this.state.username) {
        //     Alert.alert('Error', 'Username required!');
        //     return;
        // }
        // if (!this.state.password) {
        //     Alert.alert('Error', 'Password required!');
        //     return;
        // }
         this.props.loginAction.loginAPIRequest(this.state.username, this.state.password);

        // this.props.loginAction.loginAPIRequest("Vish@setlmint.com", "setlmint");
        // this.props.loginAction.loginAPIRequest("vishnu@setlmint.com", "setlmint");
        // this.props.loginAction.loginAPIRequest("chandug@setlmint.com", "setlmint");
        // this.props.loginAction.loginAPIRequest(this.state.username, this.state.password);
        // this.props.loginAction.loginAPIRequest("test@setlmint.com", "setlmint123");
        // this.props.loginAction.loginAPIRequest("Raghav@setlmint.com", "setlmint");
        // this.props.loginAction.loginAPIRequest("kunal@setlmint.com", "keshavpapa");
        // this.props.loginAction.loginAPIRequest("omkar@gmail.com", "setlmint");
        //  this.props.loginAction.loginAPIRequest("omkar@gmail.com", "setlmint");
        //  this.props.loginAction.loginAPIRequest("raghav@setlmint.com", "setlmint");
        //  this.props.loginAction.navigateToVoiceList();
        // this.props.dashboardAction.navigateToDashboard();
    }

    handlePasswordChange(text) {
        this.setState({ password: text });
    }

    handleUsernameChange(text) {
        this.setState({ username: text });
    }

    render() {
        return (
            <LoginComponent
                username={this.state.username}
                password={this.state.password}
                onPressLogin={this.onPressLogin}
                handlePasswordChange={this.handlePasswordChange}
                handleUsernameChange={this.handleUsernameChange}
                username={this.state.username}
                password={this.state.password}
                onSubmitEditing={this.onSubmitEditing}
                setInputReference={this.setInputReference}
                focusNextField={this.focusNextField}
            />
        );
    }
}

LoginContainer.propTypes = {
    navigation: PropTypes.object,
    loginAction: PropTypes.shape({
        navigateToVoiceList: PropTypes.func,
        loginAPIRequest: PropTypes.func,
    }),
    dashboardAction: PropTypes.shape({
        navigateToDashboard: PropTypes.func,
    }),
};

const mapStateToProps = () => ({
});

const mapDispatchToProps = dispatch => ({
    loginAction: bindActionCreators(authActionCreator, dispatch),
    dashboardAction: bindActionCreators(dashboardActionCreator, dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps)(LoginContainer);