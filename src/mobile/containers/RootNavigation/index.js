import { StackNavigator, NavigationActions,DrawerNavigator } from 'react-navigation';
import getSlideFromRightTransition from 'react-navigation-slide-from-right-transition';
import { Dimensions } from 'react-native';
import LoginContainer from '../LoginContainer';
import SplashContainer from '../SplashContainer';
import InVoiceListContainer from '../InVoiceListViewContainer'
import ApproverInVoiceListContainer from '../InVoiceListViewContainer/InApproverVoiceListViewContainer';
import DashboardContainer from '../DashboardContainer'
import DrawerContainer from '../DrawerContainer'
import ValidateInvoiceContainer from '../ValidateInvoiceContainer'
import VenderListContainer from '../VenderListContainer'
import ShipToListContainer from '../ShipToAddressListContainer'
import ShipFromListContainer from '../ShipFromListContainer'
import AddItemContainer from '../AddItemContainer'
import EditItemContainer from '../EditItemContainer'
import ItemNameListContainer from '../ItemNameListConatiner'

import SendForApprovalContainer from '../SendForApprovalContainer'
import SuccessScreenContainer from '../SuccessScreenContainer'
import DistributionListContainer from '../DistributionListContainer'
import ValidateApproverInvoiceContainer from '../ValidateApproverInvoiceContainer'
var { height, width } = Dimensions.get('window');

const Drawer = DrawerNavigator(
    {
        Dashboard: {
            screen: DashboardContainer,
        },
        InVoiceListContainer: { screen: InVoiceListContainer },
    },
    {
        headerMode: 'none',
        contentComponent: DrawerContainer,
        drawerWidth: width*.8,
    },
);


const AppNavigator = StackNavigator(
    {
        Splash: { screen: SplashContainer },
        Login: { screen: LoginContainer },
        InVoiceListContainer: { screen: InVoiceListContainer },
        ApproverInVoiceListContainer: { screen: ApproverInVoiceListContainer },
        DashboardContainer: { screen: DashboardContainer },
        ValidateInvoiceContainer: { screen: ValidateInvoiceContainer },
        ValidateApproverInvoiceContainer: { screen: ValidateApproverInvoiceContainer },
        VenderListContainer:{screen:VenderListContainer},
        Drawer: { screen: Drawer },
        SendForApprovalContainer: { screen: SendForApprovalContainer },
        AddItemContainer : {screen:AddItemContainer},
        ShipToListContainer:{screen:ShipToListContainer},
        ShipFromListContainer:{screen:ShipFromListContainer},
        ItemNameListContainer:{screen:ItemNameListContainer},
        EditItemContainer:{screen:EditItemContainer},

        SuccessScreenContainer: { screen: SuccessScreenContainer },
        DistributionListContainer: { screen: DistributionListContainer },
        BillPaymentDetails:{screen:ValidateApproverInvoiceContainer}
    },

    {
        transitionConfig: getSlideFromRightTransition,
    },
);
export default AppNavigator;
