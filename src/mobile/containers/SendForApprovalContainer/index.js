
import React, { Component } from 'react';
import { Alert, Keyboard, Image, Dimensions, Platform, Linking, View ,Text,TouchableOpacity} from 'react-native';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import SendForApprovalComponent from '../../components/SendForApprovalComponent';
import {getBillDetails} from "../../../common/selector/InVoiceList";
import {bindActionCreators} from "redux";
import backButton from '../../../common/images/back.png'
import * as dashboardActionCreator from "../../../common/actions/dashboard";
import _ from "lodash";


const { width } = Dimensions.get('window');
class SendForApprovalContainer extends Component {

    static navigationOptions = ({ navigation }) => ({
        headerTitle: (
            <View style={{ alignItems:'flex-start',flex:1}}>
                <Text
                    style={{
                        alignSelf: 'center',
                        color:'white',
                        fontSize:16,
                        fontWeight:'700'
                    }}>
                    Approval
                </Text>
            </View>
        ),
        headerStyle: {
            backgroundColor: '#629ee4',
            borderBottomWidth: 0,
            shadowOpacity: 0,
            shadowOffset: {
                height: 0,
            },
            shadowRadius: 0,
            elevation: 0,
        },
        headerLeft: (
            <TouchableOpacity onPress={() => navigation.state.params.handleBack()}>
                <Image source={backButton} style={{ height: 24, width: 24, marginLeft: 12 }} />
            </TouchableOpacity>
        ),
    });

    componentDidMount() {
        this.props.navigation.setParams({
            handleBack: this.onBackPress,
        });
    }

    onBackPress() {
        this.props.dashboardAction.goBack();
    }

    constructor(props) {
        super(props);
        this.state = {
            isRushSelected:this.props.billDetails.rush?this.props.billDetails.rush:false,
            isReceivedSubmitSelected:this.props.billDetails.is_original_received?this.props.billDetails.is_original_received:false,
            selectedDueDate:this.props.billDetails.due_date?this.props.billDetails.due_date:"Due date",
            invoiceTitle:this.props.billDetails.vendor_name?this.props.billDetails.vendor_name:"No name",
            invoiceAmount:'',
            notes:this.props.billDetails.notes?this.props.billDetails.notes:"Enter notes",
            department:''
        };
        this.inputs = {};
        this.focusNextField = this.focusNextField.bind(this);
        this.onSubmitEditing = this.onSubmitEditing.bind(this);
        this.setInputReference = this.setInputReference.bind(this);
        this.onRadioButtonSelect = this.onRadioButtonSelect.bind(this);
        this.onDueDateChange = this.onDueDateChange.bind(this);
        this.onSendForApproval = this.onSendForApproval.bind(this);
        this.onDistributionPress = this.onDistributionPress.bind(this);
        this.handleNotesChange = this.handleNotesChange.bind(this);
        this.onBackPress = this.onBackPress.bind(this);
    }

    onSubmitEditing(nextFiled) {
        this.focusNextField(nextFiled);
    }
    setInputReference(name, refs) {
        this.inputs[name] = refs;
    }
    focusNextField(id) {
        this.inputs[id].focus();
    }

    onRadioButtonSelect=(selectedPriority,status)=>{
        if(selectedPriority === 'rush'){
            if(status){
                this.setState({
                    isRushSelected:false
                })
            }
            else{
                this.setState({
                    isRushSelected:true
                })
            }

        }
        if(selectedPriority === 'receivedSubmit'){
            if(status){
                this.setState({
                    isReceivedSubmitSelected:false
                })
            }
            else{
                this.setState({
                    isReceivedSubmitSelected:true
                })
            }


        }


    }

    onDueDateChange=(selectedDueDate)=>{
        this.setState({
            selectedDueDate
        })
    }

    handleInvoiceAmountChange=(invoiceAmount)=>{
        this.setState({
            invoiceAmount
        })
    }

    handleInvoiceTitleChange=(invoiceTitle)=>{
        this.setState({
            invoiceTitle
        })
    }

    onSendForApproval(){

        let tempObj =  _.cloneDeep(this.props.billDetailsOrignal)
        tempObj.department_id = this.props.departmentObject.id
        tempObj.department_name = this.props.departmentObject.name
        tempObj.notes = this.state.notes
        tempObj.is_original_received = this.state.isReceivedSubmitSelected
        tempObj.rush = this.state.isRushSelected
        tempObj.due_date = this.state.selectedDueDate

        this.props.dashboardAction.sendForApprovalRequest(tempObj)
        //this.props.dashboardAction.successApproval()
    }

    onDistributionPress(){
        this.props.dashboardAction.navigateToDistributionListContainer()
    }
    handleNotesChange(notes){

        this.setState({
            notes
        })
    }

    render() {

        let totalGSTValue = 0;
        let subTotalAmount = 0;
        let finalCalculatedAmount = 0
        _.each(this.props.billDetails.items, (item) => {
            const calculatedAmount = (parseFloat(item.price)*parseFloat(item.quantity)).toFixed(2)
            if(item.discount > 0){
                const discountValue = item.discount?calculatedAmount * ((parseFloat(item.discount))/100.0):'0.0'
                finalCalculatedAmount = calculatedAmount - discountValue
                const gstCal = parseFloat(finalCalculatedAmount) *  ((parseFloat(item.tax))/100.0)
                totalGSTValue = parseFloat(totalGSTValue) + parseFloat(gstCal)
            }else
            {
                const gstCal = parseFloat(calculatedAmount) *  ((parseFloat(item.tax))/100.0)
                totalGSTValue = parseFloat(totalGSTValue) + parseFloat(gstCal)
            }

            if(finalCalculatedAmount){
                subTotalAmount = parseFloat(subTotalAmount) + parseFloat(finalCalculatedAmount)
            }else{
                subTotalAmount = parseFloat(subTotalAmount) + parseFloat(calculatedAmount)
            }
        });

        let grossGSTTotal =  parseFloat(subTotalAmount) + parseFloat(totalGSTValue) + parseFloat(this.props.billDetails.amount.deliverycharges)
            + parseFloat(this.props.billDetails.amount.packagingCharges) + parseFloat(this.props.billDetails.amount.other_charges)



        return (
            <SendForApprovalComponent
                onSubmitEditing={this.onSubmitEditing}
                setInputReference={this.setInputReference}
                focusNextField={this.focusNextField}
                onDueDateChange={this.onDueDateChange}
                onRadioButtonSelect={this.onRadioButtonSelect}
                selectedDueDate={this.state.selectedDueDate}
                invoiceTitle={this.state.invoiceTitle}
                invoiceAmount={grossGSTTotal}
                isReceivedSubmitSelected={this.state.isReceivedSubmitSelected}
                isRushSelected={this.state.isRushSelected}
                handleInvoiceAmountChange={this.handleInvoiceAmountChange}
                handleInvoiceTitleChange={this.handleInvoiceTitleChange}
                onSendForApproval={this.onSendForApproval}
                onDistributionPress={this.onDistributionPress}
                handleNotesChange={this.handleNotesChange}
                notes={this.state.notes}
                department={this.props.departmentObject?this.props.departmentObject.name:"Department name"}
            />
        );
    }
}

SendForApprovalContainer.propTypes = {
    navigation: PropTypes.object,
    dashboardAction: PropTypes.shape({
        successApproval: PropTypes.func,
        navigateToDistributionListContainer: PropTypes.func,
        addItem_APIRequest:PropTypes.func,
        sendForApprovalRequest:PropTypes.func
    }),
};

const mapStateToProps = state => ({
    billDetails:getBillDetails(state),
    departmentObject:state.dashboard.departmentObject,
    billDetailsOrignal:state.dashboard.billDetails,
});

const mapDispatchToProps = dispatch => ({
    dashboardAction: bindActionCreators(dashboardActionCreator, dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps)(SendForApprovalContainer);