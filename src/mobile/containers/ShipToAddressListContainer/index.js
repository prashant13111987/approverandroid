
import React, { Component } from 'react';
import {Alert, Keyboard, Image, Dimensions, Platform, Linking, View, Text, StyleSheet,TouchableOpacity} from 'react-native';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import {normalizeFont} from "../../../common/util";
import CommanListComponent from '../../components/CommanListComponent'
import backButton from '../../../common/images/back.png'
import * as dashboardActionCreator from "../../../common/actions/dashboard";
import * as authActionCreator from "../../../common/actions/login";

const { width } = Dimensions.get('window');

class ShipToListContainer extends Component {


    static navigationOptions = ({ navigation }) => ({
        headerTitle: (
            <View style={{ alignItems:'flex-start',flex:1}}>
                <Text
                    style={{
                        alignSelf: 'center',
                        color:'white',
                        fontSize:16,
                        fontWeight:'700'
                    }}>
                    Validate
                </Text>
            </View>
        ),
        headerStyle: {
            backgroundColor: '#629ee4',
            borderBottomWidth: 0,
            shadowOpacity: 0,
            shadowOffset: {
                height: 0,
            },
            shadowRadius: 0,
            elevation: 0,
        },
        headerLeft: (
            <TouchableOpacity onPress={() => navigation.state.params.handleBack()}>
                <Image source={backButton} style={{ height: 24, width: 24, marginLeft: 12 }} />
            </TouchableOpacity>
        ),
    });


    constructor(props) {
        super(props);

        this.state = {
            searchText: '',
            selectedText:'',
        };
        this.searchFilterFunction = this.searchFilterFunction.bind(this);
        this.onRowPress = this.onRowPress.bind(this);
        this.onBackPress = this.onBackPress.bind(this);
    }

    componentDidMount() {
        this.props.navigation.setParams({
            handleBack: this.onBackPress,
        });
    }

    onBackPress() {
        this.props.dashboardAction.goBack();
    }

    // On search filter for state.
    searchFilterFunction(text) {
        this.setState({
            searchText: text,
        });
    }

    onRowPress(item) {

        console.log("To Object l ",item)
        console.log("Bill Details Orignal ",this.props.billDetailsOrignal)
       // name
        // shipFromName:billDetail.shipping_branch_name,
        //     ShipToName:billDetail.vendor_branch_name,
        // const finalRegisterObject = Object.assign(districtObject, districtCustomObje);
        this.props.dashboardAction.shipToListObject(item)
        this.props.billDetailsOrignal.shipping_branch_name = item.name
        this.props.dashboardAction.billDetailSuccess(this.props.billDetailsOrignal)
        this.props.dashboardAction.goBack();
    }

    render() {

        return (
            <View style={{width:'100%',height:'100%',borderBottomLeftRadius:10,borderBottomRightRadius:10}} >
                <CommanListComponent
                    data={this.props.merchantBranches}
                    onRowPress={this.onRowPress}
                    keyName="shipTo_list"
                    searchFilterFunction={this.searchFilterFunction}
                    searchText={this.state.searchText}
                    selectedText={this.state.selectedText}
                />
            </View>
        );
    }
}


const styles = StyleSheet.create({
    modalContent: {
        padding: 22,
        justifyContent: "center",
        alignItems: "center",
        borderRadius: 4,
        borderColor: "rgba(0, 0, 0, 0.1)",
        height:'40%',
        width:'60%',
    },
});

ShipToListContainer.propTypes = {
    navigation: PropTypes.object,
    dashboardAction: PropTypes.shape({
        venderListRequest:PropTypes.func,
        goBack: PropTypes.func,
        email_Uploaded_Invoice_APIRequest:PropTypes.func,
        S_Invoice_APIRequest:PropTypes.func,
        billDetailSuccess:PropTypes.func,
        shipToListObject:PropTypes.func
    }),
    loginAction: PropTypes.shape({
        navigateToValidateScreen: PropTypes.func,
    }),
};


const mapStateToProps = state => ({
    loading: state.app.loading,
    venderList: state.dashboard.venderList,
    billDetailsOrignal:state.dashboard.billDetails,
    merchantBranches:state.dashboard.merchantBranches,
});

const mapDispatchToProps = dispatch => ({
    dashboardAction: bindActionCreators(dashboardActionCreator, dispatch),
    loginAction: bindActionCreators(authActionCreator, dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps)(ShipToListContainer);
