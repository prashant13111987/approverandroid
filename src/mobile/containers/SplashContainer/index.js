/**
 * Created by Prashant Pawar on 19/10/18
 */


import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { NavigationActions } from 'react-navigation';
import SplashComponent from '../../components/SplashComponent';
import { getUserToken } from '../../../common/db';


class SplashContainer extends Component {
    static navigationOptions = {
        header: null,
    }

    componentDidMount() {
        console.log(" getLoginStatus called ")
        this.getLoginStatus();
    }

   // // Redirect to login or dashboard on checking login status.
   //  getLoginStatus() {
   //      let navActions;
   //      console.log(" getLoginStatus called ")
   //      navActions = NavigationActions.reset({
   //          index: 0,
   //          actions: [
   //              NavigationActions.navigate({ routeName: 'Login' }),
   //          ],
   //
   //      });
   //      this.props.navigation.dispatch(navActions);
   //  }


    getLoginStatus() {
        getUserToken().then((tokenObject) => {
            console.log("Anuth Otokrm :::::::::::::::::",tokenObject);
            let navActions;
            if (tokenObject == null) {
                navActions = NavigationActions.reset({
                    index: 0,
                    actions: [
                        NavigationActions.navigate({ routeName: 'Login' }),
                    ],
                });
            } else {
                navActions = NavigationActions.reset({
                    index: 0,
                    actions: [
                        NavigationActions.navigate({ routeName: 'Drawer' }),
                    ],
                });
            }
            this.props.navigation.dispatch(navActions);
        });
    }

    // Redirect to login or dashboard on checking login status.
    // getLoginStatus() {
    //     getUserToken().then((tokenObject) => {
    //         let navActions;
    //         console.log(" getLoginStatus called tokenObject ",tokenObject)
    //         if (tokenObject == null) {
    //             navActions = NavigationActions.reset({
    //                 index: 0,
    //                 actions: [
    //                     NavigationActions.navigate({ routeName: 'Login' }),
    //                 ],
    //
    //             });
    //         } else {
    //             navActions = NavigationActions.reset({
    //                 index: 0,
    //                 actions: [
    //                     NavigationActions.navigate({ routeName: 'DashboardContainer' }),
    //                 ],
    //             });
    //         }
    //         this.props.navigation.dispatch(navActions);
    //     });
    // }

    render() {
        return (
            <SplashComponent onMenuPress={this.onMenuPress} />
        );
    }
}

SplashContainer.propTypes = {
    navigation: PropTypes.object,
};


const mapStateToProps = state => ({
   // isNetworkStateDetected: state.deviceInfo.isNetworkStateDetected,
});

const mapDispatchToProps = dispatch => ({
});

export default connect(mapStateToProps, mapDispatchToProps)(SplashContainer);
