
import React, { Component } from 'react';
import { Alert, Keyboard, Image, Dimensions, Platform, Linking, View ,Text,TouchableOpacity} from 'react-native';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import SuccessScreenComponent from '../../components/SuccessScreenComponent';
import {getBillDetails} from "../../../common/selector/InVoiceList";
import * as dashboardActionCreator from "../../../common/actions/dashboard";
import { bindActionCreators } from 'redux';



const { width } = Dimensions.get('window');
class SuccessScreenContainer extends Component {

    static navigationOptions = ({ navigation }) => ({

        headerTitle: (
            <View style={{ flex: 1 }}>
                <Text
                    style={{
                        alignSelf: 'center',
                        color:'white',
                        fontSize:16,
                        fontWeight:'700'
                    }}>
                    Success
                </Text>
            </View>
        ),
        headerLeft: null,
        headerRight: null,
        headerStyle: {
            backgroundColor: '#629ee4',
            borderBottomWidth: 0,
            shadowOpacity: 0,
            shadowOffset: {
                height: 0,
            },
            shadowRadius: 0,
            elevation: 0,
        },
        headerTitleStyle: { alignSelf: 'center' },
    });

    constructor(props) {
        super(props);
        this.state = {};

        this.onGoToHome = this.onGoToHome.bind(this);
        this.onCreateNew = this.onCreateNew.bind(this);
        }

    onGoToHome (){
           this.props.dashboardAction.navigateToDashboard();
    }

    onCreateNew (){

    }

    render() {
        return (
            <SuccessScreenComponent
                onCreateNew={this.onCreateNew}
                onGoToHome={this.onGoToHome}
            />
        );
    }
}

SuccessScreenContainer.propTypes = {
   // navigation: PropTypes.object,
    dashboardAction: PropTypes.shape({
        navigateToDashboard:PropTypes.func,
    }),
};

const mapStateToProps = state => ({
  });

const mapDispatchToProps = dispatch => ({
    dashboardAction: bindActionCreators(dashboardActionCreator, dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps)(SuccessScreenContainer);