
import React, { Component } from 'react';
import { Alert, Keyboard, Image, Dimensions, Platform, Linking, View, Text, TouchableOpacity } from 'react-native';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import SummeryInvoiceComponent from '../../components/SummeryInvoiceComponent';
import { getBillDetails } from "../../../common/selector/InVoiceList";
import { bindActionCreators } from "redux";
import * as dashboardActionCreator from "../../../common/actions/dashboard";
import _ from "lodash";

//invoice2go-icon.png

const { width } = Dimensions.get('window');
class SummeryInvoiceContainer extends Component {

    constructor(props) {
        super(props);
        this.state = {
            selectedBillDate: this.props.billDetails.billDate,
            selectedInvoiceDate: this.props.billDetails.invoiceDate,
            // invoiceNumber:this.props.billDetails.invoiceNumber?this.props.billDetails.invoiceNumber:"No Invoice Number",
            // billNumber:this.props.billDetails.bill_no?this.props.billDetails.bill_no:"No Bill Number",
            // poNumber:this.props.billDetails.po_no?this.props.billDetails.po_no:"No receipt Number",
            // shipInfo:this.props.billDetails.shipInfo?this.props.billDetails.shipInfo:"No Shipping Info"
            invoiceNumber: this.props.billDetails.invoiceNumber,
            billNumber: this.props.billDetails.bill_no,
            poNumber: this.props.billDetails.po_no,
            shipInfo: this.props.billDetails.shipInfo
        };
        this.inputs = {};
        this.onBillDateChange = this.onBillDateChange.bind(this)
        this.onInvoiceDateChange = this.onInvoiceDateChange.bind(this)
        this.handleShipFromChange = this.handleShipFromChange.bind(this)
        this.handleBillNumberChange = this.handleBillNumberChange.bind(this)
        this.handleInvoiceNumberChange = this.handleInvoiceNumberChange.bind(this)
        this.handlePoNumberChange = this.handlePoNumberChange.bind(this)
        this.focusNextField = this.focusNextField.bind(this);
        this.onSubmitEditing = this.onSubmitEditing.bind(this);
        this.setInputReference = this.setInputReference.bind(this);

    }
    // bill_no:billDetail.bill_no,
    // po_no:billDetail.po_no,
    // due_date:billDetail.due_date,
    // invoiceNumber:billDetail.ref_invoice_no,
    // vendor_name:billDetail.vendor_name,
    // shipInfo:billDetail.transp_ref_no,
    // invoiceDate:billDetail.invoice_date,
    // billDate:billDetail.created_at

    componentDidMount() {
    }


    onBillDateChange(date) {
        this.setState({
            selectedBillDate: date
        })
        let tempObj = _.cloneDeep(this.props.billDetailsOrignal)
        tempObj.created_at = date
        this.props.dashboardAction.updateBillDetail(tempObj)
    }

    onInvoiceDateChange(date) {
        this.setState({
            selectedInvoiceDate: date
        })
        let tempObj = _.cloneDeep(this.props.billDetailsOrignal)
        tempObj.invoice_date = date
        this.props.dashboardAction.updateBillDetail(tempObj)

    }


    handleShipFromChange(shipFromValue) {
        this.setState({
            shipInfo: shipFromValue
        })
        let tempObj = _.cloneDeep(this.props.billDetailsOrignal)
        tempObj.transp_ref_no = shipFromValue
        this.props.dashboardAction.updateBillDetail(tempObj)
    }

    handleBillNumberChange(billNumberValue) {
        this.setState({
            billNumber: billNumberValue
        })
        let tempObj = _.cloneDeep(this.props.billDetailsOrignal)
        tempObj.bill_no = billNumberValue
        this.props.dashboardAction.updateBillDetail(tempObj)

    }


    handleInvoiceNumberChange(invoiceNumberValue) {
        this.setState({
            invoiceNumber: invoiceNumberValue
        })
        let tempObj = _.cloneDeep(this.props.billDetailsOrignal)
        tempObj.ref_invoice_no = invoiceNumberValue
        this.props.dashboardAction.updateBillDetail(tempObj)
    }

    handlePoNumberChange(poNumberValue) {
        this.setState({
            poNumber: poNumberValue
        })
        let tempObj = _.cloneDeep(this.props.billDetailsOrignal)
        tempObj.po_no = poNumberValue
        this.props.dashboardAction.updateBillDetail(tempObj)
    }

    onSubmitEditing(nextFiled) {
        this.focusNextField(nextFiled);
    }
    setInputReference(name, refs) {
        this.inputs[name] = refs;
    }
    focusNextField(id) {
        this.inputs[id].focus();
    }

    render() {
        return (
            <SummeryInvoiceComponent
                onValidateInvoiceOptionSelect={this.props.onValidateInvoiceOptionSelect}
                selectedBillDate={this.state.selectedBillDate ? this.state.selectedBillDate : this.props.billDetails.billDate}
                selectedInvoiceDate={this.state.selectedInvoiceDate ? this.state.selectedInvoiceDate : this.props.billDetails.invoiceDate}
                onBillDateChange={this.onBillDateChange}
                onInvoiceDateChange={this.onInvoiceDateChange}
                handlePoNumberChange={this.handlePoNumberChange}
                handleShipFromChange={this.handleShipFromChange}
                handleBillNumberChange={this.handleBillNumberChange}
                handleInvoiceNumberChange={this.handleInvoiceNumberChange}
                onSubmitEditing={this.onSubmitEditing}
                setInputReference={this.setInputReference}
                focusNextField={this.focusNextField}
                invoiceNumber={this.state.invoiceNumber ? this.state.invoiceNumber : this.props.billDetails.invoiceNumber}
                billNumber={this.state.billNumber ? this.state.billNumber : this.props.billDetails.bill_no}
                shipInfo={this.state.shipInfo ? this.state.shipInfo : this.props.billDetails.shipInfo}
                poNumber={this.state.poNumber ? this.state.poNumber : this.props.billDetails.po_no}
                isEditable={this.props.isEditable}
                invoiceTitle={this.props.selectedInvoice ? this.props.selectedInvoice.customer_name : ''}
                total={this.props.selectedInvoice ? this.props.selectedInvoice.total : ''}
                due_date={this.props.selectedInvoice ? this.props.selectedInvoice.due_date : ''}
                department_name={this.props.selectedInvoice ? this.props.selectedInvoice.department_name : ''}
                notes={this.props.selectedInvoice ? this.props.selectedInvoice.notes : ''}
                credit_memo={this.props.selectedInvoice ? this.props.selectedInvoice.credit_memo : ''}
            />
        );
    }
}

SummeryInvoiceContainer.propTypes = {
    navigation: PropTypes.object,
    dashboardAction: PropTypes.shape({
        goBack: PropTypes.func,
        updateBillDetail: PropTypes.func
    }),

};

const mapStateToProps = state => ({
    billDetails: getBillDetails(state),
    isEditable: state.approverDashboard.isEditable,
    billDetailsOrignal: state.dashboard.billDetails,
    selectedInvoice: state.approverDashboard.selectedInvoice,
});

const mapDispatchToProps = dispatch => ({
    dashboardAction: bindActionCreators(dashboardActionCreator, dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps)(SummeryInvoiceContainer);