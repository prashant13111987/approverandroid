import React, { Component } from 'react';
import {
    Alert,
    Keyboard,
    Image,
    Dimensions,
    Platform,
    Linking,
    View,
    Text,
    TouchableOpacity,
    StyleSheet
} from 'react-native';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as dashboardActionCreator from '../../../common/actions/dashboard/approverAction';
import * as authActionCreator from '../../../common/actions/login';
import ValidateApproverInvoiceComponent from '../../components/ValidateApproverInvoiceComponent';
import backButton from '../../../common/images/back.png';
import gallery from '../../../common/images/ic_image.png';
import { getBillDetails } from '../../../common/selector/InVoiceList';
import Cancel from '../../../common/images/icons-cancel.png';
import Hand from '../../../common/images/icons-hand.png';
import check from '../../../common/images/ic_success_filled.png';
import WorkFlowComponent from '../../components/InVoiceListComponent/WorkFlow';
import Modal from "react-native-modal";
import ModalExample from '../../components/ImageViewer';

//invoice2go-icon.png

const { width } = Dimensions.get('window');
class ValidateApproverInvoiceContainer extends Component {
    static navigationOptions = ({ navigation }) => ({
        headerTitle: (
            <View style={{ alignItems: 'flex-start', flex: 1, justifyContent: 'space-between' }}>
                <Text
                    style={{
                        alignSelf: 'center',
                        color: 'white',
                        fontSize: 16,
                        fontWeight: '700'
                    }}
                >
                    Validate
				</Text>
            </View>
        ),
        headerStyle: {
            backgroundColor: '#629ee4',
            borderBottomWidth: 0,
            shadowOpacity: 0,
            shadowOffset: {
                height: 0
            },
            shadowRadius: 0,
            elevation: 0
        },
        headerLeft: (
            <TouchableOpacity onPress={() => navigation.goBack()}>
                <Image source={backButton} style={{ height: 24, width: 24, marginLeft: 12 }} />
            </TouchableOpacity>
        ),
        headerRight: (
            <TouchableOpacity onPress={() => navigation.state.params.onImageClick()}>
                {navigation.state.params && navigation.state.params.isBill ? <Image source={gallery} style={{ height: 24, width: 24, marginRight: 12 }} /> : null}
            </TouchableOpacity>
        )
    });

    componentDidMount() {
        this.props.navigation.setParams({
            handleBack: this.onBackPress,
            onImageClick: this.onImageClick,
            isBill: this.props.selectedInvoice.data === 'Bills'
        });
        if (this.props.selectedBillObject.operationname === 'EDIT') {
            this.props.dashboardAction.invoiceDetailEditRequest(this.props.selectedBillObject);
        } else if (this.props.selectedBillObject.operationname === 'VIEW') {
            this.props.dashboardAction.invoiceDetailRequest(this.props.selectedBillObject);
        }
    }

    onBackPress() {
        this.props.dashboardAction.goBack();
        this.props.dashboardAction.resetBillDetails();
    }

    constructor(props) {
        super(props);
        this.state = {
            selectedValidateOption: 'Summery',
            isErrorTrue: 'inVoiceTitle',
            isModalVisible: false,
            activeIndex: null,
            modalType: 'approve',
            note: '',
            isImageCliked: false
        };
        this.inputs = {};
        this.focusNextField = this.focusNextField.bind(this);
        this.onSubmitEditing = this.onSubmitEditing.bind(this);
        this.setInputReference = this.setInputReference.bind(this);
        this.onValidateInvoiceOptionSelect = this.onValidateInvoiceOptionSelect.bind(this);
        this.onBackPress = this.onBackPress.bind(this);
    }
    onSubmitEditing(nextFiled) {
        this.focusNextField(nextFiled);
    }
    setInputReference(name, refs) {
        this.inputs[name] = refs;
    }
    focusNextField(id) {
        this.inputs[id].focus();
    }

    onValidateInvoiceOptionSelect(value) {
        this.setState({
            selectedValidateOption: value
        });
    }

    onRowPress = (item) => {
        // this.props.dashboardAction.setSelectedBillObject(item)
        this.props.dashboardAction.billDetailRequest(item);
        this.props.dashboardAction.selectedInvoiceStatus('Bills');
        this.setState({
            selectedValidateOption: 'Summery'
        });
        // this.props.loginAction.navigateToBillPaymentDetailsCompon ent();
    };

    onShowModalPress = type => {
        this.setState({ isModalVisible: true, modalType: type });
    }


    onConfirm = () => {
        const { state: { modalType, note }, props: { dashboardAction, selectedInvoice, selectedInvoiceDetails } } = this;

        let data = selectedInvoiceDetails
        console.log('selectedInvoiceDetails', selectedInvoiceDetails)
        if (selectedInvoice.data === 'Payments') {
            data = selectedInvoiceDetails.bank_details && selectedInvoiceDetails.bank_details.length ? { id: selectedInvoiceDetails.bank_details[0].bill_payment_report_id } : { id: null }
        } else if (selectedInvoice.data === 'Master Data') {
            data = selectedInvoiceDetails && selectedInvoiceDetails.length ? selectedInvoiceDetails[0] : selectedInvoiceDetails.id ? selectedInvoiceDetails : { id: null }
        }
        dashboardAction.workFlow(selectedInvoice.data, data, note, modalType)
        this.setState({
            isModalVisible: false,
            modalType: null,
            note: '',
        })
    }

    hideModal = () => {
        this.setState({
            activeIndex: null,
            modalType: null,
            note: '',
            isModalVisible: false
        })
    }
    onNoteChange = note => {
        this.setState({
            note
        })
    }

    onImageClick = () => {
        this.setState({
            isImageCliked: true
        })
    }
    onImageClikedClosed = () => {
        this.setState({
            isImageCliked: false
        })
    }

    render() {
        const { onConfirm, hideModal, onNoteChange, state: { isModalVisible, modalType, note }, onShowModalPress } = this
        let modalHeaderText = 'Approve'
        let showNote = false;
        if (modalType === 'hold') {
            showNote = true
            modalHeaderText = 'On Hold'
        } else if (modalType === 'reject') {
            showNote = true
            modalHeaderText = 'Reject'
        }
        const currentElement = this.props.selectedInvoiceDetails
        const name = currentElement.bank_details && currentElement.bank_details.length ? currentElement.bank_details[0].bank_name : currentElement.party_name || currentElement.bank_name || currentElement.name || 'NA'
        const start = currentElement.created_at
        const end = currentElement.created_at
        const total = currentElement.bank_details && currentElement.bank_details.length ? currentElement.bank_details[0].gross : currentElement.total
        return (
            <View style={{ flex: 1 }}>
                <Modal isVisible={isModalVisible}>
                    <View style={{ flex: 1, justifyContent: 'center' }}>
                        <WorkFlowComponent
                            modalHeaderText={modalHeaderText}
                            name={name}
                            start={start}
                            end={end}
                            total={total}
                            confirm={onConfirm}
                            cancel={hideModal}
                            showNote={showNote}
                            note={note}
                            onNoteChange={onNoteChange}
                        />

                    </View>
                </Modal>
                <ValidateApproverInvoiceComponent
                    focusNextField={this.focusNextField}
                    onSubmitEditing={this.onSubmitEditing}
                    setInputReference={this.setInputReference}
                    // billDetails={this.props.billDetails}
                    selectedInvoice={this.props.selectedInvoice}
                    selectedInvoiceDetails={this.props.selectedInvoiceDetails}
                    imageUrl={this.props.billDetails.imageUrl}
                    selectedValidateOption={this.state.selectedValidateOption}
                    selectedType={this.props.selectedType}
                    isEditable={this.props.isEditable}
                    onValidateInvoiceOptionSelect={this.onValidateInvoiceOptionSelect}
                    onRowPress={this.onRowPress}
                    selectedItemObject={this.props.selectedItemObject}
                />
                <View
                    style={[styles.bottomButtons, {
                        flexDirection: 'row', justifyContent: 'space-evenly', borderColor: '#4785bd',
                        borderWidth: 1
                    }]}
                    onPress={this.props.onAllPress}
                >
                    <TouchableOpacity onPress={() => onShowModalPress('reject')}>
                        <Image
                            source={Cancel}
                            style={{
                                width: 30,
                                height: 30
                            }}
                        />
                        <Text style={styles.viewText}>Reject</Text>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => onShowModalPress('hold')}>
                        <Image
                            source={Hand}
                            style={{
                                width: 30,
                                height: 30
                            }}
                        />
                        <Text style={styles.viewText}>On Hold</Text>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => onShowModalPress('approve')}>
                        <Image
                            source={check}
                            style={{
                                width: 30,
                                height: 30
                            }}
                        />
                        <Text style={styles.viewText}>Approve</Text>
                    </TouchableOpacity>
                </View>
                <ModalExample onImageClikedClosed={this.onImageClikedClosed} isImageCliked={this.state.isImageCliked} imageUrl={this.props.selectedInvoiceDetails && this.props.selectedInvoiceDetails.image ? this.props.selectedInvoiceDetails.image : ''} />
            </View>
        );
    }
}

ValidateApproverInvoiceContainer.propTypes = {
    navigation: PropTypes.object,
    dashboardAction: PropTypes.shape({
        goBack: PropTypes.func,
        billDetailEditRequest: PropTypes.func,
        billDetailRequest: PropTypes.func,
        resetBillDetails: PropTypes.func
    })
};
const styles = StyleSheet.create({
    chartFooterButtonsGroupRight: {
        flex: 1,
        // height: '15%',
        flexDirection: 'row',
        justifyContent: 'center',
        marginBottom: 50
    },
    viewButton: {
        // width: '40%',
        height: '70%',
        borderRadius: 35,
        borderColor: '#4785bd',
        borderWidth: 1,
        backgroundColor: 'white'
        // alignItems: 'center',
        // justifyContent: 'center',
        // marginTop: 5,
        // padding: 20
    },
    viewText: {
        fontSize: 10,
        fontWeight: 'bold',
        color: '#4785bd'
    }
});

const mapStateToProps = (state) => ({
    selectedInvoice: state.approverDashboard.selectedInvoice,
    selectedBillObject: state.approverDashboard.selectedBillObject,
    selectedInvoice: state.approverDashboard.selectedInvoice,
    selectedType: state.approverDashboard.selectedType,
    selectedInvoiceDetails: state.approverDashboard.selectedInvoiceDetails,
    isEditable: state.approverDashboard.isEditable,
    selectedItemObject: state.approverDashboard.selectedItemObject,
    billDetails: getBillDetails(state)
});

const mapDispatchToProps = (dispatch) => ({
    loginAction: bindActionCreators(authActionCreator, dispatch),
    dashboardAction: bindActionCreators(dashboardActionCreator, dispatch)
});

export default connect(mapStateToProps, mapDispatchToProps)(ValidateApproverInvoiceContainer);
