
import React, { Component } from 'react';
import { Alert, Keyboard, Image, Dimensions, Platform, Linking, View ,Text,TouchableOpacity} from 'react-native';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as dashboardActionCreator from '../../../common/actions/dashboard';
import ValidateInvoiceComponent from '../../components/ValidateInvoiceComponent';
import backButton from '../../../common/images/back.png'
import {getBillDetails} from "../../../common/selector/InVoiceList";

//invoice2go-icon.png

const { width } = Dimensions.get('window');
class ValidateInvoiceContainer extends Component {
    static navigationOptions = ({ navigation }) => ({
        headerTitle: (
            <View style={{ alignItems:'flex-start',flex:1}}>
                <Text
                    style={{
                        alignSelf: 'center',
                        color:'white',
                        fontSize:16,
                        fontWeight:'700'
                    }}>
                    Validate
                </Text>
            </View>
        ),
        headerStyle: {
            backgroundColor: '#629ee4',
            borderBottomWidth: 0,
            shadowOpacity: 0,
            shadowOffset: {
                height: 0,
            },
            shadowRadius: 0,
            elevation: 0,
        },
        headerLeft: (
            <TouchableOpacity onPress={() => navigation.state.params.handleBack()}>
                <Image source={backButton} style={{ height: 24, width: 24, marginLeft: 12 }} />
            </TouchableOpacity>
        ),
    });


    componentDidMount() {
        this.props.navigation.setParams({
            handleBack: this.onBackPress,
        });
        if(this.props.selectedBillObject.operationname === "EDIT")
        {
            this.props.dashboardAction.billDetailEditRequest(this.props.selectedBillObject)

        }else  if(this.props.selectedBillObject.operationname === "VIEW")
        {
            this.props.dashboardAction.billDetailRequest(this.props.selectedBillObject)
        }
    }

    onBackPress() {
        this.props.dashboardAction.goBack();
        this.props.dashboardAction.resetBillDetails()
    }

    constructor(props) {
        super(props);
        this.state = {
            selectedValidateOption:'Basic',
            isErrorTrue:'inVoiceTitle'
        };
        this.inputs = {};
        this.focusNextField = this.focusNextField.bind(this);
        this.onSubmitEditing = this.onSubmitEditing.bind(this);
        this.setInputReference = this.setInputReference.bind(this);
        this.onValidateInvoiceOptionSelect = this.onValidateInvoiceOptionSelect.bind(this);
        this.onBackPress = this.onBackPress.bind(this);

    }
    onSubmitEditing(nextFiled) {
        this.focusNextField(nextFiled);
    }
    setInputReference(name, refs) {
        this.inputs[name] = refs;
    }
    focusNextField(id) {
        this.inputs[id].focus();
    }

    onValidateInvoiceOptionSelect(value){
        this.setState({
            selectedValidateOption:value
        })
    }

    render() {
        return (
            <ValidateInvoiceComponent
                focusNextField={this.focusNextField}
                onSubmitEditing={this.onSubmitEditing}
                setInputReference={this.setInputReference}
                // billDetails={this.props.billDetails}
                imageUrl={this.props.billDetails.imageUrl}
                selectedValidateOption={this.state.selectedValidateOption}
                onValidateInvoiceOptionSelect={this.onValidateInvoiceOptionSelect}/>
        );
    }
}

ValidateInvoiceContainer.propTypes = {
    navigation: PropTypes.object,
    dashboardAction: PropTypes.shape({
        goBack: PropTypes.func,
        billDetailEditRequest:PropTypes.func,
        billDetailRequest:PropTypes.func,
        resetBillDetails:PropTypes.func
    }),
};

const mapStateToProps = state => ({
    selectedBillObject:state.dashboard.selectedBillObject,
    billDetails:getBillDetails(state),
});

const mapDispatchToProps = dispatch => ({
    dashboardAction: bindActionCreators(dashboardActionCreator, dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps)(ValidateInvoiceContainer);