
import React, { Component } from 'react';
import {Alert, Keyboard, Image, Dimensions, Platform, Linking, View, Text, StyleSheet} from 'react-native';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import logo from '../../../common/images/logo.png'
import Modal from "react-native-modal";
import {normalizeFont} from "../../../common/util";
import CommanListComponent from '../../components/CommanListComponent'
import {getlegendObject, getStatusForLagent} from "../../../common/selector/InVoiceList";
import {getReceiveObject} from "../../../common/selector/dashboard";
import * as authActionCreator from "../../../common/actions/login";
import * as dashboardActionCreator from "../../../common/actions/dashboard";

const { width } = Dimensions.get('window');

class VenderListContainer extends Component {

    constructor(props) {
        super(props);

        this.state = {
            searchText: '',
            selectedText:'',
        };
        this.searchFilterFunction = this.searchFilterFunction.bind(this);
        this.onRowPress = this.onRowPress.bind(this);
    }

    componentDidMount() {
    }

    // On search filter for state.
    searchFilterFunction(text) {
        this.setState({
            searchText: text,
        });
    }

    onRowPress(item) {
        this.setState({
            selectedText: item.name,
        });
        this.props.dashboardAction.setVenderObject(item)
        //
        // if(item.source_name == "s_invoice"){
        //     if(item.vendor_id == "")
        //     {
        //         this.props.dashboardAction.navigateToVender(item)
        //     }else
        //     {
        //         this.props.dashboardAction.S_Invoice_APIRequest(item)
        //     }
        //
        // } else {
        //     console.log(" Create OCR API",item)
        //     if(item.vendor_id == "")
        //     {
        //         this.props.dashboardAction.navigateToVender(item)
        //     }
        //     else {
        //         const ocrImagesObject = {
        //             source_name:item.source_name,
        //             source_id:item.id,
        //             vendor_id:item.vendor_id,
        //         }
        //         const ocr = {
        //             ocr_image :ocrImagesObject
        //         }
        //         this.props.dashboardAction.email_Uploaded_Invoice_APIRequest(ocr)
        //     }
        // }
        console.log("Item Value form List Container :::::::::::",this.props.navigation.routes[1].params.value)
        var inVoiceItem = this.props.navigation.routes[1].params.value
        if(inVoiceItem.source_name != "s_invoice"){
            // TODO Need to Remvoe Hardcoded Vender ID
            //inVoiceItem.vendor_id = 1
             inVoiceItem.vendor_id = item.id
            this.props.dashboardAction.email_Uploaded_Invoice_APIRequest(inVoiceItem)

        }else
        {
            inVoiceItem.vendor_id = item.id
            this.props.dashboardAction.S_Invoice_APIRequest(inVoiceItem)
        }
       // const finalRegisterObject = Object.assign(districtObject, districtCustomObje);
    }

    render() {

        return (
            <View style={{width:'100%',height:'100%',borderBottomLeftRadius:10,borderBottomRightRadius:10}} >
                <CommanListComponent
                    data={this.props.venderList}
                    onRowPress={this.onRowPress}
                    keyName="state_list"
                    searchFilterFunction={this.searchFilterFunction}
                    searchText={this.state.searchText}
                    selectedText={this.state.selectedText}
                />
            </View>
        );
    }
}


const styles = StyleSheet.create({
    modalContent: {
        padding: 22,
        justifyContent: "center",
        alignItems: "center",
        borderRadius: 4,
        borderColor: "rgba(0, 0, 0, 0.1)",
        height:'40%',
        width:'60%',
    },
});

VenderListContainer.propTypes = {
    navigation: PropTypes.object,
    dashboardAction: PropTypes.shape({
        venderListRequest:PropTypes.func,
        goBack: PropTypes.func,
        email_Uploaded_Invoice_APIRequest:PropTypes.func,
        S_Invoice_APIRequest:PropTypes.func
    }),
};


const mapStateToProps = state => ({
    navigation: state.nav,
    loading: state.app.loading,
    selectedBillObject:state.dashboard.selectedBillObject,
    billDetailsOrignal:state.dashboard.billDetails,
    venderList: state.dashboard.venderList,
});

const mapDispatchToProps = dispatch => ({
    dashboardAction: bindActionCreators(dashboardActionCreator, dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps)(VenderListContainer);
