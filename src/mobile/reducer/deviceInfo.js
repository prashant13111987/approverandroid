import { createReducer } from '../../common/util';
import Constant from '../../common/constants';

const initialState = {
    netState: false,
    isNetworkStateDetected: false,
};

export default createReducer(initialState, {
    [Constant.NET_STATE_CHANGED]: (state, payload) => Object.assign({}, state, {
        netState: payload,
        isNetworkStateDetected: true,
    }),
});
