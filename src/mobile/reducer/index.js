import { combineReducers } from 'redux';
import { NavigationActions } from 'react-navigation';
import login from '../../common/reducers/login';
import deviceInfo from './deviceInfo';
import app from '../../common/reducers/app';
import dashboard from '../../common/reducers/dashboard';
import approverDashboard from '../../common/reducers/approverDashboard';
import RootNavigation from '../containers/RootNavigation';

const INITIAL_STATE = RootNavigation.router.getStateForAction(NavigationActions.init());
const navigation = (state = INITIAL_STATE, action) => {
    const newState = RootNavigation.router.getStateForAction(action, state);
    return newState || state;
};

export default combineReducers({
    nav: navigation,
    login,
    deviceInfo,
    app,
    dashboard,
    approverDashboard
});
