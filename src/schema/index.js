
export const Token = {
    name: 'Token',
    primaryKey: 'email',
    properties: {
        auth_token:'string',
        email:'string',
        userName:'string',
        loginId:'int'
    },
};

export const AppInfo = {
    name: 'AppInfo',
    primaryKey: 'id',
    properties: {
        app:'string',
        id:'int',
    },
};